<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:pleiades="https://pleiades.stoa.org/places/vocab#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:lawd="http://lawd.info/ontology/" xmlns:apc="http://patrimonium.huma-num.fr/onto#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:snap="http://onto.snapdrgn.net/snap#" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:preserve-space elements=""/>
  <xsl:strip-space elements="*"/>

<xsl:output indent="yes" method="xml" encoding="utf-8" omit-xml-declaration="yes"/>
<!-- <xsl:template match="*[@type='privateCommentary']"/> -->
 
  <xsl:template match="node()|@*">
      <xsl:copy>
         <xsl:apply-templates select="node()|@*"/>
      </xsl:copy>
    </xsl:template>
<xsl:template match="@*"><!-- removing empty attributes -->
        <xsl:if test="string-length(.)&gt;0">
        <xsl:copy/>
        </xsl:if>
    </xsl:template>
<xsl:template match="skos:note[@type='private']"/>

</xsl:stylesheet>