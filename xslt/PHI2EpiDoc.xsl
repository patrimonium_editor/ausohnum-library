<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:tei="http://www.tei-c.org/ns/1.0" 
xmlns:local="local" version="2.0" exclude-result-prefixes="">
    

<!-- <xsl:function name="local:processUnclear" as="node()">
    <xsl:param name="text" as="xs:string"/>
    <xsl:variable name="regexUnclear" select="([^\\x00-\\x7F]|[aA-zZ])̣"/>
    <xsl:variable name="substUnclear" select="$1"/>
    <unclear><xsl:value-of select="replace($text, $regexUnclear, $substUnclear, 'i')"/></unclear>
  </xsl:function> -->

    <xsl:template match="/">
        <xsl:element name="div">
            <xsl:attribute name="type">textpart</xsl:attribute>
            <xsl:attribute name="xml:lang"><xsl:value-of select="./@class/string()"></xsl:value-of></xsl:attribute>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:element>
        
    </xsl:template>
    
   
    
    <xsl:template match="*[local-name() = 'span'][@class='r']">
        <xsl:choose>
            <xsl:when test="matches(./text(), '(․)+')">
                <xsl:element name="gap">
                    <xsl:attribute name="reason">lost</xsl:attribute>
                    <xsl:attribute name="quantity"><xsl:value-of select="string-length(translate(./text(), '\[\]', ''))"/></xsl:attribute>
                    <xsl:attribute name="unit">character</xsl:attribute>
                </xsl:element>
            </xsl:when>
            
            <xsl:when test="matches(./text(), '(—)+')">
                <xsl:element name="gap">
                    <xsl:attribute name="reason">lost</xsl:attribute>
                    <xsl:attribute name="quantity">1</xsl:attribute>
                    <xsl:attribute name="unit">line</xsl:attribute>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="supplied">
                    <xsl:attribute name="reason">lost</xsl:attribute>
                    <xsl:value-of select="translate(./text(), '\[\]', '')"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>   
    
    <xsl:template match="*[local-name() = 'td']">
        <xsl:choose>
            <xsl:when test=".[@class='id']">
                <xsl:element name="lb">
                    <xsl:attribute name="n">
                        <xsl:choose>
                            <xsl:when test="normalize-space(./text()) != ''">
                                <xsl:value-of select="normalize-space(./text())"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="count(./parent::*[local-name() = 'tr']//preceding-sibling::*[local-name() = 'tr'])+ 1"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="node()"/>

        </xsl:otherwise>
    </xsl:choose>
        
    </xsl:template>
    <xsl:template match="*[local-name() = 'unclear']">
        <xsl:copy-of select="."/>
    </xsl:template>



<!-- 
<xsl:template match="@* | node()">
    <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
</xsl:template> -->
<!-- Delete useless nodes -->
<!-- <xsl:template match="*[local-name() = 'td'][not()]">
    <xsl:copy-of select="node()"/>

</xsl:template> -->
<!-- <xsl:template match="*[local-name() = 'tr']"/> -->
    


<xsl:template match="@* | node()">
    <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
</xsl:template>

</xsl:stylesheet>
