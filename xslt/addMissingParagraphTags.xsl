<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:t="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="t" version="2.0">
    <xsl:output method="html"/> 
    <xsl:template match="*">
        <xsl:copy>
            <p>
                <xsl:apply-templates/>
            </p>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>