<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.tei-c.org/ns/1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" version="2.0" exclude-result-prefixes="">
    <!--    Attribute removed:xpath-default-namespace="http://www.tei-c.org/ns/1.0" -->
    
    <xsl:output indent="no" method="xml" encoding="utf-8" omit-xml-declaration="no"/>
    <!-- <xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes" omit-xml-declaration="no" indent="no"/> -->
    <!-- <xsl:strip-space elements="*"/> -->
   
    <xsl:template match="/">
        <!-- <xsl:processing-instruction name="xml-model">href="http://www.stoa.org/epidoc/schema/latest/tei-epidoc.rng" type="xml" schematypens="http://relaxng.org/ns/structure/1.0"</xsl:processing-instruction>
        <xsl:text>&#xa;</xsl:text> -->
        <xsl:apply-templates select="node() | @*"/>
    </xsl:template>
    <!-- <xsl:preserve-space elements=""/> -->
    <!--<xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
    </xsl:template>-->
    <!--    <xsl:template match="*" priority="-0.4">
        <xsl:element name="{local-name()}">
            <xsl:apply-templates select="@*|node()"/>
        </xsl:element>
    </xsl:template>-->
    <!-- <xsl:template match="div[@type = 'textpart']/ab">
        <xsl:copy copy-namespaces="no">
            <xsl:value-of select="replace(., '&lt;', 'dd')"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="/*[local-name()='data']">
        <xsl:element name="data" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="@*, node()"/>
        </xsl:element>
    </xsl:template> -->
    <xsl:template match="*">
        <xsl:element name="{name()}" namespace="{namespace-uri()}">
            <xsl:apply-templates select="node()|@*"/>
        </xsl:element>
    </xsl:template>
    <!-- <xsl:template match="node() | @*">
        <xsl:copy copy-namespaces="no">
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
    </xsl:template> -->
    <xsl:template match="comment()"/>
    <!-- <xsl:template match=
    "*[not(@*|*|processing-instruction()) 
     and normalize-space(.)=''
      ]"/> -->
    <xsl:template match="@*"><!-- removing empty attributes -->
        <xsl:if test="string-length(.)&gt;0">
        <xsl:copy/>
        </xsl:if>
    </xsl:template>

    <!-- removing element not in EpiDoc -->
    <xsl:template match="tei:div[@type='relatedMonuments']"/>

    <!-- Correct wrong use of TEI elements -->
    <xsl:template match="tei:catRef">
        <xsl:element name="catRef">
            <xsl:apply-templates select="@*"/>
        </xsl:element>
    </xsl:template>    


    <!-- Remove empty elements that requires children in TEI -->

    <!-- <xsl:template match="tei:particDesc[not(node())]"/> -->
    
    <xsl:template match="tei:list[normalize-space(.)='']"/>
    <xsl:template match="tei:listBibl[normalize-space(.)='']"/>
    <xsl:template match="tei:msContents[normalize-space(.)='']"/>
    <xsl:template match="tei:listPlace[normalize-space(.)='']"/>
    
    
    <xsl:template name="tei:listPerson">
        <xsl:if test="tei:person">
            <xsl:copy/>
            </xsl:if>
    </xsl:template>
    <xsl:template match="tei:particDesc">
        <xsl:if test="tei:listPerson">
            <xsl:copy>
                <xsl:text>
</xsl:text>
                <xsl:apply-templates select="tei:listPerson"/>
            </xsl:copy>
        </xsl:if>    
       
    </xsl:template>

    <xsl:template match="tei:altIdentifier[not(node()) and normalize-space(.)='']"/>

<!-- Replace i elements for italics by hi/@rend=rend="italic" -->
<xsl:template match="*[local-name()='i']">
    <xsl:element name="hi">
        <xsl:attribute name="rend">italic</xsl:attribute>
        <xsl:copy/>
    </xsl:element>
</xsl:template>
<!-- Replace a elements for links by ref -->
<xsl:template match="*[local-name()='a']">
    <xsl:element name="ref">
        <xsl:attribute name="target">
                <xsl:value-of select="./@href"/>
            </xsl:attribute>
        <xsl:copy-of select="./text()"/>
    </xsl:element>
</xsl:template>
<!-- Elements to be suppressed for public viewing -->
<xsl:template match="tei:div[@type='privateCommentary']"/>




    <!-- <xsl:template match="*[not(@* | * | comment() | processing-instruction()) and normalize-space(.) = '']"/> -->
   
    <!-- <xsl:template match="node()[normalize-space(.)='']"/> -->
    <!--
    <xsl:template match="text()[normalize-space() = '']"/>
    <xsl:template match="*[not(@* | * | comment() | processing-instruction()) and normalize-space() = '']"/>
    -->
    <!-- Removes all nodes with any empty attribute -->
    
</xsl:stylesheet>