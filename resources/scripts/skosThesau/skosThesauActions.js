
/*
****************************
*     Dropdown menus       *
****************************
*/
 $(function(){
$( "#content" ).on( "click", ".dropdown-menu li a", function( event ) {
    //console.log("menu: " + $(this).attr('menu'));
                  console.log("v: " + $(this).attr('value'));
        /*        var menu = "#" + $(this).attr('id');*/
                var menu = $(this).attr('menu');
                 $(menu).html($(this).text()  + '<span class="caret"></span>');
                 $(menu).attr('value', $(this).attr('value'));
                 $(menu).attr('conceptHierarchy', $(this).attr('conceptHierarchy'));
/*                 console.log("menu: " + menu);*/
        
});
/*$(".dropdown-menu li a").click(function(){
                console.log("1) menu: " + $(this).attr('menu'));
                console.log("2) value: " + $(this).attr('value'));
        /\*        var menu = "#" + $(this).attr('id');*\/
                var menu = $(this).attr('menu');
                 $(menu).html($(this).text()  + '<span class="caret"></span>');
                 $(menu).attr('value', $(this).attr('value'));
        
          });
*/



/*    $(".dropdown-menu li a").click(function(){
        console.log("selection: " + $(this).attr('href'));
        var menu = $(this).attr('href');
         $(menu).html($(this).text() + '<span class="caret"></span>');
         $(menu).attr('value', $(this).attr('value'));

   });
*/
});


function editValue(elementName, lang){
        var displayDiv = "#" + elementName + "_" + lang + "_display";
        var editDiv = "#" + elementName +  "_" + lang +"_edit" ;
        
      $(displayDiv).toggleClass("elementHidden");
      $(editDiv).toggleClass("elementHidden");
}
function cancelEdit(elementName, lang, originalValue){
        var displayDiv = $("#" + elementName + "_" + lang + "_display");
        var editDiv = $("#" + elementName + "_" + lang + "_edit");
        var elementValue = $("#" + elementName + "_" + lang + "_value");
        var elementInput = $("#" + elementName + "_" + lang + "_input");
        if(originalValue==="") {originalValue = $("#" + elementName + "_" + lang + "_backup").val()}
           
        elementInput.val(originalValue);
        elementInput.html(originalValue);
        displayDiv.toggleClass("elementHidden");
        editDiv.toggleClass("elementHidden");
}


function editNTSortingOrder(){
        $("#ntsorting_nonalpha").toggleClass("disabled");
        $("#ntsorting_alpha").toggleClass("disabled");
        $("#edit_NT_sorting_order").toggleClass("hidden");
        $("#saveNT_sorting_orderButton").toggleClass("hidden");
        $("#editSortingOrderCancelEdit").toggleClass("hidden");
};

function toggleSelectSortingOrder(){
/*        var id = $(e).attr("id");*/
        $("#ntsorting_nonalpha").toggleClass("btn-primary", "btn-secondary");
        $("#ntsorting_alpha").toggleClass("btn-primary", "btn-secondary");

};
function cancelEditSortingOrder(){
      $("#ntsorting_nonalpha").toggleClass("disabled");
        $("#ntsorting_alpha").toggleClass("disabled");
        $("#edit_NT_sorting_order").toggleClass("hidden");
        $("#saveNT_sorting_orderButton").toggleClass("hidden");
        $("#editSortingOrderCancelEdit").toggleClass("hidden");
    
};
function saveNTSortingOrderType(conceptId, lang){
        if($("#ntsorting_nonalpha").hasClass("btn-primary"))
                {var orderingType = 'ordered'}
                else {var orderingType = 'alpha'}
                console.log(orderingType);
                
          var xmlData = "<xml>"
                    + "<orderingType>" + orderingType + "</orderingType>"
                    + "<conceptId>" + conceptId + "</conceptId>"
                    + "<lang>" + lang + "</lang>"
                    +"</xml>";      
         var request = new XMLHttpRequest();
        request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=saveNTSortingOrderType" , true);
        var xmlDoc;
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
             $("#edit_NT_sorting_order").toggleClass("hidden");
            $("#saveNT_sorting_orderButton").toggleClass("hidden");
             $("#editSortingOrderCancelEdit").toggleClass("hidden");
             //refresh tree
             var currentUrl =window.location.href;
                var currentConcept =currentUrl.substring(currentUrl.lastIndexOf("/")+1);
                var sourceFromXql = "/call-concept/" + currentConcept + "/" + lang;
               $("#conceptContent").load(sourceFromXql);
               var newSourceOption = {
              //        url: '/modules/skosThesau/build-tree.xql?lang=de'
                    //  url: '/skosThesau/build-tree/' + lang
                    url: '/skosThesau/getTreeJSon/slow/' + lang
                      };
                var tree = $('#collection-tree').fancytree('getTree');
                tree.reload(newSourceOption);
  
             
            }
            };

         request.setRequestHeader('Content-Type', 'text/xml');

        request.send(xmlData);       
};

function editConceptType(){
        $("#isConceptButton").toggleClass("disabled");
        $("#isCollectionButton").toggleClass("disabled");
        $("#editConceptType").toggleClass("hidden");
        $("#saveConceptType").toggleClass("hidden");
        $("#conceptTypeCancelEdit").toggleClass("hidden");
};
function toggleSelectConceptType(){
/*        var id = $(e).attr("id");*/
        $("#isConceptButton").toggleClass("btn-primary", "btn-secondary");
        $("#isCollectionButton").toggleClass("btn-primary", "btn-secondary");

};

function cancelEditConceptType(){
       $("#isConceptButton").toggleClass("disabled");
        $("#isCollectionButton").toggleClass("disabled");
        $("#editConceptType").toggleClass("hidden");
        $("#saveConceptType").toggleClass("hidden");
        $("#conceptTypeCancelEdit").toggleClass("hidden");
};
function saveConceptType(conceptId, lang, userType){
        if($("#isConceptButton").hasClass("btn-primary"))
                {var conceptType = 'skos:Concept'}
                else {var conceptType = 'skos:Collection'}
        var request = new XMLHttpRequest();    
        if(userType=="editor")
          {      
            $("body").css("cursor", "wait");
                $("body").css("opacity", "0.5");
                $("button").attr("disabled", true);
                $("input").attr("disabled", true);
          var xmlData = "<xml>"
                    + "<conceptType>" + conceptType + "</conceptType>"
                    + "<conceptId>" + conceptId + "</conceptId>"
                    + "<lang>" + lang + "</lang>"
                    +"</xml>";      
               
                request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=saveConceptType" , true);
                var xmlDoc;
                request.onreadystatechange = function() {
                    if (request.readyState == 4 && request.status == 200) {
                    $("#editConceptType").toggleClass("hidden");
                    $("#saveConceptType").toggleClass("hidden");
                    $("#conceptTypeCancelEdit").toggleClass("hidden"); 
                    //refresh tree
                    var currentUrl =window.location.href;
                        var currentConcept =currentUrl.substring(currentUrl.lastIndexOf("/")+1);
                        var sourceFromXql = "/call-concept/" + currentConcept + "/" + lang;
                    $("#conceptContent").load(sourceFromXql);
                    var newSourceOption = {
                    //        url: '/modules/skosThesau/build-tree.xql?lang=de'
                            url: '/skosThesau/getTreeJSon/slow/' + lang
                            };
                        var tree = $('#collection-tree').fancytree('getTree');
                        tree.reload(newSourceOption);
                        $("body").css("cursor", "default");
                        $("body").css("opacity", "1");
                        $("button").attr("disabled", false);
                        $("input").attr("disabled", false);
                    
                    }
                    };
                }
                else
            {
                $("body").css("cursor", "wait");
                $("body").css("opacity", "0.5tr");
                $("button").attr("disabled", true);
                $("input").attr("disabled", true);
            let comment = prompt("Would you like to add a comment to your suggestion?");
            var xmlData = "<xml>"
            + "<conceptType>" + conceptType + "</conceptType>"
            + "<conceptId>" + conceptId + "</conceptId>"
            + "<lang>" + lang + "</lang>"
            +"<comment>" + comment + "</comment>"
            +"</xml>";
            request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=suggestConceptType" , true);
            var xmlDoc;
            request.onreadystatechange = function() {
                if (request.readyState == 4 && request.status == 200) {
                    var tree = $.ui.fancytree.getTree("#collection-tree");      
                var currentLang = tree.getActiveNode().data.lang; 
                var sourceFromXql = "/call-concept/" + conceptId + "/" + currentLang;
                 $("#conceptContent").load(sourceFromXql);
                
                $("body").css("cursor", "default");
                $("body").css("opacity", "1");
                $("button").attr("disabled", false);
                $("input").attr("disabled", false);
                    
                 
                }
                };
            }
         request.setRequestHeader('Content-Type', 'text/xml');

        request.send(xmlData);       
};

function saveTemporalEntity(conceptId, index, userType){
    var earliestYear = $("#editTemporalEntity_" + index + "_earliest_input").val();
    var latestYear = $("#editTemporalEntity_" + index + "_latest_input").val();
    console.log(isNaN(earliestYear));
    if(isNaN(earliestYear) || isNaN(latestYear)){
                alert("Check the values for year");
    }
    else{
    var request = new XMLHttpRequest();    
    if(userType=="editor")
      {      
        $("body").css("cursor", "wait");
            $("body").css("opacity", "0.5");
            $("button").attr("disabled", true);
            $("input").attr("disabled", true);
      var xmlData = "<xml>"
                    + "<conceptId>" + conceptId + "</conceptId>"
                    + "<index>" + index + "</index>"
                    + "<earliest>" + earliestYear + "</earliest>"
                    + "<latest>" + latestYear + "</latest>"
                +"</xml>";      
           
            request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=saveTemporalEntity" , true);
            var xmlDoc;
            request.onreadystatechange = function() {
                if (request.readyState == 4 && request.status == 200) {
                $("#temporalEntity_"+ index+"_edit").toggleClass("hidden");
                $("#temporalEntity_"+ index+"_display").toggleClass("hidden");
                xmlDoc = request.responseXML;
                var updatedConcept = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedConcept')[0]);
                $("#conceptContent").html(updatedConcept);
                $("body").css("cursor", "default");
                $("body").css("opacity", "1");
                $("button").attr("disabled", false);
                $("input").attr("disabled", false);
                
                }
                };
            }
            else
        {
            $("body").css("cursor", "wait");
            $("body").css("opacity", "0.5tr");
            $("button").attr("disabled", true);
            $("input").attr("disabled", true);
        let comment = prompt("Would you like to add a comment to your suggestion?");
        var xmlData = "<xml>"
                    + "<conceptId>" + conceptId + "</conceptId>"
                    + "<index>" + index + "</index>"
                    + "<earliest>" + earliestYear + "</earliest>"
                    + "<latest>" + latestYear + "</latest>"
                    +"<comment>" + comment + "</comment>"
                    +"</xml>";
        request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=suggestTemporalEntity" , true);
        var xmlDoc;
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                
            
            $("body").css("cursor", "default");
            $("body").css("opacity", "1");
            $("button").attr("disabled", false);
            $("input").attr("disabled", false);
                
             
            }
            };
        }
     request.setRequestHeader('Content-Type', 'text/xml');

    request.send(xmlData);    
    };   
};
function addTemporalEntity(conceptId){
    
    $("body").css("cursor", "wait");
        $("body").css("opacity", "0.5");
        $("button").attr("disabled", true);
        
        $("input").attr("disabled", true);

        var xmlData = "<xml>"
             + "<conceptId>" + conceptId + "</conceptId>"
             + "</xml>"
        var request = new XMLHttpRequest();

        request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=addTemporalEntity" , true);
        var xmlDoc;
             request.onreadystatechange = function() {
                 if (request.readyState == 4 && request.status == 200) {
                  xmlDoc = request.responseXML;
                  var updatedConcept = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedConcept')[0]);
                  $("#conceptContent").html(updatedConcept);
                  $("body").css("cursor", "default");
                  $("body").css("opacity", "1");
                  $("button").attr("disabled", false);
                  $("input").attr("disabled", false);
                }
            };
            request.setRequestHeader('Content-Type', 'text/xml');

            request.send(xmlData);  

};
function saveData(elementName, lang, conceptId, index, originalValue, userType){
        $("body").css("cursor", "wait");
        $("body").css("opacity", "0.5");
        $("button").attr("disabled", true);
        $("input").attr("disabled", true);
        var displayDiv = $("#" + elementName + "_" + lang + "_display");
        var editDiv = $("#" + elementName + "_" + lang + "_edit");
        var elementValue = $("#" + elementName + "_" + lang + "_value");
        var elementInput = $("#" + elementName + "_" + lang + "_input");
        
        var newValue = elementInput.val() 
        
        
        var request = new XMLHttpRequest();
       

        if(userType==="editor"){
            var xmlData = "<xml>"
            + "<elementName>" + elementName + "</elementName>"
            + "<lang>" + lang + "</lang>"
            + "<conceptId>" + conceptId + "</conceptId>"
            + "<value>" + newValue + "</value>"
            + "<originalValue>" + originalValue + "</originalValue>"
            +"</xml>";
        request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=saveData" , true);
        var xmlDoc;
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
            var select = elementInput;
             xmlDoc = request.responseText;
             console.log("TEST element name: " + elementInput.prop("tagName"));
             var tagName = elementInput.prop("tagName");
              newValue2Display = elementInput.val();
             
             displayDiv.replaceWith(xmlDoc.substring(5, xmlDoc.lastIndexOf('<')));    
             displayDiv.toggleClass("elementHidden");
             
             editDiv.toggleClass("elementHidden");
            
              var newSourceOption = {
                            url: '/skosThesau/getTreeJSon/slow/' + lang};
                var tree = $.ui.fancytree.getTree("#collection-tree");    
                if(tree.getActiveNode()!== null){
                    var currentLang = tree.getActiveNode().data.lang; 
                       if(elementName == "prefLabel" && currentLang == lang){
                            $("#prefLabelCurrentLang").html(newValue2Display)
                        };
                    tree.getActiveNode().setTitle(newValue);
                };
                /*            tree.reload(newSourceOption);*/
             
            $("body").css("cursor", "default");
        $("body").css("opacity", "1");
        $("button").attr("disabled", false);
        $("input").attr("disabled", false);
             tree.activateKey(conceptId);
                
                
             
            }
            };
        }
        else
        {
            let comment = prompt("Would you like to add a comment to your suggestion?");
            var xmlData = "<xml>"
            + "<elementName>" + elementName + "</elementName>"
            + "<lang>" + lang + "</lang>"
            + "<conceptId>" + conceptId + "</conceptId>"
            + "<value>" + newValue + "</value>"
            + "<originalValue>" + originalValue + "</originalValue>"
            +"<comment>" + comment + "</comment>"
            +"</xml>";
            request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=submitEdit" , true);
            var xmlDoc;
            request.onreadystatechange = function() {
                if (request.readyState == 4 && request.status == 200) {
               
                 displayDiv.toggleClass("elementHidden");
                 
                 editDiv.toggleClass("elementHidden");
                 xmlDoc = request.responseXML;
                 newElement2Display = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedElement')[0]);
                 $("#revisionsList").replaceWith(newElement2Display);
                 
                $("body").css("cursor", "default");
            $("body").css("opacity", "1");
            $("button").attr("disabled", false);
            $("input").attr("disabled", false);
                    
                 
                }
                };
        }

        
        request.setRequestHeader('Content-Type', 'text/xml');

        request.send(xmlData);
};


function openDialog(dialogId){
    $("#" + dialogId).modal("show");
    
    $("#" + dialogId).find("input,textarea,select").val('').end();


};

function addSelectedNT2Concept(conceptId, $userType, alertMsg){
    var ntUri = $("#newNTconceptURI").html();
    var ntLabel = $("#concepts4NTLookupInputModal").val();
   

    var request = new XMLHttpRequest();
    
     
     if($("#newNTconceptURI").html() == "") { 
        alert(alertMsg)} 
    else {
        
        
        if($userType==="editor")
        {    
            $("body").css("cursor", "wait");
            $("body").css("opacity", "0.5");
            $("button").attr("disabled", true);
            $("input").attr("disabled", true);
            var xmlData = "<xml>"
            + "<currentConceptId>" + conceptId + "</currentConceptId>"
            + "<ntUri>" + ntUri  + "</ntUri>"
            + "<ntLabel>" + ntLabel + "</ntLabel>"
          + "</xml>";
            request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=addExistingConceptasNT" , true);

            var xmlDoc;
                request.onreadystatechange = function() {
                    if (request.status == 400) {
                        console.log("Response : Value of xmlDoc" + xmlDoc);
                /*            alert("Concept could not be added as NT. Please check XML.");*/
                        }
                   else if (request.readyState == 4 && request.status == 200) {
           /*            var el = document.getElementById(inputName.name.toString());*/
                        // xmlDoc = request.responseText;
                        xmlDoc = request.responseXML;
                        var updatedConcept = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedConcept')[0]);

                        
                        $('#dialogInsertNT').modal('hide');         
                        $("body").css("opacity", "1");
                        $("button").attr("disabled", false);
                        $("input").attr("disabled", false);
                        $("body").css("cursor", "default");
                        $("#dialogInsertNT").on('hidden.bs.modal', function () {
                           //Modal must be fully closed so that 'ancestor' html div can be updated
                           $("#conceptContent").html(updatedConcept);
                         });
                        
                        // var orderingType = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('currentConceptOrderingType')[0]);
                        // if(orderingType ==="ordered")
                        //  $('#dialogInsertNT').modal('hide');           
                        //  $("#narrower-list").append('<li class="term-list-item"><a class="conceptLink" onclick="loadOnClickConcept(' + "'"
                        //  + ntUri.substring(ntUri.lastIndexOf('/'))
                        //  + "', 'en')" + '"> '
                        //  + ntLabel
                        //  +'</a></li>'
                        //  );             
                          
                          
                          
                        var tree = $('#collection-tree').fancytree('getTree');
                        if(tree.getActiveNode()!== null){
                        var currentLang = tree.getActiveNode().data.lang;
                        var newSourceOption = {
                            url: '/skosThesau/getTreeJSon/slow/' + currentLang};
                          tree.reload(newSourceOption);
                        tree.activateKey(conceptId);
                        };
                        //console.log("Response : Value of xmlDoc" + xmlDoc);
           /*             console.log("Id of element" + idElementValue);*/
                     
                    }
                };
        }
        else {
            let comment = prompt("Would you like to add a comment to your suggestion?");
            $("body").css("cursor", "wait");
            $("body").css("opacity", "0.5");
            $("button").attr("disabled", true);
            $("input").attr("disabled", true);
            var xmlData = "<xml>"
            + "<currentConceptId>" + conceptId + "</currentConceptId>"
            + "<ntUri>" + ntUri  + "</ntUri>"
            + "<ntLabel>" + ntLabel + "</ntLabel>"
            + "<comment>" + comment + "</comment>"
          + "</xml>";
          console.log(xmlData);
            request.open("POST",
            "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=suggestAddExistingConceptasNT" , true);
            var xmlDoc;
                request.onreadystatechange = function() {
                    if (request.status == 400) {
                        console.log("Response : Value of xmlDoc" + request);
                /*            alert("Concept could not be added as NT. Please check XML.");*/
                        }
                   else if (request.readyState == 4 && request.status == 200) {
                        xmlDoc = request.responseXML;
                        newElement2Display = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedElement')[0]);
                        $("#revisionsList").replaceWith(newElement2Display);
                         $('#dialogInsertNT').modal('hide');           
                         $("#narrower-list").append('<li class="term-list-item"><a class="coneptLink" onclick="loadOnClickConcept(' + "'"
                         + ntUri.substring(ntUri.lastIndexOf('/'))
                         + "', 'en')" + '"> '
                         + ntLabel
                         +'</a></li>'
                         );             
                          var tree = $('#collection-tree').fancytree('getTree');
                            var currentLang = tree.getActiveNode().data.lang;
                          var newSourceOption = {
                            url: '/skosThesau/getTreeJSon/slow/' + currentLang};
                         }
        
        
                     
                        $("body").css("opacity", "1");
                        $("button").attr("disabled", false);
                        $("body").css("cursor", "default");
                        $("input").attr("disabled", false);
                        
                       } // END of (request.readyState == 4 && request.status == 200) {
                       
                       
                };
                
            request.setRequestHeader('Content-Type', 'text/xml');
            
            request.send(xmlData);
                   
    }
};

function createConceptAndAddAsNTTEST(conceptId, idPrefix, baseUri){
        /*$("body").css("cursor", "wait");
        $("body").css("opacity", "0.5");
        $("button").attr("disabled", true);
        $("input").attr("disabled", true);*/
/*    var ntUri = $("#newNTconceptURI").html();*/
    var ntLabelEn = $("#prefLabelEnNT").val();
    var ntLabelFr = $("#prefLabelFrNT").val();
    var ntLabelDe = $("#prefLabelDeNT").val();
    var ntLabelExtraValue = $("#prefLabelExtraValueNT").val();
    var ntLabelExtraLang = $("#prefLabelExtraLangNT").val();

   
    var xmlData = "<xml>"
                    + "<currentConceptId>" + conceptId + "</currentConceptId>"
/*                    + "<ntUri>" + ntUri  + "</ntUri>"*/
                    + "<label xml:lang='en'>" + ntLabelEn + "</label>"
                    + "<label xml:lang='fr'>" + ntLabelFr + "</label>"
                    + "<label xml:lang='de'>" + ntLabelDe + "</label>"
                    + "<label xml:lang='" + ntLabelExtraLang +"'>" + ntLabelExtraValue + "</label>"
                    + "<idPrefix>" + idPrefix + "</idPrefix>"
                    + "<baseUri>" + baseUri + "</baseUri>"
                  + "</xml>";
    console.log("xml data dans thesaurus: " + xmlData);
    var request = new XMLHttpRequest();
   var url = "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=addNewConceptasNT" ;
   
  request.open("POST", url , true);

var xmlDoc;
     
     //if($("#newNTconceptURI").html() == "") { 
     //   alert("Please look up for a concept first")} 
     //       else {
                request.onreadystatechange = function() {
                   if (request.readyState == 4 && request.status == 200) {
                   
           /*            var el = document.getElementById(inputName.name.toString());*/
                        xmlDoc = request.responseText;
           
                              
                          /*$("#prefLabelEnNT").val("");
                          $("#prefLabelFrNT").val("");
                          $("#prefLabelDeNT").val("");
                          $("#prefLabelExtraValueNT").val("");
                          $("#prefLabelExtraLangNT").val("");*/
                         
                        console.log("Response : Value of xmlDoc" + xmlDoc);
                        $('#dialogInsertNT').modal('hide');
                                         $("#dialogInsertNT").on("hidden.bs.modal", function () {
                        $("#narrower-panel").replaceWith(xmlDoc);
                        
                        var tree = $('#collection-tree').fancytree('getTree');
                            var currentLang = tree.getActiveNode().data.lang;
                          var newSourceOption = {
                            url: '/skosThesau/getTreeJSon/slow/' + currentLang
                            };
                          tree.reload(newSourceOption);
                              tree.activateKey(conceptId);
                              $("body").css("cursor", "default");
                    $("body").css("opacity", "1");
                    $("button").attr("disabled", false);
                    $("input").attr("disabled", false);
                        });                      
                         
           /*             console.log("Id of element" + idElementValue);*/
                                 
                        
                       } // END of (request.readyState == 4 && request.status == 200) {
                       
                   if (request.status == 400) {
                   console.log("Response : Value of xmlDoc" + xmlDoc);
                   $("body").css("cursor", "default");
                    $("body").css("opacity", "1");
                    $("button").attr("disabled", false);
                    $("input").attr("disabled", false);
           /*            alert("Concept could not be added as NT. Please check XML.");*/
                   }    
                       };
                
                   request.setRequestHeader('Content-Type', 'text/xml');
                
                   request.send(xmlData);
                   
    //}
};

function createConceptAndAddAsNT(conceptId, idPrefix, baseUri, userType, alertMsgEmpty, alertMsgOtherLang){
        /*$("body").css("cursor", "wait");
        $("body").css("opacity", "0.5");
        $("button").attr("disabled", true);
        $("input").attr("disabled", true);*/
/*    var ntUri = $("#newNTconceptURI").html();*/
    var ntLabelEn = $("#prefLabelEnNT").val();
    var ntLabelFr = $("#prefLabelFrNT").val();
    var ntLabelDe = $("#prefLabelDeNT").val();
    var ntLabelExtraValue = $("#prefLabelExtraValueNT").val();
    var ntLabelExtraLang = $("#prefLabelExtraLangNT").val();

    if(ntLabelEn =="" && ntLabelFr =="" && ntLabelDe=="" && ntLabelExtraValue==""){
    alert(alertMsgEmpty)} 
    else if(ntLabelExtraValue!="" && ntLabelExtraLang ==""){
    alert(alertMsgOtherLang) 
    }
    else{
    if(userType==="editor"){
        var xmlData = "<xml>"
                    + "<currentConceptId>" + conceptId + "</currentConceptId>"
/*                    + "<ntUri>" + ntUri  + "</ntUri>"*/
                    + "<label xml:lang='en'>" + ntLabelEn + "</label>"
                    + "<label xml:lang='fr'>" + ntLabelFr + "</label>"
                    + "<label xml:lang='de'>" + ntLabelDe + "</label>"
                    + "<label xml:lang='" + ntLabelExtraLang +"'>" + ntLabelExtraValue + "</label>"
                    + "<idPrefix>" + idPrefix + "</idPrefix>"
                    + "<baseUri>" + baseUri + "</baseUri>"
                  + "</xml>";
    console.log("xml data dans thesaurus: " + xmlData);
    var request = new XMLHttpRequest();
    
    request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=addNewConceptasNT" , true);
    $('#dialogInsertNT').modal('hide');
    $('#addNTButton').hide();
    $("#narrower-list").append("<li><div class='loader'></div>Please wait while data is processed</li>");
var xmlDoc;
     
     //if($("#newNTconceptURI").html() == "") { 
     //   alert("Please look up for a concept first")} 
     //       else {
                request.onreadystatechange = function() {
                   if (request.readyState == 4 && request.status == 200) {
                   
           /*            var el = document.getElementById(inputName.name.toString());*/
                        xmlDoc = request.responseText;
           
                              
                          /*$("#prefLabelEnNT").val("");
                          $("#prefLabelFrNT").val("");
                          $("#prefLabelDeNT").val("");
                          $("#prefLabelExtraValueNT").val("");
                          $("#prefLabelExtraLangNT").val("");*/
                         
                        console.log("Response : Value of xmlDoc" + xmlDoc);
                        
                                         
                        $("#narrower-panel").replaceWith(xmlDoc);
                        
                        var tree = $('#collection-tree').fancytree('getTree');
                            var currentLang = tree.getActiveNode().data.lang;
                          var newSourceOption = {
                            url: '/skosThesau/getTreeJSon/slow/' + currentLang
                            };
                          tree.reload(newSourceOption);
                              tree.activateKey(conceptId);
                              /*$("body").css("cursor", "default");
                    $("body").css("opacity", "1");
                    $("button").attr("disabled", false);
                    $("input").attr("disabled", false);*/
                               
                         
           /*             console.log("Id of element" + idElementValue);*/
                                 
                        
                       } // END of (request.readyState == 4 && request.status == 200) {
                       
                   if (request.status == 400) {
                   console.log("Response : Value of xmlDoc" + xmlDoc);
                   $("body").css("cursor", "default");
                    $("body").css("opacity", "1");
                    $("button").attr("disabled", false);
                    $("input").attr("disabled", false);
           /*            alert("Concept could not be added as NT. Please check XML.");*/
                   }    
                       };
            }
            else{

                let comment = prompt("Would you like to add a comment to your suggestion?");
                
                var xmlData = "<xml>"
                + "<currentConceptId>" + conceptId + "</currentConceptId>"
/*                    + "<ntUri>" + ntUri  + "</ntUri>"*/
                + "<label xml:lang='en'>" + ntLabelEn + "</label>"
                + "<label xml:lang='fr'>" + ntLabelFr + "</label>"
                + "<label xml:lang='de'>" + ntLabelDe + "</label>"
                + "<label xml:lang='" + ntLabelExtraLang +"'>" + ntLabelExtraValue + "</label>"
                + "<idPrefix>" + idPrefix + "</idPrefix>"
                + "<baseUri>" + baseUri + "</baseUri>"
                +"<comment>" + comment + "</comment>"
                + "</xml>";
        console.log("xml data dans thesaurus: " + xmlData);
        var request = new XMLHttpRequest();
        
        request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=suggestAddNewConceptasNT" , true);
        $('#dialogInsertNT').modal('hide');
        
        
        var xmlDoc;
            request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
             } // END of (request.readyState == 4 && request.status == 200) {
                    
                if (request.status == 400) {
                console.log("Response : Value of xmlDoc" + xmlDoc);
                $("body").css("cursor", "default");
                $("body").css("opacity", "1");
                $("button").attr("disabled", false);
                $("input").attr("disabled", false);
        /*            alert("Concept could not be added as NT. Please check XML.");*/
                }    
            };
                }        
                   request.setRequestHeader('Content-Type', 'text/xml');
                
                   request.send(xmlData);
                   
    }
};


function addNewPrefLabel(conceptId, userType){
        
    var newLabelExtraValue = $("#newPrefLabelExtraValue").val();
    var newLabelExtraLang = $("#newPrefLabelExtraLang").val();
    
    if(userType==="editor"){
    var xmlData = "<xml>"
                    + "<conceptId>" + conceptId + "</conceptId>"
                    + "<prefLabel xml:lang='" + newLabelExtraLang +"'>" + newLabelExtraValue + "</prefLabel>"
                    
                  + "</xml>";
    var request = new XMLHttpRequest();
    request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=addNewPrefLabel" , true);
    var listDivId = "#" + "prefLabel-list"
var xmlDoc;
     
     //if($("#newNTconceptURI").html() == "") { 
     //   alert("Please look up for a concept first")} 
     //       else {
                request.onreadystatechange = function() {
                   if (request.readyState == 4 && request.status == 200) {
           /*            var el = document.getElementById(inputName.name.toString());*/
                        xmlDoc = request.responseText;
                         $('#dialogInsertPrefLabel').modal('hide');           
                        
                            
                        $(listDivId).html(xmlDoc.substring(5, xmlDoc.lastIndexOf('<')))
                        $("#newPrefLabelExtraValue").val("");
                        $("#newPrefLabelExtraLang").val("");
                        
                       } // END of (request.readyState == 4 && request.status == 200) {
                       
                   if (request.status == 400) {
                   console.log("Response : Value of xmlDoc" + xmlDoc);
           
                   }    
                };
                
            }
                else{
                let comment = prompt("Would you like to add a comment to your suggestion?");

                var xmlData = "<xml>"
                + "<conceptId>" + conceptId + "</conceptId>"
                + "<prefLabel xml:lang='" + newLabelExtraLang +"'>" + newLabelExtraValue + "</prefLabel>"
                +"<comment>" + comment + "</comment>"
              + "</xml>";
            var request = new XMLHttpRequest();
            request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=suggestAddNewPrefLabel" , true);
            var listDivId = "#" + "prefLabel-list"
            var xmlDoc;
 
 //if($("#newNTconceptURI").html() == "") { 
 //   alert("Please look up for a concept first")} 
 //       else {
            request.onreadystatechange = function() {
               if (request.readyState == 4 && request.status == 200) {
      
                    xmlDoc = request.responseXML;
                    newElement2Display = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedElement')[0]);
                    $("#revisionsList").replaceWith(newElement2Display);
                     $('#dialogInsertPrefLabel').modal('hide');           
                    
                   } // END of (request.readyState == 4 && request.status == 200) {
                   
               if (request.status == 400) {
               console.log("Response : Value of xmlDoc" + xmlDoc);
       
               }    
            };

                }



                   request.setRequestHeader('Content-Type', 'text/xml');
                
                   request.send(xmlData);
    //}
};

function addNewAltLabel(conceptId, userType){
    var newLabelEn = $("#newAltLabelEn").val();
    var newLabelDe = $("#newAltLabelDe").val();
    var newLabelFr = $("#newAltLabelFr").val();
    
    var newLabelExtraValue = $("#newAltLabelExtraValue").val();
    var newLabelExtraLang = $("#newAltLabelExtraLang").val();
    

    if(userType==="editor"){
         var xmlData = "<xml>"
                    + "<conceptId>" + conceptId + "</conceptId>"
                    + "<altLabel xml:lang='en'>" + newLabelEn + "</altLabel>"
                    + "<altLabel xml:lang='de'>" + newLabelDe + "</altLabel>"
                    + "<altLabel xml:lang='fr'>" + newLabelFr + "</altLabel>"
                    + "<altLabel xml:lang='" + newLabelExtraLang +"'>" + newLabelExtraValue + "</altLabel>"
                    
                  + "</xml>";
        // console.log("xmlData: " + xmlData);
         var request = new XMLHttpRequest();
         request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=addNewAltLabel" , true);
          var listDivId = "#" + "altLabel-list"
        var xmlDoc;
     
     //if($("#newNTconceptURI").html() == "") { 
     //   alert("Please look up for a concept first")} 
     //       else {
                request.onreadystatechange = function() {
                   if (request.readyState == 4 && request.status == 200) {
           /*            var el = document.getElementById(inputName.name.toString());*/
                        xmlDoc = request.responseText;
                         $('#dialogInsertAltLabel').modal('hide');           
                        
                            
                        $(listDivId).html(xmlDoc.substring(5, xmlDoc.lastIndexOf('<')))
                        
                        
                       } // END of (request.readyState == 4 && request.status == 200) {
                       
                   if (request.status == 400) {
                   console.log("Response : Value of xmlDoc" + xmlDoc);
           
                   }    
                       };
        }
        else{
            let comment = prompt("Would you like to add a comment to your suggestion?");
            var xmlData = "<xml>"
                    + "<conceptId>" + conceptId + "</conceptId>"
                    + "<altLabel xml:lang='en'>" + newLabelEn + "</altLabel>"
                    + "<altLabel xml:lang='de'>" + newLabelDe + "</altLabel>"
                    + "<altLabel xml:lang='fr'>" + newLabelFr + "</altLabel>"
                    + "<altLabel xml:lang='" + newLabelExtraLang +"'>" + newLabelExtraValue + "</altLabel>"
                    +"<comment>" + comment + "</comment>"
                  + "</xml>";
        // console.log("xmlData: " + xmlData);
         var request = new XMLHttpRequest();
         request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=suggestAddNewAltLabel" , true);
          var listDivId = "#" + "altLabel-list"
        var xmlDoc;
     
     //if($("#newNTconceptURI").html() == "") { 
     //   alert("Please look up for a concept first")} 
     //       else {
                request.onreadystatechange = function() {
                   if (request.readyState == 4 && request.status == 200) {
                        xmlDoc = request.responseXML;
                        newElement2Display = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedElement')[0]);
                        $("#revisionsList").replaceWith(newElement2Display);             
                        $('#dialogInsertAltLabel').modal('hide');           
                    } // END of (request.readyState == 4 && request.status == 200) {
                       
                   if (request.status == 400) {
                   console.log("Response : Value of xmlDoc" + xmlDoc);
           
                   }    
                       };
        }
                
                   request.setRequestHeader('Content-Type', 'text/xml');
                
                   request.send(xmlData);
    //}
};

function addNewNote(conceptId, noteType, userType){
    var newNoteEn = $("#new" + noteType +"NoteEn").val();
    var newNoteDe = $("#new" + noteType +"NoteDe").val();
    var newNoteFr = $("#new" + noteType +"NoteFr").val();
    
    var newNoteExtraValue = $("#new" + noteType +"NoteExtraValue").val();
    var newNoteExtraLang = $("#new" + noteType +"NoteExtraLang").val();
    var xmlData;

    if(userType==="editor"){
        xmlData = "<xml>"
            + "<noteType>" + noteType + "</noteType>"
            + "<conceptId>" + conceptId + "</conceptId>"
            + "<note xml:lang='en'>" + newNoteEn + "</note>"
            + "<note xml:lang='de'>" + newNoteDe + "</note>"
            + "<note xml:lang='fr'>" + newNoteFr + "</note>"
            + "<note xml:lang='" + newNoteExtraLang +"'>" + newNoteExtraValue + "</note>"
            +"<userRights>" + userType + "</userRights>"
        + "</xml>";
        // console.log("xmlData: " + xmlData);
         var request = new XMLHttpRequest();
         request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=addNote" , true);
          var listDivId = "#" + noteType + "Notes-list"
        var xmlDoc;
     
     //if($("#newNTconceptURI").html() == "") { 
     //   alert("Please look up for a concept first")} 
     //       else {
                request.onreadystatechange = function() {
                   if (request.readyState == 4 && request.status == 200) {
           /*            var el = document.getElementById(inputName.name.toString());*/
                        xmlDoc = request.responseText;
                         $('#dialogAdd' + noteType +'Note').modal('hide');           
                        
                            
                        $(listDivId).html(xmlDoc.substring(5, xmlDoc.lastIndexOf('<')))
                        
                        
                       } // END of (request.readyState == 4 && request.status == 200) {
                       
                   if (request.status == 400) {
                   console.log("Response : Value of xmlDoc" + xmlDoc);
           
                   }    
                       };
        }
        else{
            let comment = prompt("Would you like to add a comment to your suggestion?");
            xmlData = "<xml>"
            + "<noteType>" + noteType + "</noteType>"
            + "<conceptId>" + conceptId + "</conceptId>"
            + "<note xml:lang='en'>" + newNoteEn + "</note>"
            + "<note xml:lang='de'>" + newNoteDe + "</note>"
            + "<note xml:lang='fr'>" + newNoteFr + "</note>"
            + "<note xml:lang='" + newNoteExtraLang +"'>" + newNoteExtraValue + "</note>"
                    +"<comment>" + comment + "</comment>"
                  + "</xml>";
        // console.log("xmlData: " + xmlData);
         var request = new XMLHttpRequest();
         request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=suggestAddNote" , true);
          var listDivId = "#" + noteType + "Note-list"
        var xmlDoc;
     
     //if($("#newNTconceptURI").html() == "") { 
     //   alert("Please look up for a concept first")} 
     //       else {
                request.onreadystatechange = function() {
                   if (request.readyState == 4 && request.status == 200) {
                        xmlDoc = request.responseXML;
                        newElement2Display = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedElement')[0]);
                        $("#revisionsList").replaceWith(newElement2Display);
                        
                         $('#dialogAdd' + noteType + 'Note').modal('hide');           
                        
                            
                        
                       } // END of (request.readyState == 4 && request.status == 200) {
                       
                   if (request.status == 400) {
                   console.log("Response : Value of xmlDoc" + xmlDoc);
           
                   }    
                       };
        }
                
                   request.setRequestHeader('Content-Type', 'text/xml');
                
                   request.send(xmlData);
    //}
};

function deleteLabel(labelType, conceptId, lang, index, labelValue, userType, alertMsg){
    if(userType==="editor"){
            if (confirm(alertMsg)) {
            var xmlData = "<xml>"
                            + "<conceptId>" + conceptId + "</conceptId>"
                            + "<labelType>" + labelType + "</labelType>"
                            + "<labelValue>" + labelValue + "</labelValue>"
                            + "<lang>" + lang + "</lang>"
                            + "<index>" + index + "</index>"
                            +"</xml>";
            console.log("xmlData in delete label: " + xmlData);
            var listDivId = "#" + labelType + "-list"

            var request = new XMLHttpRequest();
            
            request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=deletePrefLabel" , true);
        /*    console.log("xmldata: " + xmlData);*/
            var xmlDoc;
            request.onreadystatechange = function() {
                if (request.readyState == 4 && request.status == 200) {
        /*            var el = document.getElementById(inputName.name.toString());*/
                    xmlDoc = request.responseText;
                    console.log(xmlDoc);
                    $(listDivId).html(xmlDoc.substring(5, xmlDoc.lastIndexOf('<')))
                    }
                };

                request.setRequestHeader('Content-Type', 'text/xml');

                request.send(xmlData);
            }
            else {}
        }
        else
        {
           
            let comment = prompt("Would you like to add a comment to your suggestion?");
            var xmlData = "<xml>"
                            + "<conceptId>" + conceptId + "</conceptId>"
                            + "<labelType>" + labelType + "</labelType>"
                            + "<labelValue>" + labelValue + "</labelValue>"
                            + "<lang>" + lang + "</lang>"
                            + "<index>" + index + "</index>"
                            +"<comment>" + comment + "</comment>"
                            +"</xml>";
            console.log("xmlData in delete label: " + xmlData);
            var listDivId = "#" + labelType + "-list"

            var request = new XMLHttpRequest();
            
            request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=suggestDeletePrefLabel" , true);
        /*    console.log("xmldata: " + xmlData);*/
            var xmlDoc;
            request.onreadystatechange = function() {
                if (request.readyState == 4 && request.status == 200) {
                    xmlDoc = request.responseXML;
                    newElement2Display = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedElement')[0]);
                    $("#revisionsList").replaceWith(newElement2Display);
                   
                    }
                };

                request.setRequestHeader('Content-Type', 'text/xml');

                request.send(xmlData);
            }
        
};

function deleteRelatedConcept(relationType, conceptId, relatedConceptId, relatedConceptUri, language, userRights, alertMsg){
if(userRights =="editor")
    {
    if (confirm(alertMsg)) {
    var xmlData = "<xml>"
                    + "<conceptId>" + conceptId + "</conceptId>"
                    + "<relationType>" + relationType + "</relationType>"
                    + "<relatedConceptId>" + relatedConceptId + "</relatedConceptId>"
                    + "<relatedConceptUri>" + relatedConceptUri + "</relatedConceptUri>"
                    + "<language>" + language + "</language>"
                    +"</xml>";
    
    var panelDiv = "#" + relationType + "-panel"

    var request = new XMLHttpRequest();
    
    request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=deleteRelation" , true);
/*    console.log("xmldata: " + xmlData);*/
    var xmlDoc;
    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
/*            var el = document.getElementById(inputName.name.toString());*/
             xmlDoc = request.responseText;
             console.log(xmlDoc);
             //$(listDivId).html(xmlDoc);
             $(panelDiv).replaceWith(xmlDoc);
             
             var tree = $('#collection-tree').fancytree('getTree');
                            var currentLang = tree.getActiveNode().data.lang;
                          var newSourceOption = {
                            url: '/skosThesau/getTreeJSon/slow/' + currentLang};
                          tree.reload(newSourceOption);
                              tree.activateKey(conceptId);
            }
        };

        request.setRequestHeader('Content-Type', 'text/xml');

        request.send(xmlData);
     }
    }
     else {
        $("body").css("cursor", "wait");
        $("body").css("opacity", "0.5tr");
        $("button").attr("disabled", true);
        $("input").attr("disabled", true);
    let comment = prompt("Would you like to add a comment to your suggestion?");
    var xmlData = "<xml>"
    + "<conceptId>" + conceptId + "</conceptId>"
    + "<relationType>" + relationType + "</relationType>"
    + "<relatedConceptId>" + relatedConceptId + "</relatedConceptId>"
    + "<relatedConceptUri>" + relatedConceptUri + "</relatedConceptUri>"
    + "<language>" + language + "</language>"
    +"<comment>" + comment + "</comment>"
    +"</xml>";
    var request = new XMLHttpRequest();

    request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=suggestDeleteRelation" , true);
    var xmlDoc;
    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
           
            xmlDoc = request.responseXML;
            newElement2Display = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedElement')[0]);
            $("#revisionsList").replaceWith(newElement2Display);

        $("body").css("cursor", "default");
        $("body").css("opacity", "1");
        $("button").attr("disabled", false);
        $("input").attr("disabled", false);
            
         
        }
        };
    }
 request.setRequestHeader('Content-Type', 'text/xml');

request.send(xmlData); 
     
    
};

function downloadCurrentScheme(schemeUri, schemeName){
    var xmlData = "<xml>"
                    + "<schemeUri>" + schemeUri + "</schemeUri>"
                    + "<schemeName>" + schemeName + "</schemeName>"
                    +"</xml>";
    console.log("xmlData: " + xmlData);
    var request = new XMLHttpRequest();
    request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=downloadCurrentScheme" , true);
    var xmlDoc;
    console.log(request.readyState);
    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
/*            var el = document.getElementById(inputName.name.toString());*/
             xmlDoc = request.responseText;
             console.log(xmlDoc);
             //$(listDivId).html(xmlDoc);
    
             
             
            }
        };

     request.setRequestHeader('Content-Type', 'text/xml');

        request.send(xmlData);
        console.log(request.readyState);

};



$(document).ready(function(){
var concepts4NTLookupInputModal = document.getElementById("concepts4NTLookupInputModal");
if (concepts4NTLookupInputModal){

$( "#concepts4NTLookupInputModal" ).autocomplete({
    

      source: function( request, response ) {
        var currentUrl = window.location.href;
        var currentConcept =currentUrl.substring(currentUrl.lastIndexOf("/")+1);
        var queryUrl = '/thesaurus/get-data/starts-with-in-scheme/' 
        $.ajax({
            url : queryUrl,
            dataType : 'json',
            data : {
                 query : $('#concepts4NTLookupInputModal').val(),
                 currentConceptIdBis : currentConcept
            },
            success : function(data){
                  response(
                  $.map(
                        data.list.matching, function(object){
                            return {
                                    label: object.label,
                                    id: object.id,
                                    value: object.value
                                    };
                           }
                        ) 
                  );
            },
            dataFilter: function(data) {
                console.log("In filterData: " + data);
                return data; },
            error: function (xhr, ajaxOptions, thrownError) {
                            console.warn(xhr.responseText)
                            console.log(xhr.status);
                            console.log(thrownError);
                            }
        });

      },
      minLength: 2,
/*      create: function () {
   $(this).data('ui-autocomplete-item')._renderItem = function (ul, item) {
      return $('<li>')
        .append( "<a>" + item.value + ' | ' + item.label + "</a>" )
        .appendTo(ul);
    };
  },*/
      select: function( event, ui ) {

        $('#newNTconceptURI').html(ui.item.id);
        $('#selectedConceptURI').toggleClass("hidden");
        console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );
      }}
      ).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            
        return $( "<li>" )
                .data( "autocomplete-item", item )
				.append( item.label + "<span class='idConceptInSearch'>" + item.id + "</span>" )
				.appendTo( ul )};
  }
  
  
  
});//End of Ready Function

function updateThesaurusTree(){
    if(window.fetch){
        fetch("/skosThesau/buildThesau/");
        alert("Thesaurus is in the process of being updated. Please note that this takes time and might send a proxy error.");
    }
    else {alert("This feature does not work with your browser. Please try Chrome or Firefox");}
};

function validateRevision(revisionId){
    if (confirm('Please confirm you want to validate this revision')) {
        $("body").css("cursor", "wait");
        $("body").css("opacity", "0.5");
        $("button").attr("disabled", true);
        var xmlData = "<xml>"
                        + "<revisionId>" + revisionId + "</revisionId>"
                        +"</xml>";
        
        var request = new XMLHttpRequest();
        
        request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=validateRevision" , true);
    /*    console.log("xmldata: " + xmlData);*/
        var xmlDoc;
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                var currentUrl =window.location.href;
                var currentConcept =currentUrl.substring(currentUrl.lastIndexOf("/")+1);
                var tree = $.ui.fancytree.getTree("#collection-tree");      
                var currentLang = tree.getActiveNode().data.lang; 
                var sourceFromXql = "/call-concept/" + currentConcept + "/" + currentLang;
                $("#conceptContent").load(sourceFromXql);
                $("body").css("cursor", "default");
                $("body").css("opacity", "1");
                $("button").attr("disabled", false);
                }
                else if (request.status == 400) {
                     xmlDoc = request.responseText;

                    alert(xmlDoc);
            /*            alert("Concept could not be added as NT. Please check XML.");*/
                    }
            };
    
            request.setRequestHeader('Content-Type', 'text/xml');
    
            request.send(xmlData);
         }
         else {}
};
function rejectRevision(revisionId){
    if (confirm('Please confirm you want to reject this revision')) {
        $("body").css("cursor", "wait");
        $("body").css("opacity", "0.5");
        $("button").attr("disabled", true);
        var xmlData = "<xml>"
                        + "<revisionId>" + revisionId + "</revisionId>"
                        +"</xml>";
        
        var request = new XMLHttpRequest();
        
        request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=rejectRevision" , true);
    /*    console.log("xmldata: " + xmlData);*/
        var xmlDoc;
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                var currentUrl =window.location.href;
                var currentConcept =currentUrl.substring(currentUrl.lastIndexOf("/")+1);
                var tree = $.ui.fancytree.getTree("#collection-tree");      
                var currentLang = tree.getActiveNode().data.lang; 
                var sourceFromXql = "/call-concept/" + currentConcept + "/" + currentLang;
                $("#conceptContent").load(sourceFromXql);
                $("body").css("cursor", "default");
                $("body").css("opacity", "1");
                $("button").attr("disabled", false);
                }
                else if (request.status == 400) {
                     xmlDoc = request.responseText;

                    alert(xmlDoc);
            /*            alert("Concept could not be added as NT. Please check XML.");*/
                    }
            };
    
            request.setRequestHeader('Content-Type', 'text/xml');
    
            request.send(xmlData);
         }
         else {}
};
function loadOnClickConcept(conceptId, project, lang) {
    var sourceFromXql = "/call-concept/" + conceptId + "/" + lang;
     $("#conceptContent").load(sourceFromXql);
     document.title = project.charAt(0).toUpperCase() + project.slice(1) + " - Concept "+ conceptId;
     history.pushState(null, null,  "/concept/"+ conceptId);
     $.ui.fancytree.getTree("#collection-tree").activateKey(conceptId);
};

function conceptMove(parentConceptId, conceptPosition, moveDirection){
    $("body").css("cursor", "wait");
    $("body").css("opacity", "0.5");
    $("button").attr("disabled", true);
    
var request = new XMLHttpRequest();
var xmlData="<xml>"
                        + "<parentConceptId>" + parentConceptId + "</parentConceptId>"
                        + "<conceptPosition>" + conceptPosition + "</conceptPosition>"
                        + "<moveDirection>" + moveDirection + "</moveDirection>"
                +"</xml>";
    console.log("xmldata: " + xmlData);
    request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=moveConcept" , true);

    var xmlDoc;

 request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status == 200) {
    xmlDoc = request.responseText;
    //console.log("xml rsponse: " + xmlDoc);
         xmlDocXml = request.responseXML;
         /*console.log(xmlDocXml);*/
    
    newNTList= (new XMLSerializer()).serializeToString(xmlDocXml.getElementsByTagName('newNTList')[0]);
    $("#narrower-panel").replaceWith(newNTList);
    xmlString = (new XMLSerializer()).serializeToString(xmlDocXml.getElementsByTagName('newContent')[0]);
    // refreshXmlFile(xmlString);
    $("body").css("cursor", "default");
    $("body").css("opacity", "1");
    $("button").attr("disabled", false);

}
        };

    request.setRequestHeader('Content-Type', 'text/xml');

    request.send(xmlData);
        


};

function deleteTemporalEntity(conceptId, index, alertMsg){
    if (confirm(alertMsg)) {
        $("body").css("cursor", "wait");
        $("body").css("opacity", "0.5");
        $("button").attr("disabled", true);
        var xmlData = "<xml>"
                        + "<conceptId>" + conceptId + "</conceptId>"
                        + "<index>" + index + "</index>"
                        +"</xml>";
        
        var request = new XMLHttpRequest();
        
        request.open("POST", "/$ausohnum-lib/modules/skosThesau/passDataToLibrary.xql?type=deleteTemporalEntity" , true);
    /*    console.log("xmldata: " + xmlData);*/
        var xmlDoc;
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                xmlDoc = request.responseXML;
                var updatedConcept = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedConcept')[0]);
                        var updatedConcept = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedConcept')[0]);

                $("#conceptContent").html(updatedConcept);
                
                $("body").css("cursor", "default");
                $("body").css("opacity", "1");
                $("button").attr("disabled", false);
                }
                else if (request.status == 400) {
                    alert(request.responseText);
                    $("body").css("cursor", "default");
                    $("body").css("opacity", "1");
                    $("button").attr("disabled", false);
                    }
            };
    
            request.setRequestHeader('Content-Type', 'text/xml');
    
            request.send(xmlData);
         }
         else {}
};