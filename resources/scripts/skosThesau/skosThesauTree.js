
glyph_opts = {
    map: {
      doc: "glyphicon glyphicon-asterisk",
      //doc: "glyphicon glyphicon-pencil",
      docOpen: "glyphicon glyphicon-asterisk",
      checkbox: "glyphicon glyphicon-unchecked",
      checkboxSelected: "glyphicon glyphicon-check",
      checkboxUnknown: "glyphicon glyphicon-share",
      dragHelper: "glyphicon glyphicon-play",
      dropMarker: "glyphicon glyphicon-arrow-right",
      error: "glyphicon glyphicon-warning-sign",
      expanderClosed: "glyphicon glyphicon-menu-right",
      expanderLazy: "glyphicon glyphicon-chevron-right",  // glyphicon-plus-sign
      expanderOpen: "glyphicon glyphicon-menu-down",  // glyphicon-collapse-down
      folder: "glyphicon glyphicon-folder-close",
      folderOpen: "glyphicon glyphicon-folder-open",
      loading: "glyphicon glyphicon-refresh glyphicon-spin"

    }
  };
var getResponseSlow = false;
var alreadyGettingResponseSlow = false;
var lockClickFlag = false;
var lockInit = false;
var isFlagEn = false;
var isFlagDe = false;
var isFlagFr = false;
var isFlagAr = false;
$(document).ready(function () {
    var lang= $("#collection-tree").attr("xml:lang");
  skosThesauFancyTree();

function skosThesauFancyTree() {
  var currentUrl = window.location.href;
  var currentConceptId = $("#currentConceptId").text();
  if(currentConceptId === undefined || currentConceptId === "") {
    var currentConceptTmp = currentUrl.substring(currentUrl.lastIndexOf("/")+1);
  } else {
    var currentConceptTmp = currentConceptId;
  }
  $("#collection-tree").fancytree({
        extensions: [
                "glyph", 
                "filter"],
        glyph: glyph_opts,
        clickFolderMode: 3,
        minExpandLevel: 2,
        focusOnSelect: true, // Set focus when node is checked by a mouse click
        autoCollapse: false,//
        activeVisible: true,// Make sure, active nodes are visible (expanded).
        tooltip: true, // Use title as tooltip (also a callback could be specified)
        //imagePath: "/resources/images/",
        quicksearch: true,
        //initAjax: {
        //url: "/modules/jsontree.xql"
        //}
        
        source: {
            url: "/skosThesau/getTreeJSon/fast/" + lang,
            data: {
              conceptId: currentConceptTmp
            },
           cache: true
            /*         url: "http://thot.philo.ulg.ac.be/data/coll/admin/collections.json"*/
         //        url: "./modules/jsonTreeCollections.xql"
       // url: "./modules/jsontree.xql"
                 },
        
        init: function(node, data){

                var url = window.location.href;
                if(currentConceptId === undefined || currentConceptId === "") {
                  var resource = url.substr(url.lastIndexOf('/') + 1);
                } else {
                  var resource = currentConceptId;
                }
                
                if (resource == ""){var concept2display = "c1"; var resource="c1"}
                  else{var concept2display = resource}
                  console.log("Resource: " + resource);
                  console.log("concept2display: " + concept2display);
              // console.log("Uni = " +thotNoUni );

/*                 $("#collection-tree").fancytree("getTree").activateKey(thotNoUni);*/
/*              $("#collection-tree").fancytree("getTree").activateKey(concept2display);*/
/*            var activeNode = $("#collection-tree").fancytree("getTree").getActiveNode();*/
            
            var tree = $.ui.fancytree.getTree("#collection-tree");
            tree.activateKey(concept2display);
            var activeNode = tree.getActiveNode();      
            
            
/*                    activeNode.makeVisible({scrollIntoView: true});*/
/*        $( ".nodeTitleen" ).addClass( "displayedNodeTitle" );        */
          if (activeNode !== null && activeNode.data !== undefined) {
            var nodeLang = activeNode.data.lang;
            responseFast = data.tree.getRootNode().children[0]._isLoading;
            if (responseFast === false && data.status === true && getResponseSlow === false) {
              if (alreadyGettingResponseSlow === false) {
                lockInit = true;
                $('<i id="refreshSlowIcon" class="fancytree-icon  glyphicon glyphicon-refresh glyphicon-spin"/>').insertAfter($('#langflags')); 
                alreadyGettingResponseSlow = true;
                skosThesauFancyTreeSlow(nodeLang, tree);
              }
            }
          }
        },

       activate: function(node) {
            $("body").toggleClass("wait");
            //console.log("Wait");
            var tree = $.ui.fancytree.getTree("#collection-tree");
            var treeroot = tree.getRootNode().getFirstChild();
            var lang = treeroot.data.lang;
/*            console.log("language in tree: " + lang);*/
            
            var activeNode = tree.getActiveNode();
            var nodeId = activeNode.data.id;
               if(nodeId==undefined){nodeId2Use = 'intro'} else{nodeId2Use = nodeId};
            var nodeLang = activeNode.data.lang;
            console.log("lang in activate: " + nodeLang); 
            var nodeTitle=activeNode.title;
            var currentUrl =window.location.href;
            var currentConcept = currentConceptId;
            if(currentConceptId === undefined || currentConceptId === "") {
              var currentConcept = currentUrl.substring(currentUrl.lastIndexOf("/")+1);
            } else {
              var currentConcept = currentConceptId;
            }
                if(currentConcept == ""){currentConcept ="apcc0"}
                   else {currentConcept = currentConcept}
            console.log("Current concept: " +  currentConcept);
            console.log("Node ID: " +  nodeId);
            if (currentConcept == nodeId) {
              console.log("No load as same concept");
                            }
                            else{
                              if (getResponseSlow === false) {
                                refreshClickedNode(nodeLang, nodeId, tree)
                              }
                    
                    /*               var nodeLang = "en";*/
                                // console.log("In activate test: Node id 2 use=" + nodeId2Use + " - data.lang =" + lang);
                    
                                                var templatepath = treeroot.data.url4transform;
                    
                                 //var source = "/templates/processConcept.html?concept=" + nodeId2Use + "&lang=" + lang;
                    
                    /*             var source2 = templatepath + nodeId2Use + "&lang=" + lang;*/
                                 var sourceFromXql = "/call-concept/" + nodeId + "/" + nodeLang;
                                 console.log("Source=" + sourceFromXql);
                                 document.title = "Concept "+ nodeId2Use+ " - " + nodeTitle;
                                 history.pushState(null, null,  "/concept/"+ nodeId2Use);
                    
                                $("#conceptContent").load(sourceFromXql);
          }
                    $("body").removeClass("wait");

        },//End of Activate


        toggleEffect: { effect: "blind", options: {direction: "left"}, duration: 300 },
      wide: {
        iconWidth: "1.5em",     // Adjust this if @fancy-icon-width != "16px"
        iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
        levelOfs: "1.3em"     // Adjust this if ul padding != "16px"
      },
        icon: function(event, data){
         if( data.node.isFolder() ) {
           return "glyphicon glyphicon-book";
         }
      },
      /*renderNode: function(event, data) {
				// Optionally tweak data.node.span
                var node = data.node;
                if(node.data.status!="publish"){
/\*              $(data.node.span).html(data.node.data.status + ": " + data.node.title);*\/
              $(node.span).closest('li').addClass('hide');
/\*				logEvent(event, data);*\/

}else{}
			}
      */
renderNode: function(event, data) {
    var node = data.node;
    if (node.data) {
     var tree = $.ui.fancytree.getTree("#collection-tree");
            var treeroot = tree.getRootNode().getFirstChild();
            var lang = treeroot.data.lang;

        var $span = $(node.span);
                var titleCurrentLang = "";
        if ( node.title != null) {
        var titles = node.title;
          
        if ( titles[lang] != null )
            {title2display = node.title[lang]}
            else {title2display = node.title["fr"]};
        };
        $span.find("span.fancytree-title").text(node.title).css({
            "white-space": "normal",
            "margin": "0 30px 0 5px"
        });
    }
},
filter: {
                autoApply: false,   // Re-apply last filter if lazy data is loaded
                autoExpand: true, // Expand all branches that contain matches while filtered
                counter: false,     // Show a badge with number of matching child nodes near parent icons
                fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                hideExpandedCounter: false,  // Hide counter badge if parent is expanded
                hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
                highlight: true,   // Highlight matches by wrapping inside <mark> tags
                leavesOnly: false, // Match end nodes only
                nodata: true,      // Display a 'no data' status node if result is empty
                mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
        }


        });//End of FancyTree
}

function skosThesauFancyTreeSlow(nodeLangStr, currentTree) {
  $.ajax({
      type: "POST",
      dataType: "json",
      url: '/skosThesau/getTreeJSon/slow/' + nodeLangStr,
      success: function (data, textStatus, XmlHttpRequest) {
        if (XmlHttpRequest.status === 200) {
          currentTree.reload(data);
          getResponseSlow = true;
          document.getElementById('refreshSlowIcon').remove()
          lockInit = false;
          tree.getActiveNode().toggleExpanded()
        }
      }
  });
}


        //FILTER
        var tree = $.ui.fancytree.getTree("#collection-tree");

        /*
        * Event handlers for fancytree filter
        */


        $("input[name=searchTree]").keyup(function(e){
            var n,
            tree = $.ui.fancytree.getTree("#collection-tree"),

            args = "autoApply autoExpand fuzzy hideExpanders highlight leavesOnly nodata".split(" "),
            opts = {},
            filterFunc = tree.filterNodes,
            match = $(this).val();


/*        console.log("match= " + match);*/
        /*
        $.each(args, function(i, o) {
			opts[o] = $("#" + o).is(":checked");
		});
		*/

      opts.mode = "hide";

      if(e && e.which === $.ui.keyCode.ESCAPE || $.trim(match) === ""){
			$("button#btnResetSearch").click();
			return;
		}



			// Pass function to perform match
			n = filterFunc.call(tree, function(node) {
				return new RegExp(match, "i").test(node.title);
				node.titleWithHighlight;
			}, opts);
/*			console.log("regex = " + n);*/

         $("button#btnResetSearch").attr("disabled", false);
         $("span#matches").text("(" + n + " matches)");
       }).focus();


        $("button#btnResetSearch").click(function(e){
        $("input[name=searchTree]").val("");
        $("span#matches").text("");
        tree.clearFilter();
        }).attr("disabled", true);


        //FIN TEST FILTER

        $(".subChildrenPreview").accordion({
        header: ".subChildrenHeader",
        heightStyle: "content",
        collapsible: true,
        active: true,
        navigation: false
        });

        //Multilanguages





$( "#lang-de" ).click(function() {
  if (lockInit === false && isFlagEn === false && isFlagFr === false && isFlagAr === false) {
    isFlagDe = true;
    var currentTree = $.ui.fancytree.getTree("#collection-tree");
    $.ajax({
      type: "POST",
      dataType: "json",
      url: '/skosThesau/getTreeJSon/fast/' + 'de',
      success: function (data, textStatus, XmlHttpRequest) {
        if (XmlHttpRequest.status === 200) {
          currentTree.reload(data);
          getResponseSlow = false;
          alreadyGettingResponseSlow = false;
          if (alreadyGettingResponseSlow === false && lockClickFlag === false) {
            lockClickFlag = true;
            $('<i id="refreshSlowIcon" class="fancytree-icon  glyphicon glyphicon-refresh glyphicon-spin"/>').insertAfter($('#langflags')); 
            alreadyGettingResponseSlow = true;
            $.ajax({
              type: "POST",
              dataType: "json",
              url: '/skosThesau/getTreeJSon/slow/' + 'de',
              success: function (data, textStatus, XmlHttpRequest) {
                if (XmlHttpRequest.status === 200) {
                  currentTree.reload(data);
                  getResponseSlow = true;
                  lockClickFlag = false;
                  document.getElementById('refreshSlowIcon').remove()
                  currentTree.getActiveNode().toggleExpanded();
                  isFlagDe = false;
                }
              }
            });
          } else {
            alert('You must wait that current language is done before activating another flag');
          }
        }
      }
    });
  } else {
    if (isFlagFr === true || isFlagDe === true || isFlagAr === true) {
      alert('You must wait that current language is loaded before activating another flag');
    } else {
      alert('You must wait that initial loading is done before activating this language');
    }
  }
});

$( "#lang-fr" ).click(function() {
  if (lockInit === false && isFlagEn === false && isFlagDe === false && isFlagAr === false) {
    isFlagFr = true;
    var currentTree = $.ui.fancytree.getTree("#collection-tree");
    $.ajax({
      type: "POST",
      dataType: "json",
      url: '/skosThesau/getTreeJSon/fast/' + 'fr',
      success: function (data, textStatus, XmlHttpRequest) {
        if (XmlHttpRequest.status === 200) {
          currentTree.reload(data);
          getResponseSlow = false;
          alreadyGettingResponseSlow = false;
          if (alreadyGettingResponseSlow === false && lockClickFlag === false) {
            lockClickFlag = true;
            $('<i id="refreshSlowIcon" class="fancytree-icon  glyphicon glyphicon-refresh glyphicon-spin"/>').insertAfter($('#langflags')); 
            alreadyGettingResponseSlow = true;
            $.ajax({
              type: "POST",
              dataType: "json",
              url: '/skosThesau/getTreeJSon/slow/' + 'fr',
              success: function (data, textStatus, XmlHttpRequest) {
                if (XmlHttpRequest.status === 200) {
                  currentTree.reload(data);
                  getResponseSlow = true;
                  lockClickFlag = false;
                  document.getElementById('refreshSlowIcon').remove()
                  currentTree.getActiveNode().toggleExpanded();
                  isFlagFr = false;
                }
              }
            });
          } else {
            alert('You must wait that current language is loaded before activating another flag');
          }
        }
      }
    });
  } else {
    if (isFlagFr === true || isFlagDe === true || isFlagAr === true) {
      alert('You must wait that current language is loaded before activating another flag');
    } else {
      alert('You must wait that initial loading is done before activating this language');
    }
  }
});
$( "#lang-en" ).click(function() {
  if (lockInit === false && isFlagFr === false && isFlagDe === false && isFlagAr === false) {
    isFlagEn = true;
    var currentTree = $.ui.fancytree.getTree("#collection-tree");
    $.ajax({
      type: "POST",
      dataType: "json",
      url: '/skosThesau/getTreeJSon/fast/' + 'en',
      success: function (data, textStatus, XmlHttpRequest) {
        if (XmlHttpRequest.status === 200) {
          currentTree.reload(data);
          getResponseSlow = false;
          alreadyGettingResponseSlow = false;
          if (alreadyGettingResponseSlow === false && lockClickFlag === false) {
            lockClickFlag = true;
            $('<i id="refreshSlowIcon" class="fancytree-icon  glyphicon glyphicon-refresh glyphicon-spin"/>').insertAfter($('#langflags')); 
            alreadyGettingResponseSlow = true;
            $.ajax({
              type: "POST",
              dataType: "json",
              url: '/skosThesau/getTreeJSon/slow/' + 'en',
              success: function (data, textStatus, XmlHttpRequest) {
                if (XmlHttpRequest.status === 200) {
                  currentTree.reload(data);
                  getResponseSlow = true;
                  lockClickFlag = false;
                  document.getElementById('refreshSlowIcon').remove()
                  currentTree.getActiveNode().toggleExpanded();
                  isFlagEn = false;
                }
              }
            });
          } else {
            alert('You must wait that current language is loaded before activating another flag');
          }
        }
      }
    });
  } else {
    if (isFlagFr === true || isFlagDe === true || isFlagAr === true) {
      alert('You must wait that current language is loaded before activating another flag');
    } else {
      alert('You must wait that initial loading is done before activating this language');
    }
  }
});
$( "#lang-ar" ).click(function() {
  alert('Arabic has not been implemented yet');
  if (lockInit === false && isFlagFr === false && isFlagDe === false && isFlagAr === false) {
    isFlagAr = true;
    var currentTree = $.ui.fancytree.getTree("#collection-tree");
    $.ajax({
      type: "POST",
      dataType: "json",
      url: '/skosThesau/getTreeJSon/fast/' + 'en',
      success: function (data, textStatus, XmlHttpRequest) {
        if (XmlHttpRequest.status === 200) {
          currentTree.reload(data);
          getResponseSlow = false;
          alreadyGettingResponseSlow = false;
          if (alreadyGettingResponseSlow === false && lockClickFlag === false) {
            lockClickFlag = true;
            $('<i id="refreshSlowIcon" class="fancytree-icon  glyphicon glyphicon-refresh glyphicon-spin"/>').insertAfter($('#langflags')); 
            alreadyGettingResponseSlow = true;
            $.ajax({
              type: "POST",
              dataType: "json",
              url: '/skosThesau/getTreeJSon/slow/' + 'en',
              success: function (data, textStatus, XmlHttpRequest) {
                if (XmlHttpRequest.status === 200) {
                  currentTree.reload(data);
                  getResponseSlow = true;
                  lockClickFlag = false;
                  document.getElementById('refreshSlowIcon').remove()
                  currentTree.getActiveNode().toggleExpanded();
                  isFlagAr = false;
                }
              }
            });
          } else {
            alert('You must wait that current language is loaded before activating another flag');
          }
        }
      }
    });
  } else {
    if (isFlagFr === true || isFlagDe === true || isFlagAr === true) {
      alert('You must wait that current language is loaded before activating another flag');
    } else {
      alert('You must wait that initial loading is done before activating this language');
    }
  }
});

$('#langflags').css( 'cursor', 'pointer' );

    var thotNo = getURLParameter("concept");
    var url = window.location.href;
    var thotNoBis = url.substr(url.lastIndexOf('/') + 1);
    var qm = "\?";
    console.log("Thot no: " + thotNo + "From URL: " + thotNoBis);
/*    var testQM = console.log(url.indexOf(qm) > -1);*/

  // alert("url: " + url + "\n ThotNo: " + thotNo + "\n thotNoBis: " + thotNoBis);

 
 /*switch(thotNoBis.substring(0, 1)){
        case "":
          console.log("null");
          var source = "/thesauri-intro.html";
            $("#conceptContent").load(source);
        case "i":
        var source = "/thesauri-intro.html";
            $("#conceptContent").load(source);
    }*/

 })

function refreshClickedNode(nodeLangStr, currentConceptId, currentTree) {
  $("#conceptContent").load(sourceFromXql);
  var newSourceOption = {
    url: '/skosThesau/getTreeJSon/fast/' + nodeLangStr,
    data: {
      conceptId: currentConceptId
    },
  };
  var sourceFromXql = "/call-concept/" + currentConceptId + nodeLangStr;
  currentTree.reload(newSourceOption);
}

function loadOnClickConcept(conceptId, lang) {
    var sourceFromXql = "/call-concept/" + conceptId + "/" + lang;
     $("#conceptContent").load(sourceFromXql);
     document.title = "Patrimonium - Concept "+ conceptId;
     history.pushState(null, null,  "/concept/"+ conceptId);
     $.ui.fancytree.getTree("#collection-tree").activateKey(conceptId);
};
function reloadPage(){
           location.reload();
       }

function loadConcept(concept){
  var url = "/concept/" + concept;
  window.location.href =url;
};
function loadDashboard(){
  var url = "/admin/";
  window.location.href =url;


};
function getURLParameter(name) { return unescape(
                  (RegExp(name + '=' + '(.+?)($)').exec(location.search)||[,null])[1] ); };
