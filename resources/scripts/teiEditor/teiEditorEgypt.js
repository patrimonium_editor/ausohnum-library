

function addProjectPersonToDocWithRole(){
event.preventDefault();
var peopleUri = $('#selectedPeopleUri').val();
var docId = getCurrentDocId();
var request = new XMLHttpRequest();

if(peopleUri != ""){
$("body").css("cursor", "wait");
$("body").css("opacity", "0.5");
$("button").attr("disabled", true);

var xmlData="<xml>"
            + "<docId>" + docId + "</docId>"
            + "<peopleUri>" + peopleUri + "</peopleUri>"
            + "<dedicator>" + $("#person2AddDedicator").is(':checked') + "</dedicator>"    
            + "<role>" + $("#person2AddType").val() + "</role>"
            + "<bondType>" + $("#person2AddBondType").val() + "</bondType>"
            + "<owner>" + $("#person2AddOwner").val() + "</owner>"
        +"</xml>";

console.log("xmldata: " + xmlData);

/*     request.open("POST", "http://patrimonium.huma-num.fr/admin/save/document/addBiblio", true);*/
request.open("POST", "/$ausohnum-lib/modules/teiEditor/getFunctions.xql?type=addProjectPersonToDocWithRole" , true);
/*                request.open("POST", "/admin/edit/document/save-data/"+docId*/
        //+ "&project=" + "patrimonium" + "&xpath=" + xpath
/*                , true);*/
var xmlDoc;

request.onreadystatechange = function() {
if (request.readyState == 4 && request.status == 200) {
       xmlDoc= request.responseXML;
       //newElement2Display = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedElement')[0]);
       newList= (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('newList')[0]);
       if(xmlDoc.getElementsByTagName('newListForAnnotation')[0]){
       newListForAnnotation= (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('newListForAnnotation')[0]);
       }
        
        $(".valueField").html("");
        $(".valueField").val("");
        if($("#person2AddDedicator").is(':checked')){$("#person2AddDedicator").prop( "checked", false );}

        closeDialog("dialogAddPersonToDocument");

        $(".listOfPeople").html(newList);
        $("#listPeopleManager").html(newList);
        $("#peopleList").html(newListForAnnotation);

        xmlString = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('newContent')[0]);
        //console.log("newContent: " + xmlString);
        refreshXmlFile(xmlString.substring(109).substring(0, xmlString.indexOf("</newContent>")-109));
       
       // $('#peopleManager').replaceWith(newElement2Display);
/*             console.log("Response : Value of xmlDoc" + xmlDoc);*/
/*             console.log("Id of element" + idElementValue);*/
        $("body").css("cursor", "default");
        $("body").css("opacity", "1");
        $("button").attr("disabled", false);

    }
    };

request.setRequestHeader('Content-Type', 'text/xml');

request.send(xmlData);
} else {alert("Please select a person first");}

};


/*
****************************
*     Dropdown menus       *
****************************
*/
$(function(){
  $( "#content" ).on( "click", ".dropdown-menu li a", function( event ) {
      //console.log("menu: " + $(this).attr('menu'));
      //              console.log("v: " + $(this).attr('value'));
          /*        var menu = "#" + $(this).attr('id');*/
                  var menu = $(this).attr('menu');
                  $(menu).html($(this).text()  + '<span class="caret"></span>');
                  $(menu).attr('value', $(this).attr('value'));
                  console.log($(menu).attr('activateFollowing'));
                  if( $(menu).attr('activateFollowing') != ""){
                     $("#" + $(menu).attr('activateFollowing')).removeClass("hidden"); };
  });
  });
  


$("#selectPersonButton").click(function(){
    console.log("Select a person");
    $('#selectPersonPanel').collapse('show');
    if($('#addExistingPersonToDocButton').is(':hidden')){
      console.log("true");
    $('#addExistingPersonToDocButton').removeClass('hidden');};


    if($('#person2AddDedicatorContainer').is(':hidden'))
            {$('#person2AddDedicatorContainer').toggleClass('hidden');}
   
   
    if ( $('#createNewPersonPanel').is(':visible'))
        {$('#createNewPersonPanel').collapse('hide')
      //$('#createAndAddPersonToDocButton').collapse('hide');
        }
      
    if ($('#createAndAddPersonToDocButton').is(':visible'))
        {$('#createAndAddPersonToDocButton').addClass('hidden')
        }
        else{
          //$('#createAndAddPersonToDocButton').removeClass('hidden')
        }
  
      });


$("#createPersonButton").click(function(){
  console.log("Create a person");
    $('#createNewPersonPanel').collapse('show');
    if($('#createAndAddPersonToDocButton').is(':hidden')){
        $('#createAndAddPersonToDocButton').removeClass('hidden');
      };
    if($('#person2AddDedicatorContainer').is(':hidden'))
        {$('#person2AddDedicatorContainer').toggleClass('hidden');
      }
     if ( $('#selectPersonPanel').is(':visible'))
          {
          $('#selectPersonPanel').collapse('hide');
          //$('#addExistingPersonToDocButton').collapse('hide');
          }
      if ( $('#addExistingPersonToDocButton').is(':visible'))
          {console.log("addExisting is VISIBLE");
            
         $('#addExistingPersonToDocButton').addClass('hidden');
          } else{
        // $('#addExistingPersonToDocButton').removeClass('hidden');

          }
});

function addProjectPlaceToDocument(docId){
var placeUri = $('#newPlaceUri').val();
var placeType =$("#c22114_1_1").val();
var placeCertainty = $("#newProjectPlaceCertainty").prop('checked');
var request = new XMLHttpRequest();

if(placeType != ""){
$("body").css("cursor", "wait");
$("body").css("opacity", "0.5");
$("button").attr("disabled", true);

var xmlData="<xml>"
            + "<docId>" + docId + "</docId>"
            + "<placeUri>" + placeUri + "</placeUri>"
            + "<placeType>" + placeType + "</placeType>"        
            + "<placeCertainty>" + placeCertainty + "</placeCertainty>"
        +"</xml>";

/*     console.log("xmldata: " + xmlData);*/

/*     request.open("POST", "http://patrimonium.huma-num.fr/admin/save/document/addBiblio", true);*/
request.open("POST", "/$ausohnum-lib/modules/teiEditor/getFunctions.xql?type=addProjectPlaceToDocument" , true);
/*                request.open("POST", "/admin/edit/document/save-data/"+docId*/
        //+ "&project=" + "patrimonium" + "&xpath=" + xpath
/*                , true);*/
var xmlDoc;

request.onreadystatechange = function() {
if (request.readyState == 4 && request.status == 200) {
/*            var el = document.getElementById(inputName.name.toString());*/
     xmlDoc = request.responseText;
     xmlDocXml = request.responseXML;
     console.log("xmlDoc: " + xmlDocXml);
    console.log(xmlDocXml.getElementsByTagName('newList')[0].getElementsByTagName( 'div' )[0].childNodes);
    newPlacesList= (new XMLSerializer()).serializeToString(xmlDocXml.getElementsByTagName('newList')[0]);
    newPlacesListForAnnotation= (new XMLSerializer()).serializeToString(xmlDocXml.getElementsByTagName('newListForAnnotation')[0]);
    $(".listOfPlaces").html(newPlacesList);
    $("#listOfPlaces").html(newPlacesListForAnnotation);
     /*$("#listOfPlacesOverview").html(xmlDocXml.getElementsByTagName('newList')[0].getElementsByTagName( 'div' )[0].childNodes);*/
        $("#projectPlaceDetailsPreview").html("");
      $("#addProjectPlaceButtonDocPlaces").toggleClass("hidden");
      $("#newProjectPlaceTypeContainer").toggleClass("hidden");
      $("#newProjectPlaceCertaintyContainer").toggleClass("hidden");
      $("#newProjectPlaceCertainty").prop("checked", false);
      $("#projectPlacesLookUp").val("");
$("body").css("cursor", "default");
$("body").css("opacity", "1");
$("button").attr("disabled", false);
/*             console.log("Response : Value of xmlDoc" + xmlDoc);*/
/*             console.log("Id of element" + idElementValue);*/


    }
    };

request.setRequestHeader('Content-Type', 'text/xml');

request.send(xmlData);
} else {alert("Please select a type of relation with the document");}

};


/***************************************************************
*    FOR IMAGE GALLERY                                      *
*    From:https://bootsnipp.com/snippets/P2gor       *    
**************************************************************
*/
let modalId = $('#image-gallery');

$(document)
.ready(function () {

loadGallery(true, 'a.thumbnail');

//This function disables buttons when needed
function disableButtons(counter_max, counter_current) {
$('#show-previous-image, #show-next-image')
.show();
if (counter_max === counter_current) {
$('#show-next-image')
  .hide();
} else if (counter_current === 1) {
$('#show-previous-image')
  .hide();
}
}

/**
*
* @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
* @param setClickAttr  Sets the attribute for the click handler.
*/

function loadGallery(setIDs, setClickAttr) {
let current_image,
selector,
counter = 0;

$('#show-next-image, #show-previous-image')
.click(function () {
  if ($(this)
    .attr('id') === 'show-previous-image') {
    current_image--;
  } else {
    current_image++;
  }

  selector = $('[data-image-id="' + current_image + '"]');
  updateGallery(selector);
});

function updateGallery(selector) {
let $sel = selector;
current_image = $sel.data('image-id');
$('#image-gallery-title')
  .text($sel.data('title'));
$('#image-gallery-image')
  .attr('src', $sel.data('image'));
disableButtons(counter, $sel.data('image-id'));
}

if (setIDs == true) {
$('[data-image-id]')
  .each(function () {
    counter++;
    $(this)
      .attr('data-image-id', counter);
  });
}
$(setClickAttr)
.on('click', function () {
  updateGallery($(this));
});
}
});

// // build key actions
// $(document)
// .keydown(function (e) {
// switch (e.which) {
// case 37: // left
// if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
//   $('#show-previous-image')
//     .click();
// }
// break;

// case 39: // right
// if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
//   $('#show-next-image')
//     .click();
// }
// break;

// default:
// return; // exit this handler for other keys
// }
// e.preventDefault(); // prevent the default action (scroll / move caret)
// });

function openAddGraphicToSurfaceDialog(surfaceId, index){
$("#addGraphicSurfaceId").val(surfaceId);
$("#addGraphicSurfaceIndex").val(index);
openDialog("dialogAddGraphicToSurface");

};

function addGraphicToSurface(){
    var docId = getCurrentDocId();
    var graphicUrl= $("#addGraphicToSurfaceURL").val();
    var graphicCollection = $("#addGraphicToSurfaceCollection").val();
    var graphicCopyright = $("#addGraphicToSurfaceCopyright").val();
    var graphicAuthor = $("#addGraphicToSurfaceAuthor").val();
    var graphicSurfaceId = $("#addGraphicSurfaceId").val();
    var graphicSurfaceIndex = $("#addGraphicSurfaceIndex").val();
    
    if(graphicUrl == "" ) {alert("Please enter at least an URL");}
            else {
                $("body").css("cursor", "wait");
                $("body").css("opacity", "0.5");
                $("button").attr("disabled", true);
                var request = new XMLHttpRequest();
                var xmlData = "<xml>"
                    + "<docId>" + docId + "</docId>"
                    + "<graphicUrl>" + graphicUrl + "</graphicUrl>"
                    + "<graphicCollection>" + graphicCollection + "</graphicCollection>"
                    + "<graphicCopyright>" + graphicCopyright + "</graphicCopyright>"
                    + "<graphicAuthor>" + graphicAuthor + "</graphicAuthor>"
                    + "<graphicSurfaceId>" + graphicSurfaceId + "</graphicSurfaceId>"
                    + "<graphicSurfaceIndex>" + graphicSurfaceIndex + "</graphicSurfaceIndex>"
                    + "</xml>"
                    console.log("xmldata: " + xmlData);
                    request.open("POST", "/$ausohnum-lib/modules/teiEditor/getFunctions.xql?type=addGraphicToSurface" , true);
                    request.onreadystatechange = function() {
                        if (request.readyState == 4 && request.status == 200) {
                            xmlDoc = request.responseXML;
                            console.log(xmlDoc);
                            newElement2Display = (new XMLSerializer()).serializeToString(xmlDoc.getElementsByTagName('updatedElement')[0]);
                            //console.log("XML doc: " + newElement2Display);
                            $('#mediaManager').replaceWith(newElement2Display);
                            $("body").css("cursor", "default");
                            $("body").css("opacity", "1");
                            $("button").attr("disabled", false);
                            closeDialog("dialogAddGraphicToSurface");
                        }
                    };
               request.setRequestHeader('Content-Type', 'text/xml');
               request.send(xmlData);
            }
        };
