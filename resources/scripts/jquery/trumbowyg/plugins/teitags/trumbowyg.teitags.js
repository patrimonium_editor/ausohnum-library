/* ===========================================================
 * trumbowyg.teitags.js v1.0
 * teitags plugin for Trumbowyg
 * http://alex-d.github.com/Trumbowyg
 * ===========================================================
 * Author : Vincent Razanajao, based on preformattedplugin by Casella Edoardo (Civile)
 */


(function ($) {
    'use strict';

    $.extend(true, $.trumbowyg, {
        langs: {
            // jshint camelcase:false
            en: {
                teiitalic: 'I',
                teibold: 'B'
            },
            az: {
                teiitalic: 'Italic'
            },
            by: {
                teiitalic: 'Italic'
            },
            da: {
                teiitalic: 'Italic'
            },
            et: {
                teiitalic: 'Italic'
            },
            fr: {
                teiitalic: 'I',
                teibold: 'G'
            },
            hu: {
                teiitalic: 'Italic'
            },
            it: {
                teiitalic: 'Italic'
            },
            ja: {
                teiitalic: 'Italic'
            },
            ko: {
                teiitalic: 'Italic'
            },
            pt_br: {
                teiitalic: 'Italic'
            },
            ru: {
                teiitalic: 'Italic'
            },
            sl: {
                teiitalic: 'Italic'
            },
            tr: {
                teiitalic: 'Italic'
            },
            zh_cn: {
                teiitalic: 'Italic'
            },
            zh_tw: {
                teiitalic: 'Italic'
            },
        },
        // jshint camelcase:true

        plugins: {
            teiitalic: {
                init: function (trumbowyg) {
                    var btnDef = {hasIcon: false,
                        fn: function () {
                            trumbowyg.saveRange();
                            var text = trumbowyg.getRangeText();
                            if (text.replace(/\s/g, '') !== '') {
                                try {
                                    var curtag = getSelectionParentElement().tagName.toLowerCase();
                                    if (curtag === 'hi') {
                                        return unwrapCode();
                                    }
                                    else {
                                        trumbowyg.execCmd('insertHTML', '<hi rend="italic">' + strip(text) + '</hi>');
                                        trumbowyg.html(cleanXML(trumbowyg));
                                        
                                    }
                                } catch (e) {
                                }
                            }
                        },
                        tag: 'hi'
                    };

                    trumbowyg.addBtnDef('teiitalic', btnDef);
                }
            },
            teibold: {
                init: function (trumbowyg) {
                    var btnDef = {
                        hasIcon: false,
                        fn: function () {
                            trumbowyg.saveRange();
                            var text = trumbowyg.getRangeText();
                            if (text.replace(/\s/g, '') !== '') {
                                try {
                                    var curtag = getSelectionParentElement().tagName.toLowerCase();
                                    if (curtag === 'hi') {
                                        return unwrapCode();
                                    }
                                    else {
                                        trumbowyg.execCmd('insertHTML', '<hi rend="bold">' + strip(text) + '</hi>');
                                        console.log(trumbowyg.html())
                                        trumbowyg.html(cleanXML(trumbowyg));
                                    }
                                } catch (e) {
                                }
                            }
                        },
                        tag: 'hi'
                    };

                    trumbowyg.addBtnDef('teibold', btnDef);
                }
            }
        }
    });

    /*
     * GetSelectionParentElement
     */
    function getSelectionParentElement() {
        var parentEl = null,
            selection;

        if (window.getSelection) {
            selection = window.getSelection();
            if (selection.rangeCount) {
                parentEl = selection.getRangeAt(0).commonAncestorContainer;
                if (parentEl.nodeType !== 1) {
                    parentEl = parentEl.parentNode;
                }
            }
        } else if ((selection = document.selection) && selection.type !== 'Control') {
            parentEl = selection.createRange().parentElement();
        }

        return parentEl;
    }

    /*
     * cleanXML
     * returns a text without the nbsp inserted after execCmd insertHTML
     */
    function cleanXML(trumbowyg){
        console.log(trumbowyg.html());
        xml= trumbowyg.html().replace(/&nbsp;/gi, ' ')
            .replace(/<\/p><hi/gi, "<hi ")
            .replace(/<\/hi><p>/gi, "</hi>");
        console.log(xml);
        return xml;

    }
    /*
     * Strip
     * returns a text without HTML tags
     */
    function strip(html) {
        var tmp = document.createElement('DIV');
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || '';
    }

    /*
     * UnwrapCode
     * ADD/FIX: to improve, works but can be better
     * "paranoic" solution
     */
    function unwrapCode() {
        var container = null;

        if (document.selection) { //for IE
            container = document.selection.createRange().parentElement();
        } else {
            var select = window.getSelection();
            if (select.rangeCount > 0) {
                container = select.getRangeAt(0).startContainer.parentNode;
            }
        }

        //'paranoic' unwrap
        var ispre = $(container).contents().closest('hi').length;
        var iscode = $(container).contents().closest('hi').length;

        if (ispre && iscode) {
            $(container).contents().unwrap('hi').unwrap('hi');
        } else if (ispre) {
            $(container).contents().unwrap('hi');
        } else if (iscode) {
            $(container).contents().unwrap('hi');
        }
    }
})(jQuery);
