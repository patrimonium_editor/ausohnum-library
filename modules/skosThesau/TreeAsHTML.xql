xquery version "3.1";

import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/" at "/db/apps/ausohnum-library/modules/skosThesau/skosThesauApp.xql";
import module namespace transform="http://exist-db.org/xquery/transform" at "java:org.exist.xquery.functions.transform.TransformModule";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";

declare variable $project := "patrimonium";
declare variable $collection := "places/patrimonium";

let $lang:="en"

let $topConceptUri := "https://ausohnum.huma-num.fr/concept/c21849"
let $xmlElement := "textMainLang"


let $conceptId := substring-after($topConceptUri, "/concept/")
let $json := skosThesau:getTreeFromConcept($project, $topConceptUri, $lang)

let $xslt := doc("/db/apps/ausohnum-library/xslt/jsonTree2html.xsl") 
let $params := <parameters>
    <param name="topConceptUri" value="{ $topConceptUri }"/>
    <param name="topConceptUri" value="{ $xmlElement }"/>
    </parameters>

let $htmlTree := transform:transform($json, $xslt, $params)
        

return 
    $htmlTree
