(:~
: AusoHNum Library - thesaurus module
: This function was created to import Eagle thesaurus (https://www.eagle-network.eu/resources/vocabularies/). Not finalized 
: @author Vincent Razanajao
:)
xquery version "3.1";

import module namespace config="http://ausonius.huma-num.fr/ausohnum-library/config" at "../config.xqm";
import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/" at "skosThesauApp.xql";
import module namespace functx="http://www.functx.com";

declare boundary-space preserve;

declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace dct = "http://purl.org/dc/terms/";

declare namespace periodo="http://perio.do/#";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
(:declare namespace skosThesau = "https://ausohnum.huma-num.fr/skosThesau/";:)

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
(:declare option output:indent "yes";:)

declare variable $thesaurus := "ausohnum";
declare variable $project :=request:get-parameter('project', 'ausohnum');
declare variable $dbName := doc('xmldb:exist:///db/apps/' || $project || '/data/app-general-parameters.xml')//idPrefix[@type='db']/text();
declare variable $baseUri := doc('xmldb:exist:///db/apps/' || $project || '/data/app-general-parameters.xml')//uriBase[@type="thesaurus"]/text();
declare variable $thesaurusPrefix := doc('xmldb:exist:///db/apps/' || $project || '/data/app-general-parameters.xml')//idPrefix[@type='thesaurus']/text();
declare variable $conceptPrefix := doc('xmldb:exist:///db/apps/' || $project || '/data/app-general-parameters.xml')//idPrefix[@type='concept']/text();
declare variable $conceptBaseUri := $baseUri || "/concept/";

declare variable $peopleRepo := collection("xmldb:exist:///db/data/" || $project || "/accounts");
declare variable $newLine := '&#xa;';

let $mode := "existing"
let $rdfFilename := "writing.rdf"
let $thesaurusLabel := substring-before($rdfFilename, ".")

let $logs := collection("/db/apps/" || $thesaurus || "/logs")
let $now := fn:current-dateTime()
let $currentUser := sm:id()
let $personRecord := $peopleRepo/id($currentUser)
let $userName := $personRecord//firstname || ' ' || $personRecord//lastname

let  $conceptCollection := collection('xmldb:exist:///db/apps/' || $thesaurus || "Data/concepts")
let $conceptScheme := $conceptCollection//.[skos:ConceptScheme[@rdf:about="https://ausohnum.huma-num.fr/thesaurus/petrae/"]]

let $rdfInput := doc("/db/apps/" || $thesaurus || "Data/imports/" || $rdfFilename)
let $thesaurusTitle:=  $rdfInput//skos:ConceptScheme/dc:title/text()
let $idList := for $id in $conceptCollection//.[contains(./@xml:id, $conceptPrefix)]
                return
                <item>
                { substring-after($id/@xml:id, $conceptPrefix) }
                </item>
        
            let $last-id:= fn:max($idList) 
            
            (:let $newId := $conceptPrefix || fn:sum(($last-id, 1)):)
            let $newId := $conceptPrefix || fn:sum(($last-id, 1))
             
             let $idTopConcept := $newId
             
let $scheme :=(<rdf:RDF xmlns:dct="http://purl.org/dc/terms/" xmlns:periodo="http://perio.do/#"
    xmlns:skosThesau="https://ausohnum.huma-num.fr/skosThesau/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:time="http://www.w3.org/2006/time#"
    xmlns:map="http://www.w3c.rl.ac.uk/2003/11/21-skos-mapping#"
    xmlns:dc="http://purl.org/dc/elements/1.1/">
    { element {"skos:ConceptScheme"}
            {attribute {"rdf:about"} {$baseUri || '/' || $thesaurusPrefix 
            || '/' || "eagle-" || $thesaurusLabel || '/'}
            , $newLine || "        ",
            element
            {"dc:title"}
                    {attribute {"type"} {"full"},
                    $thesaurusTitle}, $newLine || "        ",
            element {"dc:title"}
                    {attribute {"type"} {"short"},
                    $thesaurusLabel}, $newLine || "        ",
            element {"skos:hasTopConcept"}
                    {attribute {"rdf:resource"} {$conceptBaseUri || $idTopConcept}}, $newLine || "        ",
            
        element {"dc:creator"} {"Eagle consortium"}, $newLine || "        ",
        element {"dc:creator"} {"epigraphy.info Community"}, $newLine || "        ",
        element {"dc:source"}
            {attribute {"rdf:resource"} { data($rdfInput//skos:ConceptScheme/@rdf:about)}, 
            $rdfInput//skos:ConceptScheme/dc:title/text() }, $newLine || "        ",
        element {"dc:publisher"} {"Ausonius Institute"}, $newLine || "        ",
        element {"dct:created"} {$now}, $newLine || "        ",
        element {"skosThesau:admin"}
                {attribute{"status"}{"draft"}},
                $newLine || "    "}
}
    <skos:Collection xml:id="{ $conceptPrefix}{($last-id + 1)}" rdf:about="{ $conceptBaseUri || "" ||$conceptPrefix}{($last-id + 1)}">
        <skos:prefLabel xml:lang="en">{ $thesaurusTitle }</skos:prefLabel>{
        for $concept at $pos in $rdfInput//skos:Concept[not(ancestor-or-self::skos:exactMatch) and not(ancestor-or-self::skos:closeMatch)]
            let $prefLabel1 := $concept/skos:prefLabel[1]
            let $prefLabel1Value := $prefLabel1/text()
            let $prefLabel1Lang := $prefLabel1/@xml:lang
            let $eagleUri := $concept/@rdf:about
            return
                ($newLine || "        ", <skos:narrower rdf:resource="{  $conceptBaseUri || "" ||$conceptPrefix}{($last-id + 1 + $pos)}"/>
                
                )
}
        <skos:inScheme rdf:resource="{$baseUri || '/' || $thesaurusPrefix || '/eagle-' || $thesaurusLabel || '/'}"/>
    </skos:Collection>{
        
        for $concept at $pos in $rdfInput//skos:Concept[not(ancestor-or-self::skos:exactMatch) and not(ancestor-or-self::skos:closeMatch)]
            let $prefLabel1 := $concept/skos:prefLabel[1]
            let $prefLabel1Value := $prefLabel1/text()
            let $prefLabel1Lang := $prefLabel1/@xml:lang
            let $eagleUri := $concept/@rdf:about
return
 ($newLine || "    ", <skos:Concept xml:id="{$conceptPrefix}{($last-id + 1 + $pos)}" 
    rdf:about="{ $conceptBaseUri || "" ||$conceptPrefix}{($last-id + 1 + $pos)}">{
        if($concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch) and not(ancestor-or-self::skos:closeMatch)]) then
            for $prefLabel in $concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch) and not(ancestor-or-self::skos:closeMatch)]
            return
                ($newLine || "        ", $prefLabel)
            else()
        }{
            if($concept//skos:exactMatch) then
                for $exactMatch at $pos in $concept//skos:exactMatch
                
                return
                    if(
                        ($concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)]/@xml:lang = $exactMatch/skos:Concept/skos:prefLabel/@xml:lang)
                        or
                        $concept//skos:exactMatch[position()<$pos]/skos:Concept/skos:prefLabel/@xml:lang = $exactMatch/skos:Concept/skos:prefLabel/@xml:lang
                        )
                    then
                ($newLine || "        ",
                <skos:altLabel xml:lang="{ $exactMatch/skos:Concept/skos:prefLabel/@xml:lang}">{$exactMatch/skos:Concept/skos:prefLabel/text()}</skos:altLabel>
                )    
                    else
                            
                ($newLine || "        ",
                <skos:prefLabel xml:lang="{ $exactMatch/skos:Concept/skos:prefLabel/@xml:lang}">{$exactMatch/skos:Concept/skos:prefLabel/text()}</skos:prefLabel>
                )
         else()}{
        if ($concept//skos:altLabel) then
        for $altLabel in $concept//skos:altLabel
        return 
            ($newLine || "        ", $altLabel)
            else()
        }{
            if($concept//skos:broader) then
                for $bt in $concept//skos:broader
                return
                ($newLine || "        ", $bt)
         else(<skos:broader rdf:resource="{ $baseUri  || "/concept/" ||$conceptPrefix}{($last-id + 1)}"/>)}{
            if($concept//skos:narrower) then
                for $nt in $concept//skos:narrower
                return
                ($newLine || "        ", $nt)
         else()}{
            if($concept//skos:scopeNote) then (
                for $scopeNote in $concept//skos:scopeNote[exists(./@xml:lang)]
                return
                ($newLine || "        ", <skos:scopeNote xml:lang="{ data($scopeNote/@xml:lang) }">{normalize-space($scopeNote/text())}</skos:scopeNote>))
                else ()}
        <skos:exactMatch>
            <skos:Concept rdf:resource="{data($concept/@rdf:about)}">
                <skos:prefLabel xml:lang="{ $prefLabel1Lang }">{ $prefLabel1Value}</skos:prefLabel>
                <skos:notation>{ data($concept/@rdf:about)}</skos:notation>
                <skos:inScheme rdf:resource="{data($concept//skos:inScheme/@rdf:resource)}"/>
            </skos:Concept>
        </skos:exactMatch>{
            if($concept//skos:exactMatch) then
                for $exactMatch in $concept//skos:exactMatch
                return
                ($newLine || "        ", <skos:exactMatch>
            <skos:Concept rdf:resource="{ $exactMatch/skos:Concept/@rdf:about }">
                <skos:prefLabel xml:lang="{ $exactMatch/skos:Concept/skos:prefLabel/@xml:lang }">{ $exactMatch/skos:Concept/skos:prefLabel/text() }</skos:prefLabel>
                <skos:notation>{ data($exactMatch/skos:Concept/@rdf:about) }</skos:notation>
                <skos:inScheme rdf:resource="{ data($concept//skos:inScheme/@rdf:resource)}"/>
            </skos:Concept>
        </skos:exactMatch>)
         else()}{
            if($concept//skos:closeMatch) then
                for $closeMatch in $concept//skos:closeMatch
                return
                ($newLine || "        ", $closeMatch)
         else()}
        <skos:inScheme rdf:resource="{$baseUri || '/' || $thesaurusPrefix || '/eagle-' || $thesaurusLabel || '/'}"/>    
        <dct:created>{ $now }</dct:created>
        <dct:modified>{ $now }</dct:modified>
    </skos:Concept>)} 
</rdf:RDF>
)
let $filePath := "/db/apps/" || $thesaurus || "Data/imports/outputs"
let $fileName := "eagle-" || $thesaurusLabel || "-" || replace(substring($now, 1, 19), ":", "") || ".rdf"
let $storeScheme := xmldb:store($filePath, $fileName, $scheme)
let $newDoc := doc($filePath || "/" || $fileName)
let $updateNT :=
    for $nt in ($newDoc//skos:narrower[contains(./@rdf:resource, "eagle")], $newDoc//skos:broader[contains(./@rdf:resource, "eagle")])
        let $newUri := data($newDoc//skos:Concept[.//skos:exactMatch/skos:Concept[@rdf:resource = $nt/@rdf:resource]]/@rdf:about)
        let $newUriChecked := if(empty($newUri)) then "no-uri-for:" || $nt/@rdf:resource
        else $newUri
        return 
            update value $nt/@rdf:resource with $newUriChecked
            

return $scheme