(:~
: AusoHNum Library - thesaurus module
: This function exports the concepts of a thesaurus 
: @author Vincent Razanajao
: @param name of the project
: @param cs format
: @return text file.
:)

xquery version "3.1";

import module namespace functx = "http://www.functx.com";
import module namespace skosThesau = "https://ausohnum.huma-num.fr/skosThesau/" at "skosThesauApp.xql";


declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare variable $project :=request:get-parameter('project', ())[1];



declare variable $appParam := doc('/db/apps/' || $project || '/data/app-general-parameters.xml');
declare variable $thesaurus-app := $appParam//thesaurus-app/text();
declare variable $concept-collection := collection('/db/apps/' || $thesaurus-app || 'Data/concepts');

(:declare option output:method 'adaptative';:)

let $now := fn:current-dateTime()


let $schemeUri := request:get-parameter('schemeUri', ())
let $schemeName := request:get-parameter('schemeName', ())

 
let $fileContent := $concept-collection//rdf:RDF[skos:ConceptScheme[@rdf:about = $schemeUri ]]

let $filename := replace($schemeName, " ", "_") || substring($now, 1, 10) ||'-' || replace(substring($now, 12, 5), ':', '') || ".xml"
let $target-directory := 
    (: Mac :)
    (: '/Users/Joe/workspace/' :)
    (: Windows :)
    'c:\computing\'
let $target-path:= 
    (: Construct the full filesystem path for the file :)
    concat($target-directory, $filename)

return

(:response:stream-binary(util:string-to-binary($fileContent), "application/zip", $filename):)
response:stream-binary(util:string-to-binary(serialize($fileContent)), "application/zip", $filename)
