xquery version "3.0";

module namespace jsonWrapper="http://ausonius.huma-num.fr/jsonWrapper";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";

declare namespace json = "http://www.json.org";
declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace srophe="https://srophe.app";

declare option output:indent "yes";
declare option output:method "json";
declare option output:media-type "text/javascript";
(:~
 : Serialize XML as JSON
:)
declare function jsonWrapper:json($nodes as node()*){
    serialize($nodes, 
        <output:serialization-parameters>
            <output:method>json</output:method>
        </output:serialization-parameters>)
    (: xqjson:serialize-json(geojson:json-wrapper($nodes)) :)
};