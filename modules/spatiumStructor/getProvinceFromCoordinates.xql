 xquery version "3.1";

import module namespace spatial=
"http://exist-db.org/xquery/spatial" at
"java:org.exist.xquery.modules.spatial.SpatialModule";

declare namespace gml = "http://www.opengis.net/gml";
declare namespace ogr="http://ogr.maptools.org/";

let $gml-xml-1 :=
    <gml:Polygon xmlns:gml="http://www.opengis.net/gml" srsName="ourn:ogc:def:crs:EPSG::4326">
        <gml:outerBoundaryIs>
            <gml:LinearRing>
                <gml:coordinates>27.00808,35.42428 27.00761,35.4165 26.99872,35.40839 26.99883,35.40175 26.98653,35.39992 26.97944,35.39889 26.97683,35.39455 26.97372,35.39417 26.96986,35.39631 26.96481,35.395 26.94194,35.37614 26.94006,35.36769 26.93692,35.36614 26.93058,35.37014 26.92492,35.36658 26.91028,35.36247 26.90656,35.35978 26.90531,35.35453 26.89597,35.35008 26.89444,35.34817 26.89222,35.34533 26.88714,35.34358 26.87856,35.34694 26.87428,35.34311 26.87078,35.34869 26.86636,35.35175 26.8635,35.35142 26.85739,35.35075 26.85519,35.35261 26.85836,35.35622 26.85322,35.36297 26.85742,35.37528 26.86036,35.38053 26.86833,35.37983 26.87011,35.37967 26.87411,35.3828 26.87367,35.38672 26.87772,35.39353 26.88508,35.39661 26.89506,35.40403 26.90553,35.41989 26.91181,35.42484 26.91547,35.42547 26.9327,35.42081 26.93581,35.421 26.94111,35.42136 26.94739,35.42536 26.96619,35.42803 26.98147,35.43372 26.99014,35.43378 26.9992,35.43753 27.02072,35.43622 27.02122,35.43344 27.011,35.42814 27.00808,35.42428
                </gml:coordinates>
            </gml:LinearRing>
        </gml:outerBoundaryIs>
    </gml:Polygon>
    
let $romanProvinces117 := doc("/db/apps/patrimoniumData/places/gml-places/roman-provinces-117.xml")

let $provincesPolygons :=
    for $coordinates in $romanProvinces117//gml:coordinates
        let $provinceName := $coordinates/ancestor::gml:featureMember//ogr:NAME/text()
        
        return 
    <gml:Polygon xmlns:gml="http://www.opengis.net/gml" srsName="http://www.opengis.net/def/crs/EPSG/0/4326" name="{ $provinceName }">
        <gml:outerBoundaryIs>
            <gml:LinearRing>
                <gml:coordinates>
                { $coordinates/text()}
                </gml:coordinates>
            </gml:LinearRing>
        </gml:outerBoundaryIs>
    </gml:Polygon>
let $testPlace :=<gml:Point srsName="EPSG:4326">
    <gml:pos>27.34551256 37.94218342</gml:pos>
    </gml:Point>
let $province := $romanProvinces117/ogr:FeatureCollection//gml:featureMember[1]
let $provinceName := $province//ogr:NAME/text()
let $provincePolygon := $province[1]//gml:Polygon[1]
let $gml-xml-test :=
    <gml:Polygon xmlns:gml="http://www.opengis.net/gml" srsName="ourn:ogc:def:crs:EPSG::4326">
        <gml:outerBoundaryIs>
            <gml:LinearRing>
                <gml:coordinates>{ $provincePolygon[1]//gml:coordinates/text() }
                </gml:coordinates>
            </gml:LinearRing>
        </gml:outerBoundaryIs>
    </gml:Polygon>

let $provinceCentroid := 
            (spatial:getCentroidX($gml-xml-test) || " prov: " || spatial:getCentroidX($provincePolygon[1]/self::gml:Polygon)
            )
(:            return :)
(:                $provincePolygon[1]:)

            (: return
                for $polygon in $provincesPolygons :)
                
                
                return <result>
                { spatial:within($provincesPolygons[1], $testPlace) }
                </result>
                (: return spatial:within($provincesPolygons, $testPlace) :)
                
(:return geospatial:getMaxX($provincePolygon[1]):)
(:return (geospatial:getGeometryType($province)):)
(:  return $provinceName || " " || count($provincePolygon) || ". Centroidx= " || $provinceCentroid:)