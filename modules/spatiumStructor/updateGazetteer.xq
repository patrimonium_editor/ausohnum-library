xquery version "3.1";


import module namespace functx="http://www.functx.com";
import module namespace spatiumStructor="http://ausonius.huma-num.fr/spatiumStructor" at "spatiumStructor.xql";
import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/" at "../skosThesau/skosThesauApp.xql";
import module namespace util = "http://exist-db.org/xquery/util";

declare namespace trigger = "http://exist-db.org/xquery/trigger";

declare namespace json="http://www.json.org";

declare namespace apc="http://patrimonium.huma-num.fr/onto#";
declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dct = "http://purl.org/dc/terms/";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace ev = "http://www.w3.org/2001/xml-events";
declare namespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace http="http://expath.org/ns/http-client";

declare namespace lawdi="http://lawd.info/ontology/";
declare namespace owl="http://www.w3.org/2002/07/owl#";
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace rdfs = "http://www.w3.org/2000/01/rdf-schema#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace spatial="http://geovocab.org/spatial#";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace thot = "http://thot.philo.ulg.ac.be/";
declare namespace xf = "http://www.w3.org/2002/xforms";
declare namespace local = "local";


(:declare option exist:serialize "method=xml media-type=xml";:)

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:indent "yes";
(: Switch to JSON serialization :)
(:
declare option output:method "xml";
declare option output:media-type "text/xml";:)

(:declare variable $local:project := "patrimonium";:)
declare variable $local:project external;

declare function local:buildProjectPlacesCatalogue( $local:project ){
let $project := $local:project
let $startTime := util:system-time()
let $now := fn:current-dateTime()
let $currentUser := data(sm:id()//sm:username)
let $placeCollection := util:eval('collection("/db/apps/' || $project || 'Data/places/' || $project || '")')
let $appVariables := doc("/db/apps/" || $project || "/data/app-general-parameters.xml")
let $concept-collection:= doc("/db/apps/" || $appVariables//thesaurus-app/text() || "Data/concepts/" || $project || ".rdf")
let $productionUnitTypes := skosThesau:getChildren($appVariables//productionUnitsUri/text(), $project)
let $romanProvinces := ($placeCollection//pleiades:Place[pleiades:hasFeatureType[@rdf:resource="https://ausohnum.huma-num.fr/concept/c22264"]])
let $romanProvincesUriList := string-join($romanProvinces//@rdf:about, " ")
let $features :=
            for $placeSpatialFeature in $placeCollection//spatial:Feature
                    let $uriLong := data($placeSpatialFeature/@rdf:about)
                    let $uri := substring-before($uriLong, "#this")
                    let $id := functx:substring-after-last($uri, "/")
                    let $place := $placeSpatialFeature//pleiades:Place
                    let $exactMatch := string-join($placeSpatialFeature//skos:exactMatch/@rdf:resource, " ")
                    let $placeName := $place/dcterms:title/text()
                    let $altNames := string-join($place//skos:altLabel, " ")
                    let $provinceUri := $placeSpatialFeature//spatial:P[functx:contains-any-of($romanProvincesUriList, ./@rdf:resource/string()) ]/@rdf:resource/string()
                    let $provinceName := $romanProvinces//.[@rdf:about = $provinceUri]//dcterms:title/text()      
                     (:let $mainPlaceType := if($place/pleiades:hasFeatureType[@type = "main"]/@rdf:resource) 
                        then  $projectThesaurus//skos:Concept[@rdf:about = $place/pleiades:hasFeatureType[matches(./@type, "main")]/@rdf:resource]//skos:prefLabel[@xml:lang ="en"]/text()
                                                            else("untyped place"):)
                     let $mainPlaceTypeUri := functx:if-absent($place//pleiades:hasFeatureType[@type = "main"]/@rdf:resource, "no-type")
                     let $mainPlaceType :=
                           functx:if-absent(
                                $concept-collection//skos:Concept[@rdf:about = $mainPlaceTypeUri]//skos:prefLabel[@xml:lang ="en"]/text(),
                                "untyped place")
                            
                     let $productionTypeUris := $place/pleiades:hasFeatureType[@type = "productionType"]/@rdf:resource
                                         let $productionType := if($productionTypeUris) then
                                    for $productionTypeUri at $pos in $productionTypeUris 
                                        where $productionTypeUri != ""
                                        return 
                                            <productionType>{
                                                $concept-collection//skos:Concept[@rdf:about = $productionTypeUri]//skos:prefLabel[@xml:lang ="en"]/text()
                                                }</productionType>
                                         else
                                             ()
                       let $marker := concat("marker-",
                                            switch(lower-case($mainPlaceType))
                                                        case "landed estate" return
                                                        (
                                                            if($productionType[1]) then 
                                                                switch(lower-case($productionType[1]))
                                                                        case "wheat" return "farming-wheat"
                                                                        case "cereals"
                                                                            return "farming-wheat-and-others"
                                                                        case "vineyard" 
                                                                        case "wine" 
                                                                        case "vines" return "farming-vineyard"
                                                                default return "farming-wheat"
        (:                                                    || "Icon":)
                                                            else("farming-wheat")
                                                        )
                                                       case "forest"
                                                       case "forest or pastureland" return "forest-pasture"
                                                       case "workshop" return 
                                                       ( if($productionType[1]) then 
                                                                switch(lower-case($productionType[1]))
                                                                    case "bricks" return "workshops-bricks"
                                                                    default return concat("workshops-", lower-case($productionType[1]))
                                                                
                                                                else ("color-violet")
                                                            )
                                                       case "military camp/outpost" return "default"
                                                       case "modern place" case "city" case "settlement" case "village/settlement" return "default"
                                                       case "mine" case "quarry" return "extraction-marble"
                                                       case "area" case "geographic region" return "color-green"
                                                       case "production units" return "default"
                                                       case "administrative district" return "default"
                                                       case "station" return "default"
                                                       case "roman provinces" case "province" return "color-black"
                                                       case "ethnic region" return "color-black"
                                                       case "villa" return "villa"
                                                       case "untyped place" return "color-red"
                                                       default return 
                                                             "default"
                                                       , ".png")

        
         
                 let $isPartOf := ($placeSpatialFeature//spatial:P)
                 let $isMadeOf := ($placeSpatialFeature//spatial:Pi)
                 let $isInVicinityOf := ($placeSpatialFeature//spatial:C[@type='isInVicinityOf'])
                 let $coordinates :=(
                    if($place//geo:long/text() != "")
                        then      (<geoCoord>
                                                    <coordinates json:array="true" json:literal="false">{ $place/geo:long/text()}, {$place/geo:lat/text()}</coordinates>
                                      </geoCoord>)
                        
                        
                        else 
                                (
(:                                if isMadeOf take coordinates:)
                                (if($isMadeOf or $isInVicinityOf) 
                                            then
                                                 <geoCoord>{
                                                     for $parent in ($isMadeOf, $isInVicinityOf)
                                                              return
                                                                     
                                                                                 try { spatiumStructor:getRelatedPlacesCoordinates($project, $placeCollection, data($parent/@rdf:resource), 0) } 
                                                                                 catch * {"error in in isMadeOf"}
                                                                               
                                                      }</geoCoord>
                                     else if ($isPartOf) then (
                                               <geoCoord>{
                                                    for $parent in $isPartOf
                                                        return 
                                                           
                                                                try { spatiumStructor:getRelatedPlacesCoordinates($project, $placeCollection, data($parent/@rdf:resource), 0) } 
                                                                catch * {"error in is InVicinityOf" || " - Error ", $err:code, ": ", $err:description}    
                                                               
                                           }</geoCoord>
                                               )
                                         else (<geoCoord><coordinates type="lastChance">0, 0</coordinates></geoCoord>
                                         )
                                  )
(:                                if isInVicinityOf take coordinates:)
                                (:(
                                    if($isInVicinityOf//node())
                                        then (
                                            <geoCoord>{
                                                    for $parent in $isInVicinityOf
                                                        return 
                                                            <coordinates>{ 
                                                                          try { spatiumStructor:getRelatedPlacesCoordinates($project, $placeCollection, data($parent/@rdf:resource), 0) } 
                                                                         catch * {"error in is InVicinityOf" || " - Error ", $err:code, ": ", $err:description}    }
                                                            </coordinates>
                                              }</geoCoord>
                                    ) else()
                                    ):)
(:                                    if no isMadeOf and no IsInVicinity of take coordinates:)
                                (:(
                                if($isPartOf//node())
                                        then (
                                        <geoCoord>{
                                                    for $parent in $isPartOf
                                                        return 
                                                            <coordinates>
                                                                {
                                                                try { spatiumStructor:getRelatedPlacesCoordinates($project, $placeCollection, data($parent/@rdf:resource), 0) } 
                                                                catch * {"error in is InVicinityOf" || " - Error ", $err:code, ": ", $err:description}    
                                                                }
                                                                </coordinates>
                                           }</geoCoord>
                                                )
                                    else(<coordinates>[0, 0]</coordinates>)
                                ):)
            )                  )
                      
                                   
                      
                      
      return
        (
        <features type="Feature">
                                <properties>
                                    <name>{ $placeName }</name>
                                    <altNames>{ $altNames }</altNames>
                                    <uri>{ $uri }</uri>
                                    <id>{ $id }</id>
                                    <placeType>{ if ( $mainPlaceType = "MAN MADE MATERIAL") then "Please check type of place"
                                        else $mainPlaceType }</placeType>
                                    <placeTypeUri>{ if ( $mainPlaceType = "MAN MADE MATERIAL") then "Please check type of place"
                                        else if ($mainPlaceTypeUri = "") then "no-uri" else 
                                        data($mainPlaceTypeUri) }</placeTypeUri>
                                    <provinceUri>{ $provinceUri }</provinceUri>
                                    <provinceName>{ $provinceName }</provinceName>
                                    <productionType>{ if($productionType) then string-join($productionType, ", ") else ()
                                                                        }</productionType>
                                    <exactMatch>{ $exactMatch }</exactMatch>
                                    <icon>{ $marker }</icon>
                                    <amenity></amenity>
                                    <popupContent>{"<h5>"  || $placeName || "</h5>"}
                                   {if($mainPlaceType) then "<div>" || $mainPlaceType || "</div>" else ("no main type")} 
                                   {'<span class="uri">' || $uri || '</span>'}
                                   {if($productionType) then '<div class="margin-top: 5em;"><span>Types of production: </span>' || string-join($productionType, ', ') || '</div>'
                                   else("")}</popupContent>
                                 </properties>
                                 <style>
                                     <fill>red</fill>
                                     <fill-opacity>1</fill-opacity>
                                  </style>
                                  <geometry>
                                     <type>MultiPoint</type>
                                     {if (normalize-space($coordinates) = "") then <coordinates json:array="true" json:literal="false">[0, 0]</coordinates>
                                     else if($coordinates//coordinates[(normalize-space(.) !="") or (not(contains(./text(), "0, 0")))]) then 
                                            let $numberOfCoordinates := count($coordinates//coordinates)
                                            let $numberOfCoordinatesWith00 := count($coordinates//coordinates[./text() ="0, 0"])
                                            return
                                                    if ($numberOfCoordinates >1) then
                                                            for $coord in $coordinates//coordinates[normalize-space(.) !=""][not(contains(./text(), "0, 0"))]
                                                            return (<coordinates json:array="true" json:literal="false">[{ normalize-space($coord)}]</coordinates>, $spatiumStructor:newLine )
                                                   
                                                   else (<coordinates json:array="true" json:literal="false">[{ normalize-space($coordinates//coordinates/text())}]</coordinates>)
                                           else (<coordinates json:array="true" json:literal="false">[2, 2]</coordinates>)
                                        }
                                </geometry>
                           </features>,
                                                                $spatiumStructor:newLine)
                           
      let $endTime := util:system-time()      
      let $duration := $endTime - $startTime
      let $seconds := $duration div xs:dayTimeDuration("PT1S")
return
<places xmlns:json="http://www.json.org" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
    <last-update>{  $now }</last-update>
    <generated-in>{ $seconds }</generated-in>
    <count>{ count($placeCollection//spatial:Feature) }</count>
    <user>{ $currentUser }</user>
        <root json:array="true" type="FeatureCollection">
        { $features }
                   </root>
             </places>
};


(:let $project := "patrimonium":)
(:let $now := fn:current-dateTime():)
let $library-path := "/db/apps/ausohnum-library/"
let $places := collection("/db/apps/" || $local:project || "Data/places" || "/" || $local:project)//rdf:RDF
let $appVariables := doc("/db/apps/" || $local:project || "/data/app-general-parameters.xml")
let $placesGazetteer := doc("/db/apps/" || $local:project || "Data/places/project-places-gazetteer.xml")

let $currentUser := data(sm:id()//sm:username)

let $lastUpdate := xs:dateTime(data(spatiumStructor:getLastUpdatePlace($local:project)[1]/@lastModified))
let $gazetteerDate := xs:dateTime( doc("/db/apps/" || $local:project || "Data/places/project-places-gazetteer.xml")//last-update/text())
let $updateGazetteer := 
            if ($lastUpdate > $gazetteerDate ) then 
        update replace doc("/db/apps/" || $local:project || "Data/places/project-places-gazetteer.xml")//places with spatiumStructor:buildProjectPlacesCatalogue($local:project)
                else (util:log("INFO", "Place Gazetteer: no update because no place record has changed"))
(:return:)

 (:         if ($lastUpdate > $gazetteerDate ) then (
     :)
(:         update replace doc("/db/apps/" || $local:project || "Data/places/project-places-gazetteer.xml")//places with spatiumStructor:buildProjectPlacesCatalogue($local:project),:)
(:         util:log("INFO", "Places gazetteer of project " || $local:project || " updated after a place record had been updated."):)
(:         ):)
(:                else ():)

(:let $newCatalogue := spatiumStructor:buildProjectPlacesCatalogue($local:project):)
(:let $test := update replace doc("/db/apps/" || $local:project || "Data/places/project-places-gazetteer.xml")//places with $newCatalogue:)

return 
    if ($lastUpdate > $gazetteerDate ) then 
       util:log("INFO", "Places gazetteer of project " || $local:project || " updated after a place record was updated.")
       else ()
       
       

 


