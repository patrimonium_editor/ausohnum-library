(:~
: AusoHNum Library - teiEditor module
: This function serves as an interace between a project front-end and XQuery functions related to tei Editor
: @author Vincent Razanajao
: @param type is the paramater of the http request that calls this function
:)


xquery version "3.1";
import module namespace console="http://exist-db.org/xquery/console";

import module namespace teiEditor="http://ausonius.huma-num.fr/teiEditor"
      at "./teiEditorApp.xql";
import module namespace teiEditorEgypt="http://ausonius.huma-num.fr/teiEditorEgypt"
      at "./teiEditorEgypt.xql";

declare variable $type := request:get-parameter('type', ());
declare variable $data := request:get-data()[1];
declare variable $project := request:get-parameter('project', ());
declare variable $username := request:get-parameter('user', ());
declare variable $corpus := request:get-parameter('corpus', ());
declare variable $logdata := console:log( $data//docId);

switch ($type)
   case "dashboard" return teiEditor:dashboard(())
   case "dashboard4corpus" return teiEditor:dashboard($corpus)
   case "loginForm" return teiEditor:loginForm($project, $username)
   case "newUserForm" return teiEditor:newUserForm($project) 
   case "createUser" return teiEditor:createUser($data, $project)
   case "newDocument" return teiEditor:newDocument($data, $project)
   case "newDocumentFromExternalResource" return teiEditor:newDocumentFromExternalResource($data, $project)
   case "newCollection" return teiEditor:newCollection($data, $project)
   case "saveData" return teiEditor:saveData($data, $project)
   case "addData" return teiEditor:addData($data, $project)
   case "addGroupData" return teiEditor:addGroupData($data, $project)
   
   case "addBiblio" return teiEditor:addBiblio($data, $project)
   case "insertBiblioRef" return teiEditor:insertBiblioRef($data, $project)
   case "addProjectPerson" return teiEditor:addProjectPersonToDoc($data, $project)
   case "addRoyalPerson" return teiEditor:addRoyalPersonToDoc($data, $project)
   (: case "addPlace" return teiEditor:addPlaceToDoc($data, $project) :)
   case "addProjectPlace" return teiEditor:addProjectPlaceToDoc($data, $project)

   case "absDatingReloadPanel" return teiEditor:absDatingReloadPanel($data, $project)
   case "absDatingUpdate" return teiEditor:absDatingUpdate($data, $project)

   case "removeItemFromList" return teiEditor:removeItemFromList($data, $project)
   case "removeListItem" return teiEditor:removeListItem($data, $project)
   case "saveText" return teiEditor:saveText($data, $project)
   case "saveTextarea" return teiEditor:saveTextarea($data, $project)
   case "saveFile" return teiEditor:saveFile($data, $project)
   
  case "epiconverter" return teiEditor:epiConverter()

  case "addRepository" return teiEditor:addRepositoryToDocument($data, $project)
   case "addAltRepositoryTomsFrag" return teiEditor:addAltRepositoryTomsFrag($data, $project)
   case "updateRepository" return teiEditor:updateRepository($data, $project)
   case "updateRepositoryOnmsFrag" return teiEditor:updateRepositoryOnmsFrag($data, $project)
   case "getCleanedTEIFile" return teiEditor:getCleanedTEIFile(request:get-parameter('docId', ()), $project)
   case "selectedDocsAction" return teiEditor:selectedDocsAction($data, $project)

   (:MODULE EGYPT:)  
   case "addmsItemToDoc" return teiEditor:addmsItemToDoc($data, $project)
   case "addmsItemWithLocationToDoc" return teiEditor:addmsItemWithLocationToDoc($data, $project)
   case "addmsFragToDoc" return teiEditor:addmsFragToDoc($data, $project)
   case "addProjectPersonToDocWithRole" return teiEditorEgypt:addProjectPersonToDocWithRole($data, $project)
   case "addProjectPlaceToDocument" return teiEditorEgypt:addProjectPlaceToDocument($data, $project)   
   case "addGraphicToSurface" return teiEditorEgypt:addGraphicToSurface($data, $project)
   
   
   case "editor-widgets" return teiEditor:widgets($data, $project)
   case "moveItem" return teiEditor:moveItem($data, $project)
   case "moveBiblioItem" return teiEditor:moveBiblioItem($data, $project)
   
   default return null
