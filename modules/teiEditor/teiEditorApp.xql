(:~
: AusoHNum Library - teiEditor module
: This module contains the main functions of the teiEditor module.
: @author Vincent Razanajao
:)

(: TODO: Many variables declared in header load huge amount of data and should be replaced by variables declared in functions :)

xquery version "3.1";

module namespace teiEditor="http://ausonius.huma-num.fr/teiEditor";

import module namespace config="http://ausonius.huma-num.fr/ausohnum-library/config" at "../config.xqm";

import module namespace dbutil="http://exist-db.org/xquery/dbutil" at "/db/apps/shared-resources/content/dbutils.xql";
import module namespace functx="http://www.functx.com";
(:import module namespace httpclient="http://exist-db.org/xquery/httpclient" at "java:org.exist.xquery.modules.httpclient.HTTPClientModule";:)
import module namespace http="http://expath.org/ns/http-client" at "java:org.expath.exist.HttpClientModule";
import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/" at "../skosThesau/skosThesauApp.xql";
import module namespace templates="http://exist-db.org/xquery/templates" ;
import module namespace xmldb="http://exist-db.org/xquery/xmldb";
import module namespace zoteroPlugin="http://ausonius.huma-num.fr/zoteroPlugin" at "../zoteroPlugin/zoteroPlugin.xql";
(:import module namespace tan="http://alpheios.net/namespaces/text-analysis" at "./cts-3/textanalysis_utils.xquery";:)
(:import module namespace templates="http://exist-db.org/xquery/templates" ;:)
(:import module namespace config="http://patrimonium.huma-num.fr/config" at "../config.xqm";:)

import module namespace console="http://exist-db.org/xquery/console";
declare boundary-space preserve;

declare namespace apc="http://patrimonium.huma-num.fr/onto#";
declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dct = "http://purl.org/dc/terms/";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace err = "http://www.w3.org/2005/xqt-errors";
declare namespace ev = "http://www.w3.org/2001/xml-events";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace lawd="http://lawd.info/ontology/";
declare namespace owl="http://www.w3.org/2002/07/owl#";
declare namespace periodo="http://perio.do/#";
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace rdfs = "http://www.w3.org/2000/01/rdf-schema#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace snap="http://onto.snapdrgn.net/snap#";
declare namespace spatial="http://geovocab.org/spatial#";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace thot = "http://thot.philo.ulg.ac.be/";
declare namespace time="http://www.w3.org/2006/time#";
declare namespace xf = "http://www.w3.org/2002/xforms";
declare namespace local = "local";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
(:declare option output:item-separator "&#xa;";:)

declare variable $teiEditor:library-path := "/db/apps/ausohnum-library/";
declare variable $teiEditor:project :=request:get-parameter('project', ());
declare variable $teiEditor:mode :=request:get-parameter('mode', ());
declare variable $teiEditor:appVariables := doc("/db/apps/" || $teiEditor:project || "/data/app-general-parameters.xml");
declare variable $teiEditor:data := request:get-data();
declare variable $teiEditor:docId :=  request:get-parameter('docid', ());

declare variable $teiEditor:lang :=request:get-parameter('lang', "fr");
declare variable $teiEditor:languages := $teiEditor:appVariables//languages;


(:declare variable $teiEditor:project := "patrimonium";:)
(:declare variable $teiEditor:data-repository := collection("/db/apps/" || $teiEditor:project || "Data");:)
declare variable $teiEditor:data-repository-path := "/db/apps/" || $teiEditor:project || "Data";

declare variable $teiEditor:doc-collection-path := $teiEditor:data-repository-path || "/documents";
declare variable $teiEditor:doc-collection := collection($teiEditor:doc-collection-path);
declare variable $teiEditor:concept-collection-path := "/db/apps/" || $teiEditor:appVariables//thesaurus-app/text() || "Data/concepts";
declare variable $teiEditor:concept-collection := collection( $teiEditor:concept-collection-path);
declare variable $teiEditor:biblioRepo := doc($teiEditor:data-repository-path || "/biblio/biblio.xml");
declare variable $teiEditor:peopleRepo := doc($teiEditor:data-repository-path || "/people/people.xml");
declare variable $teiEditor:peopleCollection := collection($teiEditor:data-repository-path || "/people");
declare variable $teiEditor:placeCollection := collection($teiEditor:data-repository-path || "/places");
declare variable $teiEditor:projectPlaceCollection := collection($teiEditor:data-repository-path || "/places/" || $teiEditor:project);
declare variable $teiEditor:placeRepo := doc($teiEditor:data-repository-path || "/places/listOfPlaces.xml");
declare variable $teiEditor:objectRepositoriesUri := $teiEditor:appVariables//objectRepositoriesUri/text();

declare variable $teiEditor:baseUri := $teiEditor:appVariables//uriBase[@type='app']/text();
declare variable $teiEditor:documentBaseUri := $teiEditor:appVariables//uriBase[@type='document']/text();


declare variable $teiEditor:teiElements := doc($teiEditor:library-path || 'data/teiEditor/teiElements.xml');
declare variable $teiEditor:teiElementsCustom := doc("/db/apps/" || $teiEditor:project || '/data/teiEditor/teiElements.xml');
declare variable $teiEditor:docTemplates := collection($teiEditor:library-path || 'data/teiEditor/docTemplates');
declare variable $teiEditor:teiTemplate := doc($teiEditor:library-path || 'data/teiEditor/teiTemplate.xml');
declare variable $teiEditor:externalResources := doc($teiEditor:library-path || 'data/teiEditor/externalResources.xml');
declare variable $teiEditor:teiDoc := $teiEditor:doc-collection/id($teiEditor:docId) ;
declare variable $teiEditor:docTitle :=  $teiEditor:teiDoc//tei:fileDesc/tei:titleStmt/tei:title/text() ;

declare variable $teiEditor:logs := collection($teiEditor:data-repository-path || '/logs');
declare variable $teiEditor:now := fn:current-dateTime();
declare variable $teiEditor:currentUser := data(sm:id()//sm:username);
declare variable $teiEditor:currentUserUri := concat($teiEditor:baseUri, '/people/' , data(sm:id()//sm:username));
declare variable $teiEditor:zoteroGroup :=request:get-parameter('zoteroGroup', ());

declare variable $teiEditor:nl := "&#10;"; (:New Line:)
declare variable $teiEditor:labels := collection($teiEditor:library-path || 'data/i18n/labels');
declare variable $teiEditor:customLabels := doc("/db/apps/" || $teiEditor:project || '/data/customLabels.xml');

declare variable $teiEditor:functionName:= QName("http://ausonius.huma-num.fr/teiEditor", "teiEditor:repositoryManager");
declare
    %templates:wrap
    function teiEditor:version($node as node(), $model as map(*)){
    data( $config:expath-descriptor//@version)

};
declare
    %templates:wrap
    function teiEditor:variables($docId as xs:string, $project as xs:string){
    <div class="hidden">
        <div id="currentDocId">{ $docId }</div>
        <div id="currentProject">{ $project } </div>
    </div>

};
(:~ HTML user form to be displayed when a non authorized user reached a restricted page :)
declare function teiEditor:newUserForm($project){

 <div data-template="templates:surround" data-template-with="templates/page.html" data-template-at="content">
       <div class="container form">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                <div>
                        <form id="newUserForm" role="form" data-toggle="validator" novalidate="true">
                                      <div class="form-group row">
                                                <label for="newUserFirstName" class="col-sm-2 col-form-label">Firstname</label>
                                                <div class="col-sm-10">
                                                <input type="text" class="form-control" id="newUserFirstName" name="newUserFirstName" required="required" />
                                                </div>
                                      </div>
                                       <div class="form-group row">
                                                <label for="newUserLastName" class="col-sm-2 col-form-label">Lastname</label>
                                                <div class="col-sm-10">
                                                <input type="text" class="form-control" id="newUserLastName" name="newUserLastName" required="required" />
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                                <label for="newUserUsername" class="col-sm-2 col-form-label">Username</label>
                                                <div class="col-sm-10">
                                                <input type="text" class="form-control" id="newUserUsername" name="newUserUsername" required="required"/>
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                                <label for="newUserPassword" class="col-sm-2 col-form-label">Password</label>
                                                <div class="col-sm-10">
                                                <input type="password" class="form-control" placeholder="Password" id="newUserPassword" name="newUserPassword" autocomplete="off" required="required"/>
                                                 <input type="password"  class="form-control" placeholder="Confirm Password" id="confirm_password" autocomplete="off" required="required"/>
                                                  <span id='message'></span>
                                                </div>
                                        </div>
                                </form>
                                    <div class="modal-footer">
                                        <button id="createUser" class="pull-left" onclick="createUser()">Create User</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/teiEditor-dashboard.js"/>
                    </div>
};
(:~ Function that processes data sent by http request (1st paramater) to create an user in the db :)
declare function teiEditor:createUser($data, $project) {
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)

    let $template := collection('/db/apps/ausohnum-library/data/teiEditor/docTemplates')//.[@xml:id=$data//template/text()]
    let $doc-collection := collection($teiEditor:data-repository-path || "/documents/" || $data//collection/text())
    let $doc-collection-path := $teiEditor:data-repository-path || "/documents/" || $data//collection/text()
    let $collectionPrefix := doc($teiEditor:data-repository-path || "/documents/" || $data//collection/text() || ".xml")//docPrefix/text()
(:    let $docIdPrefix := $teiEditor:appVariables//idPrefix[@type='document']/text():)

    let $firstName := $data//newUserFirstName/text()
    let $lastName := $data//newUserLastName/text()
    let $username := $data//newUserUsername/text()
    let $password := $data//newUserPassword/text()
    let $createUser := sm:create-account($username , $password, "sandbox", ())


let $logEvent := teiEditor:logEvent("new-user" , $username, (),
                        "New user " || $username || " has been created in in group sandbox")
    return
    <result>
    <sentData>{ $data }</sentData>

    </result>
};







(:~ This function is the entry point to generate the HTML widget related to an XML node to be displayed in an Editor page.
 : The 1st parameter refers to the XML node definition in the definition file under 1) the AusoHNum library or 2) the project's own file, if exists.
 : This function calls another function according to node type.
:)
declare function teiEditor:displayElement($teiElementNickname as xs:string,
                                          $docId as xs:string?,
                                          $index as xs:int?,
                                          $xpath_root as xs:string?) {
   
   let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[./nm eq $teiElementNickname]))) 
                                        then $teiEditor:teiElements//teiElement[./nm eq $teiElementNickname] 
                                        else $teiEditor:teiElementsCustom//teiElement[./nm eq $teiElementNickname]
(:    let $elementNode := util:eval(doc($teiEditor:library-path || 'data/teiEditor/teiElements.xml')):)


    let $elementIndex := if ($index ) then ("[" || string($index) || "]" ) else ("")
    let $fieldType := $elementNode/fieldType/text()
    let $attributeValueType := $elementNode/attributeValueType/text()
    let $conceptTopId := if($elementNode/thesauDb/text()) then
                        substring-after($elementNode/thesauTopConceptURI, '/concept/')
                        else()
    let $xpathRaw := $elementNode/xpath/text()
    let $xpathEnd := if(contains($xpathRaw, "/@"))
            then( functx:substring-before-last($xpathRaw[1], '/') || $elementIndex || "/" || functx:substring-after-last($xpathRaw[1], '/'))
            else($xpathRaw)
    let $elementAncestors := $elementNode/ancestor::teiElement
    let $teiXPath := if($xpath_root !="")
                    then
                        $xpath_root 
(:                        || $xpathRaw:)
                    else
                     if($elementNode/ancestor::teiElement)
                                then
                                    string-join(
                                    for $ancestor at $pos in $elementAncestors
                                        let $ancestorIndex := if($pos = 1 ) then
                                            if($index) then "[" || string($index ) || "]" else ("")
                                            else ("")
                                    return
                                    if (contains($ancestor/xpath/text(), '/@'))
                                         then
                                             substring-before($ancestor/xpath/text(), '/@')
                                             || $ancestorIndex
                                          else $ancestor/xpath/text() ||
                                        $ancestorIndex
                                    )
                                 || $xpathEnd
                            else
                        $xpathEnd


    let $teiElementDataType := $teiEditor:teiElements//teiElement[.//nm eq $teiElementNickname]/contentType/text()
    let $teiElementFormLabel := $teiEditor:teiElements//teiElement[.//nm eq $teiElementNickname]/formLabel[@xml:lang=$teiEditor:lang]/text()
    let $docID := if($docId != "") then $docId else $teiEditor:docId
(:    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)
    (:let $teiElementValue :=
         (data(
            util:eval( "$teiEditor:doc-collection/id('" ||$docID ||"')/" || $teiXPath)))
    :)        
            
              
 (:   let $teiElementValue :=
         (data(
            util:eval( "$teiEditor:doc-collection/id('" ||$docID ||"')/" || $teiXPath)))
    :)        
(:    let $logEventTest:= teiEditor:logEvent("test-before-display-Element", $docID, <data>{"teiElementNM: " || $teiElementNickname ||
    " ; $conceptTopId: " || (if ($conceptTopId) then $conceptTopId else ())|| " ; docId: " || $docID || " ; index: " || (if ($index ) then $index else ()) || "teiXPath: " || $teiXPath}</data>, "test")
:)
    return

        switch ($fieldType)
        case "input" return
        teiEditor:displayTeiElementCardiMultiLang($teiElementNickname, $docID, $index, 'input', $teiXPath)
        case "directInput" return
        teiEditor:displayTeiElementCardiMultiLang($teiElementNickname, $docID, $index, 'directInput', $teiXPath)
        case "textarea" return
        teiEditor:displayTeiElementCardiMultiLang($teiElementNickname, $docID, $index, 'textarea', $teiXPath)
        case "combobox" return
        teiEditor:displayTeiElementWithThesauCardi($teiElementNickname, $conceptTopId, $docID, $index, $teiXPath)
        case "radioGroup" return
        teiEditor:displayTeiElementWithThesauCardi($teiElementNickname, $conceptTopId, $docID, $index, $teiXPath)
        case "comboboxWithConceptHierarchy" return
        teiEditor:displayXmlElementWithThesauCardiWithConceptHierarchy($teiElementNickname, $conceptTopId, $docID, $index, $teiXPath)
        
        case "comboboxAndInput" return
        teiEditor:displayTeiElementWithThesauComboInputCardi($teiElementNickname, $elementNode/thesauTopConceptURI/text(), $docID, $index, $teiXPath)
        case "place" return
        teiEditor:displayPlace($teiElementNickname, $docID, $index, $teiXPath)
        case "group" return
         teiEditor:displayGroup($teiElementNickname, $docID, $index, (), $teiXPath)
(:        teiEditor:displayTeiElementAndChildren($teiElementNickname, $docID, $index, 'input', $teiXPath):)
        default return
                "Error: Element type '" || $teiElementNickname || "' not found in teiElement definitions."
(:                teiEditor:displayTeiElement($teiElementNickname, $index, 'input', $teiXPath):)
};

(:~ Function similar to teiEditor:displayElement used in group calls :)
declare function teiEditor:displayElementWithDef($elementNode as node(),
                                          $docId as xs:string?,
                                          $index as xs:int?,
                                          $xpath_root as xs:string?) {
   
  (: let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]))) 
                                        then $teiEditor:teiElements//teiElement[nm=$teiElementNickname] 
                                        else $teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]:)
(:    let $elementNode := util:eval(doc($teiEditor:library-path || 'data/teiEditor/teiElements.xml')):)
let $teiElementNickname := $elementNode/nm/text()

    let $elementIndex := if ($index ) then ("[" || string($index) || "]" ) else ("")
    let $fieldType := $elementNode/fieldType/text()
    let $attributeValueType := $elementNode/attributeValueType/text()
    let $conceptTopId := $elementNode/thesauTopConceptURI
                        (: if($elementNode/thesauDb/text()) then
                        substring-after($elementNode/thesauTopConceptURI, '/concept/')
                        else() :)
    let $xpathRaw := $elementNode/xpath/text()
    let $xpathEnd := if(contains($xpathRaw, "/@"))
            then( functx:substring-before-last($xpathRaw[1], '/') || $elementIndex || "/" || functx:substring-after-last($xpathRaw[1], '/'))
            else($xpathRaw)
    let $elementAncestors := $elementNode/ancestor::teiElement
    let $teiXPath := if($xpath_root !="")
                    then
                        $xpath_root 
(:                        || $xpathRaw:)
                    else
                     if($elementNode/ancestor::teiElement)
                                then
                                    string-join(
                                    for $ancestor at $pos in $elementAncestors
                                        let $ancestorIndex := if($pos > 1 ) then
                                            if($index) then "[" || string($index -1) || "]" else ("")
                                            else ("")
                                    return
                                    if (contains($ancestor/xpath/text(), '/@'))
                                         then
                                             substring-before($ancestor/xpath/text(), '/@')
                                             || $ancestorIndex
                                          else $ancestor/xpath/text() ||
                                        $ancestorIndex
                                    )
                                 || $xpathEnd
                            else
                        $xpathEnd


    (: let $teiElementDataType := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]/contentType/text() :)
    (: let $teiElementFormLabel := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]/formLabel[@xml:lang=$teiEditor:lang]/text() :)
    let $docID := if($docId != "") then $docId else $teiEditor:docId
(:    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)
    (:let $teiElementValue :=
         (data(
            util:eval( "$teiEditor:doc-collection/id('" ||$docID ||"')/" || $teiXPath)))
    :)        
            
              
 (:   let $teiElementValue :=
         (data(
            util:eval( "$teiEditor:doc-collection/id('" ||$docID ||"')/" || $teiXPath)))
    :)        
(:    let $logEventTest:= teiEditor:logEvent("test-before-display-Element", $docID, <data>{"teiElementNM: " || $teiElementNickname ||
    " ; $conceptTopId: " || (if ($conceptTopId) then $conceptTopId else ())|| " ; docId: " || $docID || " ; index: " || (if ($index ) then $index else ()) || "teiXPath: " || $teiXPath}</data>, "test")
:)
    return

        switch ($fieldType)
        case "input" return
        teiEditor:displayTeiElementCardiMultiLang($teiElementNickname, $docID, $index, 'input', $teiXPath)
        case "directInput" return
        teiEditor:displayTeiElementCardiMultiLang($teiElementNickname, $docID, $index, 'directInput', $teiXPath)
        case "textarea" return
        teiEditor:displayTeiElementCardiMultiLang($teiElementNickname, $docID, $index, 'textarea', $teiXPath)
        case "combobox" return
        teiEditor:displayTeiElementWithThesauCardi($teiElementNickname, $conceptTopId, $docID, $index, $teiXPath)
        case "radioGroup" return
        teiEditor:displayTeiElementWithThesauCardi($teiElementNickname, $conceptTopId, $docID, $index, $teiXPath)
        case "comboboxWithConceptHierarchy" return
        teiEditor:displayXmlElementWithThesauCardiWithConceptHierarchy($teiElementNickname, $conceptTopId, $docID, $index, $teiXPath)
        case "comboboxAndInput" return
        teiEditor:displayTeiElementWithThesauComboInputCardi($teiElementNickname, $elementNode/thesauTopConceptURI/text(), $docID, $index, $teiXPath)
        case "place" return
        teiEditor:displayPlace($teiElementNickname, $docID, $index, $teiXPath)
        case "group" return
         teiEditor:displayGroup($teiElementNickname, $docID, data($index +1), (), $teiXPath)
(:        teiEditor:displayTeiElementAndChildren($teiElementNickname, $docID, $index, 'input', $teiXPath):)
        default return
                "Error: Element type '" || $teiElementNickname || "' not found in teiElement definitions."
(:                teiEditor:displayTeiElement($teiElementNickname, $index, 'input', $teiXPath):)
};

(:~ Function not USED :)
declare function teiEditor:displayTeiElement($teiElementNickname as xs:string, $index as xs:int?, $type as xs:string?, $xpath_root as xs:string?) {
(:    PLESE NOT USE this function but displayTeiElementCardi:)


    let $elementNode := if (exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname])) then
                        $teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]
                        else ($teiEditor:teiElements//teiElement[nm=$teiElementNickname])
    let $cardinality := $elementNode/cardinality/text()
    let $elementIndex := if($index ) then
                         if($cardinality != "1") then ("[" || string($index) || "]" ) else ("") else ("")
    let $xpathRaw := $elementNode/xpath/text()
    let $xpathEnd := if(contains($xpathRaw, "/@"))
            then( functx:substring-before-last($xpathRaw, '/') || $elementIndex || "/" || functx:substring-after-last($xpathRaw, '/'))
            else($xpathRaw)

    let $elementAncestors := $elementNode/ancestor::teiElement/.
    let $teiXPath := if($elementNode/ancestor::teiElement)
                    then
                        string-join(
                        for $ancestor at $pos in $elementAncestors
                        let $ancestorIndex := if($pos = 1 ) then
                            if($index) then "[" || string($index) || "]" else ("")
                            else ("")
                        return
                        if (contains($ancestor/xpath/text(), '/@'))
                        then
                            substring-before($ancestor/xpath/text(), '/@')
                            || $ancestorIndex
                            else $ancestor/xpath/text() ||
                            $ancestorIndex
                        )
                     || $xpathEnd
                    else
                        $xpathEnd


    let $teiElementDataType := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]/contentType/text()
    let $teiElementFormLabel := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]/formLabel[@xml:lang=$teiEditor:lang]/text()
    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId)
    let $teiElementAttributeValue :=
         (data(util:eval( "$teiEditor:doc-collection/id('" ||$teiEditor:docId ||"')/" || $teiXPath)))
    let $teiElementTextNodeValue := if($teiElementDataType = "textNodeAndAttribute" ) then
         (data(util:eval( "$teiEditor:doc-collection/id('" ||$teiEditor:docId ||"')/" || substring-before($teiXPath, '/@'))))
                                    else(data(util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" ||$teiEditor:docId ||"')/" || $teiXPath)))
    return

        <div class="teiElementGroup">
        <div id="{$teiElementNickname}_display_{$index}_1" class="teiElementDisplay">
        <div class="TeiElementGroupHeaderInline">
        <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
            <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
            </span></span>
            </div>
            <div id="{$teiElementNickname}_value_{$index}_1" class="teiElementValue" style="{if($type= "textarea") then "width: 100%;" else ()}">
            {switch ($teiElementDataType)
                    case "text" return $teiElementTextNodeValue
                    case "textNodeAndAttribute" return
                        (<span>{$teiElementTextNodeValue}
                        <a href="{$teiElementAttributeValue}" target="_blank" class="urlInTeiElement">{$teiElementAttributeValue}</a></span>)
                    default return "Error! check type of field"
            }</div>
            <button id="edit{$teiElementNickname}_{$index}_1" class="btn btn-primary editbutton pull-right"
             appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                              editConceptIcon"></i></button>
        </div>

        <div id="{$teiElementNickname}_edit_{$index}_1" class="teiElementHidden teiElementEdit form-group">
        <div class="input-group" >
        <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
            <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
            </span></span>po

                {switch ($type)
                 case "input" return
                    switch ($teiElementDataType)
                    case "text" return
                    <input id="{$teiElementNickname}_{$index}_1" class="form-control" name="{$teiElementNickname}_{$index}_1" value="{$teiElementAttributeValue}"></input>
                    case "textNodeAndAttribute" return
                    <div>
                    <span>Value of <em>Attribute</em> {functx:substring-after-last($teiXPath, '/')}</span>
                    <input id="{$teiElementNickname}_text_{$index}_1" class="form-control" name="{$teiElementNickname}_text_{$index}_1"
                     placeholder="Value of Attribute{functx:substring-after-last($teiXPath, '/')}"
                    value="{ $teiElementAttributeValue }"></input>
                    <span> Value of <em>Node Text</em></span><input id="{$teiElementNickname}_attrib_{$index}_1" class="form-control" name="{$teiElementNickname}_attrib_{$index}_1" value="{ $teiElementTextNodeValue }" placeholder="Value of Text node"></input>
                    </div>
                    default return "Error! Check data type - l. 351"

                 case "textarea" return
                 <textarea id="{$teiElementNickname}_{$index}_1" class="form-control summernote" name="{$teiElementNickname}_{$index}_1">{ $teiElementTextNodeValue }</textarea>
                 default return null
                 }
                <button id="{$teiElementNickname}SaveButton" class="btn btn-success"
                onclick="saveData('{$teiEditor:docId}', '{$teiElementNickname}', '{$teiXPath}',
                    '{$teiElementDataType}', '{$index}', {$cardinality})"
                        appearance="minimal" type="button"><i class="glyphicon
glyphicon glyphicon-ok-circle"></i></button>
                <button id="{$teiElementNickname}CancelEdit" class="btn btn-danger"
                onclick="cancelEdit(this, '{$teiElementNickname}', '{$index}', '{$teiElementTextNodeValue}', 'input', '1') "
                        appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>

        </div></div>
        </div>
};

(:~ Function to generate the HTML widget for a XML node edited with an input, with possible mutiple cardinality :)
declare function teiEditor:displayTeiElementCardi($teiElementNickname as xs:string,
             $docId as xs:string?,
             $index as xs:integer?,
             $type as xs:string?,
             $xpath_root as xs:string?) {


let $currentDocId := if($docId != "") then $docId else  $teiEditor:docId
        let $indexNo := if($index) then data($index) else "1"
        let $elementNode := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]
        let $elementIndex := if($elementNode/ancestor::teiElement)
                    then ""
                    else if
                        ($index) then ("[" || string($index) || "]" ) else ("")

        let $xpathEnd := if(contains($elementNode//xpath[1]/text(), "/@"))
            then(functx:substring-before-last($elementNode//xpath/text(), '/') || $elementIndex || "/"
            || functx:substring-after-last($elementNode//xpath/text(), '/')
            )
            else (
            $elementNode/./xpath/text()
            )
       let $elementAncestors := $elementNode/ancestor::teiElement
    let $teiXPath :=
                if($xpath_root)
                    then
                        $xpath_root
                                else if($elementNode/ancestor::teiElement)
                                then
                                    string-join(
                                    for $ancestor at $pos in $elementAncestors
                                    let $ancestorIndex := if($pos = 1 ) then
                                        if($index) then "[" || string($index) || "]" else ("")
                                        else ("")
                                    return
                                    if (contains($ancestor/xpath/text(), '/@'))
                                    then
                                        substring-before($ancestor/xpath/text(), '/@')
                                        || $ancestorIndex
                                        else $ancestor/xpath/text() ||
                                        $ancestorIndex
                                    )
                                 || $xpathEnd
                                else
                                    $xpathEnd




     let $xpathBaseForCardinalityX :=
            if (contains($teiXPath, "/@")) then
            (functx:substring-before-last(functx:substring-before-last($teiXPath, "/@"), '/'))
            else
                ($teiXPath)

     let $selectorForCardinalityX :=
            if (contains($teiXPath, "/@")) then
            (functx:substring-after-last(functx:substring-before-last($teiXPath, "/@"), "/"))
            else
                (functx:substring-after-last($teiXPath, "/"))

    let $contentType :=$elementNode/contentType/text()
    let $teiElementDataType := $elementNode/contentType/text()
    let $teiElementFormLabel := $elementNode/formLabel[@xml:lang=$teiEditor:lang]/text()
    let $teiElementCardinality := $elementNode/cardinality/text()
    let $attributeValueType := $elementNode/attributeValueType/text()

(:    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)

    let $teiElementValue :=
        if($teiElementCardinality = "1" ) then (
         util:eval( "$teiEditor:doc-collection/id('"||$currentDocId ||"')/" || $teiXPath ))
         else if($teiElementCardinality = "x" ) then (
         util:eval( "$teiEditor:doc-collection/id('"|| $currentDocId ||"')/"
         || $xpathBaseForCardinalityX || "/" || $selectorForCardinalityX ))
         else(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"|| $currentDocId ||"')/" || $teiXPath ))
    let $valuesTotal := count($teiElementValue)
    (:let $data2display :=
    if($teiElementCardinality = "1" ) then ( "e"||
        data(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')//.[@rdf:about='" || $teiElementValue || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']"))
        ) else():)
    let $inputName := 'selectDropDown' (:||$topConceptId:)

    (:let $itemList :=
        util:eval( "collection('/db/apps/" || $teiEditor:project || "/data/documents')//id('"||$teiEditor:docId
                    ||"')/"
                    || functx:substring-before-last($teiXPath2Ref, '/') || "//tei:category"):)
    return

        (
        if($teiElementCardinality ="1") then

        (
                let $teiElementAttributeValue :=
                  (data(util:eval( "$teiEditor:doc-collection/id('" ||$teiEditor:docId ||"')/" || $teiXPath)))
             let $teiElementTextNodeValue :=
                     if($teiElementDataType = "textNodeAndAttribute" ) then
                    (data(util:eval( "$teiEditor:doc-collection/id('" ||$teiEditor:docId ||"')/" || substring-before($teiXPath, '/@'))))
(:                  ANCIENNE VERSION"là" || (serialize(functx:change-element-ns-deep(util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" ||$teiEditor:docId ||"')/" || substring-before($teiXPath, '/@')), "", ""))):)
                   else
                  (serialize(functx:change-element-ns-deep(util:eval( "$teiEditor:doc-collection/id('" ||$teiEditor:docId ||"')/" || $teiXPath || "/node()"), "", "")))
             let $formattedCurrentValue := $teiElementTextNodeValue => replace("'", "’")
             return

                 <div class="teiElementGroup">
                 <div id="{$teiElementNickname}_display_{$index}_1" class="teiElementDisplay">
                 <div class="TeiElementGroupHeaderInline">
                 <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                     <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                     </span></span>
                     </div>
                     { if($valuesTotal = 0) then <span>This document has no {$xpathEnd}</span>
                        else ()}
                     <div id="{$teiElementNickname}_value_{$index}_1" class="teiElementValue"  style="{if($type= "textarea") then "width: 100%;" else ()}">
                     {switch ($teiElementDataType)
                             case "text" return $teiElementTextNodeValue
                             case "attribute" return $teiElementAttributeValue
                             case "textNodeAndAttribute" return
                                 (<span>{$teiElementTextNodeValue}
                                 <a href="{$teiElementTextNodeValue}" target="_blank" class="urlInTeiElement">{$teiElementAttributeValue}</a></span>)
                             default return "Error; check type of field l466"
                     }</div>
                     <button id="edit{$teiElementNickname}_{$index}_1" class="btn btn-primary editbutton pull-right"
                             appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                                       editConceptIcon"></i></button>
                 </div>

                 <div id="{$teiElementNickname}_edit_{$index}_1" class="teiElementHidden teiElementEdit form-group">
                 <div class="input-group" >
                 <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                     <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                     </span></span>

                         {switch ($type)
                          case "input" return
                             switch ($teiElementDataType)
                             case "text" return
                             <input id="{$teiElementNickname}_{$index}_1" class="form-control" name="{$teiElementNickname}_{$index}_1" value="{$teiElementAttributeValue}"></input>
                             case "textNodeAndAttribute" return
                             <div class="input-group">
                             <div class="input-group-prepend">
                                <span>Value of <em>Attribute</em> {functx:substring-after-last($teiXPath, '/')}</span>
                             </div>
                             <input id="{$teiElementNickname}_text_{$index}_1" class="form-control" name="{$teiElementNickname}_text_{$index}_1" value="{ $teiElementAttributeValue }"></input>
                             <span>Value of <em>Node Text</em></span><input id="{$teiElementNickname}_attrib_{$index}_1" class="form-control" name="{$teiElementNickname}_attrib_{$index}_1" value="{ $teiElementTextNodeValue }"></input>
                             </div>
                             default return "Error! Check data type - l. 515"

                          case "textarea" return
                          <textarea id="{$teiElementNickname}_{$index}_1" class="form-control summernote" name="{$teiElementNickname}_{$index}_1">{$teiElementTextNodeValue}</textarea>
                          default return null
                          }
                         <button id="{$teiElementNickname}SaveButton" class="btn btn-success savedatabutton"
                         data-docId="{$teiEditor:docId}" data-elementNickname="{ $teiElementNickname }" data-xpath="{ $teiXPath }"
                            data-elementDataType="{ $teiElementDataType }" data-index="{ $index }" data-elementCardi="{ $teiElementCardinality }"
                                 appearance="minimal" type="button"><i class="glyphicon
         glyphicon glyphicon-ok-circle"></i>SAVE</button>
                         <button id="{$teiElementNickname}CancelEdit" class="btn btn-danger"
                         onclick="javascript:cancelEdit(this, '{$teiElementNickname}', '{$index}', '{ $formattedCurrentValue }', 'input', '1') "
                                 appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>

                 </div></div>
                 </div>
        )
        else
        <div id="{$teiElementNickname}_group_{$indexNo}" class="teiElementGroup">
        <div class="TeiElementGroupHeaderBlock">
            <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                    <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                    </span></span>
                    { if($teiElementCardinality ="x") then
                    <button id="{$teiElementNickname}addItem_{$indexNo}" class="btn btn-primary addItem"
                        onclick="addItem(this, '{ $inputName }_add_{ $indexNo }', '{ $indexNo }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-plus"></i></button>



                  else()
                     }
              </div>
              {
              for $item at $pos in $teiElementValue

                let $teiElementAttributeValue :=
                                                (util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" ||$teiEditor:docId ||"')/"
                                                || substring-before($teiXPath, '/@') || "[" || $pos || "]/" || functx:substring-after-last($teiXPath, '/')  ))
                let $teiElementTextNodeValue := if($teiElementDataType = "textNodeAndAttribute") then
                                        (util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" ||$teiEditor:docId ||"')/" || substring-before($teiXPath, '/@') || "[" || $pos || "]" ))
                                        else()

              return
              (
              <div class="teiElementGroup">
                    <div id="{$teiElementNickname}_display_{$index}_{$pos}" class="teiElementDisplay">
                    <div class="TeiElementGroupHeaderInline">
                    <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                        <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                        </span></span>
                        </div>
                        <div id="{$teiElementNickname}_value_{$index}_{$pos}" class="teiElementValue">{ $teiElementTextNodeValue } <a href="{$teiElementAttributeValue}" target="_blank" class="urlInTeiElement">{$teiElementAttributeValue}</a></div>
                        <button id="edit{$teiElementNickname}_{$index}_{$pos}" class="btn btn-primary editbutton pull-right"
                          appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                                          editConceptIcon"></i></button>
                    </div>

        <div id="{$teiElementNickname}_edit_{$index}_{$pos}" class="teiElementHidden teiElementEdit form-group">
        <div class="input-group" >
        <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
            <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
            </span></span>

                {switch ($type)
                 case "input" return
                    switch ($teiElementDataType)
                    case "text" return

                    <input id="{$teiElementNickname}_{$index}_{$pos}" class="form-control" name="{$teiElementNickname}_{$index}_{$pos}" value="{$teiElementAttributeValue}"></input>

                    case "textNodeAndAttribute" return

                            <div>
                            <div class="input-group">
                            <span class="input-group-prepend" id="{$teiElementNickname}_text_{$index}_{$pos}_addon">Text</span>

                            <input id="{$teiElementNickname}_text_{$index}_{$pos}" class="form-control" name="{$teiElementNickname}_text_{$index}_{$pos}" value="{ $teiElementTextNodeValue  }" aria-describedby="{$teiElementNickname}_text_{$index}_{$pos}_addon"></input>
                            </div>

                            <div class="input-group">
                            <span class="input-group-prepend" id="{$teiElementNickname}_attrib_{$index}_{$pos}_addon">{functx:substring-after-last($teiXPath, '/')}</span>
                                <input id="{$teiElementNickname}_attrib_{$index}_{$pos}" class="form-control" name="{$teiElementNickname}_attrib_{$index}_{$pos}" value="{ $teiElementAttributeValue }" aria-describedby="{$teiElementNickname}_attrib_{$index}_{$pos}_addon"></input>
                            </div>
                            </div>

                    default return "Error! Check data type  l. 603"

                 case "textarea" return
                 <textarea id="{$teiElementNickname}_{$index}_1" class="form-control" name="{$teiElementNickname}_{$index}_1">{$teiElementAttributeValue}</textarea>
                 default return null
                 }
                <button id="{$teiElementNickname}SaveButton" class="btn btn-success"
                data-docId="{$teiEditor:docId}" data-elementNickname="{ $teiElementNickname }" data-xpath="{ $teiXPath }"
                            data-elementDataType="{ $teiElementDataType }" data-index="{ $index }" data-elementCardi="{ $teiElementCardinality }"
                                 appearance="minimal" type="button">SAVE<i class="glyphicon
glyphicon glyphicon-ok-circle"></i></button>
                <button id="{$teiElementNickname}CancelEdit" class="btn btn-danger"
                onclick="cancelEdit(this, '{$teiElementNickname}', '{$index}', '{$teiElementValue}', 'input', '{$pos}') "
                        appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>

        </div></div>
        </div>
                      )}


                <div id="{$inputName}_add_{$indexNo}" class="teiElement teiElementAddItem teiElementHidden">

                    { switch ($teiElementDataType)
                    case "text" return
                    <input id="{$teiElementNickname}_text_{$index}_1" class="form-control" name="{$teiElementNickname}_{$index}_1" value=""></input>
                    case "textNodeAndAttribute" return
                    <div>
                    <div class="input-group">
                    <div class="input-group-prepend">
                          <span>Value of <em>Attribute</em>{ functx:substring-after-last($teiXPath, '/') }</span>
                          </div>
                          <input id="{$teiElementNickname}_add_attrib_{$indexNo}_1" class="form-control" name="{$teiElementNickname}_text_{$index}_1" value=""></input>
                    </div>
                    <span>Value of <em>Node Text</em></span><input id="{$teiElementNickname}_add_text_{$indexNo}_1" class="form-control" name="{$teiElementNickname}_add_text_{$index}_1" value=""></input>
                    </div>
                    default return "Error! Check data type - l. 638" }


                        <button id="addNewItem" class="btn btn-success" onclick="addData(this, '{$currentDocId}', '{$inputName}_add_{$indexNo}', '{$teiElementNickname}', '{$teiXPath}', '{$contentType}', '{$indexNo}', '')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                <button id="{$teiElementNickname}CancelEdit_{$indexNo}_add" class="btn btn-danger"
                        onclick="cancelEdit(this, '{$inputName}_add_{$indexNo}', '{$indexNo}', '{$teiElementValue}', 'thesau', {$valuesTotal +1}) "
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                </div>
            </div>
        )
};

(:~ Function to generate the HTML widget for a XML node edited with an input. Deals with mutiple cardinality implying an attribute @xml:lang :)
declare function teiEditor:displayTeiElementCardiMultiLang($teiElementNickname as xs:string,
                                                                                             $docId as xs:string?,
                                                                                             $index as xs:integer?,
                                                                                             $type as xs:string?,
                                                                                             $xpath_root as xs:string?) {


        let $currentDocId := if($docId != "") then $docId
                            else if ( $docId ="getFunctions.xql") then "eee"
                            else  $teiEditor:docId

        let $doc := $teiEditor:doc-collection/id($docId)    
    

        let $indexNo := if($index) then data($index) else "1"
        let $index := if($index) then data($index) else "1"
        let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]))) then
                        $teiEditor:teiElements//teiElement[nm=$teiElementNickname]
                        else ($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname])
        let $elementIndex := if($elementNode/ancestor::teiElement)
                    then ""
                    else if
                        ($index) then ("[" || string($index) || "]" ) else ("")

        let $xpathEnd := if(contains($elementNode//xpath[1]/text(), "/@"))
            then(functx:substring-before-last($elementNode//xpath/text(), '/') || $elementIndex || "/"
            || functx:substring-after-last($elementNode//xpath/text(), '/')
            )
            else (
            $elementNode//xpath[1]/text()
            )
       let $elementAncestors := $elementNode/ancestor::teiElement
       let $teiXPath :=
                if($xpath_root !="")
                    then $xpath_root
                        else if($elementNode/ancestor::teiElement)
                            then
                                (
                                string-join(
                                for $ancestor at $pos in $elementAncestors
                                    let $ancestorIndex := if($pos = count($elementAncestors) ) then
                                        if(exists($index)) then "[" || string($index) || "]" else ("")
                                            else ("")
                                    return
                                        if (contains($ancestor/xpath/text(), '/@'))
                                            then substring-before($ancestor/xpath/text(), '/@')
                                                    || $ancestorIndex
                                            else $ancestor/xpath/text() || $ancestorIndex)
                                    || $xpathEnd
                                    )
                            else
                                    $xpathEnd

     let $xpathBaseForCardinalityX :=
            if (contains($teiXPath, "/@")) then
            (functx:substring-before-last(functx:substring-before-last($teiXPath, "/@"), '/'))
            else
                ($teiXPath)

     let $selectorForCardinalityX :=
            if (contains($teiXPath, "/@")) then
            (functx:substring-after-last(functx:substring-before-last($teiXPath, "/@"), "/"))
            else
                (
                functx:substring-after-last($teiXPath, "/")
                )

    let $contentType :=$elementNode/contentType/text()
    let $teiElementDataType := $elementNode/contentType/text()
    let $teiElementFormLabel := teiEditor:labelForXMLElement($elementNode)
    (: $elementNode/formLabel[@xml:lang=$teiEditor:lang]/text() :)
    let $teiElementCardinality := $elementNode/cardinality/text()
    let $attributeValueType := $elementNode/attributeValueType/text()

(:    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)

    let $teiElementValue :=
        if($teiElementCardinality = "1" )
            then (
                    util:eval( "$doc/" || $teiXPath ))

            else if($teiElementCardinality = "x" ) then (

                    util:eval( "$doc/" || $teiXPath )
                    (:util:eval( "$teiEditor:doc-collection/id('"|| $currentDocId ||"')"
                    || $xpathBaseForCardinalityX
                    || "/" || $selectorForCardinalityX
                    ):)
                    )
            else
                (<test>test</test>
(:         util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"|| $currentDocId ||"')/" || $teiXPath ):)
                )
(: let $unit :=    if(util:eval( "$teiEditor:doc-collection/id('"||$currentDocId ||"')/" || $teiXPath || "/@unit" ))
                                then (" " ||
                                    util:eval( "$teiEditor:doc-collection/id('"||$currentDocId ||"')/" || $teiXPath || "/@unit" ))
           else ()
:)
 let $unit :=    try {
                           " " || util:eval( "$doc/" || $teiXPath || "/@unit" )
                            }
                      catch * { "" }

let $valuesTotal := count($teiElementValue)
    (:let $data2display :=
    if($teiElementCardinality = "1" ) then ( "e"||
        data(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')//.[@rdf:about='" || $teiElementValue || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']"))
        ) else():)
let $inputName := 'selectDropDown' (:||$topConceptId:)

    (:let $itemList :=
        util:eval( "collection('/db/apps/" || $teiEditor:project || "/data/documents')//id('"||$teiEditor:docId
                    ||"')/"
                    || functx:substring-before-last($teiXPath2Ref, '/') || "//tei:category"):)
    return

        (
        if($teiElementCardinality ="1") then
            (let $teiElementAttributeValue :=
                  (util:eval( "$doc/" || $teiXPath))

             let $teiElementTextNodeValue :=
                switch($teiElementDataType)
                    case "textNodeAndAttribute" return
                                    util:eval( "$doc/" || substring-before($teiXPath, '/@')) || $unit 
    (:                  ANCIENNE VERSION"là" || (serialize(functx:change-element-ns-deep(util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" ||$teiEditor:docId ||"')/" || substring-before($teiXPath, '/@')), "", ""))):)
                    case "enrichedText" return
                    (:serialize( :)
                    functx:change-element-ns-deep(
                                    util:eval( "$doc/" || $teiXPath), "", "")/node()
(:                                    ) :)
                    default return 

                        if(contains($teiXPath, "text()")) then 
                        serialize(
                            functx:change-element-ns-deep(
                                util:eval( "collection('" || $teiEditor:doc-collection-path || "')/id('" || $currentDocId ||"')/" || $teiXPath)
                                , "", ""))
                        else
                        serialize(
                            functx:change-element-ns-deep(
                                util:eval( "collection('" || $teiEditor:doc-collection-path || "')/id('" || $currentDocId ||"')/" || $teiXPath || "/node()")
                                , "", "")) 
                          
                    
             return

                 <div class="teiElementGroup">
                 <div id="{$teiElementNickname}_display_{$index}_1" class="teiElementDisplay">
                 <div class="{switch($teiElementDataType)
                                case 'enrichedText' return 'TeiElementGroupHeaderBlock'
                                default return 'TeiElementGroupHeaderInline'}">
                 <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                     <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                     </span></span>
                     </div>
                     <div id="{$teiElementNickname}_value_{$index}_1" class="teiElementValue"  style="{if($type= "textarea") then "width: 100%;" else ()}">
                     {switch ($teiElementDataType)
                             case "text" return <span>{$teiElementTextNodeValue}
                             {if(starts-with($teiElementTextNodeValue, "http")) then <a href="{$teiElementTextNodeValue}" target="_blank" class="urlInTeiElement"><i class="glyphicon glyphicon-new-window"/></a>else()}
                                </span>
                             case "attribute" return
                                if($type = "directInput") then 
                                <span>
                                    <input id="{$teiElementNickname}_{$index}_1" class="form-control input4attribute"
                                    name="{$teiElementNickname}_{$index}_1"
                                     value="{ data($teiElementAttributeValue) }"
                                     formervalue=""></input>
                                    <button id="{$teiElementNickname}_{$index}_1_cancel" class="hidden"><i class="glyphicon glyphicon-remove-sign"/></button>
                                    <span id="{$teiElementNickname}_{$index}_1_message"/>
                                    <script>
                                    $( "#{$teiElementNickname}_{$index}_1" ).on( "focus", function() {{
                                        $(this).attr("formervalue", $(this).val());
                                        
                                    }});
                                       $( "#{$teiElementNickname}_{$index}_1" ).on( "focus", function() {{
                                        $(this).attr("formervalue", $(this).val());
                                        
                                    }});
                                        $( "#{$teiElementNickname}_{$index}_1" ).on( "blur", function() {{
                                            $("#{$teiElementNickname}_{$index}_1_cancel").toggleClass("hidden");
                                        //$(this).css()
                                        $("#{$teiElementNickname}_{$index }_1_message").css("display", "block");
                                        $("#{$teiElementNickname}_{$index }_1_message").html("Saving data...");
                                        
                                        saveDirectInput('{$currentDocId}', '{$teiElementNickname}_{$index}_1',
                                                '{$teiElementNickname}', '{$teiXPath}',
                                                    '{$teiElementDataType}', '{$index}')
                                        
                                        $("#{$teiElementNickname}_{$index }_1_message").css("display", "block");
                                        $("#{$teiElementNickname}_{$index }_1_message").html("Data has been saved...");
                                        $("#{$teiElementNickname}_{$index }_1_message").css('background-color', '#a8fa78');
                                        $("#{$teiElementNickname}_{$index }_1_message").fadeOut(3000);
                                        
                                        
                                        
                                        }} );
                                        $("#{$teiElementNickname}_{$index}_1_cancel").on( "click", function() {{
                                            $( "#{$teiElementNickname}_{$index}_1" ).val(
                                                $( "#{$teiElementNickname}_{$index}_1" ).attr("formervalue"));
                                                $(this).toggleClass("hidden")
                                        }});
                                    </script>
                                </span>
                                else
                                <span>{data($teiElementAttributeValue)}
                                {if(starts-with($teiElementAttributeValue, "http")) then <a href="{$teiElementTextNodeValue}" target="_blank" class="urlInTeiElement"><i class="glyphicon glyphicon-new-window"/></a>else()}
                                </span>
                             case "textNodeAndAttribute" return

                                 (<span>{$teiElementTextNodeValue}
                                 <a href="{$teiElementTextNodeValue}" target="_blank" class="urlInTeiElement">{$teiElementAttributeValue}</a></span>)
                             case "nodes" return $teiElementTextNodeValue
                            
                             case "enrichedText" return 
                                (
                                   <div>
                                        <!--<textarea id="{$teiElementNickname}_{$index}_1" class="form-control summernote" name="{$teiElementNickname}_{$index}_1">{ $teiElementTextNodeValue }</textarea>-->
                                        <textarea id="{$teiElementNickname}_{$index}_1" class="form-control" name="{$teiElementNickname}_{$index}_1">{ $teiElementTextNodeValue }</textarea>
                                            <span id="{$teiElementNickname}_{$index}_1_message"/>
                                            <script>
                                            $('#{$teiElementNickname}_{$index}_1').trumbowyg({{
                                            btns: [
                                                ['viewHTML'],
                                                ['strong', 'em'],
                                                ['undo', 'redo'], // Only supported in Blink browsers
                                                ['link']//,
                                                //['fullscreen'] 
                                                    ],
                                            btnsDef: {{
                                                    teiItalicBis: {{
                                                        fn: function (trumbowyg) {{
                                                            console.log("in tei italic");
                                                            trumbowyg.saveRange();
                                                            var text = trumbowyg.getRangeText();
                                                            trumbowyg.execCmd('insertHTML', '<hi rend="italic">' + strip(text) + '</hi>');

                                                        }},
                                                        tag: 'hi rend="italic"',
                                                        title: 'TEI Italic',
                                                        text: 'Italic',
                                                        isSupported: function () {{ return true; }},
                                                        key: 'K',
                                                        param: '' ,
                                                        forceCSS: false,
                                                        class: '',
                                                        hasIcon: true
                                                        }}
                                                    }},

                                            semantic: {{}},
                                            tagsToKeep: ['condition', 'script[src]'],
                                            tagsToRemove: [''],
                                            autogrow: true,
                                            disabled: true,
                                            removeformatPasted: true
                                        }})
                                        .on('tbwblur', function(){{ 
                                            $("#{$teiElementNickname}_{$indexNo}_1_message").css("display", "block");
                                                                $("#{$teiElementNickname}_{$indexNo}_1_message").html("Saving text...");
                                                                $("#{$teiElementNickname}_{$indexNo}_1_message").css('background-color', '#e6f4ff');
                                                                saveTextarea('{$currentDocId}', '{$teiElementNickname}_{$index}_1', 
                                                                '{$teiElementNickname}', '{replace($teiXPath, "'", "'")}', '{$index}')
                                         }});
                                         $('#{$teiElementNickname}_{$index}_1').closest(".trumbowyg-box").css("min-height", "150px");
                                         $('#{$teiElementNickname}_{$index}_1').closest(".trumbowyg-box").css("max-height", "200px");
                                        $('#{$teiElementNickname}_{$index}_1').prev(".trumbowyg-editor").css("min-height", "150px");
                                        $('#{$teiElementNickname}_{$index}_1').prev(".trumbowyg-editor").css("max-height", "200px");
                                        $('#{$teiElementNickname}_{$index}_1').css("min-height", "500px"); 
                                                           // markupStr = '';
                                                         $('#{$teiElementNickname}_{$index}_1s').summernote(
                                                               //'pasteHTML', markupStr,
                                                         {{
                                                         toolbar: [
                                                           //['style', ['style']],
                                                           ['font', ['italic','bold','underline','clear']],
                                                           //['para', ['ul','ol','paragraph']],
                                                           ['insert', ['link']],
                                                           ['view', ['fullscreen','codeview','help']],
                                                             ],
                                                       
                                                            callbacks: {{
                                                                onChange: function(){{
                                                                    $("#{$teiElementNickname}_{$indexNo}_1_message").css("display", "block");
                                                                        $("#{$teiElementNickname}_{$indexNo}_1_message").html("Text modified and not saved...");
                                                                        $("#{$teiElementNickname}_{$indexNo}_1_message").css('background-color', '#ffaa99');
                                                                    }},
                                                              onBlur: function(contents, $editable) {{
                                                                
                                                                $("#{$teiElementNickname}_{$indexNo}_1_message").css("display", "block");
                                                                $("#{$teiElementNickname}_{$indexNo}_1_message").html("Saving text...");
                                                                $("#{$teiElementNickname}_{$indexNo}_1_message").css('background-color', '#e6f4ff');
                                                                saveTextarea('{$currentDocId}', '{$teiElementNickname}_{$index}_1', 
                                                                '{$teiElementNickname}', '{replace($teiXPath, "'", "'")}', '{$index}')
                                                                }}
                                                                }}
                                                                }}
                                                                );
                                               </script>
                                           </div>
                            )

                            case "enrichedTextTEI" return 
                            (: enrichedTextTei uses TEI element /hi with @rend="italic" for italic;
                            ~ Not fully developped :)
                            ( <div>
                                    <textarea id="{$teiElementNickname}_{$index}_1" class="form-control summernote" name="{$teiElementNickname}_{$index}_1">{ $teiElementTextNodeValue }</textarea>
                                    <span id="{$teiElementNickname}_{$index}_1_message"/>
<div id="dialogInsertBiblio{$teiElementNickname}_{$index}_1" title="Add a Bibliographical Reference" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Insert a bibliographical reference</h4>
                </div>
                <div class="modal-body">
                      <form id="{$teiElementNickname}_{ $index }_1_addBiblioForm" role="form" data-toggle="validator" novalidate="true">
                            <div class="form-group">
                                <label for="{$teiElementNickname}_{ $index }_1_nameLookupInputModal">Search in <a href="https://www.zotero.org/groups/{$teiEditor:appVariables//zoteroGroup/text()}" target="_blank">Zotero Group {$teiEditor:appVariables//zoteroGroup/text()}</a>
                                </label>
                                <input type="text" class="form-control zoteroLookup" id="{$teiElementNickname}_{ $index }_1_zoteroLookupInputModal" name="{$teiElementNickname}_{ $index }_1_zoteroLookupInputModal" placeholder="Start to enter a author name or a term..."/>
                            </div>
                            <div class="form-group">
                                <label for="{$teiElementNickname}_{$index}_1_citedRange">Cited Range
                                </label>
                                <input type="text" class="form-control" id="{$teiElementNickname}_{$index}_1_citedRange" name="{$teiElementNickname}_{$index}_citedRange"/>
                            </div>
                            <div id="{$teiElementNickname}_{$index}_1_zoteroGroupNo" class="hidden zoteroGroupNo">{$teiEditor:appVariables//zoteroGroup/text()}</div>
                            <div id="{$teiElementNickname}_{$index}_1_selectedBiblioAuthor" class="lookupSelectionPreview selectedBiblioAuthor"/>
                            <div id="{$teiElementNickname}_{$index}_1_selectedBiblioDate" class="lookupSelectionPreview selectedBiblioDate"/>
                            <div id="{$teiElementNickname}_{$index}_1_selectedBiblioTitle" class="lookupSelectionPreview selectedBiblioTitle"/>
                            <div id="{$teiElementNickname}_{$index}_1_selectedBiblioUri" class="lookupSelectionPreview selectedBiblioUri"/>
                           Id: <div id="{$teiElementNickname}_{$index}_1_selectedBiblioId" class="lookupSelectionPreview selectedBiblioId"/>ll
                            temp:<div id="{$teiElementNickname}_{$index}_1_cursorPosition" class=""/>sd

                    <div class="form-group modal-footer">


                        <button  class="pull-left" type="submit" onclick="insertBiblioRef('{$teiEditor:docId}', '{$teiEditor:appVariables//zoteroGroup/text()}', '{$teiElementNickname}_{$index}_1')">Add reference</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                  </form>
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>

            </div>


    </div>


                                    <script type="text/javascript" src="$ausohnum-lib/resources/scripts/summerNoteTEI.js"/>

                                    <script>
                                            
                                                           // markupStr = '';
                                                    $('#{$teiElementNickname}_{$index}_1').summernote(
                                                          //'pasteHTML', markupStr,
                                                        
                                                        
                                                        {{
                                                         toolbar: [
                                                           //['style', ['style']],
                                                          //['style', ['bold', 'italic', 'clear']],
                                                           //['font', ['italic','bold','underline','clear']],
                                                           //['para', ['ul','ol','paragraph']],
                                                           ['insert', ['link']],
                                                           ['view', ['fullscreen','codeview','help']],
                                                          //['font', ['italicTEI', 'note', 'biblioTEI', 'removeTag']]
                                                           ['font', ['italicTEI', 'note', 'biblioTEI', 'removeTag']]
                                                             ],
                                                            
                                                            buttons: {{
                                                                    italicTEI: italicTEI,
                                                                    note:noteBottom
                                                                   // biblioTEI:biblioTEI,
                                                                   // removeTag: removeTag
                                                                }},

                                                            callbacks: {{

                                                                    onChange: function(){{
                                                                    $("#{$teiElementNickname}_{$indexNo}_1_message").css("display", "block");
                                                                        $("#{$teiElementNickname}_{$indexNo}_1_message").html("Text modified and not saved...");
                                                                        $("#{$teiElementNickname}_{$indexNo}_1_message").css('background-color', '#ffaa99');
                                                                    }},
                                                                    onBlur: function(contents, $editable) {{
                                                                    
                                                                        $("#{$teiElementNickname}_{$indexNo}_1_message").css("display", "block");
                                                                        $("#{$teiElementNickname}_{$indexNo}_1_message").html("Saving text...");
                                                                        $("#{$teiElementNickname}_{$indexNo}_1_message").css('background-color', '#e6f4ff');
                                                                        saveTextarea('{$currentDocId}', '{$teiElementNickname}_{$index}_1', 
                                                                        '{$teiElementNickname}', '{replace($teiXPath, "'", "'")}', {$index})
                                                                    }}
                                                            }}
                                                          }}
                                                       );
                                                       
                                        </script>
                                        <style>
                                               hi[rend='italic']{{font-style: italic}}
                                               note{{font-size: smaller;
                                                background-color: lightgrey;
                                                vertical-align: super;
                                                padding: 2px;}}
                                                note::before{{content: '[nzote ';
                                                vertical-align: super;
                                                font-size: smaller;
                                                }}
                                                note::after{{content: ']';
                                                vertical-align: super;
                                                font-size: small;
                                                }}
                                                

                                        </style>
                                    </div>
                                    )
                             default return "Error; check type of field l766"
                             
                     }
                     
                     </div>
                       {
                        if(($type = "directInput") or contains($teiElementDataType , "enrichedText")) then (
                            if($type = "textarea" and $contentType = "enrichedText") then (
                                <button id="edit{$teiElementNickname}_{$index}_1" onclick="editBtnTrumbowyg(edit{$teiElementNickname}_{$index}_1)" class="btn editbuttonTrumbowyg"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                                                editConceptIcon"></i></button>
                            ) else ()
                        ) else (
                            <button id="edit{$teiElementNickname}_{$index}_1" class="btn btn-primary editbutton pull-right"
                            appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                                            editConceptIcon"></i></button>
                        )
                 }</div>

                 <div id="{$teiElementNickname}_edit_{$index}_1" class="teiElementHidden teiElementEdit form-group">
                 <div class="input-group" >
                 <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                     <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                     </span></span>

                         {switch ($type)
                          case "input" return
                            (
                             switch ($teiElementDataType)
                             case "text" case "attribute"  return
                                    <input id="{$teiElementNickname}_{$index}_1" class="form-control input4textnode" name="{$teiElementNickname}_{$index}_1" value="{$teiElementAttributeValue}"></input>
                             case "textNodeAndAttribute" return
                                 <div>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                        <em>Text</em>
                                        </div>
                                        <input id="{$teiElementNickname}_text_{$index}_1" class="form-control input4attribute" name="{$teiElementNickname}_attrib_{$index}_1" value="{ $teiElementAttributeValue }"></input>
                                    </div>
                                    <div class="input-group">
                                       <div class="input-group-addon">
                                            <span>{functx:substring-after-last($teiXPath, '/')}</span>
                                        </div><input id="{$teiElementNickname}_attrib_{$index}_1" class="form-control input4textnode" name="{$teiElementNickname}_text_{$index}_1" value="{ $teiElementTextNodeValue }"></input>
                                    </div>
                                </div>
                             default return "Error! Check data type l799"
                          )
                          case "textarea" return
                          <textarea id="{$teiElementNickname}_{$index}_1" class="form-control elementWithValue" name="{$teiElementNickname}_{$index}_1">{$teiElementTextNodeValue}</textarea>
                          default return "ERROR"
                          }
                         <button id="{$teiElementNickname}SaveButton" class="btn btn-success"
                         onclick="saveData2(this, '{$currentDocId}', '{switch ($teiElementDataType)
                            case "text" return $teiElementNickname || "_" || $index || "_1"
                            case "textNodeAndAttribute" return $teiElementNickname || "_text_" || $index || "_1"
                            default return $teiElementNickname || "_" || $index || "_1"
                            }',
                         '{switch ($teiElementDataType)
                            case "text" return ""
                            case "textNodeAndAttribute" return $teiElementNickname || "_attrib_" || $index || "_1"
                            default return ""
                            }',
                            '{$teiElementNickname}', '{$teiXPath}',
                             '{$teiElementDataType}', '{$index}', {$teiElementCardinality})"
                                 appearance="minimal" type="button"><i class="glyphicon
         glyphicon glyphicon-ok-circle"></i></button>
                         <button id="{$teiElementNickname}CancelEdit" class="btn btn-danger cancelEdit"
                         onclick="cancelEdit(this, '{$teiElementNickname}', '{$index}', '{$teiElementTextNodeValue}', 'input', '1') "
                                 appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>

                 </div></div>
                 </div>
        )
  else (:cardinality > 1:)

        <div id="{$teiElementNickname}_group_{$indexNo}" class="teiElementGroup teiElementCardinalityGroup">
        <div class="TeiElementGroupHeaderBlock">
            <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                    <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                    </span></span>
                    { if($teiElementCardinality ="x") then
                    <button id="{$teiElementNickname}addItem_{$indexNo}" class="btn btn-primary addItem pull-right"
                        onclick="addItem(this, '{ $inputName }_add_{ $indexNo }', '{ $indexNo }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-plus"></i></button>



                  else()
                     }
              </div>
              {
              for $item at $pos in $teiElementValue

                let $teiElementAttributeValue :=
                                                 if($teiElementDataType = "textNodeAndAttribute" ) then
                                                (util:eval( "$doc/"
                                                || substring-before($teiXPath, '/@') || "[" || $pos || "]/" || functx:substring-after-last($teiXPath, '/')  ))
                                                else()
                let $teiElementTextNodeValue :=
                                    if($teiElementDataType = "textNodeAndAttribute") then
                                        (util:eval( "$doc/" || substring-before($teiXPath, '/@') || "[" || $pos || "]" ))
                                        else($item/text())
                let $lang := if(exists($item/@xml:lang)) then
                    <span class="labelForm">{data($item/@xml:lang)}</span> else ("")
                let $teiXPath4Save:= if(contains($teiXPath, '/@'))
                    then substring-before($teiXPath, '/@') || "[" || $pos || "]/" || functx:substring-after-last($teiXPath, '/')
                    else $teiXPath || "[" || $pos || "]" 
              
                let $formattedCurrentValue := $teiElementTextNodeValue => replace("'", "’")
              return
        (
        <div class="teiElementGroup">
            <div id="{$teiElementNickname}_display_{$index}_{$pos}" class="teiElementDisplay">
                <!--
                <div class="TeiElementGroupHeaderInline">
                        <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                                <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                            </span>
                            </span>
                </div>
                -->
                <div id="{$teiElementNickname}_value_{$index}_{$pos}" class="teiElementValue">
                            {$lang}<span >{ if(starts-with($teiElementTextNodeValue, "http")) then 
                    <a href="{$teiElementTextNodeValue}" target="_blank" class="urlInTeiElement">{$teiElementTextNodeValue}</a>
                    else  $teiElementTextNodeValue } <a href="{$teiElementAttributeValue}" target="_blank" class="urlInTeiElement">{$teiElementAttributeValue}</a></span>
                </div> 
                <button id="edit{$teiElementNickname}_{$index}_{$pos}" class="btn btn-primary editbutton pull-right"
                    appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                                    editConceptIcon"></i></button>
                <button class="removeItem btn btn-warning pull-right"
                    onclick="removeItemFromList('{$currentDocId}', '{$teiElementNickname}', 'xmlNode', {$pos}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button>
            </div>

            <div id="{$teiElementNickname}_edit_{$index}_{$pos}" class="teiElementHidden teiElementEdit">
            <!--<div class="" >
                
                <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                </span></span>
                -->
                {switch ($type)
                 case "input" return
                    switch ($teiElementDataType)
                    case "text" return
                    (<div class="input-group">
                        {
                        if(exists($item/@xml:lang)) then
                            (

                            <div class="input-group-prepend">
                                    <!--<span class="input-group-text">lang.</span>-->
                                    <span class="labelForm">{$lang}</span>
                            </div>

                            )

                        else()
                        }
                        <input id="{$teiElementNickname}_{$index}_{$pos}" class="form-control input4textnode" name="{$teiElementNickname}_{$index}_{$pos}" value="{$teiElementTextNodeValue}"></input>
                    </div>)
                    case "textNodeAndAttribute" return

                            <div>
                            <div class="input-group">
                            <span class="input-group-addon" id="{$teiElementNickname}_text_{$index}_{$pos}_addon">Text</span>

                            <input id="{$teiElementNickname}_text_{$index}_{$pos}" class="form-control input4textnode" name="{$teiElementNickname}_text_{$index}_{$pos}" value="{ $teiElementTextNodeValue  }" aria-describedby="{$teiElementNickname}_text_{$index}_{$pos}_addon"></input>
                            </div>

                            <div class="input-group">
                            <span class="input-group-addon" id="{$teiElementNickname}_attrib_{$index}_{$pos}_addon">{functx:substring-after-last($teiXPath, '/')}</span>
                                <input id="{$teiElementNickname}_attrib_{$index}_{$pos}" class="form-control input4attribute" name="{$teiElementNickname}_attrib_{$index}_{$pos}" value="{ $teiElementAttributeValue }" aria-describedby="{$teiElementNickname}_attrib_{$index}_{$pos}_addon"></input>
                            </div>
                            </div>

                    default return "Error! Check data type - l. 955"

                 case "textarea" return
                 <textarea id="{$teiElementNickname}_{$index}_{$pos}" class="form-control input4textnode" name="{$teiElementNickname}_{$index}_1">{$teiElementAttributeValue}</textarea>
                 default return null
                 }

                <button id="{$teiElementNickname}SaveButton" class="btn btn-success"
                onclick="saveData2(this, '{$currentDocId}',
                '{switch ($teiElementDataType)
                            case "text" return $teiElementNickname || "_" || $index || "_" || $pos
                            case "textNodeAndAttribute" return $teiElementNickname || "_text_" || $index || "_" || $pos
                            default return $teiElementNickname || "_" || $index || "_" || $pos
                            }',
                         '{switch ($teiElementDataType)
                            case "text" return ""
                            case "textNodeAndAttribute" return $teiElementNickname || "_attrib_" || $index || "_" || $pos
                            default return ""
                            }',
                 '{$teiElementNickname}',
                 '{replace($teiXPath4Save, "'", "&quot;")}',
                    '{$teiElementDataType}', '{$index}', '{$pos}')"
                        appearance="minimal" type="button"><i class="glyphicon
glyphicon glyphicon-ok-circle"></i></button>
                <button id="{$teiElementNickname}CancelEdit" class="btn btn-danger"
                onclick="cancelEdit(this, '{$teiElementNickname}', '{$index}', '{$formattedCurrentValue}', 'input', '{$pos}') "
                        appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>

        <!--</div>-->
        </div>
        </div>
                      )}


                <div id="{$inputName}_add_{$indexNo}" class="teiElement teiElementAddItem teiElementHidden">

                                { switch ($teiElementDataType)
                                case "text" return

                                                <div class="input-group">
                                                 <div class="input-group-prepend">
                                                        <!--<span class="input-group-text">lang.</span>-->
                                                        <span class="input-group-text">
                                                        {teiEditor:projectLangDropDown('', $docId, $teiElementNickname, "add")}

                                                        </span>
                                                </div>


                                            <input id="{$teiElementNickname}_text_{$index}_1" class="form-control elementWithValue" name="{$teiElementNickname}_text_{$index}_1" value=""></input>
                                            </div>

                                case "textNodeAndAttribute" return
                                            <div>
                                            <span>Value of <em>Attribute</em> {functx:substring-after-last($teiXPath, '/')}</span><input id="{$teiElementNickname}_add_attrib_{$indexNo}_1" class="form-control" name="{$teiElementNickname}_text_{$index}_1" value=""></input>
                                            <span>Value of <em>Node Text</em></span><input id="{$teiElementNickname}_add_text_{$index}_1" class="form-control" name="{$teiElementNickname}_add_text_{$indexNo}_1" value=""></input>
                                            </div>
                                default return "Error! Check data type - l. 1010"
                                }


                        <button id="addNewItem" class="btn btn-success" onclick="addData(this, '{$currentDocId}', '{$inputName}_add_{$indexNo}',
                        '{$teiElementNickname}', '{$teiXPath}', '{$contentType}', '{$indexNo}', '')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                <button id="{$teiElementNickname}CancelEdit_{$indexNo}_add" class="btn btn-danger"
                        onclick="cancelEdit(this, '{$inputName}_add_{$indexNo}', '{$indexNo}', '{$teiElementValue}', 'thesau', {$valuesTotal +1}) "
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                </div>
            </div>
        )
};

(:~ Deprecated. Function to generate the HTML widget for a XML node made of child nodes. 
 : Prefer teiEditor:displayGroup :)
declare function teiEditor:displayTeiElementAndChildren($teiElementNickname as xs:string,
                                                        $docId as xs:string?,
                                                        $index as xs:int?,
                                                        $type as xs:string?,
                                                        $xpath_root as xs:string?) {
     
     let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]))) then
                        $teiEditor:teiElements//teiElement[nm=$teiElementNickname]
                        else ($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname])
    
    let $elementIndex := if($index ) then ("[" || string($index) || "]" ) else ("")
    let $xpathRaw := $elementNode/xpath/text()
    let $xpathEnd := if(contains($xpathRaw, "/@"))
            then( functx:substring-before-last($xpathRaw, '/') || $elementIndex || "/" || functx:substring-after-last($xpathRaw, '/'))
            else($xpathRaw)
    let $elementAncestors := $elementNode/ancestor::teiElement
    let $elementChildren := $elementNode//child::teiElement

    let $teiXPath := if($xpath_root)
                    then $xpath_root
                    
                    else
                        string-join(
                        for $ancestor in $elementAncestors
                        return
                        if (contains($ancestor/xpath/text(), '/@')) then
                            substring-before($ancestor/xpath/text(), '/@')
                            else $ancestor/xpath/text()
                        )
                    || $elementIndex || $xpathEnd
                    (:else
                        $xpathEnd
:)


    let $teiElementDataType := $elementNode/contentType/text()
    let $teiElementFormLabel := $elementNode/formLabel[@xml:lang=$teiEditor:lang]/text()
    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId)
    let $teiElementValue :=
         (util:eval( "$teiEditor:doc-collection/id('" ||$teiEditor:docId ||"')/tei:TEI" || $teiXPath))
    
    (:let $unit :=       if(util:eval( "$teiEditor:doc-collection/id('"||$teiEditor:docId ||"')/" || $teiXPath || "/@unit" ))
                                then (" (" ||
                                    util:eval( "$teiEditor:doc-collection/id('"||$teiEditor:docId ||"')/" || $teiXPath || "/@unit" )
                                    || ")")
                        else ():)
    let $unit :=        try {
                    " (" || util:eval( "$teiEditor:doc-collection/id('"||$teiEditor:docId ||"')/" || $teiXPath || "/@unit" ) || ")"
                        }
                        catch * { " "}
    return
        <div id="{$teiElementNickname}_display_{$index}_1" class="teiElement teiElementDisplay">
        <span class="labelForm">{$teiElementFormLabel} {$unit}<span class="teiInfo">
            <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
            </span></span>

        {
        for $child at $pos in $elementChildren
        return
            if ($child/child::teiElement) then
                for $subchildren at $position in $child//teiElement
                     return
                           <div>{teiEditor:displayElement($subchildren/nm/text(), $teiEditor:docId, $position, $teiXPath)}</div>

                else
                    <div>{$child/nm/text()}; pos: { $pos }{teiEditor:displayElement($child/nm/text(), $teiEditor:docId, $pos, $teiXPath)}</div>

        }
      </div>

};

(:~ Function to generate the HTML widget for a XML node made of child nodes :)
declare function teiEditor:displayGroup($teiElementNickname as xs:string,
                                                        $docId as xs:string?,
                                                        $index as xs:int?,
                                                        $type as xs:string?,
                                                        $xpath_root as xs:string?) {
     
     let $elementNode := if (not(($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]))) then
                        $teiEditor:teiElements//teiElement[nm=$teiElementNickname]
                        else ($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname])
    let $elementXPath := $elementNode/xpath/text()
    let $teiElementFormLabel := teiEditor:labelForXMLElement($elementNode)
    let $teiElementCardinality := $elementNode/cardinality/text()
    let $indexNo := if(not(empty($index))) then data($index) else "1"
    let $randomNumber := map:get(random-number-generator(), "number") => substring(3, 6)
    let $elementAncestors := $elementNode/ancestor::teiElement
    let $xpathRootWhenSubGroup :=
            if($xpath_root !="") then 
                    (if($teiElementCardinality = "1") then ($xpath_root||"")
                    else (
                        if(contains($xpath_root, $elementXPath)) then "" || $xpath_root
                        else $xpath_root|| "/" || $elementXPath
                     )
                    )
                else
                (
                    if($elementNode/ancestor::teiElement)
                    then ("" ||
                        string-join(
                            for $ancestor at $pos in $elementAncestors
                                let $ancestorIndex := 
                                    if($pos = 1 ) 
                                            then if(not(empty($index))) 
                                                    then "[" || string($indexNo) || "]" 
                                                    else ("")
                                            else ("")
                                return
                                    if (contains($ancestor/xpath/text(), '/@'))
                                        then substring-before($ancestor/xpath/text(), '/@')
                                                || $ancestorIndex
                                        else $ancestor/xpath/text() || $ancestorIndex
                        )
                    
                )
                else ()
                    )

        (: if($teiElementCardinality = "1") then ($xpath_root|| $elementXPath)
            else
                if($xpath_root !="")
                then
                 ("" || $xpath_root || "/" || $elementXPath)
        
                else (
                if($elementNode/ancestor::teiElement)
                    then ("" ||
                        string-join(
                            for $ancestor at $pos in $elementAncestors
                                let $ancestorIndex := 
                                    if($pos = 1 ) 
                                            then if(not(empty($index))) 
                                                    then "[" || string($indexNo) || "]" 
                                                    else ("")
                                                else ("")
                        return
                            if (contains($ancestor/xpath/text(), '/@'))
                                then substring-before($ancestor/xpath/text(), '/@')
                                        || $ancestorIndex
                                else $ancestor/xpath/text() || $ancestorIndex
                   )
                    
                )
           else () :)
    (: ) :)
    let $xpathRoot := 
                (if($xpathRootWhenSubGroup != "") then $xpathRootWhenSubGroup || "/" 
                else ())
                || (if(ends-with($elementNode/xpath[1]/text(), '/self')) then
                                       substring-before($elementNode/xpath/text(), '/self')
                                       else 
                                        "/child::" || substring-after($elementNode/xpath[1]/text(), "/"))
                                       (: "rrr" || $elementNode/xpath[1]/text()) :)
                                       
                                       
    let $docId2Use := if($docId != "") then $docId else $teiEditor:docId  
    let $groupNodeBaseXPath := functx:substring-before-last($xpathRoot, "/")
    let $groupNodeXPath := functx:substring-after-last($xpathRoot, "/")
    (:let $groupNodes := util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" ||$teiEditor:docId ||"')/" || $groupNodeBaseXPath
                                 || "/" || $groupNodeXPath):)
    let $groupNodes := util:eval( "collection($teiEditor:doc-collection-path)/id('" || 
                $docId2Use ||"')/" || $xpathRootWhenSubGroup)
    let $elementChildren := $elementNode/child::teiElement
    let $regex4NotInXPath := '([a-z])(\[not\([\.])/tei:[aA-zZ\[@="\-]*\)\]'
    let $xpathRootCleaned := $xpathRoot => replace($regex4NotInXPath, "")
    let $xpath4GroupAdd := $xpathRootWhenSubGroup
                    => replace(($regex4NotInXPath), "")
                    => functx:substring-before-last("/")
                    => replace("/$", "")
    (: $xpathRootCleaned => functx:substring-before-last("/") :)
    (: "frfrf"|| :)
                   

     (: if(empty($groupNodes)) :)
            (: then "eerty" || substring-before($xpathRootCleaned, "//child::") => replace("/$", "") :)
            (: then ("" || (if(ends-with($xpathRootCleaned, "]")) then
                 ("" || 
                    functx:substring-before-last(
                            functx:substring-before-last($xpathRootCleaned, "[")
                        , "/") => replace("/$", ""))
                else ("" ||functx:substring-before-last($xpathRootCleaned, "/")
                     => replace("/$", "") )
                    )) :)
            (: else ("" || (replace(
                        substring-after(
                            functx:substring-before-last(
                                functx:path-to-node-with-pos($groupNodes[1]), "/")
                            , "TEI")
                    , "/", "/tei:") => replace("/$", ""))=> replace($regex4NotInXPath, ""))                          :)
    return 
<div id="{ $teiElementNickname }_group_{ $randomNumber }_{ $indexNo }" class="panel panel-default teiElementGroup">
    <!--
    $xpath_root !="" : { $xpath_root !="" }
    <br/>teiElementCardinality: { $teiElementCardinality }
    <br/>elementXPath: { $elementXPath }
    <br/>$xpathRoot:{ $xpathRoot}
    <br/> $xpath_root : { $xpath_root }
    $index : { $index} - $indexNo : { $indexNo }
    <br/>
    empty($groupNodes) : { empty($groupNodes)}<br/>
    xpathRootCleaned: { $xpathRootCleaned }<br/>
    xpath4GroupAdd: { $xpath4GroupAdd }
    <br/>xpathRootWhenSubGroup: {$xpathRootWhenSubGroup }
    -->
        <div id="{ $teiElementNickname }_display_{$indexNo}_1" class="teiElementDisplay panel">
           <div class="TeiElementGroupHeaderBlock">
            <span class="labelForm">{ $teiElementFormLabel }<span class="teiInfo">
                    <a title="TEI element: { $xpathRoot }"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                </span>
            </span>{ if($teiElementCardinality ="x") then
                    <button id="{$teiElementNickname}addItem_{$indexNo}" class="btn btn-primary addItem pull-right"
                        onclick="addGroupItem(this, '{ $teiElementNickname }_add_{ $indexNo }', '{ $indexNo }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-plus"></i></button>
                     else()
         }
         </div>
    <ul class="list-group">{
        for $group at $pos in $groupNodes
            return
                <li class="list-group-item elementsByGroup" style="border: solid 1px DarkGray">
                <div class="TeiElementGroupHeaderBlock">
                <!--<span class="labelForm">{ $teiElementFormLabel } <span class="teiInfo">
                    <a title="TEI element: { $xpathRoot }"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                </span></span>
                -->
               { if($teiElementCardinality ="x") then <button class="removeItem btn btn-xs btn-warning pull-right"
                                          onclick="removeListItem(this, '{$docId}', '{$xpathRootWhenSubGroup}', '', {$pos}, 
                                          '{ $teiElementNickname }', '{ $teiElementNickname }_group_{ $randomNumber }_{ $indexNo }', 'Confirmation')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button>
                                        else ()}
            </div>
           {
                for $elementChild at $position in $elementChildren
                    let $xpathEnd := if($elementChild/contentType/text() ="nodes" and $elementChild/cardinality/text() ="x")
                        then "" else  $elementChild/xpath/text()
                    
                    return
                    <div class="d-block">
                        {
                            teiEditor:displayElementWithDef($elementChild,
                            $docId2Use,
                            ($index + $pos),
                            $xpathRootWhenSubGroup
                             || "[" || $pos || "]" 
                             || $xpathEnd
                               )
                        }</div>
                    }
                           </li>
             }
             </ul>
        </div>
        <div id="{$teiElementNickname}_add_{$indexNo}" class="teiElement teiElementAddGroup teiElementAddGroupItem teiElementHidden">
        <div class="TeiElementGroupHeaderBlock">
                <span class="labelForm"><span style="margin-right: 1em;">{teiEditor:label("newData", "New ")} </span>{ $teiElementFormLabel } <span class="teiInfo">
                    <a title="TEI element: { $xpathRoot }"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a></span>
                </span>
        </div>
            {
            
            for $elementChild at $pos in $elementChildren[./fieldType/text()!="group"]
                let $teiElementNickname := $elementChild/nm/text()
                let $xpathEnd := $elementChild/xpath/text()
                let $teiElementDataType := $elementChild/contentType/text()
                let $teiElementFieldType := $elementChild/fieldType/text()
                let $attributeValueType := $elementChild/attributeValueType/text()
                let $label :=teiEditor:labelForXMLElement($elementChild)
                let $topConceptUri := functx:substring-after-last($elementChild/thesauTopConceptURI/text(), "/")
                return 
                    switch ($teiElementFieldType)
                        case "combobox"
                            return 
                            <div>
                                {skosThesau:dropDownThesauForElement($teiElementNickname, $topConceptUri, $teiEditor:lang, $label, 'inline', 
                                    sum(9999, $pos), $pos, $attributeValueType)}
                            
                            </div>
                        case "comboboxWithConceptHierarchy"
                            return 
                            <div>
                                { skosThesau:dropDownThesauForElementWithConceptHierarchy($teiElementNickname,
                                    $topConceptUri, $teiEditor:lang, $label, 'inline',  sum(9999, $pos), $pos, $attributeValueType) }

                                
                            
                            </div>
                        case "input"
                            return
                            <div>
                            <div class="input-group-prepend">
                                <span>{ $label }</span>
                                </div>
                                <input id="{$teiElementNickname}_add_{$index}_1" class="form-control elementWithValue" name="{$teiElementNickname}" value=""></input>
                                </div>
                        
                        case "textarea"
                            return
                                <div>
                            <div class="input-group-prepend">
                                <span>{ $label }</span>
                                </div>
                                <textarea id="{$teiElementNickname}_add_{$index}_1" class="form-control elementWithValue" name="{$teiElementNickname}"></textarea>
                            </div>
                        case "textNodeAndAttribute" return
                        <div/>
                        case "group" return ()
                        case "radioGroup" return ()
                        (: skosThesau:radioGroupThesauForXMLElement($teiElementNickname, $topConceptUri, $teiEditor:lang, 'noLabel', 'inline', $index, $pos, $attributeValueType, ()) :)

                        default return "Error! Check data type (" || $teiElementFieldType || ")- l. 1172"
            (:switch ($teiElementDataType)
                        case "text"
                        case "attribute"
                        return
                        <div>
                        type{$teiElementFieldType}
                        <input id="{$teiElementNickname}_text_{$index}_1" class="form-control" name="{$teiElementNickname}_{$index}_1" value=""></input>
                        </div>
                        case "textNodeAndAttribute" return
                        <div>
                        type{$teiElementFieldType}<div class="input-group">
                        <div class="input-group-prepend">
                                <span>Value of <em>Attribute</em>{ functx:substring-after-last($xpathRoot, '/') }</span>
                                </div>
                                <input id="{$teiElementNickname}_add_attrib_  {$index}_1" class="form-control" name="{$teiElementNickname}_text_{$index}_1" value=""></input>
                        </div>
                        <span>Value of <em>Node Text</em></span><input id="{$teiElementNickname}_add_text_{$index}_1" class="form-control" name="{$teiElementNickname}_add_text_{$index}_1" value=""></input>
                        </div>
                        default return "Error! Check data type (" || $teiElementDataType || ")- l. 1172":)
                                        
                                        }
                         {if($teiElementCardinality != "1") then                                   
                            <div>
                            <button id="{$teiElementNickname}addNewGroup" class="btn btn-success"
                        onclick="addGroupData(this, '{$docId}', '{$teiElementNickname}', '{$indexNo}', '{ $xpath4GroupAdd }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                         <button id="{$teiElementNickname}CancelEdit_{$indexNo}_add" class="btn btn-danger"
                        onclick="cancelAddItem(this)"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                            </div> else () }       
                                    </div>
                                    </div>
};

(:~ Deprecated? Function to generate the HTML widget for a XML node edited with a HTML select populated with a thesaurus.
 : Prefer displayTeiElementWithThesau :)
declare function teiEditor:displayTeiElementWithTaxo($teiElementNickname as xs:string, $index as xs:int?, $xpath_root as xs:string?) {
        let $elementNode := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]
        let $elementIndex := if($index) then ("[" || string($index) || "]" ) else ("")
        let $taxoId := if ($elementNode/taxoId/text()) then $elementNode/taxoId/text() else ()

        let $xpathEnd := if(contains($elementNode//xpath/text(), "/@"))
            then(functx:substring-before-last($elementNode//xpath/text(), '/') || $elementIndex || "/" || functx:substring-after-last($elementNode//xpath/text(), '/')) else(
            $elementNode//xpath/text())
        let $teiXPath := if($elementNode/ancestor::teiElement)
                    then $elementNode/ancestor::teiElement/xpath/text() || $xpathEnd
                    else
                        $xpathEnd

    let $teiXPath2Ref := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]/xpath2ref/text()
    let $teiElementDataType := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]/contentType/text()
    let $teiElementFormLabel := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]/formLabel[@xml:lang=$teiEditor:lang]/text()

    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId)

    let $teiElementValue :=
         util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('"||$teiEditor:docId ||"')/" || $teiXPath)
    let $data2display :=
            substring($teiElementValue, 2) ||
                util:eval( "$teiEditor:doc-collection/id('" ||$teiEditor:docId
                    ||"')/"
                    || $teiXPath2Ref || "[@xml:id='" || substring($teiElementValue, 2) || "']/string()")


    let $itemList :=
       util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('"||$teiEditor:docId
                    ||"')/"
                    || functx:substring-before-last($teiXPath2Ref, '/') || "//tei:category")
    return

        <div>
        <div id="{$teiElementNickname}_display_{$index}_1" class="teiElement teiElementDisplay">
        <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
            <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
            </span></span>
            <div id="{$teiElementNickname}_value_{$index}_1" title="{normalize-space($data2display)} = concept {substring($teiElementValue, 2)}" class="teiElementValue">{$data2display}</div>

            <button id="edit{$teiElementNickname}{$index}_1" class="btn btn-primary editbutton pull-right"
             appearance="minimal" type="button"><i class="glyphicon glyphicon-edit editConceptIcon"></i></button>
        </div>
        <div id="{$teiElementNickname}_edit_{$index}_1" class="teiElement teiElementEdit teiElementHidden">
        <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
            <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
            </span></span>
                <select id="{$teiElementNickname}_{$index}_1" name="{$teiElementNickname}{$index}">
                    {for $items in $itemList
                    return
                        if ($items/@xml:id = $teiElementValue)
                        then (<option value="#{$items/@xml:id}" selected="selected">{$items/tei:catDesc/string()} </option>) else (
                        <option value="#{$items/@xml:id}">{$items/tei:catDesc/string()} </option>
                        )

                     }


                    </select>


                <button id="saveTitleStmt" class="btn btn-success"
                onclick="saveData('{$teiEditor:docId}', '{$teiElementNickname}', '{$teiXPath}', '{$teiElementDataType}', '{$index}', '1')"
                        appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                        <button id="{$teiElementNickname}CancelEdit" class="btn btn-danger"
                onclick="javascript:cancelEdit(this, '{$teiElementNickname}', '{$index}', '', 'taxo', '1') "
                        appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
        </div>
        </div>
};

(:~ Function to generate the HTML widget for a XML node edited with a dropdown menu bound to a thesaurus.
 : This function does not deal with multiple cardianlity; see teiEditor:displayTeiElementWithThesauCardi :)
declare function teiEditor:displayTeiElementWithThesau($teiElementNickname as xs:string,
            $topConceptId as xs:string,
            $index as xs:int?,
            $xpath_root as xs:string?) {


         let $elementNode := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]

        let $elementIndex := if($index) then ("[" || string($index) || "]" ) else ("")
        let $xpathEnd := if(contains($elementNode//xpath/text(), "/@"))
            then(functx:substring-before-last($elementNode//xpath/text(), '/') || $elementIndex || "/" || functx:substring-after-last($elementNode//xpath/text(), '/')) else(
            $elementNode//xpath/text())
        let $teiXPath := if($elementNode/ancestor::teiElement)
                    then $elementNode/ancestor::teiElement/xpath/text() || $xpathEnd
                    else
                        $xpathEnd

    let $contentType :=$elementNode/contentType/text()
    let $teiElementDataType := $elementNode/contentType/text()
    let $teiElementFormLabel := $elementNode/formLabel[@xml:lang=$teiEditor:lang]/text()
    let $teiElementCardinality := $elementNode/cardinality/text()

(:    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)

    let $teiElementValue :=
         util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('"||$teiEditor:docId ||"')/" || $teiXPath )

    let $data2display :=
        if ($teiElementCardinality = "1" ) then
        util:eval( "collection('" || $teiEditor:doc-collection-path || "')//.[@rdf:about='" || $teiElementValue || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']")
        else()
    let $inputName := 'selectDropDown' ||$topConceptId

    (:let $itemList :=
        util:eval( "collection('/db/apps/" || $teiEditor:project || "/data/documents')//id('"||$teiEditor:docId
                    ||"')/"
                    || functx:substring-before-last($teiXPath2Ref, '/') || "//tei:category"):)
    return
        if ($teiElementCardinality = "1") then(
        <div>{$teiElementCardinality}
        <div id="{$inputName}-display{$index}" class="teiElement teiElementDisplay">
        <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
            <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
            </span></span>

            <div id="{$teiElementNickname}-value{$index}" title="{normalize-space($data2display)} = concept {substring($teiElementValue, 2)}" class="teiElementValue">{string($data2display)}</div>

            <button id="edit{$teiElementNickname}{$index}" class="btn btn-primary editbutton pull-right"
             appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                              editConceptIcon"></i></button>
        </div>
        <div id="{$teiElementNickname}-edit{$index}" class="teiElement teiElementEdit teiElementHidden">
        <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
            <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
            </span></span>
                {skosThesau:dropDownThesauForXMLElement($teiElementNickname, $topConceptId, 'en', 'noLabel', 'inline', $index, 1, ())}


                <button id="saveTitleStmt" class="btn btn-success"
                onclick="saveData('{$teiEditor:docId}', '{$teiElementNickname}', '{$teiXPath}', '{$contentType}', '{$index}')"
                        appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                        <button id="{$teiElementNickname}CancelEdit" class="btn btn-danger"
                onclick="cancelEdit(this, '{$teiElementNickname}', '{$index}', '{$teiElementValue}', 'thesau') "
                        appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
        </div>
        </div>)
        else()

};

(:~ Function to generate the HTML widget for an XML node edited with a dropdown menu bound to a thesaurus, multiple cardinality enabled  :)
declare function teiEditor:displayTeiElementWithThesauCardi($teiElementNickname as xs:string,
             $topConceptId as xs:string,
             $docId as xs:string?,
             $index as xs:integer?,
             $xpath_root as xs:string?) {

        let $currentDocId := if($docId != "") then $docId else  $teiEditor:docId
      
        let $doc := $teiEditor:doc-collection/id($currentDocId)    
        
        let $indexNo := if(string($index) != "") then data($index) else "1"
(:        let $elementNode := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]:)
        let $elementNode := if (exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname])) then
                        $teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]
                        else ($teiEditor:teiElements//teiElement[nm=$teiElementNickname])
        let $contentType :=$elementNode/contentType/text()
        let $teiElementDataType := $elementNode/contentType/text()
        let $teiElemenFieldType := $elementNode/fieldType/text()
        let $teiElementFormLabel := teiEditor:labelForXMLElement($elementNode)
        let $teiElementCardinality := $elementNode/cardinality/text()
        let $attributeValueType := $elementNode/attributeValueType/text()

        let $elementIndex := if($index) then ("[" || string($index) || "]" ) else ("")

        let $xpathEnd := if(contains($elementNode//xpath/text(), "/@"))
            then(functx:substring-before-last($elementNode//xpath/text(), '/') || $elementIndex || "/"
            || functx:substring-after-last($elementNode//xpath/text(), '/')
            )
            else (
            $elementNode/./xpath/text()
            )
        let $elementAncestors := $elementNode/ancestor::teiElement
        let $teiXPath := 
                    if($xpath_root != "") then $xpath_root
                else if($elementNode/ancestor::teiElement)
                    then
                        string-join(
                        for $ancestor in $elementAncestors
                        return
                        if (contains($ancestor/xpath/text(), '/@')) then
                            substring-before($ancestor/xpath/text(), '/@')
                            else $ancestor/xpath/text()
                        )
                    || $elementIndex || $xpathEnd
                    else
                        $xpathEnd
        let $xpathBaseForCardinalityX :=
               if (contains($teiXPath, "/@")) then
               (functx:substring-before-last(functx:substring-before-last($teiXPath, "/@"), '/'))
               else
                   ($teiXPath)
        let $xpathBaseForCardinalityOne :=
   (:                    Test on $contentType:)
                       if($contentType ="textNodeAndAttribute") then
                       (:(if 
                       (contains($teiXPath, "/@")) then:) 
                       substring-before($teiXPath, "/@"
                       ) else 
                        $teiXPath
        let $selectorForCardinalityX :=
               if (contains($teiXPath, "/@")) then
               (functx:substring-after-last(functx:substring-before-last($teiXPath, "/@"), "/"))
               else
                   (functx:substring-after-last($teiXPath, "/"))

    
(:    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)

    let $teiElementValue :=
        if($teiElementCardinality = "1" ) then (
            util:eval( "$doc/" || 
            $xpathBaseForCardinalityOne ))
         
         else if($teiElementCardinality = "x" ) then
                (if($attributeValueType = "multi-uri" or $attributeValueType = "xml-value") then
                   (tokenize(util:eval( "$doc/" || $teiXPath), " "))
                else (
                    util:eval( "$doc/" || $xpathBaseForCardinalityX || "//" || $selectorForCardinalityX ))
                )
         else (util:eval( "$doc/" || $teiXPath ))
    let $valuesTotal := count($teiElementValue)
(:    let $data2display :=
                if(($teiElementCardinality = "1" ) 
                and (util:eval( "collection('" || $teiEditor:concept-collection-path ||"')//skos:Concept[@rdf:about='" || data($teiElementValue[1]) || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']"))
                ) then (
                     data(util:eval( "collection('" || $teiEditor:concept-collection-path ||"')//skos:Concept[@rdf:about='" || $teiElementValue || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']"))
                   ) else()
:)  
                    


  let $fieldSize:= switch($elementNode//fieldSize)
                case "small" return " col-sm-4 col-md-4 col-lg-4"
                case "medium" return " col-sm-6 col-md-6 col-lg-6"
                case "large" return " col-sm-12 col-md-12 col-lg-12"
                default return ""
    let $styleAccordingToSize := if($elementNode//fieldSize !="") then "width: inherit!important;" else ""
let $inputName := 'selectDropDown' ||$topConceptId
let $comboDataType := "ajax"

    (:let $itemList :=
        util:eval( "collection('/db/apps/" || $teiEditor:project || "/data/documents')//id('"||$teiEditor:docId
                    ||"')/"
                    || functx:substring-before-last($teiXPath2Ref, '/') || "//tei:category"):)
    return

        (
        <div id="{$teiElementNickname}_group_{$indexNo}" class="teiElementGroup teiElementCardinalityGroup{ $fieldSize }" style="{ $styleAccordingToSize}">
        <div class="TeiElementGroupHeaderBlock">
            <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                    <a title="TEI element: {$teiXPath }"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                    </span></span>
                    { if($teiElementCardinality ="x") then
                    <button id="{$teiElementNickname}addItem_1_{$indexNo}" class="btn btn-primary addItem"
                        onclick="addItem(this, '{ $teiElementNickname }_add_{ $indexNo }', '{ $indexNo }')"
                                appearance="minimal" type="button">{
                            if($teiEditor:appVariables//comboboxDataType/text() = "ajax") then
                               (attribute data-widgettype {"dropdownAdd" },
                                attribute data-teielementnickname {$teiElementNickname },
                                attribute data-topconceptid { $topConceptId },
                                attribute data-lang { $teiEditor:lang },
                                attribute data-label { "noLabel" },
                                attribute data-format {"inline" },
                                attribute data-index { $index + 1 },
                                attribute data-subindex {""},
                                attribute data-attributevaluetype { $attributeValueType }
                                )
                                else ()}
                            <i class="glyphicon glyphicon glyphicon-plus"></i></button>

                   else()
                     }
              </div>
              <div class="itemList" id="{$teiElementNickname}List"> 
              {
              for $item at $pos in $teiElementValue

                    let $itemPathEnding := if(contains($teiXPath, '/@')) then "/@" || substring-after($teiXPath, '/@')
                                                  else ()
                     let $value2Bedisplayed:= 
                        if (not(contains($contentType, 'text'))) 
                            then ( 
                                    if (not($attributeValueType) or contains($attributeValueType, "uri")) then
                                        (let $concept := 
                                                util:eval( "$teiEditor:concept-collection//skos:Concept[@rdf:about='" || $item || "" 
                                                || "']")
                                        let $label:= if($concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang=$teiEditor:lang]/text() != "") 
                                            then $concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang=$teiEditor:lang]/text()
                                        else if($concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']/text()!="")
                                            then $concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']/text()
                                        else $concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][1]/text()
                                        return $label
                                        )
                                        else (skosThesau:getLabelFromXmlValue($item, $teiEditor:lang))
                        
                                )
                           else if (($contentType ="text") and ($attributeValueType="xml-value") and (not($item[.='']))) then
                       data(util:eval( "$teiEditor:concept-collection//skos:Concept[skos:prefLabel[@xml:lang='xml']='" || $item/string() || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']/text()"))
                        else if (contains($contentType, "textNodeAndAttribute")) then  $item/text()

                        else(
                               $contentType || " " || (if ($attributeValueType) then $attributeValueType
                                                                else ($item/string())       
                              
                                                                )           
                                )
                    return
              (
              <div class="itemInDisplayElement">
            {switch($teiElemenFieldType)
                    case "radioGroup" return
                (
                    <div>{ 
                        skosThesau:radioGroupThesauForXMLElement($teiElementNickname, $topConceptId, $teiEditor:lang, 'noLabel', 'inline', $index, $pos, $attributeValueType, $teiElementValue
                        )
                    }
                    <button id="{ $teiElementNickname }_{ $indexNo }_{ $pos }SaveButton" class="btn btn-success hidden"
                         
                                 appearance="minimal" type="button"><i class="glyphicon
         glyphicon glyphicon-ok-circle"></i></button>
                        <script>
                        $(":input[name={$teiElementNickname}_value_{$indexNo}_{$pos}]").change(function(){{
                            $("#{ $teiElementNickname }_{ $indexNo }_{ $pos }SaveButton").removeClass("hidden");
                        }})
                        $("#{ $teiElementNickname }_{ $indexNo }_{ $pos }SaveButton").on("click" , function() {{
                            saveDataRadioGroup(this, '{$currentDocId}', '{$teiElementNickname}_value_{$indexNo}_{$pos}',
                            '{$teiElementNickname}', '{$teiXPath}',
                             '{$teiElementDataType}', '{$indexNo}', {$pos});

                            $(this).addClass("hidden");
                        }})
                        
                        </script>
                        </div>
                    )
                    default return   
                    (<div>
                        <div id="{$teiElementNickname}_display_{$indexNo}_{$pos}" class="teiElement teiElementDisplay">
                            <!--<span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                                <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                                </span></span>

                                title="{normalize-space(string-join($item/string(), ""))} = concept {$item/@ref/string()}"
                                -->
                          <div id="{$teiElementNickname}_value_{$indexNo}_{$pos}"
                          class="teiElementValue">{ $value2Bedisplayed }</div>
                          <button id="edit{$teiElementNickname}_{$indexNo}_{$pos}" class="btn btn-primary editbutton pull-right"
                           appearance="minimal" type="button" onclick="editValue('{$teiElementNickname}', '{$indexNo}', '{$pos}')">{
                            if($teiEditor:appVariables//comboboxDataType/text() = "ajax") then
                               (attribute data-widgettype { "dropdownEdit" },
                                attribute data-teielementnickname {$teiElementNickname },
                                attribute data-topconceptid { $topConceptId },
                                attribute data-lang { $teiEditor:lang },
                                attribute data-label { "noLabel" },
                                attribute data-format {"inline" },
                                attribute data-index { $indexNo },
                                attribute data-subindex { $pos },
                                attribute data-attributevaluetype { $attributeValueType }
                                )
                                else ()}
                           <i class="glyphicon glyphicon-edit
                                            editConceptIcon"></i></button>
                { if("eee" = "eee")
                then (let $xpath :=if($attributeValueType = "multi-uri" or $attributeValueType = "xml-value") then
                   ($teiXPath)
                    else ($xpathBaseForCardinalityX || "//" || $selectorForCardinalityX )
                     return
                    <span class="pull-right">
                    <span>{if($valuesTotal>1) then () else ""}</span>
                    {if($pos >1 and ($valuesTotal>1)) then 
                    <a class="btn btn-xs btn-default" data-itemIndex="" onclick="moveItem('{$teiElementNickname}_group_{$indexNo}', '{ $currentDocId }', '{ $teiElementNickname}', '{ $xpath }', '{ $indexNo }', '{ $pos }', '{ $teiEditor:lang }', 'up')">
                    <i class="glyphicon glyphicon-arrow-up"/></a>
                         else ""}
                    {if($pos < $valuesTotal ) then
                    <a class="btn btn-xs btn-default" data-itemIndex="" onclick="moveItem('{$teiElementNickname}_group_{$indexNo}', '{ $currentDocId }', '{ $teiElementNickname}', '{ $xpath }', '{ $indexNo }', '{ $pos }', '{ $teiEditor:lang }', 'down')"><i class="glyphicon glyphicon-arrow-down"/></a>
                             else ""}
                    
                    
                    </span>)
                else ()
                 }

                {if($teiElementCardinality ="x") then
                <a class="removeItem" onclick="removeItemFromList('{$currentDocId}', '{$teiElementNickname}', '{functx:substring-before-last($xpathEnd, '/@')}', {$pos}, '{$topConceptId}')">
                <i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a>
                else()}
                      </div>
                      <div id="{$teiElementNickname}_edit_{$indexNo}_{$pos}" class="teiElement teiElementEdit teiElementHidden">

                      <!--
                      <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                          <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                          </span></span>
                          -->
                          
                             <span><em>{ $value2Bedisplayed }</em>
                      { if($teiEditor:appVariables//comboboxDataType/text() = "ajax") then <div class="dropDownContainerEdit"><img class="loaderImage" src="/$ausohnum-lib/resources/images/ajax-loader.gif" /></div>
                                                else () }

                             { if($teiEditor:appVariables//comboboxDataType/text() != "ajax") then
                                 skosThesau:dropDownThesauForXMLElement($teiElementNickname, $topConceptId, $teiEditor:lang, 'noLabel', 'inline', $index, $pos, $attributeValueType) 
                                 else ()
                             }
                            </span>
                            <button class="btn btn-success"
                              onclick="saveData2(this, '{$currentDocId}',
                                            '{$teiElementNickname}_{$indexNo}_{$pos}',
                                            '',
                                            '{$teiElementNickname}',
                                            '{$teiXPath}', '{$contentType}', '{$indexNo}', '{$pos}')"
                                      appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                      <button id="{$teiElementNickname}CancelEdit_{$indexNo}_{$pos}" class="btn btn-danger"
                              onclick="cancelEdit(this, '{$teiElementNickname}', '{$indexNo}', '{functx:trim(string-join($teiElementValue))}', 'thesau', '{$pos}') "
                                      appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                      </div>

                      </div>
                        )   

                     }
                        </div>
                      )}
                      </div>


                <div id="{$teiElementNickname}_add_{$indexNo}" class="teiElement teiElementAddItem teiElementHidden">
                      { if($teiEditor:appVariables//comboboxDataType/text() = "ajax") then <div class="dropDownContainerAdd"><img class="loaderImage" src="/$ausohnum-lib/resources/images/ajax-loader.gif" /></div>
                                                                                      else()}
                        {if($teiEditor:appVariables//comboboxDataType/text() != "ajax") then
                            skosThesau:dropDownThesauForXMLElement($teiElementNickname, $topConceptId, $teiEditor:lang , 'noLabel', 'inline', $index + 1, (), $attributeValueType)
                            else ()}


                        <button id="{$teiElementNickname}addNewItem" class="btn btn-success"
                        onclick="addData(this, '{$currentDocId}', '{$teiElementNickname}_add_{$indexNo}', '{$teiElementNickname}', '{$teiXPath}', '{$contentType}', '{$indexNo}', '{ $topConceptId }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                <button id="{$teiElementNickname}CancelEdit_{$indexNo}_add" class="btn btn-danger"
                        onclick="cancelAddItem(this)"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                </div>



        </div>
        )
};

(:~ Function to generate the HTML widget for a XML node edited with a dropdown list bound to a thesaurus, with concepts displayed as a hierarchical tree;
: Atribute will contains the uris of a concept and its parents, separated by whitespaces
: Deals with possible mutiple cardinality :)


declare function teiEditor:displayXmlElementWithThesauCardiWithConceptHierarchy($teiElementNickname as xs:string,
             $topConceptId as xs:string,
             $docId as xs:string?,
             $index as xs:integer?,
             $xpath_root as xs:string?) {
        let $topConceptId := if(contains($topConceptId, "http"))
                then functx:substring-after-last($topConceptId, "/")
                else $topConceptId
        let $currentDocId := if($docId != "") then $docId else  $teiEditor:docId
      
        let $doc := $teiEditor:doc-collection/id($currentDocId)    
        
        let $indexNo := if(string($index) != "") then data($index) else "1"
(:        let $elementNode := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]:)
        let $elementNode := if (exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname])) then
                        $teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]
                        else ($teiEditor:teiElements//teiElement[nm=$teiElementNickname])
        let $contentType :=$elementNode/contentType/text()
        let $teiElementDataType := $elementNode/contentType/text()
        let $teiElementFormLabel := $elementNode/formLabel[@xml:lang=$teiEditor:lang]/text()
        let $teiElementCardinality := $elementNode/cardinality/text()
        let $attributeValueType := $elementNode/attributeValueType/text()

        let $elementIndex := if($index) then ("[" || string($index) || "]" ) else ("")

        let $xpathEnd := if(contains($elementNode//xpath/text(), "/@"))
            then(functx:substring-before-last($elementNode//xpath/text(), '/') || $elementIndex || "/"
            || functx:substring-after-last($elementNode//xpath/text(), '/')
            )
            else (
            $elementNode/./xpath/text()
            )
        let $elementAncestors := $elementNode/ancestor::teiElement
        let $teiXPath := 
                    if($xpath_root != "") then $xpath_root
                else if($elementNode/ancestor::teiElement)
                    then
                        string-join(
                        for $ancestor in $elementAncestors
                        return
                        if (contains($ancestor/xpath/text(), '/@')) then
                            substring-before($ancestor/xpath/text(), '/@')
                            else $ancestor/xpath/text()
                        )
                    || $elementIndex || $xpathEnd
                    else
                        $xpathEnd
        let $xpathBaseForCardinalityX :=
               if (contains($teiXPath, "/@")) then
               (functx:substring-before-last(functx:substring-before-last($teiXPath, "/@"), '/'))
               else
                   ($teiXPath)
        let $xpathBaseForCardinalityOne :=
   (:                    Test on $contentType:)
                       if($contentType ="textNodeAndAttribute") then
                       (:(if 
                       (contains($teiXPath, "/@")) then:) 
                       substring-before($teiXPath, "/@"
                       ) else 
                        $teiXPath
        let $selectorForCardinalityX :=
               if (contains($teiXPath, "/@")) then
               (functx:substring-after-last(functx:substring-before-last($teiXPath, "/@"), "/"))
               else
                   (functx:substring-after-last($teiXPath, "/"))

    
(:    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)

    let $teiElementValue :=
        if($teiElementCardinality = "1" ) then (
            util:eval( "$doc/" || 
            $xpathBaseForCardinalityOne ))
         
         else if($teiElementCardinality = "x" ) then (
                    util:eval( "$doc/" || $xpathBaseForCardinalityX || "//" || $selectorForCardinalityX ))
         else (util:eval( "$doc/" || $teiXPath ))
    let $valuesTotal := count(data($teiElementValue))
(:    let $data2display :=
                if(($teiElementCardinality = "1" ) 
                and (util:eval( "collection('" || $teiEditor:concept-collection-path ||"')//skos:Concept[@rdf:about='" || data($teiElementValue[1]) || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']"))
                ) then (
                     data(util:eval( "collection('" || $teiEditor:concept-collection-path ||"')//skos:Concept[@rdf:about='" || $teiElementValue || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']"))
                   ) else()
:)  
(:   let $data2display :=
                if($teiElementCardinality = "1"  ) 
                    then (
                        try{
                            util:eval( "$teiEditor:concept-collection//skos:Concept[@rdf:about='" || $teiElementValue || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']")
                            }
                            catch * {""}
                          )
                     else () :)

  

let $inputName := 'selectDropDown' ||$topConceptId

    (:let $itemList :=
        util:eval( "collection('/db/apps/" || $teiEditor:project || "/data/documents')//id('"||$teiEditor:docId
                    ||"')/"
                    || functx:substring-before-last($teiXPath2Ref, '/') || "//tei:category"):)
    return

        (
        <div id="{$teiElementNickname}_group_{$indexNo}" class="teiElementGroup">
            <div class="TeiElementGroupHeaderBlock">
                <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                    <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                    </span></span>
                    { if(($teiElementCardinality ="x") or ($valuesTotal=0)) then
                    <button id="{$teiElementNickname}addItem_1_{$indexNo}" class="btn btn-primary addItem"
                        onclick="addItem(this, '{ $teiElementNickname }_add_{ $indexNo }', '{ $indexNo }')"
                                appearance="minimal" type="button">{
                            if($teiEditor:appVariables//comboboxDataType/text() = "ajax") then
                               (attribute data-widgettype {"dropdownwithhierarchyAdd" },
                                attribute data-teielementnickname {$teiElementNickname },
                                attribute data-topconceptid { $topConceptId },
                                attribute data-lang { $teiEditor:lang },
                                attribute data-label { "noLabel" },
                                attribute data-format {"inline" },
                                attribute data-index { $index + 1 },
                                attribute data-subindex { "" },
                                attribute data-attributevaluetype { $attributeValueType }
                                )
                                else ()}
                                <i class="glyphicon glyphicon glyphicon-plus">
                                </i></button>

                  else()
                     }
              </div>
              <div class="itemList" id="{$teiElementNickname}List">
              {
              for $item at $pos in $teiElementValue
                    let $itemPathEnding := if(contains($teiXPath, '/@')) then "/@" || substring-after($teiXPath, '/@')
                                        else ()
                    let $attrValues :=if($contentType = "attribute") then $item 
                        else util:eval("data($item" || $itemPathEnding || ")")                             
                    let $value2Bedisplayed:=( 
                        if (not(contains($contentType, 'text'))) 
                            then (
                               
                                if (not($attributeValueType) or $attributeValueType="uri") then
                                          util:eval( "$teiEditor:concept-collection//skos:Concept[@rdf:about='" || "" || "/string()" 
                                            || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']/text()")
                               
                                else if (not($attributeValueType) or $attributeValueType="multi-uri") then
                                    for $uri at $pos in tokenize($attrValues, " ")
                                        (: let $concept :=util:eval( "$teiEditor:concept-collection//skos:Concept[@rdf:about='" || $uri  
                                            || "']") :)
                                        let $concept :=$teiEditor:concept-collection//skos:Concept[@rdf:about=$uri]  
                                        
                                        (: let $label :=
                                            if($concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang= $teiEditor:lang]/text()!= "")
                                                then $concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang= $teiEditor:lang]/text()
                                            else if($concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang= "en"]/text() != "") 
                                                then $concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang= "en"]/text()
                                            else $concept//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][1]/text() :)
                                let $label := skosThesau:getLabel($uri, $teiEditor:lang)

                                        return (
                                            <span><a href="" title="{ $uri }" target="_blank">{ $label }</a>{
                                                if($pos < count(tokenize($attrValues, " "))) then " &gt; " else ""}</span>
                                            )
                               
                                
                                else 
                                (skosThesau:getLabelFromXmlValue($item, $teiEditor:lang))
                                    
                                    )
                           else if (($contentType ="text") and ($attributeValueType="xml-value") and (not($item[.='']))) then
                                data(util:eval( "$teiEditor:concept-collection//skos:Concept[skos:prefLabel[@xml:lang='xml']='" || $item/string() || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']/text()"))
                        else if (contains($contentType, "textNodeAndAttribute")) then "mmmr" || $item/text()

                        else(
                               $contentType || ": " || (if ($attributeValueType) then $attributeValueType
                                                            else ($item/string())       
                                                                )           
                                )
                    )
            return
              (
              <div class="itemInDisplayElement">
                      <div id="{$teiElementNickname}_display_{$indexNo}_{$pos}" class="teiElement teiElementDisplay">
                      <!--<span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                          <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                          </span></span>
                          -->
                          <div id="{$teiElementNickname}_value_{$indexNo}_{$pos}"
                          title="{normalize-space(string-join($item/string(), ""))} = concept {$item/@ref/string()}" class="teiElementValue">
                          { $value2Bedisplayed }</div>
                            <button id="edit{$teiElementNickname}_{$indexNo}_{$pos}" class="btn btn-primary editbutton pull-right"
                            appearance="minimal" type="button" onclick="editValue('{$teiElementNickname}', '{$indexNo}', '{$pos}')">{
                            if($teiEditor:appVariables//comboboxDataType/text() = "ajax") then
                               (attribute data-widgettype {"dropdownwithhierarchyEdit" },
                                attribute data-teielementnickname {$teiElementNickname },
                                attribute data-topconceptid { $topConceptId },
                                attribute data-lang { $teiEditor:lang },
                                attribute data-label { "noLabel" },
                                attribute data-format {"inline" },
                                attribute data-index { $indexNo },
                                attribute data-subindex {  $pos },
                                attribute data-attributevaluetype { $attributeValueType }
                                )
                                else ()}
                            <i class="glyphicon glyphicon-edit editConceptIcon"></i></button>
                            

                       
                        <a class="removeItem" onclick="removeItemFromList('{$currentDocId}', '{$teiElementNickname}', '{functx:substring-before-last($xpathEnd, '/@')}', {$pos}, '{$topConceptId}')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a>
                        
                      </div>
                      <div id="{$teiElementNickname}_edit_{$indexNo}_{$pos}" class="teiElement teiElementEdit teiElementHidden">
                      <em>{ $value2Bedisplayed }</em>
                      { if($teiEditor:appVariables//comboboxDataType/text() = "ajax") then <div class="dropDownContainerEdit"><img class="loaderImage" src="/$ausohnum-lib/resources/images/ajax-loader.gif" /></div>
                                                                                    else() }
                           {if($teiEditor:appVariables//comboboxDataType/text() != "ajax") then
                            skosThesau:dropDownThesauForElementWithConceptHierarchy($teiElementNickname,
                        $topConceptId, $teiEditor:lang, 'noLabel', 'inline', $indexNo, $pos, $attributeValueType)
                        else ()}
                            <button class="btn btn-success"
                              onclick="saveData2(this, '{$currentDocId}',
                                            '{$teiElementNickname}_{$indexNo}_{$pos}',
                                            '',
                                            '{$teiElementNickname}',
                                            '{$teiXPath}', '{$contentType}', '{$indexNo}', '{$pos}')"
                                      appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                      <button id="{$teiElementNickname}CancelEdit_{$indexNo}_{$pos}" class="btn btn-danger"
                              onclick="cancelEdit(this, '{$teiElementNickname}', '{$indexNo}', '{functx:trim(string-join($teiElementValue))}', 'thesau', '{$pos}') "
                                      appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                      </div>

                    </div>

                      )}
                      </div>


                <div id="{$teiElementNickname}_add_{$indexNo}" class="teiElement teiElementAddItem teiElementHidden">
                                {$attributeValueType} - { $topConceptId } { $index }
                      { if($teiEditor:appVariables//comboboxDataType/text() = "ajax") then <div class="dropDownContainerAdd"><img class="loaderImage" src="/$ausohnum-lib/resources/images/ajax-loader.gif" /></div>
                                                                                else () }
                         {if($teiEditor:appVariables//comboboxDataType/text() != "ajax") then
                           skosThesau:dropDownThesauForElementWithConceptHierarchy($teiElementNickname,
                        $topConceptId, $teiEditor:lang, 'noLabel', 'inline', $index+1, $index, $attributeValueType)
                        else ()}

                        <button id="addNewItem" class="btn btn-success"
                        onclick="addData(this, '{$docId}', '{$inputName}_add_{$indexNo}', 
                                '{$teiElementNickname}', '{$teiXPath}', '{$contentType}', '{$indexNo}', '{ $topConceptId }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                <button id="{$teiElementNickname}CancelEdit_{$indexNo}_add" class="btn btn-danger"
                        onclick="cancelAddItem(this)"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                </div>



        </div>
        )
};


(:~ Function to generate the HTML widget for a XML node edited with a dropdown list bound to a thesaurus, with concepts displayed as a hierarchical tree; Deals with possible mutiple cardinality :)
declare function teiEditor:displayXmlElementWithThesauCardiWithConceptHierarchyDISCARDED($elementNickname as xs:string,
             $topConceptId as xs:string,
             $docId as xs:string?,
             $index as xs:integer?,
             $xpath_root as xs:string?) {

        (: let $currentResourceURI := if(($resourceURI != "") and (contains($resourceURI, "http"))) then $resourceURI
            else $teiEditor:baseUri || "/documents/" || $teiEditor:docId :)
        let $indexNo := if($index) then data($index) else "1"
        let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]))) 
                then $teiEditor:teiElements//teiElement[nm=$elementNickname] 
                else $teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]
        
(:        let $elementNode := $spatiumStructor:placeElements//xmlElement[nm=$elementNickname]:)
        let $elementIndex := if($index) then ("[" || string($index) || "]" ) else ("")

        let $xpathEnd := if(matches($elementNode//xpath/text(), "/@"))
            then(
                functx:substring-before-last($elementNode//xpath/text(), '/') || $elementIndex || "/"
                || functx:substring-after-last($elementNode//xpath/text(), '/')
            )
            else (
                $elementNode/./xpath/text()
                )
        let $xpathEndAttrib := if(matches($elementNode/xpath/text(), "/@"))
                                            then(
                                                        functx:substring-after-last($elementNode/xpath/text(), '/') 
                                            )
                                            else (
                                                    $elementNode/./xpath/text()
                                                    )
       let $elementAncestors := $elementNode/ancestor::xmlElement
        let $XPath := if($elementNode/ancestor::xmlElement)
                    then
                        string-join(
                        for $ancestor in $elementAncestors
                        return
                        if (matches($ancestor/xpath/text(), '/@')) then
                            substring-before($ancestor/xpath/text(), '/@')
                            else $ancestor/xpath/text()
                        )
                    || $elementIndex || $xpathEnd
                    else
                        $xpathEnd
    let $xpathBaseForCardinalityX :=
            if (matches($XPath, "/@")) then
            (functx:substring-before-last(functx:substring-before-last($XPath, "/@"), '/'))
            else
                ($XPath)

    let $selectorForCardinalityX :=
            if (matches($XPath, "/@")) then
            (functx:substring-after-last(functx:substring-before-last($XPath, "/@"), "/"))
            else
                (functx:substring-after-last($XPath, "/"))
    let $contentType :=$elementNode/contentType/text()

    let $xpathBaseForCardinalityOne :=
   (:                    Test on $contentType:)
                       if($contentType ="textNodeAndAttribute") then
                       (:(if 
                       (contains($teiXPath, "/@")) then:) 
                       substring-before($XPath, "/@"
                       ) else 
                        $XPath
    let $contentType :=$elementNode/contentType/text()
    let $elementDataType := $elementNode/contentType/text()
    let $elementFormLabel := $elementNode/formLabel[@xml:lang=$teiEditor:lang]/text()
    let $elementCardinality := $elementNode/cardinality/text()
    let $attributeValueType := $elementNode/attributeValueType/text()

(:    let $Doc := $spatiumStructor:place-collection/id($spatiumStructor:docId):)

 
    

let $doc := $teiEditor:doc-collection/id($docId)    
    let $elementValue :=
        if($elementCardinality = "1" ) then (
            util:eval( "$doc/" || 
            $xpathBaseForCardinalityOne ))
         
         else if($elementCardinality = "x" ) then (
                    util:eval( "$doc/" || $xpathBaseForCardinalityX || "//" || $selectorForCardinalityX ))
         else (util:eval( "$doc/" || $XPath ))
    let $valuesTotal := count($elementValue)





    let $inputName := 'selectDropDown' ||$topConceptId

    (:let $itemList :=
        util:eval( "collection('/db/apps/" || $spatiumStructor:project || "/data/documents')//id('"||$spatiumStructor:docId
                    ||"')/"
                    || functx:substring-before-last($XPath2Ref, '/') || "//tei:category"):)
    let $xpathSingleQuote := replace($XPath, '"', "'")
    
    
     let $pos :="1" (:To be deleted:)
    let $value2Bedisplayed:=
        for $item at $pos in $elementValue 
                let $attributeValue := 
                                                       if($item/@rdf:resource) then data($item/@rdf:resource)
                                                          else if($item/@ref) then data($item/@ref)
                                                           else if ($item/@ana) then data($item/@ana)
                                                           else ()
                let $value := (
                               if (not(matches($contentType, 'text'))) then
                                   (
                                       if (not($attributeValueType) or $attributeValueType="uri") then 
                                           (
                                               skosThesau:getLabel($attributeValue, $teiEditor:lang)
                                            )
                                       else skosThesau:getLabel($item/string(), $teiEditor:lang)
                                   )
                               else if (($contentType ="text") and ($attributeValueType="xml-value") and (not($item[.=''])))
                                    then data(util:eval( "collection('"
                                               || $teiEditor:concept-collection-path ||"')//.[skos:prefLabel[@xml:lang='xml']='"
                                               || $item/string() || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']/text()"))
                              else if ($contentType = "textNodeAndAttribute") then 
                                       (
                                       if((data($item/@rdf:resource)) != "") then 
                                               skosThesau:getLabel(data($item/@rdf:resource), $teiEditor:lang)
                                       else $item/text() 
                                       )
                             else    ($contentType || " " || (if ($attributeValueType) then $attributeValueType else ()))
                          )
              return
              (<span>
              <span id="{$elementNickname}_value_{$indexNo}_1"
                          title="{$item/text()} = concept { $attributeValue }" class="xmlElementValue">{ $value }
              
             </span>{if($pos < count($elementValue)) then " > " else () }</span>
             )
    return

        (
        <div id="{$elementNickname}_group_{$indexNo}" class="teiElementGroup">
          <div class="teiElementGroupHeaderBlock">
            <span class="labelForm">{$elementFormLabel} <span class="xmlInfo">
                    <a title="XML element: {$XPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                    </span></span>
             { if($elementCardinality ="x") then
                    <button id="{$elementNickname}addItem_{$indexNo}" class="btn btn-primary addItem"
                        onclick="addItem(this, '{ $elementNickname }_add_{ $indexNo }', '{ $indexNo }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-plus"></i></button>



                  else()
                     }      
              </div>
              <div class="itemInDisplayElement">
                      <div id="{$elementNickname}_display_{$indexNo}_1" class="teiElement">{ $value2Bedisplayed }
                      <!--<span class="labelForm">{$elementFormLabel} <span class="xmlInfo">
                          <a title="XML element: {$XPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                          </span></span>
                          -->
                          
                          <button id="edit{$elementNickname}_{$indexNo}_1" class="btn btn-primary editbutton"
                           appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                                            editConceptIcon"></i></button>
                        { if($elementCardinality ="x") then <a class="removeItem" onclick="removeItem('{$docId}', '{$elementNickname}', '{$xpathBaseForCardinalityX}', '{ $selectorForCardinalityX }', 1)"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a>
                                else () }
                
                      </div>
                      <div id="{$elementNickname}_edit_{$indexNo}_1" class="teiElement teiElementHidden">

                      <!--
                      <span class="labelForm">{$elementFormLabel} <span class="xmlInfo">
                          <a title="XML element: {$XPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                          </span></span>
                          -->
                             {""
(:                             skosThesau:dropDownThesau($topConceptId, $spatiumStructor:lang, 'noLabel', 'inline', $index, $pos, $attributeValueType):)
                             }
                             {skosThesau:dropDownThesauForElement(
                             $elementNickname, $topConceptId, $teiEditor:lang,
                             'noLabel', 'inline', $index, $pos, $attributeValueType)}

                              <button class="btn btn-success"
                              onclick="saveDataWithConceptHierarchy(this, '{$docId}',
                              '{$inputName}',
                              '{$inputName}',
                              '{$elementNickname}',
                              '{$XPath}',
                              '{$contentType}',
                              '{$indexNo}',
                              '1')"

                                      appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>

                                      <button id="{$elementNickname}CancelEdit_{$indexNo}_1" class="btn btn-danger"
                              onclick="cancelEdit(this, '{$elementNickname}', '{$indexNo}', '{functx:trim(string-join($elementValue[1]/text(), " "))}', 'thesau', '1') "
                                      appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>

                      </div>

                      </div>

                      


                <div id="{$elementNickname}_add_{$indexNo}" class="teiElement teiElementAddItem teiElementHidden">

                        {""
(:                        skosThesau:dropDownThesau($topConceptId, 'en', 'noLabel', 'inline', $index + 1, (), ()):)
                        }
                        {skosThesau:dropDownThesauForElement($elementNickname, $topConceptId, $teiEditor:lang, 'noLabel', 'inline', $index+1, (), ())}

                        <button id="addNewItem" class="btn btn-success"
                        onclick="addData(this, '{$docId}', '{$inputName}_add_{$indexNo}', 
                                '{$elementNickname}', '{$XPath}', '{$contentType}', '{$indexNo}', '{ $topConceptId }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                <button id="{$elementNickname}CancelEdit_{$indexNo}_add" class="btn btn-danger"
                        onclick="cancelEdit(this, '{$inputName}_add_{$indexNo}', '{$indexNo}', '{$elementValue}', 'thesau', {$valuesTotal +1}) "
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                </div>



        </div>
        )
};

(:~ Function to generate the HTML widget for a XML node edited with a dropdown menu AND a input search field, both bound to a thesaurus.
 : This function does deal with multiple cardianlity; :)
declare function teiEditor:displayTeiElementWithThesauComboInputCardi($teiElementNickname as xs:string,
             $topConceptUri as xs:string,
             $docId as xs:string?,
             $index as xs:integer?,
             $xpath_root as xs:string?) {
        let $topConceptId := functx:substring-after-last($topConceptUri, '/')
        let $currentDocId := if($docId != "") then $docId else  $teiEditor:docId
        let $doc := $teiEditor:doc-collection/id($currentDocId)
        let $indexNo := if(string($index) != "") then data($index) else "1"
(:        let $elementNode := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]:)
        let $elementNode := if (exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname])) then
                        $teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]
                        else ($teiEditor:teiElements//teiElement[nm=$teiElementNickname])
        let $contentType :=$elementNode/contentType/text()
        let $teiElementDataType := $elementNode/contentType/text()
        let $teiElementFormLabel := $elementNode/formLabel[@xml:lang=$teiEditor:lang]/text()
        let $teiElementCardinality := $elementNode/cardinality/text()
        let $attributeValueType := $elementNode/attributeValueType/text()

        let $elementIndex := if($index) then ("[" || string($index) || "]" ) else ("")

        let $xpathEnd := if(contains($elementNode//xpath/text(), "/@"))
            then(functx:substring-before-last($elementNode//xpath/text(), '/') || $elementIndex || "/"
            || functx:substring-after-last($elementNode//xpath/text(), '/')
            )
            else (
            $elementNode/./xpath/text()
            )
        let $elementAncestors := $elementNode/ancestor::teiElement
        let $teiXPath := if($elementNode/ancestor::teiElement)
                    then
                        string-join(
                        for $ancestor in $elementAncestors
                        return
                        if (contains($ancestor/xpath/text(), '/@')) then
                            substring-before($ancestor/xpath/text(), '/@')
                            else $ancestor/xpath/text()
                        )
                    || $elementIndex || $xpathEnd
                    else
                        $xpathEnd
        let $xpathBaseForCardinalityX :=
               if (contains($teiXPath, "/@")) then
               (functx:substring-before-last(functx:substring-before-last($teiXPath, "/@"), '/'))
               else
                   ($teiXPath)
        let $xpathBaseForCardinalityOne :=
   (:                    Test on $contentType:)
                       if($contentType ="textNodeAndAttribute") then
                       (:(if 
                       (contains($teiXPath, "/@")) then:) 
                       substring-before($teiXPath, "/@"
                       ) else 
                        $teiXPath
        let $selectorForCardinalityX :=
               if (contains($teiXPath, "/@")) then
               (functx:substring-after-last(functx:substring-before-last($teiXPath, "/@"), "/"))
               else
                   (functx:substring-after-last($teiXPath, "/"))

    
(:    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)

    let $teiElementValue :=
        if($teiElementCardinality = "1" ) then (
            util:eval( "$doc/" || 
            $xpathBaseForCardinalityOne ))
         
         else if($teiElementCardinality = "x" ) then (
                    util:eval( "$doc/" || $xpathBaseForCardinalityX || "//" || $selectorForCardinalityX ))
         else (util:eval( "$doc/" || $teiXPath ))
    let $valuesTotal := count($teiElementValue)
    let $data2display :=
                if(($teiElementCardinality = "1" ) 
                and (util:eval( "$teiEditor:concept-collection//skos:Concept[@rdf:about='" || data($teiElementValue[1]) || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']"))) then (
                     data(util:eval( "$teiEditor:concept-collection//skos:Concept[@rdf:about='" || $teiElementValue || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']"))
                   ) else()
    let $inputName := 'selectDropDown' ||$topConceptId

    (:let $itemList :=
        util:eval( "collection('/db/apps/" || $teiEditor:project || "/data/documents')//id('"||$teiEditor:docId
                    ||"')/"
                    || functx:substring-before-last($teiXPath2Ref, '/') || "//tei:category"):)
    return

        (
        <div id="{$teiElementNickname}_group_{$indexNo}" class="teiElementGroup">
        <div class="TeiElementGroupHeaderBlock">
            <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                    <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                    </span></span>
                    { if($teiElementCardinality ="x") then
                    <button id="{$teiElementNickname}addItem_1_{$indexNo}" class="btn btn-primary addItem"
                        onclick="addItem(this, '{ $teiElementNickname }_add_{ $indexNo }', '{ $indexNo }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-plus"></i></button>

                  else()
                     }
              </div>
              <div class="itemList" id="{$teiElementNickname}List"> 
              {
              for $item at $pos in $teiElementValue
              let $itemPathEnding := if(contains($teiXPath, '/@')) then "/@" || substring-after($teiXPath, '/@')
                                                  else ()
                                                  
              let $value2Bedisplayed:= 
                        if (not(contains($contentType, 'text'))) 
                            then (
                                            if (not($attributeValueType) or $attributeValueType="uri") then
                                                     data(util:eval( "$teiEditor:concept-collection//skos:Concept[@rdf:about='" || $item || $itemPathEnding || "/string()" 
                                                     || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']/text()"))
                                           else 
                                            (skosThesau:getLabelFromXmlValue($item, $teiEditor:lang))
                                    
                                    )
                        else if (($contentType ="text") and ($attributeValueType="xml-value") and (not($item[.='']))) then
                       data(util:eval( "$teiEditor:concept-collection//skos:Concept[skos:prefLabel[@xml:lang='xml']='" || $item/string() || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']/text()"))
                        else if (contains($contentType, "textNodeAndAttribute")) then  $item/text()

                        else(
                               $contentType || " " || (if ($attributeValueType) then $attributeValueType
                                                                else ($item/string())       
                                                                )           
                                )
              return
              (
              <div class="itemInDisplayElement">
                      <div id="{$teiElementNickname}_display_{$indexNo}_{$pos}" class="teiElement teiElementDisplay">
                      <!--<span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                          <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                          </span></span>
                          -->
                          <div id="{$teiElementNickname}_value_{$indexNo}_{$pos}"
                          title="{normalize-space(($item[1]/text()))} = concept {$item/@ref/string()}" class="teiElementValue">{ $value2Bedisplayed }</div>
                          <!--
                          <button id="edit{$teiElementNickname}_{$indexNo}_{$pos}" class="btn btn-primary editbutton pull-right"
                           onclick="editValue('{$teiElementNickname}', '{$indexNo}', '{$pos}')"
                                  appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                                            editConceptIcon"></i></button>
                            -->

<a class="removeItem" onclick="removeItemFromList('{$currentDocId}', '{$teiElementNickname}', '{functx:substring-before-last($xpathEnd, '/@')}', {$pos}, '{$topConceptId}')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a>

                      </div>
                      <div id="{$teiElementNickname}_edit_{$indexNo}_{$pos}" class="teiElement teiElementEdit teiElementHidden">

                      <!--
                      <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                          <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                          </span></span>
                          -->
                             Current value: { $value2Bedisplayed }<br/>
                             {
                            skosThesau:dropDownThesauForElement($teiElementNickname, $topConceptId, $teiEditor:lang, 'noLabel', 'inline', $index, $pos, $attributeValueType)
                             }
                             
                             { skosThesau:conceptLookup($topConceptUri, "... or search", string($pos))}

                              <button class="btn btn-success"
                              onclick="saveNewKeyword('{$currentDocId}',
                                            '{$teiElementNickname}_{$indexNo}_{$pos}',
                                            '',
                                            '{$teiElementNickname}',
                                            '{$teiXPath}', '{$contentType}', '{$indexNo}', '{$pos}')"
                                      appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                      <button id="{$teiElementNickname}CancelEdit_{$indexNo}_{$pos}" class="btn btn-danger"
                              onclick="cancelEdit(this, '{$teiElementNickname}', '{$indexNo}', '{functx:trim($teiElementValue[1]/text())}', 'thesau', '{$pos}') "
                                      appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                      </div>

                      </div>

                      )}
                      </div>


                <div id="{$teiElementNickname}_add_{$indexNo}" class="teiElement teiElementAddItem teiElementHidden">
                        Add a new value:<br/>
                        {skosThesau:dropDownThesauForXMLElement($teiElementNickname, 
                        $topConceptId, 'en', 'noLabel', 'inline', $index + 1, (), 'uri')}
                        
                        { skosThesau:conceptLookup($topConceptUri, "or search...", "add" ||$indexNo)}

                        <button id="{$teiElementNickname}addNewItem" class="btn btn-success"
                        onclick="addDataComboAndInput(this, '{$currentDocId}', '{$teiElementNickname}_add_{$indexNo}', '{$teiElementNickname}', '{$teiXPath}', '{$contentType}', '{$indexNo}', '{ $topConceptId }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                <button id="{$teiElementNickname}CancelEdit_{$indexNo}_add" class="btn btn-danger"
                        onclick="cancelAddItem(this)"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                </div>



        </div>
        )
};

(:~ Function not used  :)
declare function teiEditor:displayTeiElementWithThesauInputCardi($teiElementNickname as xs:string,
             $topConceptUri as xs:string,
             $docId as xs:string?,
             $index as xs:integer?,
             $xpath_root as xs:string?) {
        let $topConceptId := functx:substring-after-last($topConceptUri, '/')
        let $currentDocId := if($docId != "") then $docId else  $teiEditor:docId
        let $indexNo := if(string($index) != "") then data($index) else "1"
(:        let $elementNode := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]:)
        let $elementNode := if (exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname])) then
                        $teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]
                        else ($teiEditor:teiElements//teiElement[nm=$teiElementNickname])
        let $contentType :=$elementNode/contentType/text()
        let $teiElementDataType := $elementNode/contentType/text()
        let $teiElementFormLabel := $elementNode/formLabel[@xml:lang=$teiEditor:lang]/text()
        let $teiElementCardinality := $elementNode/cardinality/text()
        let $attributeValueType := $elementNode/attributeValueType/text()

        let $elementIndex := if($index) then ("[" || string($index) || "]" ) else ("")

        let $xpathEnd := if(contains($elementNode//xpath/text(), "/@"))
            then(functx:substring-before-last($elementNode//xpath/text(), '/') || $elementIndex || "/"
            || functx:substring-after-last($elementNode//xpath/text(), '/')
            )
            else (
            $elementNode/./xpath/text()
            )
        let $elementAncestors := $elementNode/ancestor::teiElement
        let $teiXPath := if($elementNode/ancestor::teiElement)
                    then
                        string-join(
                        for $ancestor in $elementAncestors
                        return
                        if (contains($ancestor/xpath/text(), '/@')) then
                            substring-before($ancestor/xpath/text(), '/@')
                            else $ancestor/xpath/text()
                        )
                    || $elementIndex || $xpathEnd
                    else
                        $xpathEnd
        let $xpathBaseForCardinalityX :=
               if (contains($teiXPath, "/@")) then
               (functx:substring-before-last(functx:substring-before-last($teiXPath, "/@"), '/'))
               else
                   ($teiXPath)
        let $xpathBaseForCardinalityOne :=
   (:                    Test on $contentType:)
                       if($contentType ="textNodeAndAttribute") then
                       (:(if 
                       (contains($teiXPath, "/@")) then:) 
                       substring-before($teiXPath, "/@"
                       ) else 
                        $teiXPath
        let $selectorForCardinalityX :=
               if (contains($teiXPath, "/@")) then
               (functx:substring-after-last(functx:substring-before-last($teiXPath, "/@"), "/"))
               else
                   (functx:substring-after-last($teiXPath, "/"))

    
(:    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)

    let $teiElementValue :=
        if($teiElementCardinality = "1" ) then (
            util:eval( "$teiEditor:doc-collection/id('"||$currentDocId ||"')/" || 
            $xpathBaseForCardinalityOne ))
         
         else if($teiElementCardinality = "x" ) then (
                    util:eval( "$teiEditor:doc-collection/id('"|| $currentDocId ||"')/" || $xpathBaseForCardinalityX || "//" || $selectorForCardinalityX ))
         else (util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"|| $currentDocId ||"')/" || $teiXPath ))
    let $valuesTotal := count($teiElementValue)
    let $data2display :=
                if(($teiElementCardinality = "1" ) 
                and (util:eval( "collection('" || $teiEditor:concept-collection-path ||"')//skos:Concept[@rdf:about='" || data($teiElementValue[1]) || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']"))) then (
                     data(util:eval( "collection('" || $teiEditor:concept-collection-path ||"')//skos:Concept[@rdf:about='" || $teiElementValue || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']"))
                   ) else()
    let $inputName := 'selectDropDown' ||$topConceptId

    (:let $itemList :=
        util:eval( "collection('/db/apps/" || $teiEditor:project || "/data/documents')//id('"||$teiEditor:docId
                    ||"')/"
                    || functx:substring-before-last($teiXPath2Ref, '/') || "//tei:category"):)
    return

        (
        <div id="{$teiElementNickname}_group_{$indexNo}" class="teiElementGroup">
        <div class="TeiElementGroupHeaderBlock">
            <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                    <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                    </span></span>
                    { if($teiElementCardinality ="x") then
                    <button id="{$teiElementNickname}addItem_1_{$indexNo}" class="btn btn-primary addItem"
                        onclick="addItem(this, '{ $teiElementNickname }_add_{ $indexNo }', '{ $indexNo }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-plus"></i></button>

                  else()
                     }
              </div>
              <div class="itemList" id="{$teiElementNickname}List"> 
              {
              for $item at $pos in $teiElementValue
              let $itemPathEnding := if(contains($teiXPath, '/@')) then "/@" || substring-after($teiXPath, '/@')
                                                  else ()
                                                  
              let $value2Bedisplayed:= 
                        if (not(contains($contentType, 'text'))) 
                            then (
                                            if (not($attributeValueType) or $attributeValueType="uri") then
                                                     data(util:eval( "collection('" || $teiEditor:concept-collection-path 
                                                     ||"')//skos:Concept[@rdf:about='" || $item || $itemPathEnding || "/string()" 
                                                     || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']/text()"))
                                           else 
                                            (skosThesau:getLabelFromXmlValue($item, $teiEditor:lang))
                                    
                                    )
                        else if (($contentType ="text") and ($attributeValueType="xml-value") and (not($item[.='']))) then
                       data(util:eval( "collection('" || $teiEditor:concept-collection-path ||"')//skos:Concept[skos:prefLabel[@xml:lang='xml']='" || $item/string() || "']//skos:prefLabel[not(ancestor-or-self::skos:exactMatch)][@xml:lang='en']/text()"))
                        else if (contains($contentType, "textNodeAndAttribute")) then  $item/text()

                        else(
                               $contentType || " " || (if ($attributeValueType) then $attributeValueType
                                                                else ($item/string())       
                                                                )           
                                )
              return
              (
              <div class="itemInDisplayElement">
                      <div id="{$teiElementNickname}_display_{$indexNo}_{$pos}" class="teiElement teiElementDisplay">
                      <!--<span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                          <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                          </span></span>
                          -->
                          <div id="{$teiElementNickname}_value_{$indexNo}_{$pos}"
                          title="{normalize-space(($item[1]/text()))} = concept {$item/@ref/string()}" class="teiElementValue">{ $value2Bedisplayed }</div>
                          <button id="edit{$teiElementNickname}_{$indexNo}_{$pos}" class="btn btn-primary editbutton pull-right"
                           appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                                            editConceptIcon"></i></button>


<a class="removeItem" onclick="removeItemFromList('{$currentDocId}', '{$teiElementNickname}', '{functx:substring-before-last($xpathEnd, '/@')}', {$pos}, '{$topConceptId}')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a>

                      </div>
                      <div id="{$teiElementNickname}_edit_{$indexNo}_{$pos}" class="teiElement teiElementEdit teiElementHidden">

                      <!--
                      <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                          <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                          </span></span>
                          -->
                             Current value: { $value2Bedisplayed }<br/>
                             
                             { skosThesau:conceptLookup($topConceptUri, "... or search", string($pos))}

                              <button class="btn btn-success"
                              onclick="saveNewKeyword('{$currentDocId}',
                                            '{$teiElementNickname}_{$indexNo}_{$pos}',
                                            '',
                                            '{$teiElementNickname}',
                                            '{$teiXPath}', '{$contentType}', '{$indexNo}', '{$pos}')"
                                      appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                      <button id="{$teiElementNickname}CancelEdit_{$indexNo}_{$pos}" class="btn btn-danger"
                              onclick="cancelEdit(this, '{$teiElementNickname}', '{$indexNo}', '{functx:trim($teiElementValue[1]/text())}', 'thesau', '{$pos}') "
                                      appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                      </div>

                      </div>

                      )}
                      </div>


                <div id="{$teiElementNickname}_add_{$indexNo}" class="teiElement teiElementAddItem teiElementHidden">
                        Add a new value:<br/>
                        
                        
                        { skosThesau:conceptLookup($topConceptUri, "or search...", "add" ||$indexNo)}

                        <button id="{$teiElementNickname}addNewItem" class="btn btn-success"
                        onclick="addDataComboAndInput(this, '{$currentDocId}', '{$teiElementNickname}_add_{$indexNo}', '{$teiElementNickname}', '{$teiXPath}', '{$contentType}', '{$indexNo}', '{ $topConceptId }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                                <button id="{$teiElementNickname}CancelEdit_{$indexNo}_add" class="btn btn-danger"
                        onclick="cancelAddItem(this)"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                </div>



        </div>
        )
};

(:~ Function not used?  :)
declare function teiEditor:displayPlace($elementNickName as xs:string, $docId as xs:string?, $index as xs:int?, $teiXPath){

    let $elementNode := $teiEditor:teiElements//teiElement[nm=$elementNickName]
     let $elementFormLabel := $elementNode/formLabel[@xml:lang=$teiEditor:lang]/text()
    let $currentDocId := if($docId != "") then $docId else $teiEditor:docId
    let $placeRef :=
         (data(
            util:eval( "$teiEditor:doc-collection/id('" ||$currentDocId ||"')/" || $teiXPath)))
    let $numberOfPlaceRef := count(tokenize($placeRef, ' '))
    let $placeProjectUri := for $uri in tokenize($placeRef, ' ')
                                    where contains($uri,  $teiEditor:baseUri)
                                return functx:trim($uri)
    let $placeRecord := 
    util:eval( "collection('" || $teiEditor:data-repository-path || "/places')//pleiades:Place[@rdf:about ='" || $placeProjectUri || "']")
(:    $teiEditor:placeRepo//pleiades:Place[@rdf:about = $placeProjectUri]:)
    let $placePrefLabel := $placeRecord//skos:prefLabel[@xml:lang="en"]/text()
    let $placeRefsAsLink := <ul class="list-inline">
                                            {for $uri in tokenize($placeRef, ' ')

                                            return
                                                 <li class="list-inline-item"><a class="uriAsLink" href="{ $uri }" target="_blank">{ $uri }</a></li>
                                                }
                                            </ul>
    let $element2Display :=
    <div class="teiElementGroup">

                    <div class="itemInDisplayElement">
                    <div class="TeiElementGroupHeader">
                            <span class="labelForm">{$elementFormLabel} <span class="teiInfo">
                                    <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                                </span>
                                </span>
                      </div>
                             <div id="{$elementNickName}_display_{$index}_1" class="teiElement teiElementDisplay">
                                <div id="{$elementNickName}_value_{$index}_1" title="{ $placeRef }" class="teiElementValue">{ $placePrefLabel } {$placeRefsAsLink}
                                </div>
                                <button id="edit{$elementNickName}" class="btn btn-primary editbutton pull-right" 
                                        appearance="minimal" type="button"><i class="glyphicon glyphicon-edit editConceptIcon"></i></button>
                             </div>

                             <div id="{$elementNickName}_edit_{ $index }_1" class="teiElementHidden teiElementEdit form-group">
                             <select id="{$elementNickName}_{$index}_1" name="{$elementNickName}_{$index}_">
                         {for $items in $teiEditor:placeRepo//pleiades:Place
                            order by $items//skos:prefLabel[@xml:lang='en']
                            return
                                if ($items/@rdf:about = $placeProjectUri)
                                then (<option value="{$items/@rdf:about}{
                                    if($items//skos:exactMatch) then ' ' || concat(data($items//skos:exactMatch/@rdf:resource), ' ') else ()}"
                                    textValue="{$items//skos:prefLabel[@xml:lang='en']}"
                                    selected="selected">
                                    {$items//skos:prefLabel[@xml:lang='en']} {data($items/@rdf:about)}</option>)
                                        else (
                                <option value="{$items/@rdf:about}{if($items//skos:exactMatch) then ' ' || concat(data($items//skos:exactMatch/@rdf:resource), ' ') else ()}"
                                textValue="{$items//skos:prefLabel[@xml:lang='en']}">{$items//skos:prefLabel[@xml:lang='en']} {data($items/@rdf:about)}</option>
                                )
                         }</select>
<button id="save{ $elementNickName }" class="btn btn-success"
                onclick="saveData2(this, '{$currentDocId}', '{$elementNickName}_{$index}_1', '{$elementNickName}_{$index}_1', '{$elementNickName}', '{$teiXPath}', 'textNodeAndAttribute', '{ $index }', '1')"
                        appearance="minimal" type="button"><i class="glyphicon glyphicon-ok-circle"></i></button>
                        <button id="{$elementNickName}CancelEdit" class="btn btn-danger"
                onclick="cancelEdit(this, '{$elementNickName}', '{ $index }', '', 'taxo', '1') "
                        appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-remove-circle"></i></button>
                             </div>

                    </div>
                    </div>
return

    $element2Display




};

(:~ Function to generate the HTML widget related to document provenance  :)
declare function teiEditor:docProvenance() {
    let $elementNode := $teiEditor:teiElements//teiElement[nm="fragProvenanceFound"]
    let $teiXPath := $elementNode/xpath/text()
    let $teiElementNickname := $elementNode/nm/text()

    let $teiElementDataType := $elementNode/contentType/text()
    let $teiElementFormLabel := $elementNode/formLabel/text()
    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId)
    let $teiElementValue :=
         (util:eval( "collection('" || $teiEditor:doc-collection-path ||"')//id('"||$teiEditor:docId ||"')/" || $teiXPath))

    return
        <div>
        <h4>Provenance (findspot)</h4>
        
        {if (exists($teiElementValue/text())) then
        
            (
            <div id="{$teiElementNickname}-display" class="teiElement teiElementDisplay">
                <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
                    <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                    </span>
                 </span>
            <div id="{$teiElementNickname}-value" class="teiElementValue">{$teiElementValue}</div>
            <button id="edit{$teiElementNickname}" class="btn btn-xs btn-primary editbutton pull-right"
             appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                              editConceptIcon"></i></button>
        </div>)
        else("de")
        }
         <div id="{$teiElementNickname}-edit" class="teiElement teiElementHidden">
        <span class="labelForm">{$teiElementFormLabel} <span class="teiInfo">
            <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
            </span></span>
                <input id="{$teiElementNickname}" class="" name="{$teiElementNickname}" value="{$teiElementValue}"></input>
                <button id="saveTitleStmt" class="btn btn-primary"
                onclick="saveData('{$teiEditor:docId}', {$teiElementNickname}, '{$teiElementDataType}' '{$teiXPath}')"
                        appearance="minimal" type="button"><i class="glyphicon glyphicon-floppy-save"></i></button>

        </div>
        </div>
};


(:
*************************************
*            BIBLIOGRAPHY           *
*************************************:)

(:~ Function to display bibliographical reference as a tag  :)
declare function teiEditor:displayBibRef($docId as xs:string, $bibRef as node(), $refType as xs:string, $index as xs:int){
    let $targetType := if(starts-with($bibRef/tei:ptr/@target, "#")) then "ref" else "uri"
    let $bibId := if( $targetType ="ref") then substring(data($bibRef[1]/tei:ptr/@target), 2)
                                    else data($bibRef/tei:ptr/@target)
    let $teiBibRef := if( $targetType ="ref") then $teiEditor:biblioRepo/id($bibId)
                                else $teiEditor:biblioRepo//tei:biblStruct[@corresp = $bibId]
    let $authorLastName := <span class="lastname">{ 
                if($teiBibRef[1]//tei:author[1]/tei:surname) then 
                        if(count($teiBibRef[1]//tei:author) = 1) then data($teiBibRef[1]//tei:author[1]/tei:surname)
                        else if(count($teiBibRef[1]//tei:author) = 2) then data($teiBibRef[1]//tei:author[1]/tei:surname) || " &amp; " || data($teiBibRef[1]//tei:author[2]/tei:surname)
                        else if(count($teiBibRef[1]//tei:author) > 2) then  <span>{ data($teiBibRef[1]//tei:author[1]/tei:surname)} <em> et al.</em></span>
                        else ()
                        
                
                
                else if ($teiBibRef[1]//tei:editor[1]/tei:surname) then
                            if(count($teiBibRef[1]//tei:editor) = 1) then data($teiBibRef[1]//tei:editor[1]/tei:surname)
                        else if(count($teiBibRef[1]//tei:editor) = 2) then data($teiBibRef[1]//tei:editor[1]/tei:surname) || " &amp; " || data($teiBibRef[1]//tei:editor[2]/tei:surname)
                        else if(count($teiBibRef[1]//tei:editor) > 2) then  <span>{ data($teiBibRef[1]//tei:editor[1]/tei:surname)} <em> et al.</em></span>
                        else ()
                        
                else ("[no name]")
                }</span>
    let $date := data($teiBibRef[1]//tei:imprint/tei:date)
    let $citedRange :=if($bibRef//tei:citedRange != "") then
                                   
                                     if (starts-with(data($bibRef[1]//tei:citedRange), ',')) 
                                     then data($bibRef[1]//tei:citedRange)
                                     else (', ' || data($bibRef[1]//tei:citedRange))
                                  else ()
    let $suffixLetter := 
    if (matches(
    substring(data($teiBibRef[1]/@xml:id), string-length(data($teiBibRef[1]/@xml:id))),
    '[a-z]'))
    then substring(data($teiBibRef[1]/@xml:id), string-length(data($teiBibRef[1]/@xml:id)))
    else ''
(:    if (matches(functx:substring-after-last-match($teiBibRef/@xml:id, [0-9]), [a-z])) then functx:substring-after-last-match($teiBibRef/@xml:id, [0-9]) else ""    :)
    let $ref2display :=    if($teiBibRef[1]//tei:title[@type="short"]) then
            (
                
               data($teiBibRef[1]//tei:title[@type="short"]) || substring-after(data($citedRange[1]), ',')
            )
            else (
                
                $authorLastName  || " " || $date || $suffixLetter || $citedRange[1] 

            ) 

    return

    <span class="bibRef"><a href="{data($teiBibRef[1]/@corresp)}" target="_blank" class="btn btn-primary">{$ref2display}</a>
    { if ( $refType != "info") then 
    <a class="removeItem btn btn-primary" onclick="removeItemFromList('{$docId}', '{ $refType }Biblio', '{$bibId}', '{ $index }')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a> else ()}
    {if($targetType ="ref") then ("️ Please change to URI") else ()}
    
    </span>

};

(:~ Function to generate the HTML button to take place in the semantic annotator  :)
declare function teiEditor:displayPersonItem($docId as xs:string, $personUri as xs:string?, $index as xs:string?){
            let $personId := functx:substring-after-last($personUri, '/')
            let $personRecord := $teiEditor:peopleRepo/id($personId)
            let $names := string-join($personRecord//persName, ' ')
(:          order by $personRecord/persName:)

            return
            <span class="btn btn-light listItem">

            <span title="Annotate selected text with reference to {functx:trim(normalize-space($names))}"
            onclick="addPeople({$index}, '{$personUri}')">{$names}</span>
            <a class="removeItem" onclick="removeItemFromList('{$teiEditor:docId}', 'people', '{$personUri}', {$index})"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a></span>

};

(:~ Function to generate the HTML widget related to Edition .
 : Prefer teiEditor:bibliographyPanel() :)
declare function teiEditor:bibliographyEdition($docId as xs:string?){
let $teiDoc := util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" || $docId ||"')" )
(:   let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)

   return
   <div class="teiElementGroup">

   <div class="TeiElementGroupHeaderBlock">
   <span class="labelForm">Editions</span>
   <button id="docBilioAddItem" class="btn btn-primary addItem" onclick="openBiblioDialog()" appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-plus"></i></button>


   </div>
      <div id="mainBiblioList" class="itemList">
   {for $bibRef at $pos in $teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype='edition']/tei:listBibl//tei:bibl
   order by $bibRef//tei:ptr/@target
    return
    teiEditor:displayBibRef($teiEditor:docId, $bibRef, "main", $pos)
(:    teiEditor:displayBibRef($teiEditor:docId, substring(data($bibRef/tei:ptr/@target), 2)):)
   }
   </div>



    <!--Dialog for Biblio-->
    <div id="dialogInsertBiblio" title="Add a Bibliographical Reference" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Insert a bibliographical reference</h4>
                </div>
                <div class="modal-body">
                      <form id="addBiblioForm" role="form" data-toggle="validator" novalidate="true">
                            <div class="form-group">
                                <label for="nameLookupInputModal">Search in <a href="https://www.zotero.org/groups/{$teiEditor:appVariables//zoteroGroup/text()}" target="_blank">Zotero Group {$teiEditor:appVariables//zoteroGroup/text()}</a>
                                </label>
                                <input type="text" class="form-control zoteroLookup" id="zoteroLookupInputModal" name="zoteroLookupInputModal" autocomplete="on"
                                placeholder="Start to enter a author name or a word..."/>
                            </div>
                            <div class="form-group">
                                <label for="citedRange">Cited Range
                                </label>
                                <input type="text" class="form-control" id="citedRange" name="citedRange" 
                                data-error="Please enter your full name."/>
                                 
                            </div>
                            <div id="zoteroGroupNo" class="hidden">{$teiEditor:appVariables//zoteroGroup/text()}</div>
                            <div id="selectedBiblioAuthor" class="lookupSelectionPreview selectedBiblioAuthor"/>
                            <div id="selectedBiblioDate" class="lookupSelectionPreview selectedBiblioDate"/>
                            <div id="selectedBiblioTitle" class="lookupSelectionPreview selectedBiblioTitle"/>
                            <div id="selectedBiblioUri" class="lookupSelectionPreview selectedBiblioUri"/>
                            <div id="selectedBiblioId" class="lookupSelectionPreview selectedBiblioId"/>


                    <div class="form-group modal-footer">


                        <button  class="pull-left" type="submit" onclick="addBiblioRef('{$teiEditor:docId}', '{$teiEditor:appVariables//zoteroGroup/text()}', 'main')">Add reference</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                  </form>
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>

            </div>


    </div>

   </div>

};

(:~ Function to generate the HTML widget related to General bibliography.
 : Prefer teiEditor:bibliographyPanel() :)
declare function teiEditor:principalBibliography($docId as xs:string?){
let $teiDoc := util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" || $docId ||"')" )
(:   let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)

   return
   <div class="teiElementGroup">

   <div class="TeiElementGroupHeaderBlock">
   <span class="labelForm">General Bibliography</span>
   <button id="docBilioAddItem" class="btn btn-primary addItem" onclick="openBiblioDialog()" appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-plus"></i></button>


   </div>
      <div id="mainBiblioList" class="itemList">
   {for $bibRef at $pos in $teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype='edition']/tei:listBibl//tei:bibl
   order by $bibRef//tei:ptr/@target
    return
    teiEditor:displayBibRef($teiEditor:docId, $bibRef, "main", $pos)
(:    teiEditor:displayBibRef($teiEditor:docId, substring(data($bibRef/tei:ptr/@target), 2)):)
   }
   </div>



    <!--Dialog for Biblio-->
    <div id="dialogInsertBiblio" title="Add a Bibliographical Reference" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Insert a bibliographical reference</h4>
                </div>
                <div class="modal-body">
                      <form id="addBiblioForm" role="form" data-toggle="validator" novalidate="true">
                            <div class="form-group">
                                <label for="nameLookupInputModal">Search in <a href="https://www.zotero.org/groups/{$teiEditor:appVariables//zoteroGroup/text()}" target="_blank">Zotero Group {$teiEditor:appVariables//zoteroGroup/text()}</a>
                                </label>
                                <input type="text" class="form-control zoteroLookup" id="zoteroLookupInputModal" name="zoteroLookupInputModal" placeholder="Start to enter a author name or a word..."/>
                            </div>
                            <div class="form-group">
                                <label for="citedRange">Cited Range
                                </label>
                                <input type="text" class="form-control" id="citedRange" name="citedRange" 
                                data-error="Please enter your full name."/>
                                 
                            </div>
                            <div id="zoteroGroupNo" class="hidden">{$teiEditor:appVariables//zoteroGroup/text()}</div>
                            <div id="selectedBiblioAuthor" class="lookupSelectionPreview selectedBiblioAuthor"/>
                            <div id="selectedBiblioDate" class="lookupSelectionPreview selectedBiblioDate"/>
                            <div id="selectedBiblioTitle" class="lookupSelectionPreview selectedBiblioTitle"/>
                            <div id="selectedBiblioUri" class="lookupSelectionPreview selectedBiblioUri"/>
                            <div id="selectedBiblioId" class="lookupSelectionPreview selectedBiblioId"/>


                    <div class="form-group modal-footer">


                        <button  class="pull-left" type="submit" onclick="addBiblioRef('{$teiEditor:docId}', '{$teiEditor:appVariables//zoteroGroup/text()}', 'main')">Add reference</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                  </form>
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>

            </div>


    </div>

   </div>

};

declare function teiEditor:bibliographyPanel($docId as xs:string?, $type as xs:string){
    teiEditor:bibliographyPanel($docId, $type, "", "author")
};
declare function teiEditor:bibliographyPanel($docId as xs:string?, $type as xs:string, $zoteroTag as xs:string?){
    teiEditor:bibliographyPanel($docId, $type, $zoteroTag, "author")
};
(:~ Generic function to generate the HTML widget related to the Editions of a document or to general bibliography  :)
declare function teiEditor:bibliographyPanel($docId as xs:string?, $type as xs:string, $zoteroTag as xs:string?, $orderingType as xs:string?){
let $teiDoc := util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" || $docId ||"')" )
(:   let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)
let $fieldLabel := switch ($type)
                            case "edition" return teiEditor:label("biblio-edition", "Editions")
                            case "reference" return teiEditor:label("biblio-reference", "References")
                            case "primary" return teiEditor:label("biblio-primaryBiblio", "Primary bibliography")
                            case "secondary" return teiEditor:label("biblio-secondaryBiblio", "Secondary bibliography")
                            default return "Bibliography"
   let $xpath := '/tei:div[@type="bibliography"][@subtype="' || $type || '"]/tei:listBibl'
   return
   <div id="biblioPanelContainer{ $fieldLabel }">
        <div class="teiElementGroup">

        <div class="TeiElementGroupHeaderBlock">
        <span class="labelForm">{ $fieldLabel }</span>
        <button id="doc{ $type }BiblioAddItem" class="btn btn-primary addItem" onclick="openDialog('dialogInsert{ $type }Biblio')" appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-plus"></i></button>


        </div>
                { teiEditor:bibliographyPanelRefList($teiDoc, $docId, $type, $xpath, $zoteroTag, $orderingType) }

    <!--Dialog for Biblio-->
    <div id="dialogInsert{ $type }Biblio" title="Add a Bibliographical Reference" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Insert a bibliographical reference</h4>
                </div>
                <div class="modal-body">
                      <form id="add{ $type }BiblioForm" class="addBiblioForm" role="form" data-toggle="validator" novalidate="true">
                            <div class="form-group">
                                <label for="nameLookupInputModal">Search in <a href="https://www.zotero.org/groups/{$teiEditor:appVariables//zoteroGroup/text()}" target="_blank">Zotero Group {$teiEditor:appVariables//zoteroGroup/text()}</a>
                                </label>
                                <input type="text" class="form-control zoteroLookup" id="zoteroLookupInputModal{ $type }" name="zoteroLookupInputModal" 
                                placeholder="Start to enter a author name or a word..." data-zoterotag="{ $zoteroTag }"/>
                                <img class="loaderImage hidden" src="/$ausohnum-lib/resources/images/ajax-loader.gif" /><br/>
                            </div>
                            <div class="form-group">
                                <label for="citedRange">Cited Range
                                </label>
                                <input type="text" class="form-control" id="citedRange{ $type }" name="citedRange" 
                                data-error="Please enter your full name."/>
                                 
                            </div>
                            <div id="zoteroGroupNo" class="hidden">{ $teiEditor:appVariables//zoteroGroup/text() }</div>
                            <div id="biblioSearchType{ $type }" class="">{ $teiEditor:appVariables//biblioSearchType/text() }</div>
                            <div id="selectedBiblioAuthor{ $type }" class="selectedBiblioAuthor"/>
                            <div id="selectedBiblioDate{ $type }" class="selectedBiblioDate"/>
                            <div id="selectedBiblioTitle{ $type }" class="selectedBiblioTitle"/>
                            <div id="selectedBiblioUri{ $type }" class="selectedBiblioUri"/>
                            <div id="selectedBiblioId{ $type }" class="selectedBiblioId"/>
                    
                    </form>
                    </div>
                    <div class="modal-footer">


                        <button  class="pull-left" type="submit" onclick="addBiblioRef('{$teiEditor:docId}', '{$teiEditor:appVariables//zoteroGroup/text()}', '{ $type }')">Add reference</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                  
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>

            </div>
        </div>
    </div>
};

declare function teiEditor:bibliographyPanelRefList($teiDoc as node(), $docId as xs:string, $type as xs:string?, $xpath as xs:string?, $zoteroTag as xs:string?, $orderingType as xs:string?){
    if ($type ="edition") then  
                  <div id="{ $type }BiblioList" class="itemList">
                                {for $bibRef at $pos in $teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype=$type]/tei:listBibl//tei:bibl
                                       
                                 return
                                 <span>
                                 { teiEditor:displayBibRef($docId, $bibRef, $type, $pos) }
                                  {if($pos > 1 and (count($teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype=$type]/tei:listBibl//tei:bibl)>1)) then 
                    <a class="btn btn-xs btn-primary" data-itemIndex="" onclick="moveBiblioItem('{ $type }BiblioList', '{ $docId }', '{ $type }', '{ $zoteroTag }',  '{ $pos }', '{ $teiEditor:lang }', 'up')">
                    <i class="glyphicon glyphicon-arrow-up"/></a>
                         else ""}
                         {if($pos < (count($teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype=$type]/tei:listBibl//tei:bibl)
                        )) then 
                    <a class="btn btn-xs btn-primary" data-itemIndex="" onclick="moveBiblioItem('{ $type }BiblioList', '{ $docId }', '{ $type }', '{ $zoteroTag }',  '{ $pos }', '{ $teiEditor:lang }', 'down')">
                    <i class="glyphicon glyphicon-arrow-down"/></a>
                         else ""}
                         </span>
                             (:    teiEditor:displayBibRef($teiEditor:docId, substring(data($bibRef/tei:ptr/@target), 2)):)
                                }
               </div>
                    else (
                         <div id="{ $type }BiblioList" class="itemList">
                        {for $bibRef at $pos in $teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype=$type]/tei:listBibl//tei:bibl
                                (: order by  :)
                                       (: $bibRef//tei:ptr/@target :)
                                
                                return
                                <span>
                                     { teiEditor:displayBibRef($docId, $bibRef, $type, $pos) }
                                     {if($pos > 1 and (count($teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype=$type]/tei:listBibl//tei:bibl)>1)) then 
                    <a class="btn btn-xs btn-primary" data-itemIndex="" onclick="moveBiblioItem('{ $type }BiblioList', '{ $docId }', '{ $type }', '{ $zoteroTag }',  '{ $pos }', '{ $teiEditor:lang }', 'up')">
                    <i class="glyphicon glyphicon-arrow-up"/></a>
                         else ""}
                        {if($pos < (count($teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype=$type]/tei:listBibl//tei:bibl)
                        )) then 
                    <a class="btn btn-xs btn-primary" data-itemIndex="" onclick="moveBiblioItem('{ $type }BiblioList', '{ $docId }', '{ $type }', '{ $zoteroTag }',  '{ $pos }', '{ $teiEditor:lang }', 'down')">
                    <i class="glyphicon glyphicon-arrow-down"/></a>
                         else ""}
                    
                                      </span>
                                    (:    teiEditor:displayBibRef($teiEditor:docId, substring(data($bibRef/tei:ptr/@target), 2)):)
                                       }
                      </div>
                      )
            };

(:~ Function to generate the HTML widget displaying the places linked to a document. Use teiEditor:placesManager() to make possible addition of places to a document :)
declare function teiEditor:placesList($docId as xs:string?, $teiDoc as node()?){
            (:let $teiDoc := $teiEditdor:doc-collection/id($docId):)
            let $teiDoc := if(empty($teiDoc)) then util:eval( "$teiEditor:doc-collection/id('"||$docId ||"')" )
                else $teiDoc
            let $places := $teiDoc//tei:sourceDesc/tei:listPlace//tei:place
            
            return
            <div class="xmlElementGroup">
                <span class="subSectionTitle">{ teiEditor:label("teiEditor-placesManager-listOfPlaces", "List of places linked to this document") } ({count($places)})</span>
                <div id="listOfPlacesOverview" class="listOfPlaces">
                    <ul>{ for $place at $pos in $places
                            let $placeName := $place/tei:placeName/string()
                            let $placeUris := data($place/tei:placeName/@ref)
                            let $placeUriInternal :=
                                for $uri in tokenize($placeUris, " ")
                                return 
                                    if (contains($uri, $teiEditor:project)) then $uri else ()
                            let $placeStatus := data($place/tei:placeName/@ana)
                                                => skosThesau:getLabelFromXmlValue($teiEditor:lang)
                            let $placeStatusRaw := data($place/tei:placeName/@ana)
                            let $placeStatus := $placeStatusRaw => skosThesau:getLabelFromXmlValue($teiEditor:lang)
                            (:let $placeStatus2 := teiEditor:displayElement("placeStatus", $docId, $pos, '/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listPlace/tei:place/tei:placeName['|| $pos ||']')       
                                    order by $place/tei:placeName:)
                            let $placeRecord:= if($placeUriInternal != "") then $teiEditor:projectPlaceCollection//pleiades:Place[@rdf:about = $placeUriInternal][1] else()
                            let $comment := switch($placeStatusRaw)
                                            case "origin"
                                            return $teiDoc//tei:origPlace[tei:placeName[contains(./@ref, $placeUriInternal)]]/tei:note/text()
                                            case "provenance" 
                                            return $teiDoc//tei:history//tei:provenance[tei:placeName[contains(./@ref, $placeUriInternal)]]/tei:note/text()

                                            default return ()
                            let $placeCommentElementNickname :=
                                            "placeComment" || functx:capitalize-first($placeStatusRaw)
                                                                
                            return
                                <li class="placeInList">
                                    <a href="{ $placeUriInternal }" title="Open details of { $placeUriInternal }" target="_self">
                                    {$placeRecord[1]//dcterms:title[1]/text()}</a>
                                    
                                    <span class="geoLat hidden">{$placeRecord[1]//geo:lat/text()}</span>
                                    <span class="geoLong hidden">{$placeRecord[1]//geo:long/text()}</span>
                                    <!--
                                    { teiEditor:displayElement("placeStatus", $docId, $pos, '/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listPlace/tei:place/tei:placeName['|| $pos ||']')}
                                    -->
                                    <span style="font-size: smaller!important;">[{$placeStatus}]</span>
                                    <a href="{ $placeUriInternal }" title="Open details of { $placeUriInternal } in a new window" target="_blank">
                                        <i class="glyphicon glyphicon-new-window"/></a>
                                        <a class="removeItem" onclick="removeItemFromList('{$docId}', 'place', '{$placeUriInternal}', {$pos}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a>
                                        {if($teiEditor:appVariables//placeCommentsActive/text() = "true" and ($placeStatusRaw = "origin" or $placeStatusRaw ="provenance"))
                                            then teiEditor:displayElement($placeCommentElementNickname, $docId, (), ())
                                            else ()}
                                </li>
                            }</ul>
                            </div>
                            </div>
};
declare function teiEditor:placesList($docId as xs:string?){
    teiEditor:placesList($docId, ())
};
(:~ Function to generate the HTML widget displaying the places linked to a document, without header. Use teiEditor:placesManager() to make possible addition of places to a document :)
declare function teiEditor:placesListNoHeader($docId as xs:string){
            (:let $teiDoc := $teiEditdor:doc-collection/id($docId):)
            let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"
                         ||$docId ||"')" )
            
            
            return
            
                <div id="listOfPlacesOverview" class="listOfPlaces">
                            <ul>{
                            
                    for $place at $pos in $teiDoc//tei:sourceDesc/tei:listPlace//tei:place
                                let $placeName := $place/tei:placeName/text()
                                let $placeUris := data($place/tei:placeName/@ref)
                                let $placeUriInternal :=
                                    for $uri in tokenize($placeUris, " ")
                                    return 
                                        if (contains($uri, $teiEditor:project)) then $uri else ()
                                let $placeStatusRaw := data($place/tei:placeName/@ana)
                                let $placeStatus := $placeStatusRaw => skosThesau:getLabelFromXmlValue($teiEditor:lang)
                                let $comment := switch($placeStatusRaw)
                                            case "origin"
                                            return $teiDoc//tei:origPlace[tei:placeName[contains(./@ref, $placeUriInternal)]]/tei:note/text()
                                            case "provenance" 
                                            return $teiDoc//tei:history//tei:provenance[tei:placeName[contains(./@ref, $placeUriInternal)]]/tei:note/text()

                                            default return ()
                                let $placeCommentElementNickname :=
                                            "placeComment" || functx:capitalize-first($placeStatusRaw)
                                        order by $place/tei:placeName
                                        return
                                        <li><a href="{ $placeUriInternal }" title="Open details of { $placeUriInternal }" target="_self">
                                        {$placeName}</a>
                                        <span style="font-size: smaller!important;">[{$placeStatus}]</span>
                                        <a href="{ $placeUriInternal }" title="Open details of { $placeUriInternal } in a new window" target="_blank">
                                <i class="glyphicon glyphicon-new-window"/></a>
                                            <a class="removeItem" onclick="removeItemFromList('{$docId}', 'place', '{$placeUriInternal}', {$pos}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a>
                                           {if($teiEditor:appVariables//placeCommentsActive/text() = "true" and ($placeStatusRaw = "origin" or $placeStatusRaw ="provenance"))
                                            then teiEditor:displayElement($placeCommentElementNickname, $docId, (), ())
                                            else ()}
                                        </li>
                            }</ul>
                            </div>
};

(:~ Function to generate the HTML widget to deal with places linked to a document. :)
declare function teiEditor:placesManager($docId){
let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" )
        let $placeOfOrigin := $teiDoc//tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origPlace
      
      return
        <div class="row">
                 <div class="sideToolPane col-sm-5 col-md-5 col-lg-5">
                     <div class="panel panel-default">
                          <div class="panel-body">
                          <h3>{ teiEditor:label("teiEditor-placesManager-title", "List of places linked to this document")}</h3>
                          {teiEditor:placesList($docId, $teiDoc)}
                          
                      <div id="add-place" class="btn-group xmlToolBar"
                            role="group" aria-label="..." >
                        <div class="panel panel-default">
                           <div class="panel-heading collapsed"  data-toggle="collapse"  href="#add-places-panel">{ teiEditor:label("teiEditor-placesManager-linkPlace", "Link a place to the document") }</div>
                           <div id="add-places-panel" class="panel-collapse">
                                <div class="panel-body">
                                <input type="text" class="form-control hidden"
                                     id="newPlaceUri" 
                                     name="newPlaceUri"
                                     />
                                <span class="subSectionTitle">{ teiEditor:label("teiEditor-placesManager-searchProjectPlaces", "Search project's places") }</span>
                                <input type="text" class="form-control projectPlacesLookUp" id="projectPlacesLookUp" name="projectPlacesLookUp" autocomplete="on"
                                placeholder='{ teiEditor:label("teiEditor-placesManager-searchPlaceholder", "Start to enter a place name or a place ID") }'/>
                                <span id="projectPlaceDetailsPreview"/>
                               <span id="newProjectPlaceTypeContainer" class="hidden">
                               {if($teiEditor:appVariables//placeRelationToDocTypesUri/text() !="") then skosThesau:dropDownThesauXML($teiEditor:appVariables//placeRelationToDocTypesUri/text(), $teiEditor:lang, 'Type', 'inline', 1, 1, 'uri')
                               else "Error. Variable placeRelationToDocTypesUri is not set in the application general paramaters"}
                               </span>
                               <button id="addProjectPlaceButtonDocPlaces" class="btn btn-success hidden" onclick="addProjectPlaceToDoc('{$teiEditor:docId}')" appearance="minimal" type="button">{ teiEditor:label("teiEditor-placesManager-addPlaceButton", "Add place to document ")}<i class="glyphicon glyphicon glyphicon-saved"></i></button>
                               <!-- 
                                <div id="placeLookUpPanel" class="sectionInPanel"><span class="subSectionTitle">Add from Pelagios datasets</span>
                             <div class="form-group">
                             
                                    <label for="placesLookupInputDocPlaces">Search in <a href="http://pelagios.org/peripleo/map" target="_blank">Pelagios Peripleo</a></label>
                                     <input type="text" class="form-control"
                                     id="placesLookupInputDocPlaces" 
                                     name="placesLookupInputDocPlaces"
                                     />
                                     
                              </div>
                       <div class="">
                             <iframe id="placesLookupInputDocPlaces_peripleoWidget" allowfullscreen="true" height="380" src="" style="display:none;"> </iframe>
                                     <div id="previewMapDocPlaces" class="hidden"/>
                                     <div id="placePreviewPanelDocPlaces" class="hidden"/>
                                     <span id="newPlaceTypeContainer" class="hidden">
                                     <h4>Add place to document</h4>
                               {skosThesau:dropDownThesauXML('c22114', 'en', 'Relation', 'row', (), 1, ())}
                               </span>
                                     <button id="addNewPlaceButtonDocPlaces" class="btn btn-success hidden" onclick="addPlaceToDoc('{$teiEditor:docId}')" appearance="minimal" type="button">Add <i class="glyphicon glyphicon glyphicon-saved"></i></button>
                         </div>
                     </div>-->
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </div>
        </div>
        <div class="col-sm-7 col-md-7 col-lg-7">                                      
            <div id="editorMap"></div>
            <div id="positionInfo"/>
            <div id="savedPositionInfo">{ teiEditor:label("teiEditor-placesManager-printCurrentPositionMap", "Click to store current position: ") }</div>
                   <!--
                   <div id="mapid"></div>
                   -->
        </div>
    </div>
};

(:~ Function to generate the HTML widget dealing with people linked to a document.  :)
declare function teiEditor:peopleList($docId as xs:string){
let $teiDoc := $teiEditor:doc-collection/id($docId)
(:let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"||$docId ||"')" ):)
let $peopleInDoc := $teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person


return
<div class="xmlElementGroup listOfPeople">
                         <span class="subSectionTitle">List of people linked to this document ({count($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person)})</span>
                         <button class="btn btn-sm btn-primary pull-right" onclick="openDialog('dialogAddPeopleToDoc')"><i class="glyphicon glyphicon-plus"/>person</button>
                         <div id="listOfPeople">
<ul>{""
(:if(count($peopleInDoc) > 50) then ("Only 50 persons are listed here"):)
(:else ():)
}
{
    for $person at $pos in $peopleInDoc
(:            where $pos < 50:)
           
            let $personUris := data($person/@corresp)
            let $personUriInternal :=
                for $uri in tokenize($personUris, " ")
                return 
                    if (contains($uri, $teiEditor:project)) then $uri else ()
             let $personUriInternalLong := $personUriInternal || "#this"       
            let $personDetails := $teiEditor:peopleCollection//lawd:person[@rdf:about=$personUriInternalLong]
            let $persName := if($personDetails//lawd:personalName[@xml:lang="en"]) then $personDetails//lawd:personalName[@xml:lang="en"]/text() else $personDetails//lawd:personalName[1]/text()
            
            let $juridicalStatus := skosThesau:getLabel($personDetails//apc:juridicalStatus/@rdf:resource, $teiEditor:lang)
            let $personStatus := skosThesau:getLabel($personDetails//apc:personalStatus/@rdf:resource, $teiEditor:lang)
            let $personRank := skosThesau:getLabel($personDetails//apc:socialStatus/@rdf:resource, $teiEditor:lang)
                    order by $person/tei:persName
                    return
                    <li><a href="{ $personUriInternal }" title="Open details of { $personUriInternal }" target="_self">
                    {$persName}</a>
                     <span>{ if($personDetails//apc:juridicalStatus/text()) then "[" || $juridicalStatus || "]" else ()}</span>
                    <span>{ if($personDetails//apc:personalStatus/text()) then "[" || $personStatus || "]" else ()}</span>
                    { if($personDetails//skos:exactMatch) then 
                        (
                        let $uri := data($personDetails//skos:exactMatch[1]/@rdf:resource)
                        return
                            <span style="font-size: smaller;">[
                                <a href="{ $uri }" title="Open details of { $uri } in a new window" target="_blank">
                                    { if(contains($uri, "trism")) then "TM " || substring-after($uri, "person/") else $uri }
                                </a> ] 
                    </span>)
                        else ()}
                   <a href="{ $personUriInternal }" title="Open details of { $personUriInternal } in a new window" target="_blank">
           <i class="glyphicon glyphicon-new-window"/></a>
           <a class="removeItem"
                                          onclick="removeItemFromList('{$docId}', 'people', '{$personUriInternal}', {$pos}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a>
                      <!--  <i class="glyphicon glyphicon-trash" title="Remove place from list"/>-->
                    </li>
}</ul>
</div></div>
                                };
(:~ Function to generate the HTML widget dealing with people linked to a document, without header  :)
declare function teiEditor:peopleListLight($docId as xs:string){
(:let $teiDoc := $teiEditor:doc-collection/id($docId):)
let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"||$docId ||"')" )
let $peopleCollection := util:eval( "$teiEditor:peopleCollection" )
let $peopleList := $teiDoc//tei:profileDesc/tei:listPerson[@type="peopleInDocument"]

return
<div class="xmlElementGroup listOfPeople">
                         <span class="subSectionTitle">List of people linked to this document ({count($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person)})</span>
                         <button class="btn btn-sm btn-primary pull-right" onclick="openDialog('dialogAddPeopleToDoc')"><i class="glyphicon glyphicon-plus"/>person</button>
                         <div id="listOfPeople">
                         {""
(:                         if(count($peopleList//tei:person) > 100) then ("Only 200 persons are listed here") else ():)
                         }
<ul>
{
    for $person at $pos in $peopleList//tei:person
(:    where $pos <50:)
(:            let $persName := data($person/tei:persName):)
            let $personUris := data($person/@corresp)
            let $personUriInternal :=
                for $uri in tokenize($personUris, " ")
                    return 
                        if (contains($uri, $teiEditor:project)) then $uri else ()
            let $personDetails := $peopleCollection//lawd:person[@rdf:about=$personUriInternal || "#this"]        
(:            let $personDetails := $teiEditor:peopleCollection//apc:people[@rdf:about=$personUriInternal ]:)
           let $persName := $personDetails//lawd:personalName/text()
           let $personStatus := $personDetails//apc:personalStatus/text()
           let $personFunction := $personDetails//apc:hasFunction[1]
                let $personFunctionType := switch($personFunction/@rdf:resource)
                    case "https://ausohnum.huma-num.fr/concept/c23690" return "administrator"
                    case "https://ausohnum.huma-num.fr/concept/c23687" return "georgos"
                    case "https://ausohnum.huma-num.fr/concept/c23688" return "mistothes"
                    default return "unknown fonction"
           let $personFunctionTarget := data($personFunction/@target)
           let $targetName := try{$teiEditor:placeCollection//pleiades:Place[@rdf:about = $personFunctionTarget]//dcterms:title/text()} catch * {<error/>}         
                    order by $person/tei:persName
                    return
                    <li>
                    <a href="{ $personUriInternal }" title="Open details of { $personUriInternal }" target="_self">
                    {$persName}</a>
                    <span>{ if($personDetails//apc:juridicalStatus/text()) then "[" || $personDetails//apc:juridicalStatus/text() || "]" else ()}</span>
                    <span>{ if($personDetails//apc:personalStatus/text()) then "[" || $personDetails//apc:personalStatus/text() || "]" else ()}</span>
                    <span>{ if($personFunction)
                            then <span>{$personFunctionType } of { $targetName } (<a href="{ $personFunctionTarget }" target="_blank">{substring-after($personFunctionTarget, "/places/")}</a>]</span> else ()}</span>
                            
                            { if($personDetails//skos:exactMatch) then 
                        (
                        let $uri := data($personDetails//skos:exactMatch[1]/@rdf:resource)
                        return
                            <span style="font-size: smaller;">[
                                <a href="{ $uri }" title="Open details of { $uri } in a new window" target="_blank">
                                    { if(contains($uri, "trism")) then "TM " || substring-after($uri, "person/") else $uri }
                                </a> ] 
                    </span>)
                        else ()}
                    <a href="{ $personUriInternal }" title="Open details of { $personUriInternal } in a new window" target="_blank">
           <i class="glyphicon glyphicon-new-window"/></a>
           <a class="removeItem"
                                          onclick="removeItemFromList('{$docId}', 'people', '{$personUriInternal}', {$pos}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></a>
                      <!--  <i class="glyphicon glyphicon-trash" title="Remove place from list"/>-->
                    </li>
}</ul>
</div></div>
                                };
(:~ Function to generate the HTML widget dealing with people linked to a document, with indication of mentino in doc.
 : Function developped during the process of cleaning up of the Egyptian material  :)
declare function teiEditor:peopleMentionsInDoc($docId as xs:string){


let $teiDoc := $teiEditor:doc-collection/id($docId)
let $file := doc( "/db/apps/patrimoniumData/egyptianMaterial/people/mentions-in-texts.xml")
let $mentions := $file//file[@apcd = $docId ]

(:let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"||$docId ||"')" ):)


return
<div id="listOfMentionedNames" class="xmlElementGroup">
                         <span class="subSectionTitle">List of the {count($mentions//mention)} mentions of a name in the document (line / word position)</span>
                         <div>
                         {""
(:                         if(count($mentions//mention) > 100) then ("Only 100 persons are listed here") else ():)
                         }
<ul>{
    for $mention at $pos in $mentions//mention
(:            where $pos < 50:)
            let $personTm := data($mention/@corresp)
            let $personUri := data($mention/@apc) 
            
           let $persName := $mention/persName[@type="regularized"]/text()
           
           
(:                    order by $person/tei:persName:)
                    return
                    <li><span style="font-size: smaller;">{ data($mention/@row)} / { data($mention/@token)}</span> 
                    <a href="{ $personUri }" title="Open details of { $personUri }" target="_self">
                    {$persName}</a>
                    <span style="font-size: smaller;">[<a href="{ $personUri }" title="Open details of { $personUri }" target="_blank">apc {substring-after($personUri, "/people/")}</a>]</span>
                    <span style="font-size: smaller;">[<a href="{ $personTm }" title="Open details of { $personTm }" target="_blank">TM {substring-after($personTm, "/person/")}</a>]</span>
                    
                    <a href="{ $personUri }" title="Open details of { $personUri } in a new window" target="_blank">
           <i class="glyphicon glyphicon-new-window"/></a>
           {if( $teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person[@corresp = $personUri]) then 
           <a class="removeItem" onclick="removeItemFromList('{$docId}', 'people', '{$personUri}', '', '')" title="Remove corresponding person"><i class="glyphicon glyphicon-trash"/><span style="font-size: smaller; vertical-align: super;">Remove corresponding person</span></a>
           else()}
                    </li>
}</ul>
</div></div>
                                };




(:
*************************************
*            TEXT EDITOR            *
*************************************:)

(:~ Function to generate the HTML widget dealing with the tet preview toolbar :)
declare function teiEditor:previewToolBar($index as xs:int?){
        let $optionalButtons :=
                    <span><button id="displayfunctionButton" class="btn btn-default btn-xs"
                    onclick="displaySemanticAnnotations('function', {$index})"><i class="glyphicon glyphicon-eye-open"/>&#160;function ♦ </button>
                    <button id="displayeventsButton" class="btn btn-default btn-xs"
                    onclick="displaySemanticAnnotations('subject', {$index})"><i class="glyphicon glyphicon-eye-open"/>&#160;keyword</button>
                    </span>
        return
<div class="previewToolbar">
                    <button id="displayplaceButton" class="btn btn-default btn-xs"
                    onclick="displaySemanticAnnotations('place', {$index})"><i class="glyphicon glyphicon-eye-open"/>&#160;places ⌘</button>
                    <button id="displaypersonButton" class="btn btn-default btn-xs"
                    onclick="displaySemanticAnnotations('person', {$index})"><i class="glyphicon glyphicon-eye-open"/>&#160;people 👤</button>
                    
                    {if( $teiEditor:mode = "edition") then $optionalButtons else()}
                    
                    
                    </div>
                    };
(:~ Function to generate the HTML widget dealing with the tet preview toolbar :)
declare function teiEditor:previewToolBar($index as xs:int?, $options as item()*){
        <div class="previewToolbar">
            {for $option in $options
                let $icon := switch($option)
                    case "place" return "⌘"
                    case "people" return "👤"
                    default return "♦"
                return
                    <button id="display{ $option }Button" class="btn btn-default btn-xs"
                    onclick="displaySemanticAnnotations('{ $option }', {$index})"><i class="glyphicon glyphicon-eye-open"/>&#160;{ teiEditor:label("textPreviewOption-"|| $option , $option) } { $icon } </button>
            }</div>
                    };
declare function teiEditor:textEditor($docId as xs:string, $editorType as xs:string?){
    teiEditor:textEditor($docId, $editorType, ())
};
(:~ Function to generate the HTML TEI Editor.  :)
declare function teiEditor:textEditor($docId as xs:string, $editorType as xs:string?, $previewOptions as item()*){
(:    let $xslCleanDiv := xs:anyURI("xmldb:exist:///db/apps/ausohnum-library/xslt/cleanTextEdition.xsl"):)
    let $teiDoc := $teiEditor:doc-collection/id($docId)
(:    let $teiDoc := util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" ||$docId ||"')" ):)

    let $teiElementNickname :=
                if (exists($teiDoc//div[@type="textpart"])) then ('docTextSingle')
                    else if (not(exists($teiDoc//div[@type="textpart"]))) then ('docTextSingle')
                    else ('docTextSingle')

    
    let $elementNode := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]
    let $teiElementNode := if (exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname])) then
                        $teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]
                        else ($teiEditor:teiElements//teiElement[nm=$teiElementNickname])
    let $teiXPath := $teiElementNode//teiElement[nm=$teiElementNickname]/xpath/text()


    let $params :=
        <output:serialization-parameters
        xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
            <output:omit-xml-declaration value="yes"/>
            <output:method value="xml"/>
            <output:indent value="yes"/>
            <output:item-separator value="\n"/>
            <output:undeclare-prefixes value="yes"/>
        </output:serialization-parameters>
    let $paramMap :=
        map {
            "method": "xml",
            "version": "1.0",
            "indent": false(),
            "item-separator": ""
            }

    return
    <div class="editionPane">
    {if(count($teiDoc//tei:div[@type='textpart']) > 12) then <h5>⚠ Only 12 on a total of {count($teiDoc//tei:div[@type='textpart'])} textparts are displayed ⚠</h5> else ()}
        {

           (:else if($teiEditor:teiDoc//tei:div[@type='edition']//tei:div[@type='textpart']):)
           if($teiDoc/tei:text/tei:body//tei:div[@type= "edition"]//tei:div[@type='textpart'])
            then
           (
           for $textPart at $index in $teiDoc//tei:div[@type='edition']//tei:div[@type='textpart']
(:                where $index < 13:)
            let $surface := data($teiDoc/id(substring(data($textPart/@corresp), 2))/tei:desc/text())
            
            let $text := replace(
                replace(
                                functx:trim(
                                    serialize(
                                        functx:change-element-ns-deep(
                                    $teiDoc//tei:div[@type="textpart"][$index]/tei:ab/child::node()
                                    , '', '')
                                    , $paramMap)),
                        '&#9;', ''), 
                        "><lb ", "&gt;&#13;&lt;lb ")
            
            (:let $textOLD := <div><?xml-model href="http://www.stoa.org/epidoc/schema/latest/tei-epidoc.rng" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="http://www.stoa.org/epidoc/schema/latest/tei-epidoc.rng" schematypens="http://purl.oclc.org/dsdl/schematron"?>
              {normalize-space(serialize(functx:change-element-ns-deep($teiEditor:teiDoc//tei:div[@type="textpart"][$index]/tei:ab, '', '')/node(), $paramMap))}</div>
:)
            return


                <div class="textpartPane" id="editionPane-{$index}">

                    <h3> 
                        {
(:                        This is for surface:)
                        if($surface) then "" || $surface
                        else ()}
                        {
(:                        Temporary solution taking only @subtype and @n from div[@type="textpart']:)
                        if($textPart/@subtype) then "" || data($textPart/@subtype) || " " || data($textPart/@n)
                        else ()}
                        {
(:                        Temporary solution taking only @subtype and @n from div[@type="textpart']:)
                        if($textPart/@subtype) then <div id="textPartLabel{$index}" class="hidden">{if($index>1) then <br/> else()}<strong>{data($textPart/@subtype) || " " || data($textPart/@n)}</strong>{if($index=1) then <br/> else()}</div>
                        else ()}
                    </h3>
                    <div id="editionAlert{$index}" class="textModifiedAlert">


                    <div class="pull-left" id="textModifiedAlert{$index}">Text has been modified</div>
                    <textarea id="changeComment{$index}" placeholder="Enter a short description of your changes (optional)"></textarea>
                    <button id="saveTextButton{$index}" class="saveTextButton btn btn-primary" onclick="saveText('{$teiEditor:docId}', {$index})" appearance="minimal" type="button"><i class="glyphicon glyphicon-floppy-save"></i></button>
                    </div>
                    <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                                {teiEditor:annotationMenuEpigraphy($index)}
                                {teiEditor:textConverter($index)}
                     {switch($editorType)
                     case "side"
                        return ( 
                     <div>
                     <div class="editorColumn col-xs-6 col-sm-6 col-md-6">
                        <h4>XML Editor {$index}</h4>
                        <div id="xml-editor-{$index}" class="xmlEditor">{$text}</div>
                        </div><!--
                        {teiEditor:displaySurface()}
                        -->
                        <div class="previewPane col-xs-6 col-sm-6 col-md-6">
                            <h4>Preview {teiEditor:previewToolBar($index, $previewOptions)}</h4>
                                <div id="textPreviewHTML-{$index}" class="textPreviewHTML"/>
                        </div></div>
                        )
                        case "top"
                            return
                            ( <div>
                     
                     <div class="editorColumn col-xs-12 col-sm-12 col-md-12">
                        <h4>XML Editor {$index}</h4>
                        <div id="xml-editor-{$index}" class="xmlEditor">{$text}</div>
                        </div><!--
                        {teiEditor:displaySurface()}
                        -->
                        <div class="previewPane col-xs-12 col-sm-12 col-md-12">
                            <h4>Preview {teiEditor:previewToolBar($index)}</h4>
                                <div id="textPreviewHTML-{$index}" class="textPreviewHTML"/>
                        </div>
                        </div>
                        )
                          default return
                          ( 
                     <div>
                            <div class="editorColumn col-xs-6 col-sm-6 col-md-6">
                               <h4>XML Editor {$index}</h4>
                               <div id="xml-editor-{$index}" class="xmlEditor">{$text}</div>
                               </div><!--
                               {teiEditor:displaySurface()}
                               -->
                               <div class="previewPane col-xs-6 col-sm-6 col-md-6">
                                   <h4>Preview {teiEditor:previewToolBar($index)}</h4>
                                       <div id="textPreviewHTML-{$index}" class="textPreviewHTML"/>
                               </div>
                        </div>)
                        }
                        </div>
                        <!--
                        <div class="currentXMLElement col-xs-2 col-sm-2 col-md-2">
                            <h5>Info on XML selection</h5>
                            <div id="current-xml-element-{$index}" />
                        </div>
                        -->
                    </div>
                    </div>
            )
             (:if (not(exists($teiDoc//tei:div[@type="edition"]//tei:div[@type="textpart"]))) then:)
         else if (not(exists($teiDoc//tei:div[@type="textpart"]))) then
           (
            let $text := functx:trim(serialize(functx:change-element-ns-deep($teiDoc//tei:div/tei:ab, '', '')/node(), $paramMap))

                return
                        <div class="textpartPane" id="editionPane-1">
                        <div class="alert alert-info">
                        *** This document has no textpart ***
                         </div>
                         {teiEditor:textConverter(1)}
                            <!--
                            <button id="callTextImport-1"  onclick="openTextImporter(1)" class="btn btn-default pull-right" data-target="#dialogTextImport">Import text</button>
                             -->
                            <div id="editionAlert1" class="textModifiedAlert">

                            <div class="pull-left" >Text has been modified</div>
                            <textarea id="changeComment1" placeholder="Enter a short description of your changes (optional)"></textarea>
                            <button id="saveTextButton1" class="saveTextButton btn btn-primary" onclick="javascript:saveText('{$teiEditor:docId}', 1)" appearance="minimal" type="button"><i class="glyphicon glyphicon-floppy-save"></i></button>
                            </div>
                            {teiEditor:annotationMenuEpigraphy(1)}
                            <div id="xml-editor-1" class="xmlEditor">{$text}</div>
                        <!--
                        {normalize-space(serialize(functx:change-element-ns-deep($teiEditor:teiDoc//tei:div[@type="edition"]/tei:ab, '', '')/node(), $paramMap))}
                        {normalize-space(functx:trim(serialize(functx:change-element-ns-deep($teiDoc//tei:div[@type="edition"]/tei:ab, '', '')/node(), $paramMap)))}
                        {serialize(functx:change-element-ns-deep($teiDoc//tei:div[@type="edition"]/tei:ab, '', ''), $paramMap)}
                        -->
                        <div class="previewpanel col-xs-10 col-sm-10 col-md-10">
                                 <h4>Preview {teiEditor:previewToolBar(1)}
                                 </h4>

                                 <div id="textPreviewHTML-1" class="textPreviewHTML col-xs-8 col-sm-8 col-md-8"/>
                                 <div id="pseudoLeiden-editor-1" class="textPreviewHTML hidden"/>
                        </div>
                                 <div class="col-xs-2 col-sm-2 col-md-2">
                                 <h4>Current selection</h4>
                                  <div id="current-xml-element-1" />
                                 </div>
                        </div>
           )
            else()
            }
            <div id="currentEditorIndexVariable"/>
            <div id="editionDivForLoading" class="hidden">{
                        if(count($teiDoc//tei:div[@type="edition"]//tei:div[@type="textpart"]) >12)
                            then <div type="edition">{$teiDoc//tei:div[@type="edition"]//tei:div[@type="textpart"][position() <13 ]}</div>
                            else $teiDoc//tei:div[@type="edition"]}</div>
    </div>



    };
    
(:~ Function to generate the HTML widget displaying the text edition emmbed in the text editor. No cardinality :)
declare function teiEditor:textPreview($docId as xs:string){
    <div class="textPreviewPane">
    <div id="test"/>
                <div class="textpartPane" id="editionPane-9999">
                    <div class="previewPane">
                            <h4>Text Preview {teiEditor:previewToolBar(9999)}</h4>

                            <div id="textPreviewHTML-9999" class="textPreviewHTMLOverview"/>
                        </div>    
                        
                    </div>
    </div>
};
    
(:~ Function to generate the HTML widget displaying the text edition emmbed in the text editor. No cardinality :)
declare function teiEditor:textPreview($docId as xs:string, $previewOptions as item()*){
    <div class="textPreviewPane">
    <div id="test"/>
                <div class="textpartPane" id="editionPane-9999">
                    <div class="previewPane">
                            <h4>Text Preview {teiEditor:previewToolBar(9999, $previewOptions)}</h4>

                            <div id="textPreviewHTML-9999" class="textPreviewHTMLOverview"/>
                        </div>    
                        
                    </div>
    </div>



    };

(:~ Function to generate the HTML widget displaying the text edition. :)
declare function teiEditor:textPreviewMultipleTextParts($docId as xs:string, $numberOfTextparts as xs:integer){
    <div class="textPreviewPane">
    <div id="test"/>
                <div class="textpartPane" id="editionPane-9999">
                    <div class="previewPane">
                            <h4>Text Preview {teiEditor:previewToolBar(9999)}</h4>
                    {if($numberOfTextparts > 12) then <h5>⚠ Only 12 on a total of { $numberOfTextparts } textparts are displayed ⚠</h5> else ()}
                            <div id="textPreviewHTML-9999" class="textPreviewHTMLOverview"/>
                        </div>    
                        
                    </div>
    </div>



    };

(:~ Function to generate the HTML widget displaying the text edition of a document. :)
declare function teiEditor:textPreviewMulti($docId as xs:string){
(:    let $xslCleanDiv := xs:anyURI("xmldb:exist:///db/apps/ausohnum-library/xslt/cleanTextEdition.xsl"):)
    let $teiDoc := $teiEditor:doc-collection/id($docId)
(:    let $teiDoc := util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" ||$docId ||"')" ):)

    let $teiElementNickname :=
                if (exists($teiDoc//div[@type="textpart"])) then ('docTextSingle')
                    else if (not(exists($teiDoc//div[@type="textpart"]))) then ('docTextSingle')

                else ('docTextSingle')

    let $elementNode := $teiEditor:teiElements//teiElement[nm=$teiElementNickname]
    let $teiElementNode := if (exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname])) then
                        $teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]
                        else ($teiEditor:teiElements//teiElement[nm=$teiElementNickname])
    let $teiXPath := $teiElementNode//teiElement[nm=$teiElementNickname]/xpath/text()


    let $params :=
        <output:serialization-parameters
        xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
            <output:omit-xml-declaration value="yes"/>
            <output:method value="xml"/>
            <output:indent value="yes"/>
            <output:item-separator value="\n"/>
            <output:undeclare-prefixes value="yes"/>
        </output:serialization-parameters>
    let $paramMap :=
        map {
            "method": "xml",
            "indent": false(),
            "item-separator": ""

   }

    return
    <div class="textPreviewPane">
        {

           (:else if($teiEditor:teiDoc//tei:div[@type='edition']//tei:div[@type='textpart']):)
           if(exists($teiDoc//tei:div[@type='textpart']))
            then
           (
           for $textpart at $index in $teiDoc//tei:div[@type='edition']//tei:div[@type='textpart']
            where $index < 13
            let $surface := data($teiDoc/id(substring(data($textpart/@corresp), 2))/tei:desc/text())
            let $text :=
                    replace(functx:trim(serialize(functx:change-element-ns-deep(
                       $textpart/tei:ab, '', '')/node(), $paramMap)),
                        '&#9;', '')
            let $textOLD := <div><?xml-model href="http://www.stoa.org/epidoc/schema/latest/tei-epidoc.rng" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="http://www.stoa.org/epidoc/schema/latest/tei-epidoc.rng" schematypens="http://purl.oclc.org/dsdl/schematron"?>
              {normalize-space(serialize(functx:change-element-ns-deep($teiEditor:teiDoc//tei:div[@type="textpart"][$index]/tei:ab, '', '')/node(), $paramMap))}</div>

            return
                <div class="textpartPane" id="editionPane-9999">
                    <h3> {if($surface) then "Text on " || $surface
                        else ()}
                    </h3>
                    {if(count($teiDoc//tei:div[@type='edition']//tei:div[@type='textpart']) > 12) then <h5>Only 12 on a total of {count($teiDoc//tei:div[@type='edition']//tei:div[@type='textpart'])} textparts are displayed</h5> else ()}
                    <div class="previewPane">
                            <h4>Text Preview {teiEditor:previewToolBar(9999)}</h4>
{if(count($teiDoc//tei:div[@type='edition']//tei:div[@type='textpart']) > 12) then <h5>Only 12 on a total of {count($teiDoc//tei:div[@type='edition']//tei:div[@type='textpart'])} textparts are displayed</h5> else ("r")}
                            <div id="textPreviewHTML-9999" class="textPreviewHTMLOverview"/>
                        </div>    
                        
                    </div>
            )
             (:if (not(exists($teiDoc//tei:div[@type="edition"]//tei:div[@type="textpart"]))) then:)
         else if (not(exists($teiDoc//tei:div[@type="textpart"]))) then
           (
            let $text := functx:trim(serialize(functx:change-element-ns-deep($teiDoc//tei:div/tei:ab, '', '')/node(), $paramMap))

                return
                        <div class="textpartPane" id="editionPane-9999">
                        <div class="alert alert-info">
                        This document has no textpart
                         </div>
                        
                        <div class="previewpanel col-xs-10 col-sm-10 col-md-10">
                                 <h4>Text preview {teiEditor:previewToolBar(1)}
                                 </h4>

                                 <div id="textPreviewHTML-9999" class="textPreviewHTMLOverview"/>
                         </div>
                     </div>
           )
            else()
            }
            <div id="currentEditorIndexVariable"/>
    </div>



    };

(:~ Function to generate the HTML widget of the text convertor; to be embedded in the text editor. :)
declare %templates:wrap function teiEditor:textConverter($index){
    <div style="display: inline;">
    <button id="callTextImport-{$index}"  onclick="openTextImporter({$index})" class="btn btn-xs btn-primary pull-right" data-target="#dialogTextImport">Text Converter</button>
    <div id="dialogTextTmport" title="Import a text" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <span class="modal-title">Text converter</span>
                </div>
                <div class="modal-body small-font">
                <p>You can paste any text formatted according to main epigraphical standards.</p>
                    <div class="form-group row">
                                <label for="langSource">Select a script</label>
                                <select id="langSource" name="langSource">
                                     <option value="grc">Ancient Greek</option>
                                     <option value="la">Latin</option>
                                     <option value="egy-Egyd">Ancient Egyptian in demotic script (latin transliteration)</option>
                                     <option value="egy-Egyh">Ancient Egyptian in hieratic script (latin transliteration)</option>
                                     <option value="egy-Egyp">Ancient Egyptian in hieroglyphic script (latin transliteration)</option>
                                </select>

                    </div>
                    <div class="form-group row">
                                <label for="importSource">Select source format</label>
                                <select id="importSource" name="importSource">
                                     <option value="petrae">PETRAE</option>
                                     <option value="edr">EDR</option>
                                     <option value="edcs">EDCS / EDH</option>
                                     <option value="phi">PHI</option>
                                </select>
                    </div>
                     <div class="form-group row">
                                <label for="textImportStartingLine">Starting line number (if not 1)</label>
                                <input id="textImportStartingLine" name="textImportStartingLine" type="text"/>
                    </div>
                    <input id="editorIndex{$index}" name="editorIndex" type="text" class="hidden"/>
                    <input id="textImportMode" name="textImportMode" type="text" class="hidden"/>
                            <!--
                           <label for="text2import">Paste text to import below</label>

                            <textarea class="form-control" name="text2import" id="text2import" row="10" ></textarea>
                            -->
                                <p>Paste your text below</p>
                                <span id="conversionInProcess" class="hidden"><img id="f-load-indicator" class="" src="/$ausohnum-lib/resources/images/ajax-loader.gif"/></span>
                                <div class="text2importInput">
                                <div id="text2importInputEditor"/>
                                </div>
                                <label for="text2importXMLPreview">XML preview</label>
                                <div id="text2importXMLPreview"/>

                    <div class="modal-footer">
                        <button  class="btn btn-primary" onclick="importText({$index})">OK</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>



                </div>

            </div>



    </div>
    </div>
};

(:~ Function to generate the HTML toolbar for epigraphic annotation; to be embedded in the textEditor:)
declare function teiEditor:annotationMenuEpigraphy($index){
(:    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId):)
    (:let $index :=
        if (not(exists($teiDoc//div[@type="edition"]/div[@type="textpart"]))) then 1
            else (
            count($teiDoc//div[@type="edition"]/div[@type="textpart"])
            ):)
(:     return:)
            <div id="edition-toolbar-{$index}" class="btn-group xmlToolBar" role="group" aria-label="..." >

                      <span class="toolBarTitle">Epigraphic annotations</span>

                       <div class="dropdown btn-group" role="group">
                            <a id="insertLb" role="button" data-toggle="dropdown" class="btn btn-default btn-xs" data-target="#" >
                                (1)<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">

                                  <li class="dropdown-submenu">
                                    <a  role="button" onclick="insertLb({$index}, 1)">Line beginning</a>
                                    <ul class="dropdown-menu">
                                        <li><a role="button" onclick="insertLb({$index}, 1, '')">1</a><a role="button" onclick="insertLb({$index}, 1, 'no')"><em class="small">- in word</em></a></li>
                                        <li><a role="button" onclick="insertLb({$index}, 2, '')">2</a><a role="button" onclick="insertLb({$index}, 2, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertLb({$index}, 3, '')">3</a><a role="button" onclick="insertLb({$index}, 3, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertLb({$index}, 4, '')">4</a><a role="button" onclick="insertLb({$index}, 4, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertLb({$index}, 5, '')">5</a><a role="button" onclick="insertLb({$index}, 5, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertLb({$index}, 6, '')">6</a><a role="button" onclick="insertLb({$index}, 6, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertLb({$index}, 7, '')">7</a><a role="button" onclick="insertLb({$index}, 7, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertLb({$index}, 8, '')">8</a><a role="button" onclick="insertLb({$index}, 8, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertLb({$index}, 9, '')">9</a><a role="button" onclick="insertLb({$index}, 9, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertLb({$index}, 10, '')">10</a><a role="button" onclick="insertLb({$index}, 10, 'no')"><em class="small">- in word</em></a></li>
                                    </ul>
                                  </li>
                              <li class="dropdown-submenu">
                                    <a href="#">Column beginning</a>
                                    <ul class="dropdown-menu">
                                        <li><a role="button" onclick="insertCb({$index}, 1, '')">1</a><a role="button" onclick="insertCb({$index}, 1, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertCb({$index}, 2, '')">2</a><a role="button" onclick="insertCb({$index}, 2, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertCb({$index}, 3, '')">3</a><a role="button" onclick="insertCb({$index}, 3, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertCb({$index}, 4, '')">4</a><a role="button" onclick="insertCb({$index}, 4, 'no')"><em class="small">- in word</em></a></li>
                                    	<li><a role="button" onclick="insertCb({$index}, 5, '')">5</a><a role="button" onclick="insertCb({$index}, 5, 'no')"><em class="small">- in word</em></a></li>
                                    </ul>
                                  </li>

                            </ul>
        </div>

                             <button type="button" class="btn btn-xs btn-default" onclick="unclear({$index});" title="Unclear letter(s)">ạ</button>
                            <button type="button" class="btn btn-xs btn-default" onclick="apex({$index});" title="Letter with apex">á</button>
                            <button type="button" class="btn btn-xs btn-default" onclick="supraline({$index});" title="Letter with supraline">ā</button>
                            <button type="button" class="btn btn-xs btn-default" onclick="intraline({$index});" title="Struck through letter"><strike>A</strike></button>
                            <button type="button" class="btn btn-xs btn-default" onclick="ligature({$index});" title="Letters with ligature">a͡b</button>
                            <button type="button" class="btn btn-xs btn-default" onclick="supplied({$index}, 'lost', '')" title="Restauration">[a]</button>
                            <button type="button" class="btn btn-xs btn-default" onclick="supplied({$index}, 'lost', 'low')" title="Uncertain restauration">[a?]</button>
                            <button type="button" class="btn btn-xs btn-default" onclick="surplus({$index})" title="Surplus">&#123;a&#125;</button>
                            <button type="button" class="btn btn-xs btn-default" onclick="corr({$index})" title="Correction">⸢a⸣</button>
                            <button type="button" class="btn btn-xs btn-default" onclick="supplied({$index}, 'omitted', '')" title="Letters added by editor">&#60;a&#62;</button>
                            <button type="button" class="btn btn-xs btn-default" onclick="erasure({$index})" title="Erasure">⟦a⟧</button>
                            <button type="button" class="btn btn-xs btn-default" onclick="abbrev({$index}, '')" title="Expansion of an abbreviation">a(bc)</button>
                           
                           
                           <div class="btn-group" role="group">
                              <button type="button" class="btn btn-xs btn-default dropdown-toggle"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                              title="Complex abbreviations">a(b)c(d)
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                        <li><a role="button" onclick="insertComplexAbbrev({$index}, 'consul')">co(n)s(ul)</a></li>
                                        <li><a role="button" onclick="insertComplexAbbrev({$index}, 'proconsul')">proco(n)s(ul)</a></li>
                                        <li><a role="button" onclick="insertComplexAbbrev({$index}, 'cohors')">c(o)ho(rs)</a></li>
                              </ul>
                                
                            </div>
 
                            
                            <button type="button" class="btn btn-xs btn-default" onclick="abbrev({$index}, 'low')" title="Tentative expansion of an abbreviation">a(bc?)</button>
                            
                            
                            <button type="button" class="btn btn-xs btn-default" onclick="abbrevShort({$index})" title="Abbreviation with unknown development">a(- - -)</button>
                            
                            <button type="button" class="btn btn-xs btn-default" onclick="abbrevSymbol({$index})" title="Expansion of a symbol">(abc)</button>
                            
                           <div class="btn-group" role="group">
                              <button type="button" class="btn btn-xs btn-default dropdown-toggle"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                              title="Lacuna">Lacuna
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a onclick="insertGap({$index}, 'lost', 'unknown', '')">Letters</a>
                                    <ul class="dropdown-menu">
                                        <li><a role="button" onclick="insertGap({$index}, 'lost', 'unknown', 'character')">Extent unknown  [– – –]</a></li>
                                        <li><a role="button" onclick="insertGap({$index}, 'lost', 1, 'character')">1 letter</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'lost', 2, 'character')">2 letters</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'lost', 3, 'character')">3 letters</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'lost', 4, 'character')">4 letters</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'lost', 5, 'character')">5 letters</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'lost', 5, 'character', 'low')">Approximate extent [– ca.5 –]</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'lost', 'range', 'character')">Range of possible extent [– 5-7 –]</a></li>
                                    	
                                    </ul>
                                  </li>
                                    <li class="dropdown-submenu">
                                    <a onclick="insertGap({$index}, 'omitted', 'unknown', '')">Lines</a>
                                    <ul class="dropdown-menu">
                                    <li><a role="button" onclick="insertGap({$index}, 'lost', 'unknown', 'line')">Number unknown - - - - - - -</a></li>
                                    <li><a role="button" onclick="insertGap({$index}, 'lost', 1, 'line')">1 line</a></li>
                                    	
                                    </ul>
                                  </li>
                                    <!--
                                    <li class="dropdown-submenu">
                                    <a onclick="insertGap({$index}, 'omitted', 'unknown', '')">Omitted</a>
                                    <ul class="dropdown-menu">
                                        <li><a role="button" onclick="insertGap({$index}, 'omitted', 1, 'character')">1 letter</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'omitted', 2, 'character')">2 letters</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'omitted', 3, 'character')">3 letters</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'omitted', 4, 'character')">4 letters</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'omitted', 5, 'character')">5 letters</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'omitted', 1, 'line')">1 line</a></li>
                                    	<li><a role="button" onclick="insertGap({$index}, 'omitted', 2, 'line')">2 lines</a></li>
                                    </ul>
                                  </li>
                                    -->
                              </ul>
                            </div>

                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-xs btn-default dropdown-toggle"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                              title="Addition">Addition
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a role="button" onclick="add_above({$index})">Above</a></li>
                                <li><a role="button" onclick="add_below({$index})">Below</a></li>
                              </ul>
                            </div>
                                
                            <div class="btn-group" role="group">
                              <button type="button" class="btn btn-xs btn-default dropdown-toggle"
                              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                              title="Illegible">Illegible
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a role="button" onclick="illegible({$index}, 'character', 1)">1 letter</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'character', 2)">2 letters</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'character', 3)">3 letters</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'character', 4)">4 letters</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'character', 5)">5 letters</a></li>
                              </ul>
                            </div>    
                                
<!--        <div class="dropdown btn-group" role="group">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-default btn-xs" data-target="#" >
                Illegible<span class="caret"></span>
            </a>
    		<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">

              <li class="dropdown-submenu">
                    <a href="#">Letter</a>
                    <ul class="dropdown-menu">
                        <li><a role="button" onclick="illegible({$index}, 'character', 'unknown')">Unknown</a></li>
                        <li><a role="button" onclick="illegible({$index}, 'character', 1)">1</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'character', 2)">2</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'character', 3)">3</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'character', 4)">4</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'character', 5)">5</a></li>
                    </ul>
                  </li>
              <li class="dropdown-submenu">
                    <a href="#">Line</a>
                    <ul class="dropdown-menu">
                        <li><a role="button" onclick="illegible({$index}, 'line', 'unknown')">Unknown</a></li>
                        <li><a role="button" onclick="illegible({$index}, 'line', 1)">1</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'line', 2)">2</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'line', 3)">3</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'line', 4)">4</a></li>
                    	<li><a role="button" onclick="illegible({$index}, 'line', 5)">5</a></li>
                    </ul>
                  </li>
                    
            </ul>
        </div>
        -->




<div class="dropdown btn-group" role="group">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-default btn-xs" title="Vacat">
                Vacat<span class="caret"></span>
            </a>
    		<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">

              <li class="dropdown-submenu">
                    <a href="#">Letter</a>
                    <ul class="dropdown-menu">
                        <li><a href="#" role="button" onclick="vacat({$index}, 'character', 0)">Undetermined <em>vacat.</em></a></li>
                        <li><a href="#" role="button" onclick="vacat({$index}, 'character', 1)">1</a></li>
                        <li><a href="#" role="button" onclick="vacat({$index}, 'character', 2)">2</a></li>
                        <li><a href="#" role="button" onclick="vacat({$index}, 'character', 3)">3</a></li>
                        <li><a href="#" role="button" onclick="vacat({$index}, 'character', 4)">4</a></li>
                        <li><a href="#" role="button" onclick="vacat({$index}, 'character', 5)">5</a></li>
                        
                    </ul>
                  </li>
              <li class="dropdown-submenu">
                    <a href="#">Line</a>
                    <ul class="dropdown-menu">
                        <li><a href="#" role="button" onclick="vacat({$index}, 'line', 1)">1</a></li>
                    </ul>
                  </li>

            </ul>
        </div>
        
        
        
        
        
        <button type="button" class="btn btn-xs btn-default" onclick="insertGap({$index}, 'omitted', 'unknown', 'character')" title="Text left uncompleted by stonecutter">(- - -)</button>
        
        <div class="dropdown btn-group" role="group">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-default btn-xs" >
                IX<span class="caret"></span>
            </a>
    	<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                     <li><a href="#" role="button" onclick="romanNumber({$index}, 'convert')">Convert selection</a></li>
                     <li><a href="#" role="button" onclick="romanNumber({$index}, 1)">I</a></li>
                    <li><a href="#" role="button" onclick="romanNumber({$index}, 2)">II</a></li>
                    <li><a href="#" role="button" onclick="romanNumber({$index}, 3)">III</a></li>
                    <li><a href="#" role="button" onclick="romanNumber({$index}, 4)">IV</a></li>
                    <li><a href="#" role="button" onclick="romanNumber({$index}, 5)">V</a></li>
                    <li><a href="#" role="button" onclick="romanNumber({$index}, 6)">VI</a></li>
                    <li><a href="#" role="button" onclick="romanNumber({$index}, 7)">VII</a></li>
                    <li><a href="#" role="button" onclick="romanNumber({$index}, 8)">VIII</a></li>
                    <li><a href="#" role="button" onclick="romanNumber({$index}, 9)">IX</a></li>
                    <li><a href="#" role="button" onclick="romanNumber({$index}, 10)">X</a></li>
               </ul>
       </div>
        
        <div class="dropdown btn-group" role="group">
            <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-default btn-xs" >
                Symbols<span class="caret"></span>
            </a>
    	<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
        
        <li><a href="#" role="button" onclick="insertHedera({$index});" title="hedera">❦</a></li>
        <li><a href="#" role="button" onclick="insertChiRho({$index});" title="chirho">☧</a></li>
       <li><a href="#" role="button" onclick="insertInterpunct({$index});" title="Symbol Interpunct">▴</a></li>
        <li><a href="#" role="button" onclick="insertDenarius({$index});" title="Symbol Denarius">Ӿ</a></li>
        
               </ul>
       </div>
        
        <button type="button" class="btn btn-xs btn-default" onclick="insertNote({$index}, '!')" title="(!)">(!)</button>
        <button id="saveTextButton{$index}" class="saveTextButton btn btn-primary pull-right" onclick="saveText('{$teiEditor:docId}', {$index})" appearance="minimal" type="button"><i class="glyphicon glyphicon-floppy-save"></i></button>
        </div>
};

(:~ Function to generate the HTML toolbar for semantic annotation; to be embedded in the textEditor:)
declare function teiEditor:semanticAnnotation($annotationLabel as xs:string,
                                              $annotationType as xs:string,
                                              $teiElement as xs:string,
                                              $topConceptId as xs:string){
    <div id="annotation_{$annotationLabel}_" class="sectionInPanel">
    <h4>Semantic Annotations</h4>
      <span class="subSectionTitle">{$annotationLabel}</span>
                                    <div class="insertTagWithDropDown">
                                            {skosThesau:dropDownThesauXML($topConceptId, 'en', 'noLabel', 'inline', 1, 1, ())}
                            <button class="inline btn btn-primary btn-xs"
                            onclick="addReferenceString('1', '{$annotationType}', '{$topConceptId}', 1)">Annotate</button>
                                    </div>

                                </div>
};

(:~ Function to generate a simplistic HTML widget to markup text with tei:w, including @lemma :)
declare function teiEditor:manualLemmatizer(){
    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId)
    let $index :=
        if (not(exists($teiDoc//div[@type="edition"]/div[@type="textpart"]))) then 1
            else (
            count($teiDoc//div[@type="edition"]/div[@type="textpart"])
            )
     return
            <div id="lemmatizer-toolbar-{$index}" class="btn-group xmlToolBar" role="group" aria-label="..." >
                  <h4 class="toolBarTitle">Word Lemmatizer</h4>
                                    <div class="sectionInPanel panel panel-primary ">
                                                    <div class="input-group">
                                                             <span class="input-group-addon">Lemma</span>
                                                                <input type="text" class="form-control" id="lemmaForm" name="lemmaForm"
                                        placeHolder="Lemma"/>
                                                            <span class="input-group-addon">
                                                            <button class="inline btn btn-primary btn-xs" title="Copy &amp; paste selected text from editor"
                                                        onclick="pasteSelectedText({$index}, 'lemmaForm')"><i class="glyphicon glyphicon-transfer"/></button>
                                                            </span>
                                                       </div>
                                            <button class="inline btn btn-primary btn-xs"
                                                onclick="lemmatizeWord({$index})">Lemmatize Selected Word</button>
                        </div>
                      </div>
};

(:~ Function to generate the HTML toolbar for annotations related to places and people (events not done); to be embedded in the textEditor:)
declare function teiEditor:annotationPlacePeopleTime(){
    let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId)
    let $index :=
        if (not(exists($teiDoc//div[@type="edition"]/div[@type="textpart"]))) then 1
            else (
            count($teiDoc//div[@type="edition"]/div[@type="textpart"])
            )
     return
            <div id="edition-toolbar-{$index}" class="btn-group xmlToolBar" role="group" aria-label="..." >

                      <h4 class="toolBarTitle">Annotate Place, People &amp; Time</h4>
                        <div class="panel panel-default">
                           <div class="panel-heading"  data-toggle="collapse"  href="#annotation-places-panel">&#x2318; Places</div>
                           <div id="annotation-places-panel" class="panel-collapse">
                                <div class="panel-body">
                                <div><span class="subSectionTitle">List of places attached to this doc.</span>
                                <!--
                                {
                                teiEditor:listsPlaces()
                                }
                                
                                <button class="inline btn btn-secondary btn-xs"
                                                onclick="addPlaceToDoc({$index})">Tag with selected Place</button>
                                                <br/>
                         -->
                        <div id="listOfPlaces">
                                { teiEditor:placeListForAnnotation($teiEditor:docId, $index) }
                                </div>
                                </div>
                                <!--
                                <div id="placeLookUpPanel" class="sectionInPanel"><span class="subSectionTitle">Add a new place</span>

                                        <div class="form-group">
                                                    <label for="placesLookupInputSemantic">Search in <a href="http://pelagios.org/peripleo/map" target="_blank">Pelagios Peripleo</a>
                                                    </label>
                                                    <input type="text" class="form-control" id="placesLookupInputSemantic" name="placesLookupInputSemantic"/>
                                                </div>
                                                 <div class="">

                            <iframe id="peripleoWidget" allowfullscreen="true" height="380" src="" style="display:none;"> </iframe>
                            <div id="previewMap" class="hidden"/>
                            <div id="placePreviewPanel" class="hidden"/>
                            <button id="addNewPlaceButton" class="btn btn-success hidden" onclick="addPlaceToDoc('{$teiEditor:docId}')" appearance="minimal" type="button">Add place to document<i class="glyphicon glyphicon glyphicon-saved"></i></button>
                        </div>
                                </div>
                                -->
                                </div>
                           </div>
                        </div>

                      <div class="panel panel-default">
                           <div class="panel-heading"  data-toggle="collapse"  href="#annotation-people-panel">&#x1F464; People</div>
                           <div id="annotation-people-panel" class="panel-collapse">
                                <div class="panel-body">
                      <div><span class="subSectionTitle">List of people attached to this doc. ({count($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person)})
                      <!--
                      <button class="btn btn-sm btn-primary pull-right" onclick="openDialog('dialogAddPeopleToDoc')"><i class="glyphicon glyphicon-plus"/>person</button></span>
                      --></span>
                       <!--
                       <div id="peopleList" class="itemList">
                                {
                                for $people in $teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person
                                    let $personUri := $people/@corresp/string()
                                    let $personId := functx:substring-after-last($personUri, '/')
                                    let $personRecord := $teiEditor:peopleRepo/id($personId)
                                    let $names := string-join($personRecord//tei:persName, ' ') || "ee"
(:                                order by $personRecord/persName:)

                                return
                                teiEditor:displayPersonItem($teiEditor:docId, $personUri, $index)
                                }
                     </div>
                     -->
                     {
                                teiEditor:listPeople($teiEditor:docId, $index)
                                }
                                </div>
                                <div class="sectionInPanel">
                                    <span class="subSectionTitle">Annotate Names</span>


                                     <div class="sectionInPanel panel panel-primary ">
                                     <span class="subSectionTitle">PersName</span>
                                    <div class="insertTagWithDropDown">
                                            <div id="persNameType">{skosThesau:dropDownThesauXML('c19928', 'en', 'Type', 'inline', (), 1, ())}</div>
                            <button class="inline btn btn-primary btn-xs"
                                                onclick="addPersName({$index}, '')">PersName</button>

                                    </div>

                                            <div class="sectionInPanel panel panel-primary ">
                                                    <span class="subSectionTitle">Names</span>
                                                    <div class="input-group">
                                                             <span class="input-group-addon">NymRef</span>


                                                                <input type="text" class="form-control" id="standardizedForm" name="standardizedForm"
                                        placeHolder="Standardized form"/>
                                                            <span class="input-group-addon">
                                                            <button class="inline btn btn-primary btn-xs" title="Copy &amp; paste selected text from editor"
                                                        onclick="pasteSelectedText({$index}, 'standardizedForm')"><i class="glyphicon glyphicon-transfer"/></button>
                                                            </span>
                                                       </div>

                                              <button class="inline btn btn-primary btn-xs"
                                                onclick="addName({$index}, 'name')">Name</button>
                                                <br/>

                                            <button class="inline btn btn-primary btn-xs"
                                                onclick="addName({$index}, 'praenomen')">Praenomen</button>
                                                <button class="inline btn btn-primary btn-xs"
                                                onclick="addName({$index}, 'nomen')">Nomen</button>
                                            <button class="inline btn btn-primary btn-xs"
                                                onclick="addName({$index}, 'cognomen')">Cognomen</button>
                                              <br/>

                                   </div>
                                    </div>
                                </div>


                                <div class="sectionInPanel">
                                    <span class="subSectionTitle">♦ functions</span>
                                    <div class="insertTagWithDropDown">
                                            {skosThesau:dropDownThesauXML($teiEditor:appVariables//prosopoFunctionsTopConceptId/text(),
                                                                         $teiEditor:lang, 'noLabel', 'inline', $index, 1, ())}
                                            <button class="inline btn btn-primary btn-xs"
                                                onclick="addReferenceString({$index}, 'function', 'c19307', 1)">&lt;insert function&gt;</button>
                                    </div>
                                </div>
                               </div>


                           </div>










                        </div>

                      <div class="panel panel-default">
                           <div class="panel-heading"  data-toggle="collapse"  href="#annotation-places-panel">Events</div>
                           <div id="annotation-events-panel" class="panel-collapse collapse">
                                <div class="panel-body">

                                </div>
                           </div>
                        </div>

                        <div class="div4Modals">
                        
 <!--Dialog for People-->
    <div id="dialogNewPerson" title="Add a new person" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Create a new person</h4>
                </div>
                <div class="modal-body">
                <div>
                     {skosThesau:dropDownThesauXML('c19291', 'en', 'Status', 'row', (), 1, ())}
                     {skosThesau:dropDownThesauXML('c19297', 'en', 'Rank', 'row', (), 1, ())}
                     {skosThesau:dropDownThesauXML('c19303', 'en', 'Citizenship', 'row', (), 1, ())}
                </div>

                        <div class="form-group row">
                                <label for="personSex" class="col-sm-2 col-form-label">Sex</label>
                                <div class="col-sm-10">
                                <select id="personSex">
                                    <option value="m" selected="selected">Male</option>
                                    <option value="f" >Female</option>
                                    <option value="unknown">Unknown</option>
                                </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="newPersonPraenomen" class="col-sm-2 col-form-label">Praenomen</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="newPersonPraenomen" name="newPersonPraenomen"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="newPersonNomen" class="col-sm-2 col-form-label">Nomen</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="newPersonNomen" name="newPersonNomen"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="newPersonCognomen" class="col-sm-2 col-form-label">Cognomen</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="newPersonCognomen" name="newPersonCognomen"/>
                                </div>
                            </div>



                    <div class="form-group modal-footer">


                        <button id="addPeople" class="pull-left" onclick="createAndAddPerson('{$teiEditor:docId}', {$index})">Create person</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->
                </div>
            </div>
       </div><!--End of dialog-->











<!--Dialog for new document-->
    <div id="dialogInsertNymRef" title="Insert name nymRef" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Create a new Collection of documents</h4>
                </div>
                <div class="modal-body">
                <div class="form-group row">
                                <label for="standardizedForm" class="col-sm-2 col-form-label">Standardized form</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="standardizedForm" name="standardizedForm"
                                placeHolder="Insert a standardized name"/>
                                </div>
                </div>

                    <div class="form-group modal-footer">
                        <button id="addNymRef" class="pull-left" onclick="addNymRef({$index})">Insert NymRef</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->
                </div>
            </div>
       </div><!--End of dialog-->






                        </div>


                      </div>
};

(:~ Function to generate the HTML widget to list places linked to a document as buttons, for tet annotation purpose; to be embedded in the textEditor:)
declare function teiEditor:placeListForAnnotation($docId as xs:string, $index as xs:integer){

let $teiDoc := $teiEditor:doc-collection/id($docId)
(:let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"||$docId ||"')" ):)


return
<div id="listOfPlaces">{
for $place at $pos in $teiDoc//tei:sourceDesc/tei:listPlace//tei:place
    let $splitRef := tokenize(data($place/tei:placeName/@ref), " ")
    let $placeUri := 
                        for $uri in $splitRef
                            return
                                  if(contains($uri, $teiEditor:baseUri)) then 
                                       normalize-space($uri[1]) else ()      
    (: let $placeUriInternal := 
                        for $uri in tokenize($splitRef, " ")
                            return 
                                if (contains($uri, $teiEditor:project)) then $uri else ():)
     let $placeRecord:= if($placeUri != "" ) then $teiEditor:projectPlaceCollection//pleiades:Place[@rdf:about = $placeUri] else ()
                                                                         
                                order by $place/tei:placeName
                                return
                                <span class="btn btn-light listItem">
                                <span title="Annotate selected text with reference to {functx:trim(normalize-space($placeRecord[1]//dcterms:title[1]/text()))} ({ $placeUri })"
                                onclick="addPlace({$index}, '{functx:trim(normalize-space(data($place/tei:placeName/string())))}', '{$placeUri}')">{$placeRecord//dcterms:title[1]/text()}</span>
                                </span>
}</div>
};
                                

(:~ Function to generate the HTML widget to list places linked to a document as a dropdown list, for tet annotation purpose; to be embedded in the textEditor:)
declare function teiEditor:placeAnnotatorWithCombo($docId as xs:string){
let $teiDoc := $teiEditor:doc-collection/id($docId)
(:let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" ):)



return
<div>
{teiEditor:listsPlacesAsCombo()}
<button class="inline btn btn-primary btn-xs"
                                                onclick="addPlaceRefToText()">Tag with placeName</button>
                                                </div>                                                        
                                };
                                
(:~ Function to generate the HTML widget to list people linked to a document as buttons, for tet annotation purpose; to be embedded in the textEditor:)
declare function teiEditor:listPeople($docId as xs:string, $index as xs:integer){
let $teiDoc := $teiEditor:doc-collection/id($docId)
(:let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"||$docId ||"')" ):)


return
<div id="peopleList" class="itemList">{
        for $person in $teiDoc//tei:listPerson[@type='peopleInDocument']//tei:person
                                order by $person/tei:persName[1]/string()
                                return
                                <span class="btn btn-light listItem">
                                <span title="Annotate selected text with reference to {functx:trim(normalize-space($person/tei:persName[1]/string()))} ({ $person/@corresp })"
                                onclick="addPeople({$index}, '{ $person/@corresp }')">{$person/tei:persName[1]/string()} [{ substring-after($person/@corresp, "/people/") }]</span>
                                </span>
                                }
                                </div>
                                };

(:~ Function to generate the HTML widget to list places linked to a document as button, for tet annotation purpose; to be embedded in the textEditor:)
declare function teiEditor:annotatePersNames(){
 <div class="sectionInPanel">
                                    <span class="subSectionTitle">Annotate Names</span>


                                     <div class="sectionInPanel panel panel-primary ">
                                     <span class="subSectionTitle">PersName</span>
                                    <div class="insertTagWithDropDown">
                                            <div id="persNameType">{skosThesau:dropDownThesauXML('c19928', 'en', 'Type', 'inline', (), 1, ())}</div>
                            <button class="inline btn btn-primary btn-xs"
                                                onclick="addPersName('', '')">PersName</button>

                                    </div>

                                            <div class="sectionInPanel panel panel-primary ">
                                                    <span class="subSectionTitle">Names</span>
                                                    <div class="input-group">
                                                             <span class="input-group-addon">NymRef</span>


                                                                <input type="text" class="form-control" id="standardizedForm" name="standardizedForm"
                                        placeHolder="Standardized form"/>
                                                            <span class="input-group-addon">
                                                            <button class="inline btn btn-primary btn-xs" title="Copy &amp; paste selected text from editor"
                                                        onclick="pasteSelectedText('', 'standardizedForm')"><i class="glyphicon glyphicon-transfer"/></button>
                                                            </span>
                                                       </div>

                                              <button class="inline btn btn-primary btn-xs"
                                                onclick="addName('', 'name')">Name</button>
                                                <br/>

                                            <button class="inline btn btn-primary btn-xs"
                                                onclick="addName('', 'praenomen')">Praenomen</button>
                                                <button class="inline btn btn-primary btn-xs"
                                                onclick="addName('', 'nomen')">Nomen</button>
                                            <button class="inline btn btn-primary btn-xs"
                                                onclick="addName('', 'cognomen')">Cognomen</button>
                                              <br/>

                                   </div>
                                    </div>
                                </div>


};


(:~ Function to generate the HTML widget for displaying and direct editing of XML file :)
declare function teiEditor:xmlFileEditor(){
     let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId)
    return
    <div>

               <div class="textModifiedAlert" id="fileModifiedAlert">File has been modified</div>
               <button id="saveFileButton" class="saveTextButton btn btn-primary" onclick="saveFile('{$teiEditor:docId}', 1)" appearance="minimal" type="button"><i class="glyphicon glyphicon-floppy-save"></i></button>
               <div id="xml-editor-file" class="">{serialize($teiDoc, ())}</div>
   </div>
};

(:~ Function to generate a hTML widget to display a thesaurus as a html select. :)
declare function teiEditor:comboboxThes($thesaurus as xs:string){
<select id="{$teiElementNickname}" name="{$teiElementNickname}">
                    {for $items in $itemList
                    return
                        if ($items/@xml:id = $teiElementValue)
                        then (
                        <option value="#{$items/@xml:id}" selected="selected">{$items/tei:catDesc/string()} </option>)
                        else (
                        <option value="#{$items/@xml:id}">{$items/tei:catDesc/string()} </option>
                        )
                     }
</select>
};

(:~ Function to generate the HTML widget to manage tei:msFrag :)
declare function teiEditor:displayFragment($docId as xs:string?){
let $teiDoc := $teiEditor:doc-collection/id($teiEditor:docId)
(:let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" )
:)

return
    <div class="sectionPanel">

    {for $frag at $pos in $teiDoc//tei:msFrag
    let $teiXPath := "/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msFrag"
        return
        <div id="fragment_display_{$pos}" class="teiElement teiElementDisplay">
        <h5 class="subSectionTitle">Object  {if(count($teiEditor:teiDoc//tei:msFrag) > 1) then $pos else ()}
        <span class="teiInfo">
            <a title="TEI element: {$teiXPath}"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
            </span>
        </h5>
            <div>
           { ""
(:           teiEditor:displayElement('fragProvenance', $docId, $pos, ()) :)
           }
           {"" 
(:           teiEditor:displayElement('fragmentDimensions', $docId, $pos, ()) :)
           }
           { 
           teiEditor:displayElement('msFragmentTown', $docId, $pos, ()) 
           }
            {teiEditor:displayElement('msFragmentRepository', $docId, $pos, ()) }
<!--           teiEditor:displayElement('msFragmentAltIdentifier', $docId, $pos, ())-->
            </div>

           </div>

    }
    </div>
};

(:~ Function to generate the HTML widget to manage tei:surface :)
declare function teiEditor:displaySurface(){
    <div class="col-sm-3 col-md-3 hidden-xs-down surfaceImage">
    {for $surface in $teiEditor:teiDoc//tei:sourceDoc//tei:surface
        return

        <img src="{$surface//tei:graphic/@url}">
        </img>
    }
    </div>
};






(:~ Function to update a document node with data received from a HTTP request :)
declare function teiEditor:saveData($data, $project ){

let $now := fn:current-dateTime()
let $currentUser := data(sm:id()//sm:username)

(:let $doc-collection := collection($config:data-root || "/" || $teiEditor:project || "/documents"):)


let $contentType := $data//contentType/text()
let $fieldType := $data//fieldType/text()

let $docId := $data//docId/text()

let $index := $data//index/text()
let $cardinality := $data//cardinality/text()

let $xpath := replace($data//xpath/text(), 'tei:', '')
let $xpathEnd := if(contains(functx:substring-after-last($xpath, '/'), "/@") or contains(functx:substring-after-last($xpath, '/'), "self"))
            then(
                functx:substring-after-last(functx:substring-before-last($xpath, '/'), '/')
                )
            else
            (functx:substring-after-last($xpath, '/')
            )
let $xpathEndNoSelector :=if(contains($xpathEnd, "["))
        then substring-before($xpathEnd, '[')
        else $xpathEnd 
        
        
let $xpathEndSelector := if(contains($xpathEnd, "[@")) then
                    substring-before(substring-after($xpathEnd, '[@'), ']') else ""
let $xpathEndSelectorName :=
                    substring-before($xpathEndSelector, '=')
let $xpathEndSelectorValue :=
                    substring-before(substring-after($xpathEndSelector, '="'), '"')

let $endingSelector := if(contains(functx:substring-after-last($xpath, '/'), "@"))
            then(
                functx:substring-after-last($xpath, '/@')
                )
            else
            (
            )

(:let $docId := request:get-parameter('docid', ()):)
(:let $teiDoc := $teiEditor:doc-collection/id($docId):)
let $paramMap :=
        map {
            "method": "xml",
            "indent": false(),
            "item-separator": ""}
let $updatedData := if($data//value/text())
                then ($data//value)
                    else " "


let $newElement := if($contentType = "text" and not(contains($xpath, "text()"))) then
                if(contains(functx:substring-after-last($xpath, '/'), "@")) then(
                    <newElement>{element {string($xpathEndNoSelector)}
                    
                        {attribute {string($xpathEndSelectorName)} {$xpathEndSelectorValue }, functx:trim($data//value/node())
                      }}</newElement>
                     )
                     
                    else(
                        <newElement>{
                            element {string($xpathEndNoSelector)}
                            
                            { 
(:                            functx:trim(
:)
                            
                            $data//value/node()
(:                            ):)
                            }
                              }</newElement>)


            else ""



let $updatedDataTextValue := $data//valueTxt/text()


let $xpathWithTeiPrefix := 

        if(contains($data//xpath/text(), "/@"))
            then
            $data//xpath/text()
        (:                                          substring-before($data//xpath/text(), '/@') || '[' || $index || ']/' || functx:substring-after-last($data//xpath/text(), '/'):)
            else
            $data//xpath/text() 
        (:                                          || '[' || $index || ']':)

(:if($index = 0) then $data//xpath/text()
                                          else if ($index >= 1) then
                                          $data//xpath/text() || '[' || $index || ']'
(\:                                            substring-before($data//xpath/text(), '/@') || "[" || $index || "]/" || functx:substring-after-last($data//xpath/text(), '/'):\)
                                          else ( $data//xpath/text() )
:)
let $quote := "&amp;quote;"
(:let $xpathWithTeiPrefix := replace($xpathWithTeiPrefix, $quote, '"') :)




        (:let $nodesArray := tokenize($xpath, '/')
        let $lastNode := $nodesArray[last()]:)

(:let $log := teiEditor:logEvent("test-before-updateData", $docId, (), <data>
$index {$index}
$contentType: {$contentType};
$updatedDataTextValue: {$updatedDataTextValue}
$updatedData: {$updatedData}
$docId: {$docId}
$teiEditor:doc-collection-path : {$teiEditor:doc-collection-path }
$xpathWithTeiPrefix: { $xpathWithTeiPrefix}
replace($xpathWithTeiPrefix, '[1]', "") {replace($xpathWithTeiPrefix, "\[1\]", "") }
$endingSelector: { $endingSelector }
OriginalNode : {util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" || $xpathWithTeiPrefix)}
 $xpathWithTeiPrefix {$xpathWithTeiPrefix}
 $newElement: {$newElement}
 $xpathEndNoSelector: { $xpathEndNoSelector } | {string($xpathEndNoSelector)}
$xpathEndSelectorName: {$xpathEndSelectorName}
 functx:trim($data//value/node()): <node>{  serialize($data//value/node(), $paramMap) }</node>
</data>):)

let $originalTEINode :=util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" || $xpathWithTeiPrefix)


let $oldValueTxt := data($originalTEINode)

let $originalTEINodeWithoutAttribute := 
            if(contains($xpathWithTeiPrefix, '/@')) then util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')/" || functx:substring-before-last($xpathWithTeiPrefix, '/') )
             else (util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')" || $xpathWithTeiPrefix ))

(:let $originalTEINodeWithoutAttribute := 
            if(contains($xpathWithTeiPrefix, '/@')) then util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')/" || functx:substring-before-last($xpath, '/') )
             else (util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')" || $xpath )):)

(:            let $updatedNode :=  <updatedNode  xmlns="http://www.tei-c.org/ns/1.0">{parse-xml('<' || $lastNode || ">" || $updatedData|| '</' || $lastNode || '>')}</updatedNode>        :)
(:let $updatedTEINode :=  <updatedNode>{parse-xml('<' || $lastNode || ">" || $updatedData|| '</' || $lastNode || '>')}</updatedNode>:)

(:let $updatedTEINode := functx:change-element-ns-deep($updatedNode, 'http://www.tei-c.org/ns/1.0', ''):)

(:let $logs := collection($config:data-root || $teiEditor:project || "/logs"):)

(:let $updateXml := update insert $aaa/node() following $originalTEINode :)
(: let $console:= console:log("contentType : " || $contentType) :)
let $console:= console:log(normalize-space($updatedData) = "")
let $console:= console:log(serialize($xpathWithTeiPrefix))
let $elementNickname := $data//elementNickname/text()
let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]))) 
    then $teiEditor:teiElements//teiElement[nm=$elementNickname] 
                        else $teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]
let $elementCardinality := $elementNode/cardinality
let $upateData :=
    switch ($contentType)

         case "textNodeAndAttribute" return
                (
                    (if( exists($originalTEINode)) then update value $originalTEINode with $updatedData
                    else
                    update insert attribute ref { functx:trim($data//value/node()) } into $originalTEINodeWithoutAttribute,
                    update value $originalTEINode with $updatedData),
                update value $originalTEINodeWithoutAttribute with $updatedDataTextValue
                )
         case "attribute" return
                if(($elementNode//attributeValueType[1] = "multi-uri" or $elementNode//attributeValueType[1] = "xml-value")
                    and $elementNode/fieldType != "comboboxWithConceptHierarchy") then
                    let $tokenizedOldValue:= tokenize($originalTEINode, " ")
                    (: let $newValue:= $updatedData :)
                    let $newValue:= string-join(($tokenizedOldValue[position()< $cardinality], $updatedData, $tokenizedOldValue[position()> $cardinality]), " ")
                    return
                    update value $originalTEINode with $newValue
                else
                update value $originalTEINode with data($updatedData)
         case "text"  return
                    if(normalize-space($updatedData) = "") then 
                        (if(contains($xpathWithTeiPrefix, "text()")) then update delete $originalTEINodeWithoutAttribute
                        else update delete $originalTEINodeWithoutAttribute/text())
                    (: with $updatedData :)
                    else  update value $originalTEINodeWithoutAttribute with $updatedData/text()
(:                        update replace $originalTEINode with functx:change-element-ns-deep($newElement, "http://www.tei-c.org/ns/1.0", "")/node():)
         case "enrichedText"  return
                   if(normalize-space($updatedData) = "") then 
                        (if(contains($xpathWithTeiPrefix, "text()")) then update delete $originalTEINodeWithoutAttribute
                        else update delete $originalTEINodeWithoutAttribute/text())
                    (: with $updatedData :)
                    else  update value $originalTEINodeWithoutAttribute with $updatedData/*[local-name()='p']/node()
        case "nodes" return
                update value $originalTEINode with $updatedData/node()
         default return
                update replace $originalTEINode with functx:change-element-ns-deep($newElement, "http://www.tei-c.org/ns/1.0", "")
(:            update replace $originalTEINode/node() with $updatedData/node():)

let $newContent := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" )
             (: Must use util:eval as xml has changed :)
(: let $newContent := $teiEditor:doc-collection/id($docId)              :)

(:let $log := teiEditor:logEvent("test-before-new-Element", $docId, (), <data>

</data>):)

let $elementNickname4update := if($elementNode/ancestor::teiElement[1]/fieldType/text() = "group")
                                then $elementNode/ancestor::teiElement[1]/nm/text()
                                                    else $elementNickname
 let $index4Update:= if($elementNode/ancestor::teiElement[1]/fieldType/text() = "group")
                                                    then ()
                                                    else 
                                                    (
                                                    if( $index="") then "1" else $cardinality)
let $xpath4Update:= if($elementNode/ancestor::teiElement[1]/fieldType/text() = "group")
                                                    then
                                                $data//xpath/text()
                                                    (: string-join(
                                                    for $element at $pos in $elementNode/ancestor::teiElement
                                                        return
                                                            if($pos < count($elementNode/ancestor::teiElement))
                                                                then $element/xpath/text()
                                                                else ()
                                                    , "") :)
                                                    else 
                                                    ($xpathWithTeiPrefix
                                                    )


let $updatedElement :=if($fieldType = "directInput") then ()
            else if($elementCardinality="x") then
                teiEditor:displayElement(
                    $elementNickname,
            (: $elementNickname4update, :)
             $docId, (), ())
else
    (: if (data($elementNode/ancestor::teiElement[1]/specificXQueryFunction/@name) != "")
        then (
            (: let $functionName:= QName("http://ausonius.huma-num.fr/teiEditor",
            data($elementNode/ancestor::teiElement[1]/specificXQueryFunction/@name))  :)
            let $functionArity:= data($elementNode/ancestor::teiElement[1]/specificXQueryFunction/@arity)
                (: let $functionName:= xs:QName("teiEditor:repositoryManager('lead-ps6')") :)
            let $function:= util:function($teiEditor:functionName, 1)
                return
                    util:call($function, data($docId))
        )
    else if($elementNode/ancestor::teiElement[1]/fieldType/text() = "group")
        then teiEditor:displayGroup($elementNickname4update, $docId, $index4Update, (), $xpath4Update)
    else  :)
        teiEditor:displayElement(
            $elementNickname,
            (: $elementNickname4update, :)
         $docId, $index4Update, $xpath4Update)

(: let $log := teiEditor:logEvent("document-update" ||$index, $docId,
    (), "Change in " || $docId ||
    " $elementNickname: " || $elementNickname ||
    " $elementNickname4update: " || $elementNickname4update ||
    " $xpathWithTeiPrefix: " || $xpathWithTeiPrefix ||
    " $xpath: " || $xpath ||
    " $xpath4Update: " || $xpath4Update ||
    " index: " || $index ||
    " Group? " || $elementNode/ancestor::teiElement[1]/fieldType/text() || 
    " $index4Update: " || $index4Update || "END$index4Update"||
    " SpecificXquery: " || ($elementNode/ancestor::teiElement[1]/specificXQueryFunction/text() ) ||
    "; $contentType: " || $contentType || "; New element: " || $newElement

    ) :)



    return
<http:response status="200"> 
                    <http:header name="Cache-Control" value="no-cache"/> 
                    <data>{$data}
    <test>{ ($elementNode/ancestor::teiElement[1]/fieldType/text() = "group") }</test>
   <test2>{ $elementNickname4update }</test2>
   <xpath4Update>{ $xpath4Update }</xpath4Update>
    <oldContent>{ $oldValueTxt }</oldContent>
    <newContent>{ $newContent }</newContent>
    <updatedElement>{ $updatedElement }</updatedElement>
    <fieldType>{ $elementNode/ancestor::teiElement[1]/fieldType/text() }</fieldType>
    <elementCardinality>{ $elementCardinality/text() }</elementCardinality>
    <ancestorfieldType>{ $elementNode/ancestor::teiElement[1]/nm/text() }</ancestorfieldType>
    
</data>

                </http:response> 


};

(:~ Function to update a tei:div[@type="textpart"] node with data received from a HTTP request :)
declare function teiEditor:saveText($data, $project){
let $now := fn:current-dateTime()
let $currentUser := sm:id()//sm:real/sm:username
(:let $currentUser := 

                            if(count($currentUser//sm:real) > 1) then 
                            for $user at $pos in $currentUser
                            return
                                if (contains($user//sm:username, 'admin'))
                                then ()
                                else(data($user[1]//sm:username[1]) || $pos)
                            
                            else $currentUser :)

let $currentUserUri := concat($teiEditor:baseUri[1], '/people/' , $currentUser)
(:let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)


let $docId := $data//docId/text()
(:let $docId := request:get-parameter('docid', ()):)
(:let $teiDoc := $teiEditor:doc-collection//id($docId):)
let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')")


let $elementNickname :=
if (exists($teiDoc//tei:div[@type="edition"]//tei:div[@type="textpart"])) then ('docTextsMultiple')
                    else  ('docTextSingles')

let $elementNode := $teiEditor:teiElements//teiElement[nm=$elementNickname]
let $teiXPath := $teiEditor:teiElements//teiElement[nm=$elementNickname]/xpath/text()
let $index := data($data//index)


let $newText := functx:change-element-ns-deep(<ab>{$data//newText/node()}</ab>, 'http://www.tei-c.org/ns/1.0', '')


let $abNode2beReplaced := if(exists(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId || "')//tei:div[@type='textpart'][" || $index || "]/tei:ab")))
        
                                                        then (util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId || "')//tei:div[@type='textpart'][" || $index || "]/tei:ab"))
                                                 else if (exists($teiDoc//tei:div[@type='edition']/tei:ab))
                                                 then ($teiDoc//tei:div[@type='edition']/tei:ab)
                                                 else()


(:BEFORE fixing index problem:)
(:let $abNode2beReplaced := if(exists($teiDoc//tei:div[@type='textpart'][" || $index || "]/tei:ab))
        
                                                        then ($teiDoc//tei:div[@type='textpart'][" || $index || "]/tei:ab)
                                                 else if (exists($teiDoc//tei:div[@type='edition']/tei:ab))
                                                 then ($teiDoc//tei:div[@type='edition']/tei:ab)
                                                 else()
:)

(:
util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId || "')//tei:div[@type='textpart'][" || $index || "]/tei:ab")
:)
(:            let $updatedNode :=  <updatedNode  xmlns="http://www.tei-c.org/ns/1.0">{parse-xml('<' || $lastNode || ">" || $updatedData|| '</' || $lastNode || '>')}</updatedNode>        :)
(:let $updatedTEINode :=  <updatedNode>{parse-xml('<' || $lastNode || ">" || $updatedData|| '</' || $lastNode || '>')}</updatedNode>:)

(:let $updatedTEINode := functx:change-element-ns-deep($updatedNode, 'http://www.tei-c.org/ns/1.0', ''):)

(:let $logs := collection($config:data-root || $teiEditor:project || "/logs"):)

(:let $updateXml := update insert $aaa/node() following $originalTEINode :)

let $logTest := teiEditor:logEvent("test" , "", (),
                        "$abNode2beReplaced: " || serialize($abNode2beReplaced, ())
                        ||
                        "$newText: " || $newText
                        )




let $updateXml :=if (not(exists($teiDoc//tei:div[@type="edition"]//tei:div[@type="textpart"]))) 
                    then (
(:                    update replace $teiDoc//tei:div[@type="edition"][$index]/tei:ab with $newText:)
                    )
                    else if (exists($teiDoc//tei:body/tei:div[@type="edition"]/tei:div[@type="textpart"][string($index)])) then(
                    update replace $abNode2beReplaced with $newText
(:                        update replace $teiDoc//tei:div[@type='textpart'][string($index)]/tei:ab with $newText:)
                    )
                    else(update replace $abNode2beReplaced with $newText)
let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')")

let $changeNode := <node>
           <change when="{$teiEditor:now}" who="{$currentUserUri}">{$data//comment/text()}</change></node>
let $insertRevisionChange := if($teiDoc//tei:revisionDesc/tei:listChange/tei:change) then
                            update insert
                            ('&#xD;&#xa;',
                            functx:change-element-ns-deep($changeNode, "http://www.tei-c.org/ns/1.0", "")/node())
                              following $teiDoc//tei:revisionDesc/tei:listChange/tei:change[last()]
                            else update insert
                                functx:change-element-ns-deep($changeNode, "http://www.tei-c.org/ns/1.0", "")/node()
                                into $teiDoc//tei:revisionDesc/tei:listChange

(:

let $logInjection :=
    update insert
    <apc:log type="document-update-text-{$index}" when="{$now}" what="{data($data/xml/docId)}" who="{$currentUser}">
        {$data}
        <docId>{$docId}</docId>
        <!--<lastNode>{$lastNode}</lastNode>
        -->
        <!--<origNode2>{$originalTEINode}</origNode2>
        -->
      <index>{$index}</index>
        <updatedData>{$newText}</updatedData>

    </apc:log>
    into $teiEditor:logs/id('all-logs')
    :)


(:let $save := teiEditor:saveData(string($data/xml/docId), string($data/xml/input), string($updatedData)):)
let $log := teiEditor:logEvent("document-update-text-" ||$index, $docId, $data, "" || exists($teiDoc//tei:div[@type="edition"]//tei:div[@type="textpart"]))
let $newContent := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" )

return

<data>
<newContent>{ $newContent}</newContent>
</data>


};


(:~ Function to update a node edited with a HTML textarea with data received from a HTTP request :)
declare function teiEditor:saveTextarea($data, $project){
let $now := fn:current-dateTime()
let $currentUser := sm:id()//sm:real/sm:username

let $currentUserUri := concat($teiEditor:baseUri[1], '/people/' , $currentUser)

let $docId := $data//docId/text()
let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"||$docId ||"')")

let $elementNickname :=
            $data//elementNickName/text()
let $elementNode :=if (not(exists($teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]))) 
                                        then $teiEditor:teiElements//teiElement[nm=$elementNickname] 
                                        else $teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]

let $xpath :=  $data//xpath/text()
let $index := data($data//index)


let $tmp0NewText := if(string-length($data//newText) > 0 and empty($data//newText/p)) then (teiEditor:addMissingParagraphTags($data//newText)) else ($data//newText)
let $tmp1NewText := if($tmp0NewText = " " or $tmp0NewText = "" or string-length($tmp0NewText) = 0) then (
   $tmp0NewText
) else (
    let $tryNewText := if ($elementNode/contentType/text() = "enrichedText") then (
        teiEditor:encodeHtmlToTeiFormat($tmp0NewText)
    ) else (
        $tmp0NewText
    )
    return $tryNewText
)
let $tmp2NewText := teiEditor:cleanEmptyNodesAndNamespaces($tmp1NewText)
let $newText := if ($tmp2NewText = " " or $tmp2NewText = "" or string-length($tmp2NewText) = 0) then (<p></p>) else ($tmp2NewText)
(:functx:change-element-ns-deep(<ab>{$data//newText/node()}</ab>, 'http://www.tei-c.org/ns/1.0', ''):)



let $originalTEINodeWithoutAttribute := 
            if(contains($xpath, '/@')) then util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')/" || functx:substring-before-last($xpath, '/') )
             else (util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')" || $xpath ))
let $log := console:log(ends-with($xpath, "tei:p"))

let $updateXml :=
            if($newText = " " or $newText = "" or string-length($newText) = 0) then (
                update value $originalTEINodeWithoutAttribute//text() with $newText
            ) else (
                update value $originalTEINodeWithoutAttribute with $newText/./node()
            )
let $newContent := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" )

return

<data>{$data}
<originalTEINodeWithoutAttribute>{ serialize( $originalTEINodeWithoutAttribute ) }</originalTEINodeWithoutAttribute> 
<newContent>{ teiEditor:cleanEmptyNodesAndNamespaces($newContent)}</newContent>
</data>


};


(:~ Function to update the XML file edited in the XML editor with data received from a HTTP request :)
declare function teiEditor:saveFile($data, $project){

let $docId := $data//docId/text()
(:let $teiDoc := $teiEditor:doc-collection//id($docId):)

let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')")

let $newContent := functx:change-element-ns-deep(<teiFile>{$data//newContent/node()}</teiFile>, 'http://www.tei-c.org/ns/1.0', '')
(:let $newContent := $data//newContent//TEI/node():)
let $originalFile :=util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId || "')")

let $updateHeader := update replace $teiDoc//tei:teiHeader with $newContent//tei:teiHeader
let $updateText := update replace $teiDoc//tei:text with $newContent//tei:text
let $updateSourceDoc := if($teiDoc//tei:SourceDoc) then update replace $teiDoc//tei:SourceDoc with $newContent//tei:sourceDoc else()
let $updateFacSim := if($teiDoc//tei:facsimile) then update replace $teiDoc//tei:facsimile with $newContent//tei:facsimile else()

(:let $updateXml :=   update replace $teiDoc//tei:TEI/node() with $newContent/././././node():)
return


teiEditor:logEvent('document-saveXmlFile', $docId, <originalData>{$originalFile}</originalData>, ''),
<data>{$data}</data>

};


(:~ Function to add a node to a document with data received from a HTTP request :)
declare function teiEditor:addData( $data as node(), $project as xs:string){

    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)

    (:let $data := request:get-data():)
(:    let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)

    let $docId := $data//docId/text()
    let $topConceptId := $data//topConceptId/text()
    let $xpath := replace($data//xpath/text(), 'tei:', '')
    let $lang := $data//lang/text()
    let $contentType := $data//contentType/text()
    let $elementNickname := $data//teiElementNickname/text()
    let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]))) 
                                        then $teiEditor:teiElements//teiElement[nm=$elementNickname] 
                                        else $teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]
    let $elementNickname4update := $elementNickname
        (: if($elementNode/ancestor::teiElement[last()]/fieldType/text() = "group")
            then $elementNode/ancestor::teiElement[last()]/nm/text()
            else $elementNickname :)
    let $attributeValueType := $elementNode//attributeValueType/text()
    (:let $docId := request:get-parameter('docid', ()):)
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $value4Attribute :=
                if(contains($data//valueConceptUris/text(), " ")) then $data//valueConceptUris/text() else $data//value/text()
    (: let $log := teiEditor:logEvent("test-before-insertNewElement", $docId, (), 
    "attribute: " || $attributeValueType
    || " exisits: " || exists(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                                    || $docId ||"')" || $data//xpath/text()))
        || " xpath: " || $data//xpath/text()
        ) :)
    let $processData:=
        (
        switch($attributeValueType)
        case "multi-uri" case "xml-value"
        return (
            (: Case attribute exists :)
            if(exists(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                                    || $docId ||"')" || $data//xpath/text())))
                then (
                    let $existingValuesInAttribute := data(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                                        || $docId ||"')" || $data//xpath/text()))
                    let $newValue:= if(normalize-space($existingValuesInAttribute)!="") then string-join(($existingValuesInAttribute, $value4Attribute), " ")
                                    else $value4Attribute
                    return
                    update value util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                                        || $docId ||"')" || $data//xpath/text()) 
                        with $newValue
                )
            else
                let $attributeName := substring-after($xpath, "/@")
               
                let $insertProvAttribute :=
                    update insert attribute provAttribute { $value4Attribute } into 
                    util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                                    || $docId ||"')" || substring-before($data//xpath/text(), "/@")) 
                let $renameProvAttribute:=
                    update rename util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                                    || $docId ||"')" || substring-before($data//xpath/text(), "/@") || "/@provAttribute")
                        as $attributeName
               
                        return ()
            )
        

        
        default return 
        (
            let $xpathEnd := 
                    if(contains(functx:substring-after-last($xpath, '/'), "@"))
                    then(
                        functx:substring-after-last(functx:substring-before-last($xpath, '/'), '/')
                        )
                    else
                    (functx:substring-after-last($xpath, '/')
                    )
            let $endingSelector := if(contains(functx:substring-after-last($xpath, '/'), "@"))
                    then(
                        functx:substring-after-last($xpath, '/@')
                        )
                    else
                    (
                    )
            let $xpathInsertLocation :=
                        if(contains(functx:substring-after-last($data//xpath/text(), '/'), "@"))
        (:                 if xpath is ending with \@, this must be removed:)
                            then(
                            if(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')//id('"
                                    || $docId ||"')" || $data//xpath/text()) )
        (:                             If there is already one node with same xpath, then location is last:)
                                    then (
                                    functx:substring-before-last($data//xpath/text(), '/') || '[last()]'
                                        )
                                else(functx:substring-before-last(functx:substring-before-last($data//xpath/text(), '/'), '/'))
                                )
                            else
                            (
                            $data//xpath/text()
        (:                    functx:substring-before-last($data//xpath/text(), '/'):)
                            )

        let $insertLocationElement :=
            (: If same element exists, then location is the last existing element, otherwise location is parent node :)
            if(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                || $docId ||"')" || $data//xpath/text()) )
                then (
                    util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                            || $docId ||"')" || $xpathInsertLocation)
                )
                else(

                        util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                        || $docId ||"')" ||

                        $xpathInsertLocation
                    ))
            



            let $newElement :=
            if(contains(functx:substring-after-last($xpath, '/'), "@"))
                    then(<newElement>
                            {element {replace($xpathEnd, "\[[0-9]*\]", "")}
                            {attribute {string($endingSelector)} {
                                $value4Attribute 
                                },
                                        if($lang and $lang!= "undefined") then attribute xml:lang {$lang} else (),
                            (if(contains($contentType, "text")) then functx:trim($data//valueTxt/text())
                            else ()
                            
                            )



                }}</newElement>
                )
                else(<newElement>
                {
                element {replace($xpathEnd, "\[[0-9]*\]", "")}
                    {attribute xml:lang {$lang},
                    $data//valueTxt/text()
                        }
                    }</newElement>
                )


            (: let $log := teiEditor:logEvent("test-before-insertNewElement", $docId, (),
                <result>

                    {exists(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                        || $docId ||"')" || $data//xpath/text()))}
                    $data//xpath/text(){ $data//xpath/text()}
                    $insertLocationElement::: {$insertLocationElement}
                    $xpathInsertLocation::: {$xpathInsertLocation}
                    Exist? {exists(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                || $docId ||"')" || $data//xpath/text()) )}
                Ending selector : { $endingSelector }
                $newElement: {serialize($newElement)}
                </result>
        ) :)



            let $insertData :=
                if($attributeValueType = "multi-uri") then 
                        util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                                    || $docId ||"')" || $data//xpath/text())
                    else
                    if(util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                        || $docId ||"')" || $data//xpath/text()) )
                        then (
                            update insert
                            ('&#xD;&#xa;',
                            functx:change-element-ns-deep($newElement, "http://www.tei-c.org/ns/1.0", "")/node())
        (:                     following $insertLocationElement:)
                            following $insertLocationElement
                        )
                    else(
                    update insert functx:change-element-ns-deep($newElement, "http://www.tei-c.org/ns/1.0", "")/node() into $insertLocationElement
                    )
            return ()
        )   (: END OF DEFAULT RETURN :)
        )   (: End of $processData :)
    (: let $insertLog :=
      update insert
      <log type="document-add-data" when="{$now}" what="{data($data/xml/docId)}" who="{$currentUser}">aa{$newElement}
      <nm>sd{$data/xml/teiElementNickname/text()}</nm>
      <xpath>{$data//xpath/text()}</xpath>
      <inserrtLoc>{$xpathInsertLocation}</inserrtLoc>
      <insertLocationElement>{ $insertLocationElement }</insertLocationElement>
      </log> into $teiEditor:logs/id('all-logs')
 :)

    (:let $updateXml := update value $originalTEINode with $updatedData:)



let $cn := console:log("fieldDtype: " || $elementNode/ancestor::teiElement[last()]/fieldType/text())
let $cn := console:log("elementNickname: " || $elementNickname)
    (:let $save := teiEditor:saveData(string($data/xml/docId), string($data/xml/input), string($updatedData)):)
    let $targetElement := $elementNickname || "_group_1"  
        (: (if($elementNode/ancestor::teiElement[last()]/fieldType/text() = "group")
            then $elementNode/ancestor::teiElement[last()]/nm/text() 
            else $elementNickname) || "_group_1"  :)
return
    <data>
        <updatedElement>{ util:eval("teiEditor:displayElement( $elementNickname4update, $docId,  (), ())")}</updatedElement>
        <targetElement>{ $targetElement }</targetElement>
        <newContent>{ util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" )}</newContent>
    </data>
};

(:~ Function to add a node with children to a document with data received from a HTTP request :)
declare function teiEditor:addGroupData( $data as node(), $project as xs:string){

    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)
    let $teiElementNickname := $data//teiElementNickname/text()
    let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]))) 
                                        then $teiEditor:teiElements//teiElement[nm=$teiElementNickname] 
                                        else $teiEditor:teiElementsCustom//teiElement[nm=$teiElementNickname]
    (:let $data := request:get-data():)
(:    let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)
    let $attributeValueType := $elementNode//attributeValueType/text()
    let $xpath := $elementNode/xpath/text()
    let $elementXPath := $elementNode/xpath/text()

    let $docId := $data//docId/text()
    let $topConceptId := $data//topConceptId/text()
    let $index := $data//index/text()
    let $xpathParam := replace($data//xpath/text(), 'tei:', '')
    let $dataTemplate := serialize($elementNode/template)
    (:let $updateTemplate :=
        for $item in $data//groupItem
            let $template := $dataTemplate
            let $element := "\$" || data($item/@teiElement)
            let $value := $item/text()
            let $updatedData := teiEditor:replaceData($dataTemplate, $element, $value)
                return
            $updatedData:)
    
    let $variables :=
        for $item in $data//groupItem
            return
             "\$" || data($item/@teiElement)
        (: let $log := console:log(serialize($data)) :)
        (: let $log := console:log(count($data//groupItem)) :)
    let $values :=
        for $item in $data//groupItem
            return
                if($item/text()) then ($item/text()) else ""
    (: let $log := console:log($values) :)
    let $node2insert := parse-xml(functx:replace-multi($dataTemplate, $variables, $values))
    (: let $log := console:log($node2insert) :)

    let $elementAncestors := $elementNode/ancestor::teiElement
    (: let $xpathRootWhenSubGroup := $xpath || "/" || $elementXPath :)
        (: if($elementNode/ancestor::teiElement)
            then (
                string-join(
                    for $ancestor at $pos in $elementAncestors
                        let $ancestorIndex :=   (:"[1]"       index on ancestor not really handled:)
                                    if($pos = 1 ) 
                                            then if($index and $attributeValueType!="multi-uri") 
                                                    then "[" || string($index) || "]" 
                                                    else ("")
                                    else ("")
                        return
                            if (contains($ancestor/xpath/text(), '/@'))
                                then substring-before($ancestor/xpath/text(), '/@')
                                        || $ancestorIndex
                                else $ancestor/xpath/text() || $ancestorIndex
                ) 
                (: || $xpath :)
                )
        else () :)

    (: let $xpath := functx:substring-before-last($elementNode/xpath, '/') :)
    (: let $xpath4Update := functx:substring-before-last($xpathRootWhenSubGroup, '/') :)
    (: let $xpath4Update := functx:substring-before-match($xpathRootWhenSubGroup, $elementNode/xpath/text()) :)
    let $xpath4Update := $data//xpath/text()
                
    let $xpathRootWhenSubGroup := $xpath4Update || "/" || $elementXPath
    let $xpath4Reload := $xpath4Update => replace(functx:substring-before-last($elementXPath, "/"), "")
                                        => replace("/$", "")
    (: let $log:=console:log("$xpathRootWhenSubGroup : " || $xpathRootWhenSubGroup) :)
    (: let $log:=console:log("$xpath4Update : " || $xpath4Update) :)
    (: let $log:=console:log("4Reload: " || $xpath4Reload) :)
        (: let $log:=console:log("repalce: " || functx:substring-before-last($elementXPath, "/")) :)
    let $location :=  util:eval( "$teiEditor:doc-collection/id('" ||$docId ||"')/" || $xpath4Update)

    let $log:=console:log($location)
     (: let $log:=console:log($elementNode/contentType="nodes") :)
    let $insertNode := 
        update insert functx:change-element-ns-deep($node2insert/template, "http://www.tei-c.org/ns/1.0", "")/node() into $location
    (: util:eval( "$teiEditor:doc-collection/id('" ||$docId ||"')/" || $xpath) :)

return 
<data>
<dataTemplate>{$dataTemplate}</dataTemplate>
<elementXPath>{ $elementXPath }</elementXPath>
<node2insert>{ $node2insert }</node2insert>
<xpath>{ $xpath }</xpath>
<xpathParam>{ $xpathParam }</xpathParam>
<xpath4Update>{ $xpath4Update }</xpath4Update>
<xpathGrp>{ $xpathRootWhenSubGroup}</xpathGrp>
<index>{ $index }</index>
<loc>{ $location }</loc>
<teiElementNickname>{ $teiElementNickname }</teiElementNickname>
<updatedElement>
{ teiEditor:displayElement($teiElementNickname, $docId, $index, $xpath4Reload) }</updatedElement>

</data>

};


(:~ Function to add bibliographical data to a document with data received from a HTTP request :)
declare function teiEditor:addBiblio( $data as node(), $project as xs:string){

    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)

    (:let $data := request:get-data():)
(:    let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)

    let $docId := $data//docId/text()
    (:let $docId := request:get-parameter('docid', ()):)
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"||$docId ||"')")

    let $bibRef := $data//biblioId/text()
    let $typeRef := $data//type/text()
    let $citedRange := $data//citedRange/text()
    let $calculatedCitedRange := if($citedRange != '') then 
                            <citedRange>{$citedRange}</citedRange>
                            else ()
    let $zoteroGroup := $data//zoteroGroup/text()
    let $zoteroTag := $data//zoteroTag/text()
    let $xpath :=
    switch ($typeRef)
       case "main"
       case "edition" return
                '//tei:div[@type="bibliography"][@subtype="edition"]/tei:listBibl'
       case "secondary" return
                '//tei:div[@type="bibliography"][@subtype="secondary"]/tei:listBibl'
        case "reference" return
                '//tei:div[@type="bibliography"][@subtype="reference"]/tei:listBibl'

       default return '//tei:div[@type="bibliography"][@subtype="' || $typeRef || '"]/tei:listBibl'


            (:let $nodesArray := tokenize($xpath, '/')
            let $lastNode := $nodesArray[last()]:)
(:    let $BibRefAsTei := zoteroPlugin:get-bibItem($zoteroGroup, $bibRef, "tei"):)
    let $BibRefAsTei := functx:change-element-ns-deep(zoteroPlugin:get-bibItem($zoteroGroup, $bibRef, "tei")
                                            , 'http://www.tei-c.org/ns/1.0', 'tei')

    let $bibTeiId := data($BibRefAsTei//tei:biblStruct/@xml:id)
    let $bibUri := data($BibRefAsTei//tei:biblStruct/@corresp)
    let $bibTeiIdRef := if ($bibUri != "") then $bibUri else concat("#", $bibTeiId)





    (:let $originalTEINode :=util:eval( "collection('/db/apps/" || $saveFunctions:project || "/data/documents')//id('"
                 ||$docId ||"')/" || $xpathWithTeiPrefix)
    :)



    (:insert new reference in main bibliography:)
    let $insertBiblioInBiblioRepo :=
        if ($teiEditor:biblioRepo//tei:biblStruct[@corresp = $bibUri])
            then (
                if($teiEditor:appVariables//biblioUpdateOnReferenceInsert ="noUpdate") then ()
                else update replace $teiEditor:biblioRepo//tei:biblStruct[@corresp = $bibUri] with $BibRefAsTei//tei:biblStruct)
            else(
                   update insert $BibRefAsTei//tei:biblStruct into $teiEditor:biblioRepo//tei:listBibl[@xml:id="mainBiblio"]
            )


(:let $insertLocationElementInDoc :=         util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                || $docId ||"')" || '//tei:div[@type="bibliography"][@subtype="edition"]/tei:listBibl'  )

:)


    let $insertBiblioInTeiDocument :=
        switch ($typeRef)
           case "main" 
           case "edition" return
           let $biblNode := <bibl xmlns="http://www.tei-c.org/ns/1.0">
                                 <ptr target="{$bibTeiIdRef}" />,
                                 <citedRange>{$citedRange}</citedRange>
                             </bibl>
                          return
                          update insert $biblNode into
                          $teiDoc//tei:div[@type="bibliography"][@subtype="edition"]/tei:listBibl
                          
 (:                  if (not(exists($teiDoc//tei:div[@type="bibliography"][@subtype="edition"]//tei:ptr[@target =  $bibTeiIdRef])))
                        then (
                        let $biblNode := <bibl xmlns="http://www.tei-c.org/ns/1.0">
                                 <ptr target="{$bibTeiIdRef}" />,
                                 <citedRange>{$citedRange}</citedRange>
                             </bibl>
                          return
                          update insert $biblNode into
                          $teiDoc//tei:div[@type="bibliography"][@subtype="edition"]/tei:listBibl
                        )
                        
                   else()
:)
           case "secondary" return
                   (: //tei:div[@type="bibliography"][@subtype="secondary"]/tei:listBibl:)
                   if (not(exists($teiDoc//tei:div[@type="bibliography"][@subtype="seconday"]//tei:ptr[@target =  $bibTeiIdRef])))
                        then (
                        let $biblNode := <bibl xmlns="http://www.tei-c.org/ns/1.0">
                                 <ptr target="{$bibTeiIdRef}"/>,
                                 <citedRange>{$citedRange}</citedRange>
                             </bibl>
                          return
                          update insert $biblNode into
                          $teiDoc//tei:div[@type="bibliography"][@subtype="secondary"]/tei:listBibl
                        )
                        
                   else()

           default return
                if (not(exists($teiDoc//tei:div[@type="bibliography"][@subtype=$typeRef]//tei:ptr[@target =  $bibTeiIdRef])))
                        then (
                        let $biblNode := <bibl xmlns="http://www.tei-c.org/ns/1.0">
                                 <ptr target="{$bibTeiIdRef}"/>,
                                 <citedRange>{$citedRange}</citedRange>
                             </bibl>
                          return
                          update insert $biblNode into
                          $teiDoc//tei:div[@type="bibliography"][@subtype=$typeRef]/tei:listBibl
                        )
                        
                   else()
(:    let $logs := collection($config:data-root || $teiEditor:project || "/logs"):)



    (:let $updateXml := update value $originalTEINode with $updatedData:)

let $newContent := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" )

let $newBiblList := teiEditor:bibliographyPanelRefList($newContent, $docId, $typeRef, $xpath, $zoteroTag, ())
      
return
        <data>
        <newBiblList>{ $newBiblList }
 (: { switch($typeRef)
     case "main"
     case "edition"
     case "secondary" return
        $newBiblList
(:     teiEditor:principalBibliography( $docId ) :)
     default return 
(:     teiEditor:principalBibliography( $docId ) :)
     for $bibRef at $pos in $teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype='edition']/tei:listBibl//tei:bibl
        order by $bibRef//tei:ptr/@target
        return
            teiEditor:displayBibRef($docId, $bibRef, $typeRef, $pos)
           
     } :)
       </newBiblList>
       <newContent>{ $newContent}</newContent>
        </data>
};


(:~ Function related to biblio ref insertion into a note:)
declare function teiEditor:insertBiblioRef($data as node(), $project as xs:string){

    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)

    (:let $data := request:get-data():)
(:    let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)

    let $docId := $data//docId/text()
    (:let $docId := request:get-parameter('docid', ()):)
    
    let $bibRef := $data//biblioId/text()
    let $typeRef := $data//type/text()
    let $citedRange := $data//citedRange/text()
    let $calculatedCitedRange := if($citedRange != '') then 
                            <citedRange>{$citedRange}</citedRange>
                            else ()
    let $zoteroGroup := $data//zoteroGroup/text()
    let $BibRefAsTei := functx:change-element-ns-deep(zoteroPlugin:get-bibItem($zoteroGroup, $bibRef, "tei")
                                            , 'http://www.tei-c.org/ns/1.0', 'tei')

    let $bibTeiId := data($BibRefAsTei//tei:biblStruct/@xml:id)
    let $bibUri := data($BibRefAsTei//tei:biblStruct/@corresp)
    let $bibTeiIdRef := if ($bibUri != "") then $bibUri else concat("#", $bibTeiId)
    
    (:insert new reference in main bibliography:)
    let $insertBiblioInBiblioRepo :=
        if ($teiEditor:biblioRepo//tei:biblStruct[@corresp = $bibUri])
            then (update replace $teiEditor:biblioRepo//tei:biblStruct[@corresp = $bibUri] with $BibRefAsTei//tei:biblStruct)
            else(
                   update insert $BibRefAsTei//tei:biblStruct into $teiEditor:biblioRepo//tei:listBibl[@xml:id="mainBiblio"]
            )
    return
        $bibUri

};

declare function teiEditor:addmsItemToDoc($data as node(), $project as xs:string){
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)
    let $docId := $data//docId/text()
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $msItemList:= $teiDoc//tei:msItem
    let $msItemNo := (count($msItemList), 1)
    
    let $textTypeUris := $data//textTypeUri[./text() != ""]
    
                           
    let $msItemNodes :=
<node>
                {for $textTypeUri at $pos in $textTypeUris
                   let $textTypeConcept:= skosThesau:getSkosConcept($textTypeUri)        
                    return
                  <msItem xml:id="{ $docId }-msItem-{ sum($msItemNo, $pos) }" class="{ $textTypeUri }">
                      {for $lang in $teiEditor:languages//lang
                        let $label:= $textTypeConcept//skos:prefLabel[@xml:lang=$lang]/text() 
                      return
                        if(not($label)) then () else 
                          element {"title"}
                            {attribute {"xml:lang" } {$lang/text() }, $label
                            }
                        }
                        <note/>
                  </msItem>}
</node>
    
    let $updatemsItem :=
            update insert functx:change-element-ns-deep($msItemNodes, "http://www.tei-c.org/ns/1.0", "")/node()
                    into $teiDoc//tei:msDesc/tei:msContents
    
    return
    <data>
    <updatedElement>{ teiEditor:msItemManager($docId)}</updatedElement>
    </data>

};

declare function teiEditor:addmsItemWithLocationToDoc($data as node(), $project as xs:string){
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)
    let $docId := $data//docId/text()
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $msItemList:= $teiDoc//tei:msItem
    let $msItemNo := sum((count($msItemList), 1))
    
    let $locationTypeUri :=  $data//locationTypeUri[./text() != ""]
    let $textTypeUri := $data//textTypeUri[./text() != ""]
    
    let $surfaceList := $teiDoc//tei:sourceDoc
    let $surfaceNo := sum((count($surfaceList)), (1))
(:    let $surfaceTypeAttribute := skosThesau:getLabel($locationTypeUri, "xml"):)
    let $surfaceToInsert :=
    <node>
                {
                    for $surface in $locationTypeUri
                    let $surfaceTypeAttribute := skosThesau:getLabel(data($surface), "xml")
                    return
                        if($surfaceList//tei:surface[@ana = $surfaceTypeAttribute]) then ()
                            else 
                  <surface xml:id="{ $docId }-{ $surfaceTypeAttribute }" ana="{ $surfaceTypeAttribute }"/>
              
              }</node>
                
    
    let $surfaceId := string-join((for $surface in $locationTypeUri
                        let $surfaceTypeAttribute := skosThesau:getLabel(data($surface), "xml")
                        return
                        if($surfaceList//tei:surface[@ana = $surfaceTypeAttribute])
                                    then '#' || data($surfaceList//tei:surface[@ana = $surfaceTypeAttribute]/@xml:id)
                                    else '#' || $docId || "-" || $surfaceTypeAttribute), " ")
    let $textTypeConcept:= skosThesau:getSkosConcept($textTypeUri)                               
    let $msItemNode :=
<node>

                  <msItem xml:id="{ $docId }-msItem-{ $msItemNo }" class="{ $textTypeUri }">
                      <locus target="{ $surfaceId }"/>
                      {for $lang in $teiEditor:languages//lang
                        let $label:= $textTypeConcept//skos:prefLabel[@xml:lang=$lang]/text() 
                      return
                        if(not($label)) then () else 
                          element {"title"}
                            {attribute {"xml:lang" } {$lang/text() }, $label
                            }
                        }
                  </msItem>
</node>
    
    let $updateSurface :=
            for $surface in $locationTypeUri
                    let $surfaceTypeAttribute := skosThesau:getLabel(data($surface), "xml")
            return
            if(exists($surfaceList//tei:surface[@ana = $surfaceTypeAttribute]))
            then ()
            else (update insert functx:change-element-ns-deep($surfaceToInsert, "http://www.tei-c.org/ns/1.0", "")/node()
                    into $teiDoc//tei:sourceDoc)
    
    let $updatemsItem :=
            update insert functx:change-element-ns-deep($msItemNode, "http://www.tei-c.org/ns/1.0", "")/node()
                    into $teiDoc//tei:msDesc/tei:msContents
    
    return
    <data>
    <updatedElement>{ teiEditor:msItemManager($docId)}</updatedElement>
    {$surfaceList}
    {$surfaceToInsert}
    ID : {$surfaceId }
    { $msItemNode }
    </data>

};

declare function teiEditor:addmsFragToDoc($data as node(), $project as xs:string){
    let $teiTemplate:= doc("/db/apps/" || $project || "/data/teiEditor/docTemplates/teiTemplate" || functx:capitalize-first($project) || ".xml")
    return
    if(not($teiTemplate//tei:msFrag)) then
        let $message:= 'Cannot add a msFrag to the current document because there is no msFrag element in main TEI template! Please consider adding the TEI element to the main template ("/data/teiTemplates/teiTemplate' || functx:capitalize-first($project) || '.xml}").'
        return
            <data>
        <message>{ $message }</message>
        </data>
    else
    (
    let $docId := $data//docId/text()
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $msFragList:= $teiDoc//tei:msFrag
    let $msFragNo := count($msFragList)
    let $msFragNode :=
<node>
                { $teiTemplate//tei:msFrag }
</node>
   let $updatemsItem :=
        if($msFragNo =0) then
            update insert functx:change-element-ns-deep($msFragNode, "http://www.tei-c.org/ns/1.0", "")/node()
                    into $teiDoc//tei:fileDesc/tei:sourceDesc/tei:msDesc
        else 
            update insert functx:change-element-ns-deep($msFragNode, "http://www.tei-c.org/ns/1.0", "")/node()
                    following $teiDoc//tei:msFrag[last()]
    return
    <data>
       <message></message>
        <newmsFragManager>{ teiEditor:msFragManager($docId)}</newmsFragManager>
        <newRepositoryManager>
        { teiEditor:repositoryManagerOnMsFrag($docId,  (util:eval( "collection('" || $teiEditor:doc-collection-path || "')/id('" || $docId || "')")//tei:msFrag))
            }</newRepositoryManager>
    </data>
    )
};

(:~ Function to create a prosoporgraphical record and add a link to it in a document, based on data received from a HTTP request :)
declare function teiEditor:createAndAddPersonToDoc( $data as node(), $project as xs:string){

let $now := fn:current-dateTime()
let $currentUser := data(sm:id()//sm:username)

(:let $data := request:get-data():)
(:let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)

let $docId := $data//docId/text()
(:let $docId := request:get-parameter('docid', ()):)
let $teiDoc := $teiEditor:doc-collection/id($docId)

let $idPrefix := $teiEditor:appVariables//idPrefix[@type='people']/text()

let $idList := for $id in $teiEditor:peopleRepo//.[contains(./@xml:id, $idPrefix)]
        return
        <item>
        {substring-after($id/@xml:id, $idPrefix)}
        </item>

let $last-id:= fn:max($idList)
let $newId := $idPrefix || fn:sum(($last-id, 1))
let $newUri := $teiEditor:baseUri|| "/apc/people/" || $newId


let $personRef :=
    <nodeToInsert>
        <person corresp="{$newUri}"/>
    </nodeToInsert>
let $personRecord :=
    <nodeToInsert>
        <person rdf:about="{$newUri}" xml:id="{$newId}" sex="">
            {for $names at $pos in $data//persName[text()]
            return
            (<persName type="{data($names/@type)}">{$names/text()}</persName>,
            if($pos < count($data//persName[text()])) then ('&#xD;&#xa;') else())}
        <dct:created>{$now}</dct:created>
        </person>
    </nodeToInsert>

(:let $addSex := if($data//sex[string()]) then (functx:add-attributes($personRecord/self, xs:QName('sex'),
   $data//sex))
                else():)

(:insert new reference in main bibliography:)
(:let $insertPeopleInRepo :=
    if ($teiEditor:biblioRepo//tei:biblStruct[@xml:id = $bibTeiId]) then ()
    else(
               update insert $BibRefAsTei//tei:biblStruct into $teiEditor:biblioRepo//.[@xml:id="mainBiblio"]
        )
:)

let $insertRefToPeopleInTeiDocument :=
    if($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person[@corresp=$newUri]) then()
    else(
            if($teiDoc//tei:profileDesc/tei:particDesc/tei:listPerson[@type="peopleInDocument"]//tei:person) then(
            update insert ('&#xD;&#xa;',
                functx:change-element-ns-deep($personRef, "http://www.tei-c.org/ns/1.0", "")/node())
                    following
                 $teiDoc//tei:profileDesc/tei:particDesc/tei:listPerson[@type="peopleInDocument"]//tei:person[last()]
            )
            else(
             update insert functx:change-element-ns-deep($personRef, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:profileDesc/tei:particDesc/tei:listPerson[@type="peopleInDocument"]
                 )
                 )

let $createPersonInPeopleRep :=
             update insert $personRecord/node() into
                 $teiEditor:peopleRepo//.[@type="ancientPeople"]



(:let $logs := collection($config:data-root || $teiEditor:project || "/logs"):)



(:let $updateXml := update value $originalTEINode with $updatedData:)





let $logInjection :=
    update insert
    <apc:log type="document-update-add-person" when="{$now}" what="{data($data/xml/docId)}" who="{$currentUser}">

        {$personRecord}
    </apc:log>
    into $teiEditor:logs/id('all-logs')

(:let $save := teiEditor:saveData(string($data/xml/docId), string($data/xml/input), string($updatedData)):)

return

<div>
                        {
                                for $people in $teiDoc//tei:listPerson[@type="peopleInDocument"]/tei:person
                                    let $personUri := data($people/@corresp)
                                    let $personId := functx:substring-after-last($personUri, '/')
                                    let $personRecord := $teiEditor:peopleRepo/id($personId)
                                    let $names := string-join($personRecord//persName, ' ')
(:                                order by $personRecord/persName:)

                                return
                                <span class="btn btn-light listItem">

                                <span title="Annotate selected text with reference to {functx:trim(normalize-space($names))}"
                                onclick="addPeople({$data//index}, '{functx:trim(normalize-space(data($personUri)))}')">
                                {$names}</span>
                                </span>
                                }
                                </div>

};

(:~ Function to add a project's place reference to tei:listPlace. :)
declare function teiEditor:addProjectPlaceToDoc( $data as node(), $project as xs:string){

let $now := fn:current-dateTime()
let $currentUser := data(sm:id()//sm:real//sm:username)

(:let $data := request:get-data():)
(:let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)

let $docId := $data//docId/text()
let $placeUri := $data//placeUri/text()
let $placeUriShort := substring-before($placeUri, "#this")
let $placeType := skosThesau:getLabel($data//placeType/text(), 'xml')

(:let $docId := request:get-parameter('docid', ()):)
let $teiDoc := $teiEditor:doc-collection/id($docId)
let $placeCollectionProject := collection($teiEditor:data-repository-path|| "/places/" || $project)
let $placeDetails := $placeCollectionProject//spatial:Feature[@rdf:about=$placeUri][1]
let $placename := if(exists($placeDetails//dcterms:title[@xml:lang = $teiEditor:lang]))
                    then $placeDetails//dcterms:title[@xml:lang = $teiEditor:lang]/text()
                    else $placeDetails//dcterms:title[1]/text()
let $placeExactMatch := if($placeDetails//skos:exactMatch/@rdf:resource != "") then
                ( for $exactMatch in functx:distinct-nodes($placeDetails//skos:exactMatch)
                return ' ' || $exactMatch/@rdf:resource )
                else()
let $placeRefinDoc :=
            if(not($teiDoc//tei:fileDesc/tei:sourceDesc//tei:listPlace)) then(
            <nodeToInsert>
        <listPlace>
            <place>
                    <placeName ref="{$placeUriShort}{ $placeExactMatch }" ana="{ $placeType }">{ $placename }</placeName>
            </place>
        </listPlace>

    </nodeToInsert>
)
            else
<nodeToInsert>
    <place>
        <placeName ref="{$placeUriShort}{ $placeExactMatch }" ana="{ $placeType }">{$placename }</placeName>
        {if($teiEditor:appVariables//placeCommentsActive/text() = "true")
                        then <note/> else ()}
        </place>

    </nodeToInsert>





let $insertRefToPlaceInTeiDocument :=

    if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:listPlace//tei:place[@corresp=$placeUriShort]) then()
    else(
       if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:listPlace//tei:place) then(
            update insert ('&#xD;&#xa;',
                functx:change-element-ns-deep($placeRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node())
                    following
                 $teiDoc//tei:sourceDesc/tei:listPlace//tei:place[last()]
            )
            else(
                  if($teiDoc//tei:fileDesc/tei:sourceDesc//tei:listPlace) then(
                  update insert functx:change-element-ns-deep($placeRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:sourceDesc/tei:listPlace
                 )
                  else

             update insert functx:change-element-ns-deep($placeRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:sourceDesc
                 )
                 )

let $updateProvenance := 
        if(lower-case($placeType) = "provenance") then
            (if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:provenance/tei:location/tei:placeName)
            then 
            update value $teiDoc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:provenance/tei:location/tei:placeName/@ref with $placeUriShort
            else if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msFrag[1]/tei:history/tei:provenance/tei:location/tei:placeName) then
            update value $teiDoc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msFrag[1]/tei:history/tei:provenance/tei:location/tei:placeName/@ref with $placeUriShort
            (: no location element :)
            else if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:provenance/tei:placeName)
            then 
            update value $teiDoc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:provenance/tei:placeName/@ref with $placeUriShort
            else if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msFrag[1]/tei:history/tei:provenance/tei:placeName) then
            update value $teiDoc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msFrag[1]/tei:history/tei:provenance/tei:placeName/@ref with $placeUriShort
            else()
        )
        else ()
let $updateOrigPlace := 
        if(lower-case($placeType) = "origin") then
        (: Check if place already as origPlace :)
            if($teiDoc//tei:origPlace/tei:placeName[contains(./@ref, $placeUriShort)])
                then ()
        (: Check if placeName node exists :)
            else if(exists($teiDoc//tei:origPlace/tei:placeName[@ref=""]))
                then (update value $teiDoc//tei:origPlace/tei:placeName/@ref with concat($placeUriShort, $placeExactMatch),
                    update value $teiDoc//tei:origPlace/tei:placeName with $placeDetails//dcterms:title/text())
        (: Check if origPlace no exists :)
            else if(exists($teiDoc//tei:origPlace))
                then (
                    let $newPlaceName := <node>
                        <placeName type="ancient" ref="{ concat($placeUriShort, $placeExactMatch) }">{ $placeDetails//dcterms:title/text() }</placeName>
                        </node>
                    return
                        update insert functx:change-element-ns-deep($newPlaceName/node(), "http://www.tei-c.org/ns/1.0", "") into $teiDoc//tei:origPlace
                    )
        (: if no origPlace, then creates node :)
            else (
                    let $newOrigPlace := <node>
                        <origPlace>
                            <placeName type="ancient" ref="{ concat($placeUriShort, ($placeExactMatch)) }">{ $placeDetails//dcterms:title/text() }</placeName>
                        </origPlace>
                        </node>
                    return
                        update insert functx:change-element-ns-deep($newOrigPlace/node(), "http://www.tei-c.org/ns/1.0", "") following $teiDoc//tei:origDate
            )
        else ()


return
(:    teiEditor:logEvent("document-insert-place" , $docId, $newPlaceRecord, ()),:)
    <result>
      <newList>{
        
       teiEditor:placesListNoHeader($docId)
       
      }
      </newList>
        <newListForAnnotation><div id="listOfPlaces">{ teiEditor:placeListForAnnotation($docId, 1)}</div>
      </newListForAnnotation>

    {
    if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:listPlace//tei:place/tei:idno[contains(., $placeUri)])
    then
    (<result>{$placeUri} Place already listed</result>)
    else(
            if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:listPlace//tei:placeName) then(
            <div id="listOfPlacesOverview">{ teiEditor:placesList($docId, ())}</div>
            )
            else(
                  if($teiDoc//tei:fileDesc/tei:sourceDesc//tei:listPlace) then(
              <div id="listOfPlacesOverview">{ teiEditor:placesList($docId, ())}</div>

                 )
                  else

             <div id="listOfPlaces">{ teiEditor:placesList($docId, ())}</div>

                 )
                 )
}</result>

};



(:~ Function to add a project's person reference to tei:listPerson. :)
declare function teiEditor:addProjectPersonToDoc( $data as node(), $project as xs:string){

let $now := fn:current-dateTime()
let $currentUser := data(sm:id()//sm:real//sm:username)

(:let $data := request:get-data():)
(:let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)

let $docId := $data//docId/text()
let $personUri := $data//peopleUri/text()
let $personUriShort := substring-before($personUri, "#this")

(:let $docId := request:get-parameter('docid', ()):)
let $teiDoc := $teiEditor:doc-collection/id($docId)
let $peopleCollectionProject := collection($teiEditor:data-repository-path|| "/people")
let $personDetails := $peopleCollectionProject//lawd:person[@rdf:about=$personUri][1]

let $personRefinDoc :=
            if(not($teiDoc//tei:listPerson[@type="peopleInDocument"]
            )) then(
            <nodeToInsert>
        <listPerson type="peopleInDocument">
    <person corresp="{$personUriShort}">
            <persName xml:lang="en">{ $personDetails//lawd:personalName[@xml:lang='en']/text() }</persName>
        </person>
        </listPerson>

    </nodeToInsert>
)
            else
<nodeToInsert>
    <person corresp="{$personUriShort}">
            <persName xml:lang="en">{ $personDetails//lawd:personalName[@xml:lang='en']/text() }</persName>
        </person>
    </nodeToInsert>





let $insertRefToPresonInTeiDocument :=

    if($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person[@corresp=$personUriShort]) then()
    else(




         if($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person) then(
            update insert ('&#xD;&#xa;',
                functx:change-element-ns-deep($personRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node())
                    following
                 $teiDoc//tei:listPerson[@type="peopleInDocument"]/tei:person[last()]
            )

        else(
                  if($teiDoc//tei:listPerson[@type="peopleInDocument"]) then(
                  update insert functx:change-element-ns-deep($personRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:listPerson[@type="peopleInDocument"]
                 )
                  
                  else


             update insert functx:change-element-ns-deep($personRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:profileDesc
                 )
                 )




return
(:    teiEditor:logEvent("document-insert-place" , $docId, $newPlaceRecord, ()),:)
    <result>
      <newList>{
        
       teiEditor:peopleList($docId)
       
      }
      
      </newList>
        <newListForAnnotation><div id="listOfPeople">{ teiEditor:listPeople($docId, 1) }</div>
      </newListForAnnotation>

    {
    if($teiDoc//tei:profileDesc//tei:listPerson//tei:person[contains(./@corresp, $personUri)])
    then 
    (<result>{$personUri} This person is already listed</result>)
    else(
            if($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person) then(
            <div id="listOfPlacesOverview">{ teiEditor:peopleList($docId)}</div>
            )
            else(
                  if($teiDoc//tei:listPerson[@type="peopleInDocument"]) then(
              <div id="listOfPlacesOverview">{ teiEditor:peopleList($docId)}</div>

                 )
                  else

             <div id="listOfPlaces">{ teiEditor:peopleList($docId) }</div>

                 )
                 )
}</result>

};

(:~ Function to add a project's person reference to tei:listPerson. :)
declare function teiEditor:addRoyalPersonToDoc( $data as node(), $project as xs:string){

let $now := fn:current-dateTime()
let $currentUser := data(sm:id()//sm:real//sm:username)

(:let $data := request:get-data():)
(:let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)

let $docId := $data//docId/text()
let $royalPersonUri := $data//royalPersonUri/text()
(: let $personUriShort := substring-before($personUri, "#this") :)

(:let $docId := request:get-parameter('docid', ()):)
let $teiDoc := $teiEditor:doc-collection/id($docId)
(: let $peopleCollectionProject := collection($teiEditor:data-repository-path|| "/people") :)
(: let $personDetails := $peopleCollectionProject//lawd:person[@rdf:about=$personUri][1] :)

let $personRefinDoc :=
            if(not($teiDoc//tei:listPerson[@type="peopleInDocument"]
            )) then(
            <nodeToInsert>
        <listPerson type="peopleInDocument">
    <person corresp="{$royalPersonUri}">
            <persName xml:lang="{ $teiEditor:lang }">{ skosThesau:getLabel($royalPersonUri, $teiEditor:lang ) }</persName>
        </person>
        </listPerson>

    </nodeToInsert>
)
            else
<nodeToInsert>
    <person corresp="{$royalPersonUri}">
            <persName xml:lang="{ $teiEditor:lang }">{ skosThesau:getLabel($royalPersonUri, $teiEditor:lang ) }</persName>
        </person>
    </nodeToInsert>
let $insertRefToPresonInTeiDocument :=
    if($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person[@corresp=$royalPersonUri]) then()
    else(
         if($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person) then(
            update insert ('&#xD;&#xa;',
                functx:change-element-ns-deep($personRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node())
                    following
                 $teiDoc//tei:listPerson[@type="peopleInDocument"]/tei:person[last()]
            )

        else(
                  if($teiDoc//tei:listPerson[@type="peopleInDocument"]) then(
                  update insert functx:change-element-ns-deep($personRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:listPerson[@type="peopleInDocument"]
                 )
                  
                  else
                update insert functx:change-element-ns-deep($personRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:profileDesc
                 )
                 )

return
(:    teiEditor:logEvent("document-insert-place" , $docId, $newPlaceRecord, ()),:)
    <result>
      <newList>{
       teiEditor:peopleList($docId)
      }
      
      </newList>
        <newListForAnnotation><div id="listOfPeople">{ teiEditor:listPeople($docId, 1) }</div>
      </newListForAnnotation>

    {
    if($teiDoc//tei:profileDesc//tei:listPerson//tei:person[contains(./@corresp, $royalPersonUri)])
    then 
    (<result>{ $royalPersonUri } This person is already listed</result>)
    else(
            if($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person) then(
            <div id="listOfPlacesOverview">{ teiEditor:peopleList($docId)}</div>
            )
            else(
                  if($teiDoc//tei:listPerson[@type="peopleInDocument"]) then(
              <div id="listOfPlacesOverview">{ teiEditor:peopleList($docId)}</div>

                 )
                  else

             <div id="listOfPlaces">{ teiEditor:peopleList($docId) }</div>

                 )
                 )
}</result>

};

(:~ Function to remove an item from a list. :)
declare function teiEditor:removeItemFromList( $data as node(), $project as xs:string){
            let $now := fn:current-dateTime()
            let $currentUser := data(sm:id()//sm:username)
            (:let $data := request:get-data():)
            (: let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents") :)
            let $docId := $data//docId/text()
            (:let $docId := request:get-parameter('docid', ()):)
            let $teiDoc :=util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" || $docId ||"')" ) 
            (:$teiEditor:doc-collection/id($docId):)
            let $item := $data//item/text()
            let $topConceptId := $data//topConceptId/text()
            let $refId := if (starts-with($item, "http")) then $item else "#" || $item
            
            let $index := if($data//index/text()) then xs:int($data//index/text())
                        else "1"
            
            let $list := $data//list/text()
            (:let $citedRange := $data//citedRange/text():)
            let $nodeToRemove :=
            if(contains(lower-case($list), "biblio"))
                then
                (      let $node := $teiDoc//tei:div[@type="bibliography"][@subtype=substring-before($list, "Biblio")]/tei:listBibl//tei:bibl[$index]
                        return
                        update delete $node
               (: case "secondaryBiblio" return
                        let $node := $teiDoc//tei:div[@type="bibliography"][@subtype="secondary"]/tei:listBibl//tei:bibl[$index]
                        return(
                        update delete $node :)
                        (:,
                        if ($teiEditor:doc-collection//tei:ptr[@target = $refId]) then ()
                        else(
                                   update delete $teiEditor:biblioRepo//.[@xml:id=$item]
                            ):)
                )            
            
            else
            switch ($list)
               case "people"
               case "peopleManagerEgypt"
                 return
                        let $node := $teiDoc//tei:person[@corresp=$item]
                        let $relationNode := if(exists($teiDoc//tei:relation[@passive=$item]))
                                            then $teiDoc//tei:relation[@passive=$item]
                                            else $teiDoc//tei:relation[contains(./@mutual, " " || $item)]
                        return
                        (update delete $node,
                        (if(exists($relationNode)) then update delete $relationNode else())
                        )
            case "place" return
                        let $placeListNode := $teiDoc//tei:listPlace//tei:place[$index]
                        let $placeType := $placeListNode/@ana 
                        let $placeNodes := $teiDoc//tei:place[tei:placeName[contains(./@ref, $item)] and not(ancestor-or-self::tei:listPlace)]
                        
                        return(
                            (if($placeType!="mentioned-in-text")
                                then update delete $placeNodes else ()),
                            update delete $placeListNode)
            case "msItem" return
                let $node := $teiDoc//tei:msContents//tei:msItem[$index]
                        return
                        update delete $node
            case "msFrag" return
                let $node := $teiDoc//tei:msDesc//tei:msFrag[$index]
                        return
                        update delete $node
            
            default return
                      let $elementNode :=if (exists($teiEditor:teiElementsCustom//teiElement[nm=$list])) then
                        $teiEditor:teiElementsCustom//teiElement[nm=$list]
                        else ($teiEditor:teiElements//teiElement[nm=$list]) 
                    let $attributeValueType:= $elementNode/attributeValueType/text()
                    
                        let $elementAncestors := $elementNode/ancestor::teiElement
                        let $xpathRootWhenSubGroup :=
                            if($elementNode/ancestor::teiElement)
                                then (
                                                string-join(
                                                    for $ancestor at $pos in $elementAncestors
                                                        let $ancestorIndex :=   (:"[1]"       index on ancestor not really handled:)
                                                                    if($pos = 1) 
                                                                            then if($index and $attributeValueType!="multi-uri") 
                                                                                    then "[" || string($pos) || "]" 
                                                                                    else ("")
                                                                             else ("")
                                                        return
                                                            if (contains($ancestor/xpath/text(), '/@'))
                                                                then substring-before($ancestor/xpath/text(), '/@')
                                                                        || $ancestorIndex
                                                                else $ancestor/xpath/text() || $ancestorIndex
                                                )
                                        
                                        )
                                else ()
    
                        let $teiXPath := 
                            if($attributeValueType = "multi-uri" or $attributeValueType = "xml-value") then $elementNode/xpath/text()
                            else if (contains($elementNode/xpath/text(), '/@')) then substring-before($elementNode/xpath/text(), '/@')
                                                            else $elementNode/xpath/text()
                        let $constructedXPath := (if($xpathRootWhenSubGroup != "") then
                                        $xpathRootWhenSubGroup else ()) || $teiXPath
                        let $node := util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                                            || $docId ||"')" || $constructedXPath || "[" || $index ||"]")
                        let $log := console:log($constructedXPath)
                        return
                      
                    if($attributeValueType = "multi-uri" or $attributeValueType = "xml-value")
                    then
                        let $oldValue := util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                                            || $docId ||"')" || $constructedXPath)
                        let $tokenizedOldValue:= tokenize($oldValue, " ")
                        let $newValue := string-join(($tokenizedOldValue[position()< $index], $tokenizedOldValue[position()> $index]), " ")
                        return 
                        
                        update value $oldValue with $newValue 
                    else
                      update delete $node
            
            
            
            (: let $deleteBiblioFromBiblioRepo :=
                if ($teiEditor:doc-collection//tei:ptr[@target = $refId]) then ()
                else(
                           update delete $teiEditor:biblioRepo//.[@xml:id=$item]
                    ) :)
            
            (:let $logInjection :=
                update insert
                <log type="document-update-remove-item-from-{$list}" when="{$now}" what="{data($data/xml/docId)}" who="{$currentUser}">
                    {$data}
                    <docId>{$docId}</docId>
                    <!--<lastNode>{$lastNode}</lastNode>
                    -->
                    <origNode2>$originalTEINode</origNode2>
                    <refId>{$item}</refId>
                      <node2beDeleted>{util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                             || $docId ||"')" || $item || "[" || $index ||"]")}</node2beDeleted>
            
                </log>
                into $teiEditor:logs/id('all-logs'):)
            
            (:let $save := teiEditor:saveData(string($data/xml/docId), string($data/xml/input), string($updatedData)):)
            let $newContent := util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')" )
            
            
            return  (:Main return:)
             <data>
          
                { if(contains(lower-case($list), "biblio"))
                then
                    (
                        let $biblioType := substring-before($list, "Biblio")
                        return
                    <newList>
                        {for $bibRef at $pos in $teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype=$biblioType]/tei:listBibl//tei:bibl
                            return
                            teiEditor:displayBibRef($docId, $bibRef, $biblioType, $pos)
                        }</newList>
                    )
                (: case "secondaryBiblio" return
                    <div><newList>
                        {for $bibRef at $pos in $teiDoc//tei:text/tei:body/tei:div[@type='bibliography'][@subtype='secondary']/tei:listBibl//tei:bibl
                        order by $bibRef//tei:ptr/@target
                        return
                            teiEditor:displayBibRef($docId, $bibRef, "secondary", $pos)
                           }
                           </newList></div> :)
                (:case "people" return
                            (
                                            for $people in $teiDoc//tei:listPerson[@type="peopleInDocument"]/tei:person
                                                let $personUri := data($people/@corresp)
                                                let $personId := functx:substring-after-last($personUri, '/')
                                                let $personRecord := $teiEditor:peopleRepo/id($personId)
                                                let $names := string-join($personRecord//persName, ' ')
            (\:                                order by $personRecord/persName:\)
            
                                            return
                                            teiEditor:displayPersonItem($teiEditor:docId, $personUri, $index)
                             ):)
                else
                switch($list)
                case "place" return
                            <div>
                            <newList>{
                        teiEditor:placesListNoHeader($docId)
                        }</newList>
                        <newListForAnnotation>
                             <div id="listOfPlaces">{ teiEditor:placeListForAnnotation($docId, 1)}</div>
                        </newListForAnnotation>
                        </div>
                case "people"
                 return
                            <div>
                            <newList>{
                       teiEditor:peopleList($docId)
                        }</newList>
                        <newListForAnnotation>
                            <div id="listOfPeople">{ teiEditor:listPeople($docId, 1) }</div>
                        </newListForAnnotation>
                        <newListForMentionedNames>
                            <div id="newListOfMentionedNames">{ teiEditor:peopleMentionsInDoc($docId) }</div>
                        </newListForMentionedNames>
                        </div>   
               case "peopleManagerEgypt" return
                       <div>
                            <newList>{
                       teiEditor:listPeopleManager($docId)
                        }</newList>
                        <newListForAnnotation>
                            <div id="listOfPeople">{ teiEditor:listPeople($docId, 1) }</div>
                        </newListForAnnotation>
                        <newListForMentionedNames>
                            <div id="newListOfMentionedNames">{ teiEditor:peopleMentionsInDoc($docId) }</div>
                        </newListForMentionedNames>
                        </div>  
               case "msItem" return
                        <div>
                            <newList>{ teiEditor:msItemManager($docId) }</newList>
                    </div> 
                case "msFrag" return
                        <div>
                            <newList>{ teiEditor:msFragManager($docId) }</newList>
                            <newRepo>{ teiEditor:repositoryManager($docId) }</newRepo>
                    </div>            
                case  "xmlElement" return
                teiEditor:displayElement($list, $docId, (), ())
            (:      teiEditor:displayTeiElementWithThesauCardi($list, $topConceptId, $docId, $index):)
                default return 
                <div><newList>{teiEditor:displayElement($list, $docId, (), ())}</newList></div>
                }
                <newContent>{ $newContent }</newContent>
                </data>
};

(:~ Function to remove an item from a list. :)
declare function teiEditor:removeListItem($data as node(), $project as xs:string){
            let $now := fn:current-dateTime()
            let $currentUser := data(sm:id()//sm:username)
            (:let $data := request:get-data():)
            (: let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents") :)
            let $docId := $data//docId/text()
            (:let $docId := request:get-parameter('docid', ()):)
            let $teiDoc :=util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" || $docId ||"')" ) 
            (:$teiEditor:doc-collection/id($docId):)
            let $rootXPath := $data//rootXPath/text()
            let $xmlElement := $data//xmlElement/text()
            
            
            let $itemIndex := if($data//itemIndex/text()) then xs:int($data//itemIndex/text())
                        else "1"
            
            let $list := $data//list/text()

            let $elementNickname := $data//elementNicknameForReload/text()
            let $console := console:log($elementNickname)
            let $console := console:log($rootXPath)
            let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]))) 
                then $teiEditor:teiElements//teiElement[nm=$elementNickname] 
                        else $teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]

            let $console := console:log( "collection('" || $teiEditor:doc-collection-path || "')/id('" || $docId ||"')/" || $rootXPath || "//"
                        || $xmlElement || "[" || $itemIndex || "]" ) 

            let $nodeToRemove := if($xmlElement != "") then 
            util:eval( "collection('" || $teiEditor:doc-collection-path || "')/id('" || $docId ||"')/" || $rootXPath || "//"
                        || $xmlElement || "[" || $itemIndex || "]" )
                    else util:eval( "collection('" || $teiEditor:doc-collection-path || "')/id('" || $docId ||"')/" || $rootXPath || "[" || $itemIndex || "]" )

            let $removeItem := update delete $nodeToRemove
            (: let $newDoc :=  util:eval( "collection('" || $teiEditor:doc-collection-path || "')//id('" || $docId ||"')") :)
            let $updatedList := switch($elementNickname)
                case "msFragRepository" return teiEditor:repositoryManagerOnMsFrag($docId,  (util:eval( "collection('" || $teiEditor:doc-collection-path || "')/id('" || $docId || "')")//tei:msFrag))
                (: case "fragCustodialEvent"
                let $node := util:eval("
                    $teiDoc/" || substring-after($list, "-") || "[" || $index || "]")
                        return
                        update delete $node
                else if(contains($list, "fragCustodialEvent")) then
                    let $indexFragment := $list => substring-after("tei:msFrag[") => substring-before("]")
                    return
                    <div>
                        <newList>{teiEditor:displayElement("fragCustodialHistory", $docId, $indexFragment, "/tei:msFrag[" || $indexFragment || "]")}</newList>
                    </div> :)
                
                
                default return 
                teiEditor:displayElement($elementNickname, $docId, (), $rootXPath)
            let $console := console:log($updatedList)
            return
            <data>
                <updatedElement>{ $updatedList }</updatedElement>
            </data>

};

declare function teiEditor:getTextGeneralClass(){
    <result>
    {for $item in $teiEditor:teiTemplate
    order by $item
    return
    <item>
        <label>{$item/text()}</label>
        <id>{data($item/@xml:id)}</id>
    </item>
    }
    </result>
};

(:~ Function to generate a HTML panel to manage data related to tei:surface. :)
declare function teiEditor:surfaceManager(){
 <div class="sectionPanel">
    <h4 class="subSectionTitle">Writing Support(s)<a onclick="openDialog('dialogNewSurface')"><i class="glyphicon glyphicon-plus"/></a></h4>
    <p>This TEI document has { count($teiEditor:teiDoc//tei:sourceDoc//tei:surface) } /sourceDoc/surface element{ if (count($teiEditor:teiDoc//tei:sourceDoc//tei:surface) > 1) then "s" else ""}</p>
        <ul>
            {
              for $surface at $index in $teiEditor:teiDoc//tei:sourceDoc//tei:surface
              return
              <li class="sectionPanel">
              <h4 class="subSectionTitle">
              <span class="labelForm">{$index}</span>
              <span class="labelForm">{data($surface/@ana)}</span>
              <span class="surfaceId">{data($surface/@xml:id)}</span>
              </h4>
              <div class="surfaceDesc">
              {teiEditor:displayElement('docTextSurfaceType', (), (), ())}</div>
              <div class="sectionPanel">
              <h5>Figures <a title="Not implemented yet"><i class="glyphicon glyphicon-plus"/></a></h5>
              {for $graphic in $surface//tei:graphic
              return
              <img src="{$graphic/@url}" alt="" height="50px"/>
              }
              </div>
             </li>
             }
        </ul>

         <!--Dialog for Surface-->
    <div id="dialogNewSurface" title="Add a new surface" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Create a new surface</h4>
                </div>
                <div class="modal-body">

                <div>

                </div>



                    <div class="form-group modal-footer">


                        <button id="addPeople" class="pull-left" onclick="createAndAddPerson('{$teiEditor:docId}')">Create person</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->
                </div>
            </div>
       </div><!--End of dialog-->
  </div>


};

(:~ Function to generate a HTML panel to manage data related to tei:layoutDesc. :)
declare function teiEditor:layoutManager(){
 <div class="sectionPanel">
    <h4 class="subSectionTitle">Text Layout Metadata</h4>
    <p>This TEI document has <strong>{ count($teiEditor:teiDoc//tei:physDesc/tei:objectDesc/tei:layoutDesc//tei:layout) } layout </strong> Element{ if (count($teiEditor:teiDoc//tei:msItem) > 1) then "s" else ""}</p>
    {teiEditor:displayTeiElement('docTextLayoutSummmary', (), 'textarea', ())}
        <ul>
            {
              for $layout in $teiEditor:teiDoc//tei:physDesc/tei:objectDesc/tei:layoutDesc//tei:layout
              let $description := if ($layout//tei:desc) then <span class="surfaceDesc"><strong>Description: </strong>{data($layout//tei:desc)}</span>
                                                else <span class="surfaceDesc">No description</span>
              let $correspondingItems := tokenize(data($layout/@corresp), ' ')

              let $correspondingElement :=
                    for $item in $correspondingItems

                    let $matchingElement :=
                            if ($teiEditor:teiDoc//tei:surface[contains(./@xml:id, substring($item, 2))]) then
                            <matchingElement type="surface">{ data($teiEditor:teiDoc//tei:surface[contains(./@xml:id, substring($item, 2))]/@ana) }</matchingElement>
                            else if ($teiEditor:teiDoc//tei:decoNote[contains(./@xml:id, substring($item, 2))]) then
                            <matchingElement type="deco">{ data($teiEditor:teiDoc//tei:decoNote[contains(./@xml:id, substring($item, 2))]/tei:desc/text()) }</matchingElement>
                           else ()

                    return $matchingElement


              return
              <li>
              <span class="label label-primary">{data($layout/@xml:id)}</span>
              {for $item in $correspondingElement
                let $label := switch($item/@type)
                    case "surface" return "Surface"
                    case "deco" return "Fig."
                    default return null

                return
                    <span><span class="surfaceDesc">{ $label || ": " }</span> {$item/text()} | </span>
              }


              {$description}


             </li>
             }
        </ul>
  </div>
  };

(:~ Function to generate a HTML panel to manage data related to tei:mlsItem. :)
declare function teiEditor:msItemManager(){
    <div class="sectionPanel">
    <h4 class="subSectionTitle">Abstract Text Metadata</h4>
    <p>This TEI document has <strong>{ count($teiEditor:teiDoc//tei:msItem) } msItem </strong> Element{ if (count($teiEditor:teiDoc//tei:msItem) > 1) then "s" else ""}</p>
    <ul>
    {
      for $msItems at $index in $teiEditor:teiDoc//tei:msItem

     return
     <li class="sectionPanel">
        <span class=""><h4>msItem {$index}</h4>
        {teiEditor:displayTeiElement('msItemModernTitle', $index, 'input', ())}
        {teiEditor:displayElement('textMainLang', (), 1, ())}
        {teiEditor:displayElement('textOtherLangs', (), $index, ())}

        {teiEditor:displayTeiElementWithThesau('msItemClassification', 'c19298', $index, ())}
        {teiEditor:displayTeiElement('msItemRefToCanonical', $index, 'input', ())}
        </span>


     </li>
     }
     </ul>
  </div>
};

(:~ Function to return the tei:TEI file of a document. :)
declare function teiEditor:getXmlFile($docId){
    (:let $docId := $data//docId/text():)

(:    return:)
      util:eval( "collection('" || $teiEditor:data-repository-path || "/documents')/id('"
             || $docId ||"')")
};

(:~ Function to generate a log entry in project's log (in /[projectName]Data/logs) .:)
declare function teiEditor:logEvent($eventType as xs:string,
                                                      $docId as xs:string,
                                                      $data as node()?,
                                                      $description as xs:string?){
    let $project := request:get-parameter("project", ())
    let $logs := collection($teiEditor:data-repository-path || '/logs')
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:real/sm:username)
    let $log :=
<data>
    <log type="{$eventType}" when="{$now}" what="{$docId}" who="{$currentUser}">{$data}<description>{$description}</description></log>
 </data>
return
    update insert
         $log/node()
         into $logs/rdf:RDF/id('all-logs')
};

(:~ Function to generate the HTML page to display a login form. :)
declare function teiEditor:loginForm($project, $username){

<div xmlns="http://www.w3.org/1999/xhtml" data-template="templates:surround"
    data-template-with="./templates/page.html" data-template-at="content">

   <div class="container">
   <div class="row">
            <div class="container-fluid">
                 <h2>Welcome { data(sm:id()//sm:username) }!</h2>
                 { if ($username != 'guest') then
                        <p>Your account does not include access to the Editor. Please contact <a href="mailto:vincent.razanajao@u-bordeaux-montaigne.fr">vincent.razanajao@u-bordeaux-montaigne.fr</a>.</p>
                 else
                 <p>Access not authorized. Please log in with your account from the <a href="/" target="_self">homepage</a>. 
                 </p>}
                 <!--<form class="form form-horizontal" action="" method="POST">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label col-sm-2">
                                    Username
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="user" class="form-control" value="{$username}">{$username}</input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">
                                    Password
                                </label>
                                <div class="col-sm-10">
                                    <input type="password" name="password" class="form-control"/>

                                </div>
                            </div>

                                <div class="col-sm-12">
                                    <a class="pull-right" href="/admin/new-user/" >Create a user</a>
                                </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                        <input type="hidden" name="duration" value="P7D"/>

                    </form>
                    -->
                </div>
</div></div></div>   };

(:~ Function to generate a HTML select populated with project's places. :)
declare function teiEditor:listsPlacesAsCombo(){
    let $places := doc($teiEditor:data-repository-path || "/places/" || $teiEditor:project || "/list.xml")

    return
     <select id="placeList" name="placeList">
                    {for $items in $places//Place
                    return
                        <option value="{data($items/@rdf:about)} {data($items/skos:exactMatch/@rdf:resource)}">
                        {$items/skos:prefLabel[@xml:lang="en"]/text()} </option>}</select>
        };

(:~ Function to generate a HTML select populated with project's languages defined in the general application parameters files. :)
declare function teiEditor:projectLangDropDown($selectedLang as xs:string,
                                                                            $docid as xs:string,
                                                                            $elementNickname as xs:string,
                                                                            $indexNo as xs:string){
                <select id="lang_{$elementNickname}_{$indexNo}" name="lang_{$elementNickname}_{$indexNo}" class="langSelector">
                    {for $lang in $teiEditor:languages//lang
                            return
                            if($lang=$selectedLang) then
                                     <option value="{$lang}"
                                     selected="selected">{$lang}</option>
                               else
                                     <option value="{$lang}">{$lang}</option>

                      }
                      </select>
                };

(:~ Function to generate a HTML panel to display document provenance. :)
declare function teiEditor:listPlacesOfProvenance($docId){
let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" )

let $plural := if (count($teiDoc//tei:msFrag/tei:history/tei:provenance/tei:placeName) >1) then "s" else ()
return
<div class="panel panel-default">
    <div class="panel-body">
        <h4>Place{$plural} of provenance</h4>
        {
        if (count($teiDoc//tei:msFrag/tei:history/tei:provenance/tei:placeName) >1) then (
                       <ul>
                       {for $placeOfProvenance at $pos in $teiDoc//tei:msFrag/tei:history/tei:provenance/tei:placeName
                                order by $pos
                                return
                                
                              <li>
                              <span>Fragment {$pos}: </span><span>{$placeOfProvenance}</span>
                              
                              </li>
                              }
                              </ul>
                              )
        else ( 
                let $placeName := $teiDoc//tei:msFrag/tei:history/tei:provenance/tei:placeName/text()
                let $placeURI := if (data($teiDoc//tei:msFrag/tei:history/tei:provenance/tei:placeName/@ref) != "") then
                        data($teiDoc//tei:msFrag/tei:history/tei:provenance/tei:placeName/@ref)
                        else ("no URI")
                 return
        
           <div>
           <p>{ $placeName }</p>
           <em>{ $placeURI }</em>
           <a class="nav-link" id="pills-textbearer-tab" data-toggle="pill" href="#nav-textbearer" role="tab" aria-controls="pills-textbearer" aria-selected="false">
<i class="glyphicon glyphicon-edit"/>Edit place{$plural} in 'Support' tab</a>
           </div>                   
               )
                              }
           
           </div>  
           </div>
};


(:~ Function to convert ancient text edition written in Leiden or similar, into EpiDoc.
 : Not fully tested :)
declare function teiEditor:convertIntoEpiDoc($textInput as xs:string){
         let $textBackup :=$textInput
         let $quote := "&#34;"
         
         
(: Clean tabs:)
(:    text = text.toString().replace(/\t+/g, '')        :)
    let $textInput:= replace($textInput, "\t+", "") 
      
(:Clean Former line numbers:)
(:   let $regex := "(n=)(&quot;)([0-9]*)&quot;(/>\s?)([0-9]*)(\s?)"
    let $replacement := "$1&quot;$3&quot;$4"
:)
let $regex := "([0-9]*)"
    let $replacement := ""
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")      
      
(:New lines:)
let $textInput:= if (contains($textInput, "|")) then replace($textInput, "\|", "/") 
                                  else $textInput

let $lineSeparator := if (contains($textInput, "/")) then "/"
                                   
                                  else "\n"
let $lineNumber := count(tokenize($textInput,  $lineSeparator))




let $textInput := 
"<lb n='1'/>" || string-join(
                            (for $line at $count in tokenize($textInput, $lineSeparator)
                                
                                let $newLine :=
                                    if (contains($line, "-")) then
                                    
                                     replace($line, '-', '') || codepoints-to-string(10) || "<lb n='"||
                                        $count +1 || "' break='no'/>"
                                else 
                                    $line || codepoints-to-string(10) || 
                                    (if ($count < $lineNumber)
                                        then "<lb n='"||$count +1 || "'/>"
                                         else ()
                                    )
                            return 
                            $newLine),
                    "")
   



   

(::)
(:let $regex := "\\n|\\r":)
(:let $replacement := '\\n' || "ff":)
(:let $textInput := replace($textInput,:)
(:                            $regex,:)
(:                            $replacement, 'm':)
(:                       ):)

(::)
(:Corrections:)
(::)
(:<t>]:)
let $regex := "<(\w*)>"
let $replacement := "<supplied reason='omitted'>$1</supplied>"
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")

let $regex := "&lt;(\w*)&gt;>"
let $replacement := "<supplied reason='omitted'>$1</supplied>"
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")



(::)
(:Superfluous:)
(::)
let $regex := "\{(\w*)\}"
let $replacement := "<surplus>$1</surplus>"
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")

(::)
(:Gaps and Lacunaes:)
(::)

(:Line in Lacuna [— — — —]:)
let $regex := "\[(— — — —)\]"
let $replacement := "<gap unit='line'/>"
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")

(:let $regex := "\-"   
let $replacement := "<gap unit='line'/>"
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")
:)

(:Basic Abbreviations:)
let $regex := '(\w*)\((\w*)\)'
let $replacement := '<expan><abbr>$1</abbr><ex>$2</ex></expan>'
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")

(:
(\:Abbreviations in lacunae:\)

let $regex := "\[(\w*)\((\w*)\)(\])"
let $replacement := "<supplied reason='lost'><expan><abbr>$1</abbr><ex>$2</ex></expan></supplied>"
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")

:)
(:Abbreviation with uncertain resolution:)
let $regex := "(\w*)\((\w*\?)\)"
let $replacement := "<expan><abbr>$1</abbr><ex cert='low'>$2</ex></expan>"
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")

(:Lacunae not ended in original text:)
let $regex := "\[(\w*)\s*(— — —)+"
let $replacement := "<supplied reason='lost'>$1</supplied>ss<gap reason='lost' extent='unknown'/>"
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")

(:supplied:)
(:let $textInput := replace($textInput, '\[', "<supplied reason='lost'>"):)
(:let $textInput := replace($textInput, '\]', '</supplied>'):)
let $regex := "(\[)(\w*([^\[])*)(\])"
let $replacement := "<supplied reason='lost'>$2</supplied>"
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")
(:
(\:Correction to supplied:\)
(\:let $textInput := replace($textInput, '\[', "<supplied reason='lost'>"):\)
(\:let $textInput := replace($textInput, '\]', '</supplied>'):\)
let $regex := "(\])(.*([^\[])*)(\[)"
let $replacement := "</supplied>$2<supplied reason='lost'>"
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")

:)

(:Dotted letters:)
let $regex := '(\w)̣'
let $replacement := '<unclear>$1</unclear>'
let $textInput := replace($textInput,
                            $regex,
                            $replacement, "")
 
 
 
 
 
 
    let $logs := collection("xmldb:exist:///db/apps/patrimoniumData" || '/logs')
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:real/sm:username)
    let $log :=
<data>
    <log type="regex-test" when="{$now}" what="test-regex" who="{$currentUser}"><description>{$textInput}</description></log>
 </data>
 let $logTrigger :=
    update insert
         $log/node()
         into $logs/rdf:RDF/id('all-logs')
         

return 
        try { 
            parse-xml('<ab>' || replace($textInput, "'", '"') || "</ab>")
            }
          catch err:FODC0006 { <ab>{
          "XML Error during conversion: text could not be processed:" || $teiEditor:nl || 
          $textBackup } </ab>}


};

(:~ Function to return tei:div[@type="edition"] of a document. :)
declare function teiEditor:getTextDivFromXml($externalDocUri as xs:string){
    let $url4httpRequest := 
                    if($externalDocUri !='' ) then 
                               (if(contains($externalDocUri, 'edh-www.adw.uni-heidelberg.de/edh/inschrift/')) then
                                            $externalDocUri || ".xml"
                               else if(contains($externalDocUri, 'http://papyri.info/ddbdp')) then
                                            $externalDocUri || "/source"
                               else if(contains($externalDocUri, 'http://mama.csad.ox.ac.uk/monuments/MAMA-XI')) then
                                            (
                                            let $externalDocId := substring-before(substring-after($externalDocUri, "http://mama.csad.ox.ac.uk/monuments/"), ".html")
                                            return 
                                            "http://mama.csad.ox.ac.uk/xml/" || $externalDocId || ".xml"
                                            )
                                            else())
                            else ()

let $logEvent := teiEditor:logEvent("test-import" , "vdrazanajao", <description>{ "$url4httpRequest: " || $url4httpRequest }</description>,
                        "$url4httpRequest: " || $url4httpRequest)


    let $http-request-data := <request xmlns="http://expath.org/ns/http-client"
        method="GET" href="{$url4httpRequest}"/>
    let $responses :=
        http:send-request($http-request-data)
    let $response :=
        <results>
          {if ($responses[1]/@status ne '200')
             then
                 <failure>{$responses[1]}</failure>
             else
               <success>
                 {$responses[2]}
                 {'' (: todo - use string to JSON serializer lib here :) }
               </success>
          }
        </results>
    let $textDiv := $response//tei:div[@type='edition']
    
    return $textDiv
};

(:~ Function using TM API to retrieve metadata from EDCS nID. :)
declare function teiEditor:edcsMatcher($edcsID as xs:string){
           let $cleanEdcsID := if(contains($edcsID, "EDCS-")) then substring-after($edcsID, "EDCS-") else $edcsID
           let $url4httpRequest := "https://www.trismegistos.org/dataservices/texrelations/xml/" || $cleanEdcsID || "?source=edcs"
           
           let $http-request-data := <request xmlns="http://expath.org/ns/http-client"
          method="GET" href="{$url4httpRequest}">
              
          </request>
          
          let $responses := 
          http:send-request($http-request-data)
          let $response :=
          <results>
            {if ($responses[1]/@status ne '200')
               then
                   <failure>{$responses[1]}</failure>
               else
                 <success>
                   {$responses[2]}
                   {'' (: todo - use string to JSON serializer lib here :) }
                 </success>
            }
          </results>
          
          let $xmlDocUri :=
              if(contains($response//link/@cp, "EDH")) then
                  let $docId := $response//link[@cp="EDH"]/text() 
                  let $uri := "https://edh-www.adw.uni-heidelberg.de/edh/inschrift/" || $docId || ".xml"
                  return $uri
              else if(contains($response//link/@cp, "ddpdb")) then
                  let $docId := $response//link[@cp="EDH"]/text() 
                  let $uri := "http://papyri.info/ddbdp/" || $docId || "/source"
                  return $uri    
              else("Cannot get any uri")
              
          return 
              $xmlDocUri
};

(:~ Function to build a document URI, based on the URI base defined in the project general parameters and the doc ID. :)
declare function teiEditor:buildDocumentUri($docId as xs:string){
    $teiEditor:documentBaseUri ||  $docId  
};

(:~ Function to generate the HTML widget to search in project's people repository. :)
declare function teiEditor:searchProjectPeopleModal(){

    <div id="dialogAddPeopleToDoc" title="Add a Person to the document" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Search in people's project</h4>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                                  <label for="projectPeopleLookup">Search in project's people</label>
                                   <input type="text" class="form-control projectPeopleLookup"
                                   id="projectPeopleLookup"
                                   name="projectPeopleLookup"
                                   autocomplete="on"
                                   />
                        </div>
                        <div id="projectPeopleDetailsPreview" class=""/>
                        <input id="currentDocumentUri" type="text" class="hidden"/>
                        <input id="selectedPeopleUri" type="text" class="hidden"/>
                  </div>
                  <div class="modal-footer">
                      <button  id="addPersonToDocButton" class="btn btn-primary pull-left" type="submit" onclick="addProjectPersonToDoc()">Validate</button>
                        <button type="button" class="btn btn-default" onclick="closeAddPersonToDocModal()">Cancel</button>
                    </div>
                  
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>
</div>

};

(:~ Function using the TM API to retieve metadata from TM number. :)
declare function teiEditor:TMTextRelations($TMNo as xs:string, $projectCode){
           
           let $url4httpRequest := "https://www.trismegistos.org/dataservices/texrelations/uri/" || $TMNo
           
           let $http-request-data := <request xmlns="http://expath.org/ns/http-client"
          method="GET" href="{$url4httpRequest}">
              
          </request>
          
          let $responses := http:send-request($http-request-data)
          let $response :=
          <results>
            {if ($responses[1]/@status ne '200')
               then
                   <failure>{$responses[1]}</failure>
               else
                 <success>
                   {$responses[2]}
                   {'' (: todo - use string to JSON serializer lib here :) }
                 </success>
            }
          </results>
          
          let $result :=
          if($projectCode = "all")
           then $responses
           else $responses[1]
          
              
          return
              $result
(:          <data>{ $TMNo}:)
(:              {$result}</data>:)
};


(:~ Function to generate the HTML widget for converting an edition into epidoc. :)
declare function teiEditor:epiConverter(){

<div xmlns="http://www.w3.org/1999/xhtml" data-template="templates:surround"
    data-template-with="./templates/page.html" data-template-at="content">

   <div class="container">
            <div class="row">
                     <div class="container-fluid">
                            <div class="form-group row">
                                       <label for="langSource">Select a script</label>
                                       <select id="langSource" name="langSource">
                                            <option value="grc">Ancient Greek</option>
                                            <option value="la">Latin</option>
                                            <option value="egy-Egyd">Ancient Egyptian in demotic script (latin transliteration)</option>
                                            <option value="egy-Egyh">Ancient Egyptian in hieratic script (latin transliteration)</option>
                                            <option value="egy-Egyp">Ancient Egyptian in hieroglyphic script (latin transliteration)</option>
                                       </select>
       
                           </div>
                            <div class="form-group row">
                                        <label for="importSource">Select source format</label>
                                        <select id="importSource" name="importSource">
                                             <option value="petrae">PETRAE</option>
                                             <option value="edr">EDR</option>
                                             <option value="edcs">EDCS / EDH</option>
                                             <option value="phi">PHI</option>
                                        </select>
                                
                                <input id="textImportMode" name="textImportMode" type="text" class="hidden" value="newText"/>
                            </div>
                            <!--
                           <label for="text2import">Paste text to import below</label>

                            <textarea class="form-control" name="text2import" id="text2import" row="10" ></textarea>
                            -->
                     
                     <div class="col-xs-5 col-sm-5 col-md-5">
                            <label for="text2beConvertedEditor">Epigraphic Text input</label>
                            <div class="text2importInput">
                                <div id="text2beConvertedEditor"/>
                            </div>
                    </div>
                    <div class="col-xs-7 col-sm-7 col-md-7">           
                     <label for="text2importXMLPreview">HTML preview</label>
                               <div id="textPreviewHTML-9999" class="textPreviewHTML"/>
                          
                     </div>
 
                    </div>
                     <div class="row">
                     
                                <label for="text2importXMLPreview">XML Preview</label>
                                { teiEditor:annotationMenuEpigraphy("1")}
                                <div id="xml-editor-1"/>
                    </div>
                    </div>
</div>
 <link rel="stylesheet" href="$ausohnum-lib/resources/css/teiEditor.css"/>
        <link href="$ausohnum-lib/resources/css/skosThesau.css" rel="stylesheet" type="text/css"/>
        <link href="/resources/css/editor.css" rel="stylesheet" type="text/css"/>
        
        
        
     

  

        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/teiEditorEvents.js"/>
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/ancientTextImportRules.js"/>
         <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/tei2Html4Preview.js"/>
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/tei2leiden.js"/>
<script type="text/javascript">
editor_options ={{
    enableBasicAutocompletion: true,
                    enableSnippets: true,
                    enableLiveAutocompletion: true,
    
        //mergeUndoDeltas: "always",
                    //maxLines: Infinity,
                    behavioursEnabled: true, // autopairing of brackets and tags
                    wrapBehavioursEnabled: false,
                    showLineNumbers: true, 
                    wrap: 'free',
                    showPrintMargin: false,
                    printMarginColumn: false,
                    printMargin: false,
                    //fadeFoldWidgets: true,
                    //showFoldWidgets: true,
                    showInvisibles: true,
                    showGutter: true, // hide or show the gutter 
                    displayIndentGuides: false,
                    cursorStyle: "wide",
                    //navigateWithinSoftTabs: false,
                    highlightGutterLine: true,
                    
                    //printMarginColumn: 20,
                    //printMargin: 70,
                    fontSize: 14,
                    fixedWidthGutter: false,
                    //showInvisibles: false,
                    newLineMode: 'auto',
                    maxLines: 30,
                    minLines: 10,
                    //enableBlockSelect: true
                    //printMarginColumn: true,
//                    printMarginColumn: false,
                    //readOnly: true,
                    
                    highlightSelectedWord: true,
                    wrapBehavioursEnabled: true,
                    highlightGutterLine: true
                    
                    //tabSize: 15
                    
}};

    var importEditor = ace.edit("text2beConvertedEditor");
    importEditor.setOptions(editor_options);
    var text2importXMLPreview = ace.edit("xml-editor-1");
    text2importXMLPreview.setOptions(editor_options);
       
       text2importXMLPreview.session.setMode("ace/mode/xml");
      
      text2importXMLPreview.setOptions({{
            maxLines: Infinity
            }});

$("#text2beConvertedEditor").keydown(function(){{
   
   
        var importEditor = ace.edit("text2beConvertedEditor");
       var importEditorPreview = ace.edit("xml-editor-1");
       
    importEditor.getSession().on('change', function(){{

         //var text2convert = $(this).val();

                var editorPreview = ace.edit("xml-editor-1");
                var importSource = $( "#importSource option:selected" ).val();
                
                editorPreview.getSession().setValue(
                    convertAncientText(importEditor.getValue(), importSource));
                $("#textPreviewHTML-9999").html(tei2Html4Preview(
                    convertAncientText(importEditor.getValue(), importSource)));    
            /*                editorPreview.getSession().setValue(convertEDR2TEI(importEditor.getValue()));*/
}});
       
        }});
 
 $("#xml-editor-1").keydown(function(){{
    var importEditorPreview = ace.edit("xml-editor-1");
    
    importEditorPreview .getSession().on('change', function(){{
                var importSource = $( "#importSource option:selected" ).val();
                
                $("#textPreviewHTML-9999").html(tei2Html4Preview(
                    convertAncientText(text2importXMLPreview.getValue(), importSource)));    
            /*                editorPreview.getSession().setValue(convertEDR2TEI(importEditor.getValue()));*/
}});
       
}});

</script>

</div>
};

(:~ Function to return tei:div[@type="edition"] of a document. :)
 declare function teiEditor:getEditionDivFromDoc($docId as xs:string, $project as xs:string){
let $doc-collection := collection('/db/apps/' || $project || 'Data/documents')
let $teiDoc := $doc-collection/id($docId)
return
    <data>
        {
        if (exists($teiDoc//*[local-name() = 'div'][@type="edition"]/*[local-name() = 'div'][@type="textpart"])) then
            (
            for $text in $teiDoc/*[local-name() = 'text']/*[local-name()='body']/*[local-name() = 'div'][@type='edition']//*[local-name()='div'][@type='textpart']
            return
            $text
    (:        $text/*[local-name()='ab']):)
            )
         else (
               <ab>{$teiDoc/*[local-name() = 'text']/*[local-name()='body']/*[local-name() = 'div'][@type='edition']//*[local-name()='ab']/node()}</ab>
               )
        }     
    </data>
};

(:~ Function to get and return data from api.archives-ouvertes.fr. :)
 declare function teiEditor:displayOpenArchivesFlux($node as node(), $model as map(*), $europeanProject_id as xs:string)  as element(div) {
   
   let $url4httpRequest := encode-for-uri('https://api.archives-ouvertes.fr/search/?wt=xml-tei&amp;q=europeanProjectReference_s:" ' || $europeanProject_id || '"')
   let $http-request-data := <request xmlns="http://expath.org/ns/http-client"
    method="GET" href="{$url4httpRequest}"/>

    let $responses := try {
            http:send-request($http-request-data)
            }
            catch * { "error"}
    let $response :=
    <results>
      {if ($responses[1]/@status ne '200')
         then
             <failure>{$responses[1]}</failure>
         else
           <success>
             {$responses[2]}
             {'' (: todo - use string to JSON serializer lib here :) }
           </success>
      }
    </results>      
    return 
    <div>{ $response }</div>
    
};



(:~ Function to generate the HTML widget related to a repository (museum) linked to a document.
 : Developped for the Karnak Cachette database. :)
 declare function teiEditor:repositoryManager($docId as xs:string?){
   

  let $objectRepositoriesUri := $teiEditor:appVariables//objectRepositoriesUri/text()
  let $msFragList := if($docId = "") then $teiEditor:teiDoc//tei:msFrag
        else util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')")//tei:msFrag
  

  return
<div id="repositoryManager" class="panel panel-default">
   <div class="TeiElementGroupHeaderBlock">
   <span class="labelForm">{ teiEditor:label("repoManager-repository", "Repository")}</span>
    <div id="repositoryDetailsPanel panel panel-body">
        {for $frag at $index in $msFragList

            let $repositoryUri := data($frag//tei:msIdentifier/tei:repository/@ref)
            let $townName := if(normalize-space($frag//tei:msIdentifier/tei:settlement/@ref) != "") then skosThesau:getLabel($frag//tei:msIdentifier/tei:settlement/@ref, $teiEditor:lang) else()
            let $repositoryName := if(normalize-space($frag//tei:msIdentifier/tei:repository/@ref) != "") then skosThesau:getLabel($frag//tei:msIdentifier/tei:repository/@ref, $teiEditor:lang) else()
  
            
        return
        <div id="repositoryDetails{$index}" class="panel panel-default" style="border-width: thick!important;">


                { teiEditor:repositorySummary($docId, $frag//tei:msIdentifier[@type="former-repository"], $index)}



            <div id="repository_display_{$index}_1" class="teiElementDisplay">
                <h3 style="margin-top: 3px!important;">{if(count($msFragList) > 1) then (
                    <span>{ teiEditor:label("fragment", "Fragment ") || $index || "" }<br/></span>) else ()}
                { teiEditor:label("repoManager-presentAndLastKnownRepository", "Current or last known repository")}</h3>
                   
                   <div class="teiElementGroup">
                   <button id="editRepository{ $index }Button" class="btn btn-xs btn-primary pull-right"
                          onclick="openDialog('dialogEditRepository{ $index }')" appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                              editConceptIcon"></i></button>
                    <div class="TeiElementGroupHeaderInline" style="margin: 0 5px 0 2px;">
                        <span class="labelForm">{ teiEditor:label("repoManager-town", "Town") }
                            <span class="teiInfo">
                                <a title="TEI element: "><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                            </span>
                        </span>
                         { $townName }
                    </div>
                    <div class="TeiElementGroupHeaderInline" style="margin: 0 5px 0 2px;">
                        <span class="labelForm">{ teiEditor:label("repoManager-museum", "Museum") }
                            <span class="teiInfo">
                                <a title="TEI element: "><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                            </span>
                        </span>
                        { $repositoryName }
                    </div>
                </div>
                { teiEditor:displayElement("msFragmentMainIdno", $docId, $index, ())}
                { teiEditor:displayElement("msFragmentAltIdentifier", $docId, $index, ())}
                { teiEditor:displayElement("msFragmentAltIdentifierIdnoIIIF", $docId, $index, ())}
            <div id="altRepositoryDetails{$index}" class="panel panel-default">

                <h3>{ teiEditor:label("repoManager-custodialHistory", "Custodial history") }</h3>
                 
                {teiEditor:displayElement("fragCustodialEvent", $docId, $index, "//tei:msFrag[" || $index || "]")}
                
            </div>


            </div>
    
    
    <div id="dialogEditRepository{ $index }" title="Edit Repository" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">{ teiEditor:label("repoManager-editRepoTitle", "Edit repository") }</h4>
                </div>
                <div class="modal-body">
                    <div>{
                    if(( $townName = "") and ($repositoryName = ""))
                            then teiEditor:label("repoManager-editNoRepo", "There is currently no Reposotiry")
                            else if ( $townName  = "") then (teiEditor:label("repoManager-editNoTownMuseum", "No town - museum: ") || $repositoryName) 
                            else if ( $repositoryName = "") then ( teiEditor:label("repoManager-town", "Town") || ": " || $townName ||  teiEditor:label("repoManager-editNoMuseum", " - no museum") )
                            else (<span><strong>{ teiEditor:label("repoManager-editCurrentRepo", "Current repository") }</strong>: { $repositoryName} ({ $townName }) [{ $repositoryUri }]</span>)
                    }</div>
                    <br/>
                        <div class="form-group">
                                  <label for="repositoryLookup{ $index }">{ teiEditor:label("repoManager-editSearchAnother", "Search another Repository") }</label>
                                   <input type="text" class="form-control repositoryLookup"
                                   id="repositoryLookup{ $index }"
                                   name="repositoryLookup{ $index }"
                                   />
                         <div class="loaderImage hidden" >
                            <img src="$ausohnum-library/resources/images/ajax-loader.gif"/>
                        </div>
                        </div>
                        <div id="repositoryDetailsPreview{ $index }" class="repositoryDetailsPreview"/>
                        <br/>
                        <input id="currentDocumentUri{ $index }" type="text" class="currentDocumentUri hidden" value="{  teiEditor:buildDocumentUri($teiEditor:docId)  }"/>
                        <input id="repositoryUri{ $index }" type="text" class="repositoryUri hidden"/>
                       <input id="repositoryLabel{ $index }" type="text" class="repositoryLabel hidden"/>
                       <input id="townrepositoryUri{ $index }" type="text" class="townrepositoryUri hidden"/>
                       <input id="townrepositoryLabel{ $index }" type="text" class="townrepositoryLabel hidden"/>
                        <input id="countryrepositoryUri{ $index }" type="text" class="countryrepositoryUri hidden"/>
                        
                  </div>
                  <div class="modal-footer">
                      <button  id="updateRepositoryButton" class="btn btn-primary pull-left" type="submit" onclick="updateRepository('{ $index }')">OK</button>
                        <button type="button" class="btn btn-default" onclick="closeDialog('dialogEditRepository{ $index }')">Cancel</button>
                    </div>
                  
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>
</div><!-- End of dialog -->
    
    <div id="dialogAddCustodialHist" title="Add a custodial history to the fragment" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Add a Repository</h4>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                                  <label for="addRepositoryLookup{ $index }">Search a Repository</label>
                                   <input type="text" class="form-control repositoryLookup"
                                   id="addRepositoryLookup{ $index }"
                                   name="addRepositoryLookup{ $index }"
                                   />
                        </div>
                        <div id="addRepositoryDetailsPreview{ $index }" class=""/>
                        <input id="addRepositoryUri{ $index }" type="text" class=""/>
                        <input id="addRepositoryLabel{ $index }" type="text" class=""/>

                        <input id="townaddRepositoryUri{ $index }" type="text" class=""/>
                        <input id="townaddRepositoryLabel{ $index }" type="text" class=""/>
                        
                                                    
                        
                  </div>
                  <div class="modal-footer">
                      <button  id="addRepositoryToDocButton" class="btn btn-primary pull-left" type="submit" onclick="addRepositoryToDoc({ $index })">Validate</button>
                        <button type="button" class="btn btn-default" onclick="closeDialog('dialogAddRepository')">Cancel</button>
                    </div>
                  
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>
</div>
    </div>
    
        
 (:    END of loop on ListFrag        :)       
        }

            
    </div>

   </div>
     
    
    
    
    
    
    
    
    
</div>
};

declare function teiEditor:repositorySummary($identifierNode as node()){
    let $town := $identifierNode/tei:settlement/tei:placeName/text()
    let $museum := $identifierNode/tei:repository/text()
    let $mainInvNo := $identifierNode/tei:idno[@type="invNo"]
    let $altInvNo:= string-join($identifierNode//tei:altIdentifier[not(tei:settlement)]/tei:idno[@type="invNo" and (./text() != "")], "|")
    return
      $town || ", " || $museum ||  ", " ||  $mainInvNo || ( if($altInvNo != "") then " (" || $altInvNo || ")" else () )                    
};

declare function teiEditor:repositoryManagerOnMsFrag($docId as xs:string, $fragments as node()*){
<div id="repositoryManagerOnMsFrag" class="TeiElementGroupHeaderBlock">
   <span class="labelForm">{ teiEditor:label("repoManager-repository", "Repository")}</span>
   {
    for $fragmentNode at $indexFragment in $fragments
        let $currentOrLastRepoSummary := teiEditor:repositorySummary($fragmentNode//tei:msIdentifier)
        return
    <div>
        <div id="repository_main_display_1_1" class="panel panel-default">
            <h4 style="margin-top: 3px!important; font-variant: small-caps!important; letter-spacing: .2rem; color: #452604!important;">{if(count($fragments) >1 ) then <span>Fragment { $indexFragment }. </span> else () }</h4>
            
            <div class="teiElement">
                <span class="labelForm">
                    { teiEditor:label("repoManager-presentAndLastKnownRepository", "Current or last known repository")}
                </span>
                
                <div class="TeiElementGroupHeaderInline panel panel-default" style="margin: 0 5px 0 2px;">
                    { $currentOrLastRepoSummary }
                </div>
                { teiEditor:editRepositoryModal($docId, $fragmentNode//tei:msIdentifier, $currentOrLastRepoSummary, "Main", $indexFragment, 1) }
            </div>
            <div class="teiElementGroup">
                 <span class="labelForm">
                    { teiEditor:label("repository-former-repositories", "Former repositories")}
                </span>
                <button id="addAltRepositoryButton" class="btn btn-xs btn-primary pull-right"
                          onclick="openDialog('dialogAddAltRepositoryTomsFrag{ $indexFragment }')" 
                          appearance="minimal" type="button"><i class="glyphicon glyphicon-plus editConceptIcon"></i>
                </button> 
                { if(count($fragmentNode//tei:altIdentifier[@type="former-repository"]) = 0 )
                    then teiEditor:label("repoManager-noFormerRepositories", "No former repository")
                    else(
                        for $repo at $repoPos in $fragmentNode//tei:altIdentifier[@type="former-repository"]
                        let $repoSummary := teiEditor:repositorySummary($repo)
                        let $xmlElement := 'tei:altIdentifier[@type="former-repository"]'
                        let $confirmationMsg := teiEditor:label("repoManager-confirmationMsg", "Are you sure you want to delete this item?")
                            return 
                                <div class="teiElementGroup">
                                    <div class="TeiElementGroupHeaderInline" style="margin: 0 5px 0 2px;">
                                        { $repoSummary }
                                        <span class="pull-right">
                                            { teiEditor:editRepositoryModal($docId, $repo, $repoSummary, "Alt", $indexFragment, $repoPos) }
                                            <button class="removeItem btn btn-xs btn-warning pull-right" style="margin: 0 1em 0 1em"
                                            onclick="removeListItem(this, '{$docId}', '/tei:msFrag[{ $indexFragment }]', '{ $xmlElement }', '{ $repoPos }', 'msFragRepository',
                                             'repositoryManagerOnMsFrag', '{ $confirmationMsg }')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button>
                                        </span>
                                    </div>
                                </div>
                        ) }
            </div>
                
                 <div class="teiElementGroup">
                <!-- <span class="labelForm">{ teiEditor:label("repoManager-custodialHistory", "Custodial history") }</span>-->
                 {teiEditor:displayElement("fragCustodialHistory", $docId, $indexFragment, "/tei:teiHeader//tei:msFrag[" || $indexFragment || "]")}
            
            </div>
        </div>

<div id="dialogAddAltRepositoryTomsFrag{ $indexFragment }" title="Add a Repository to the fragment" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Add a Repository</h4>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                                 <label for="altRepositoryLookup{ $indexFragment }">{ teiEditor:label("repoManager-editSearchAnother", "Search another Repository") }</label>
                                   <input type="text" class="form-control repositoryLookup"
                                   id="altRepositoryLookup{ $indexFragment }"
                                   name="altRepositoryLookup{ $indexFragment }"
                                   />
                        </div>
                        <div id="repositoryDetailsPreviewalt{ $indexFragment }" class="repositoryDetailsPreview"/>
                        <br/>
                        <input id="currentDocumentUrialt{ $indexFragment }" type="text" class="currentDocumentUri hidden" value="{  teiEditor:buildDocumentUri($teiEditor:docId)  }"/>
                        <input id="altRepositoryUri{ $indexFragment }" type="text" class="repositoryUri hidden"/>
                       <input id="altRepositoryLabel{ $indexFragment }" type="text" class="repositoryLabel hidden"/>
                       <input id="townaltRepositoryUri{ $indexFragment }" type="text" class="townrepositoryUri hidden"/>
                       <input id="townaltRepositoryLabel{ $indexFragment }" type="text" class="townrepositoryLabel hidden"/>
                        <input id="countryaltRepositoryUri{ $indexFragment }" type="text" class="countryrepositoryUri hidden"/>
                        
                  </div>
                  <div class="modal-footer">
                      <button  id="addAltRepositoryTomsFragButton" class="btn btn-primary pull-left" type="submit" onclick="addAltRepositoryTomsFrag('{ $docId }', '{ $indexFragment }')">OK</button>
                        <button type="button" class="btn btn-default" onclick="closeDialog('dialogAddAltRepositoryTomsFrag')">Cancel</button>
                    </div>
                  
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>
            </div>



    </div>
   }

 

   </div>
    

};

declare function teiEditor:editRepositoryModal($docId, $repo, $repoSummary, $type, $indexFragment as xs:int, $indexRepo as xs:int?){
    let $xpathRoot :=
    if(lower-case($type) = "main") then '/tei:teiHeader//tei:msFrag[' || $indexFragment || ']//tei:msIdentifier'
    else '/tei:teiHeader//tei:msFrag[' || $indexFragment || ']//tei:altIdentifier[@type="former-repository"][' || $indexRepo || ']'
    return
    <span id="dialogEditRepository{ $type }{ $indexFragment }-{ $indexRepo }Container">
    <button id="editRepositoryMainButton" class="btn btn-xs btn-primary pull-right"
                          onclick="openDialog('dialogEditRepository{ $type }{ $indexFragment }-{ $indexRepo }')" appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                              editConceptIcon"></i>
                </button>
    <div id="dialogEditRepository{ $type }{ $indexFragment }-{ $indexRepo }" title="Edit Repository" class="modal fade" tabindex="-1" style="display: none; ">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h3 class="modal-title">{ teiEditor:label("repoManager-editRepoTitle", "Edit repository information") }</h3>
                </div>
                <div class="modal-body" style="overflow-y: auto!important;">
                    <div>{
                    if(( $repoSummary = ""))
                            then teiEditor:label("repoManager-editNoRepo", "There is currently no Reposotiry")
                            else (<span>
                            <strong>{ teiEditor:label("repoManager-editCurrentRepo", "Current repository") }</strong>:
                            { $repoSummary}
                            </span>)
                    }</div>
                    <br/>
                        <div class="form-group" style="margin-left: 10px;">
                                  <label for="{ $type }RepositoryLookup{ $indexFragment }-{ $indexRepo }">{ teiEditor:label("repoManager-editSearchAnother", "Search another Repository") }</label>
                                   <input type="text" class="form-control repositoryLookup"
                                   id="{ $type }RepositoryLookup{ $indexFragment }-{ $indexRepo }"
                                   name="{ $type }RepositoryLookup{ $indexFragment }-{ $indexRepo }"
                                   />
                         <div class="loaderImage hidden" >
                            <img src="$ausohnum-library/resources/images/ajax-loader.gif"/>
                        </div>
                        </div>
                        <div id="repositoryDetailsPreview{ $type }{ $indexFragment }-{ $indexRepo }" class="repositoryDetailsPreview"/>
                        <br/>
                        { teiEditor:displayElement("msFragment" || $type || "RepositoryMainIdno", $docId, $indexRepo,
                         $xpathRoot || '/tei:idno[@type="invNo"]')}
                        
                        { teiEditor:displayElement("msFragment" || $type || "RepositoryAltIdentifier", $docId, $indexRepo,
                         $xpathRoot || '')
                         (: $xpathRoot || '//tei:altIdentifier[./tei:idno[@type="invNo"]]')} :)
                        }
                        { ""
                        (: teiEditor:displayElement("msFragment" || $type || "RepositoryAltIdentifier", $docId, $indexFragment, ()) :)
                        }
                        { teiEditor:displayElement("msFragment" || $type || "RepositoryAltIdentifierIdnoIIIF",
                            $docId, $indexRepo,
                            $xpathRoot || '/tei:altIdentifier/tei:idno[@type="uri"][@subtype="online-museum-collection"]')}

                        <input id="currentDocumentUri{ $type }{ $indexFragment }-{ $indexRepo }" type="text" class="currentDocumentUri hidden" value="{  teiEditor:buildDocumentUri($teiEditor:docId)  }"/>
                        <input id="{ $type }RepositoryUri{ $indexFragment }-{ $indexRepo }" type="text" class="repositoryUri hidden"/>
                       <input id="{ $type }RepositoryLabel{ $indexFragment }-{ $indexRepo }" type="text" class="repositoryLabel hidden"/>
                       <input id="town{ $type }RepositoryUri{ $indexFragment }-{ $indexRepo }" type="text" class="townrepositoryUri hidden"/>
                       <input id="town{ $type }RepositoryLabel{ $indexFragment }-{ $indexRepo }" type="text" class="townrepositoryLabel hidden"/>
                        <input id="country{ $type }RepositoryUri{ $indexFragment }-{ $indexRepo }" type="text" class="countryrepositoryUri hidden"/>
                        
                  </div>
                  <div class="modal-footer">
                      <button  id="updateRepositoryButton" class="btn btn-success pull-left" type="submit" onclick="updateRepositoryDetails('{ $type }', { $indexFragment }, { $indexRepo })"><i class="glyphicon glyphicon-ok"/></button>
                        <button type="button" class="btn btn-primary" onclick="closeDialog('dialogEditRepository{ $type }{ $indexFragment }-{ $indexRepo }')">X</button>
                    </div>
                  
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>
    </div>
</span>
};
(:~ Function to generate the HTML widget related to a repository (museum) linked to a document.
 : Developped for the Karnak Cachette database. :)
 declare function teiEditor:repositorySummary($docId as xs:string, $repositoryNode as node()?, $index as xs:int){
    let $repositoryUri := data($repositoryNode//tei:repository/@ref)
    let $townName := if(normalize-space($repositoryNode//tei:settlement/@ref) != "") then skosThesau:getLabel($repositoryNode//tei:settlement/@ref, $teiEditor:lang) else()
    let $repositoryName := if(normalize-space($repositoryNode//tei:repository/@ref) != "") then skosThesau:getLabel($repositoryNode//tei:repository/@ref, $teiEditor:lang) else()
  return
 <div id="repositorySummary{$index}" class="teiElementGroup">
            <div id="repository_display_{$index}_1" class="teiElementDisplay">
                   <div class="TeiElementGroupHeaderInline">
                       <span class="labelForm">Town<span class="teiInfo">
                     <a title="TEI element: "><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                     </span></span>
                   { $townName }
                   </div>
                   <div class="TeiElementGroupHeaderInline">
                       <span class="labelForm">Museum<span class="teiInfo">
                     <a title="TEI element: "><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                     </span></span>
                   { $repositoryName }
                   </div>
                   <button id="editRepository{ $index }Button" class="btn btn-xs btn-primary pull-right"
             onclick="openDialog('dialogEditRepository{ $index }')"
                    appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                              editConceptIcon"></i></button>

                { teiEditor:displayElement("msFragmentAltRepoMainIdno", $docId, $index, ())}
                { teiEditor:displayElement("msFragmentAltRepoAltIdentifier", $docId, $index, ())}

            </div>
</div>
};


(:~ Function to a reference to a repository (museum) to a document.
 : Developped for the Karnak Cachette database. :)
 declare function teiEditor:addRepositoryToDocument($data, $project){
    let $docId := $data//docId/text()
    let $repositoryUri := $data//repositoryUri/text()
    let $repositoryLabel := $data//repositoryLabel/text()
    let $townUri := $data//townUri/text()
    let $townLabel := $data//townLabel/text()
    let $countryUri := $data//countryUri/text()
    let $countryLabel := $data//countryLabel/text()
    let $msFragIndex := xs:int($data//index/text())
    let $msFrag:= util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" || "//tei:msFrag[" || $msFragIndex || "]")
    let $settlementNode := <node><settlement>
                            <placeName ref="{ $townUri }">{ $townLabel }</placeName>
                        </settlement></node>
    let $repositoryNode := <node><repository ref="{ $repositoryLabel }">{ $repositoryLabel }</repository></node>
    let $altIdentifier := <node>
                        <altIdentifier>
                             { $settlementNode/node() }  
                             { $repositoryNode/node() }
                             <idno type="invNo"/>
                        </altIdentifier></node>
     let $updateMode := if(exists($msFrag//tei:msIdentifier//tei:settlement/@ref) and $msFrag//tei:msIdentifier//tei:settlement/@ref !="") 
                        then "addExtraRepo"
                        else if(exists($msFrag//tei:msIdentifier//tei:settlement/@ref) and $msFrag//tei:msIdentifier//tei:settlement/@ref ="") 
                        then "updateMainRepo"
                        else if(exists($msFrag//tei:msIdentifier) and not(exists($msFrag//tei:msIdentifier//tei:settlement)))
                        then "addSettlementAndRepoToMsIdenitifer"
                        else "Error"         
    let $updateData :=switch($updateMode)
            case "addExtraRepo"
                return update insert functx:change-element-ns-deep($altIdentifier/node(), "http://www.tei-c.org/ns/1.0", "") into $msFrag//tei:msIdentifier
            case "updateMainRepo"
                return 
                (update value $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:settlement/@ref with $townUri,
                update value $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:settlement with $townLabel,
                update value $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:repository/@ref with $repositoryUri,
                update value $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:repository with repositoryLabel
                )
            case "addSettlementAndRepoToMsIdenitifer"
                return
                (update insert functx:change-element-ns-deep($settlementNode/node(), "http://www.tei-c.org/ns/1.0", "") into $msFrag//tei:msIdentifier,
                update insert functx:change-element-ns-deep($repositoryNode/node(), "http://www.tei-c.org/ns/1.0", "") into $msFrag//tei:msIdentifier
                )
            default return ()
                                
(: 
   let $updateSettlement :=
        if(exists($msFrag//tei:msIdentifier//tei:settlement/@ref))
            then (if(exists($teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:settlement/@ref))
                      then update value $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:settlement/@ref with $townUri 
                    else update insert attribute ref { $townUri } into $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:settlement
                    ,
                    update value $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:settlement with $townLabel
                    )
           else update insert $settlementNode/node into $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier
   let $updateRepository :=
        if(exists($teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:repository))
            then (if(exists($teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:repository/@ref))
                      then update value $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:repository/@ref with $townUri 
                    else update insert attribute ref { $townUri } into $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:repository
                    ,
                    update value $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier//tei:settlement with $townLabel
                    )
           else update insert $repositoryNode/node following $teiEditor:teiDoc//tei:msFrag[$msFragIndex]/tei:msIdentifier/tei:settlement
    :)
   let $updatedElement := teiEditor:repositoryManager($teiEditor:docId)
   return
        <data>updatemode :{ $updateMode }; Index : { $msFragIndex}
        <msFrag>{ $msFrag }</msFrag>
        <newContent>{ $teiEditor:teiDoc }</newContent>
        <updatedElement>{ $updatedElement }</updatedElement>
        </data>
        
};



(:~ Function to a reference to a repository (museum) to a document.
 : Developped for the Karnak Cachette database. :)
 declare function teiEditor:addAltRepositoryTomsFrag($data, $project){
    let $docId := $data//docId/text()
    let $repositoryUri := $data//repositoryUri/text()
    let $repositoryLabel := $data//repositoryLabel/text()
    let $townUri := $data//townUri/text()
    let $townLabel := $data//townLabel/text()
    let $countryUri := $data//countryUri/text()
    let $countryLabel := $data//countryLabel/text()
    let $msFragIndex := xs:int($data//msFragIndex/text())
    let $msFrag:= util:eval( "$teiEditor:doc-collection/id('"
    
             ||$docId ||"')" || "//tei:msFrag[" || $msFragIndex || "]")
    let $settlementNode :=<node><settlement>
                                    <placeName ref="{ $townUri }">{ $townLabel }</placeName>
                                </settlement></node>
    let $repositoryNode :=<node><repository ref="{ $repositoryLabel }">{ $repositoryLabel }</repository></node>
    let $altIdentifier := <node><altIdentifier type="former-repository">
                                    { $settlementNode/node() }  
                                    { $repositoryNode/node() }
                                    <idno type="invNo"/>
                                </altIdentifier>
</node>
   
    let $updateData :=
             update insert functx:change-element-ns-deep($altIdentifier/node(), "http://www.tei-c.org/ns/1.0", "") into $msFrag
    let $newDoc:= util:eval( "$teiEditor:doc-collection/id('" ||$docId || "')")
    let $newmsFrag := $newDoc//tei:msFrag[$msFragIndex]           
   let $updatedElement := teiEditor:repositoryManagerOnMsFrag($docId, $newmsFrag)
   return
        <data>Index : { $msFragIndex}
        <updatedElement>{ $updatedElement }</updatedElement>
        <msFrag>{ $msFrag }</msFrag>
        <newContent>{ $newDoc}</newContent>
        </data>
        
};

(:~ Function to update data related to a repository (museum) linked to a document.
 : Developped for the Karnak Cachette database. :)
declare function teiEditor:updateRepository($data, $project){
    let $docId := $data//docId/text()  
  let $repositoryUri := $data//repositoryUri/text()
    let $repositoryLabel := $data//repositoryLabel/text()
    let $townUri := $data//townUri/text()
    let $townLabel := $data//townLabel/text()
    let $index := $data//index/text()
    
    let $settlementNode := <node><settlement>
                            <placeName ref="{ $townUri }">{ $townLabel }</placeName>
                        </settlement></node>
   let $repositoryNode := <repository ref="{ $repositoryLabel }">{ $repositoryLabel }</repository>
   
   let $originalNodeSettlement :=util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" || "//tei:msFrag[" || $index || "]//tei:msIdentifier//tei:settlement")

   let $updateSettlement :=
        if(exists($originalNodeSettlement))
            then ( 
                    update value $originalNodeSettlement/@ref with 
                      $townUri 
                     ,
                    update value $originalNodeSettlement/tei:placeName with  
                        $townLabel
                    )
           else ()

let $originalNodeRepository:=util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" || "//tei:msFrag[" || $index || "]//tei:msIdentifier//tei:repository")

   let $updateRepository:=
        if(exists($originalNodeRepository))
            then ( 
                    update value $originalNodeRepository/@ref with 
                      $repositoryUri 
                     ,
                    update value $originalNodeRepository with  
                        $repositoryLabel
                    )
           else ()

   (:let $updateRepository :=
        if(exists($teiEditor:teiDoc//tei:msFrag//tei:msIdentifier//tei:repository))
            then (if(exists($teiEditor:teiDoc//tei:msFrag//tei:msIdentifier//tei:repository/@ref))
                      then update value $teiEditor:teiDoc//tei:msFrag//tei:msIdentifier//tei:repository/@ref with $townUri 
                    else update insert attribute ref { $townUri } into $teiEditor:teiDoc//tei:msFrag//tei:msIdentifier//tei:repository
                    ,
                    update value $teiEditor:teiDoc//tei:msFrag//tei:msIdentifier//tei:settlement/text() with $townLabel
                    )
           else update insert $repositoryNode/node following $teiEditor:teiDoc//tei:msIdentifier/tei:settlement:)
   (: let $updatedElement := teiEditor:repositoryDetails($townLabel, $repositoryLabel, $index) :)
   let $updatedElement := teiEditor:repositoryManager($docId)
   return
        <data>
        <newContent>{ $teiEditor:doc-collection/id($docId) } </newContent>
        <updatedElement>{ $updatedElement }</updatedElement>
        </data>
        
};

(:~ Function to update data related to a repository (museum) linked to a document.
 : Developped for the Karnak Cachette database. :)
declare function teiEditor:updateRepositoryOnmsFrag($data, $project){
    let $docId := $data//docId/text()  
    let $repositoryUri := $data//repositoryUri/text()
    let $repositoryLabel := $data//repositoryLabel/text()
    let $townUri := $data//townUri/text()
    let $townLabel := $data//townLabel/text()
    let $indexFragment := $data//indexFragment/text()
    let $indexRepo := $data//indexRepo/text()
    let $repositoryType := $data//repositoryType/text()
    let $updateData :=
        (
        if($repositoryUri != "") then 
        (
        let $settlementNode := <node><settlement>
                            <placeName ref="{ $townUri }">{ $townLabel }</placeName>
                        </settlement></node>
        let $repositoryNode := <repository ref="{ $repositoryLabel }">{ $repositoryLabel }</repository>
   
        let $originalNodeSettlement :=
            if(lower-case($repositoryType) ="main")
                then util:eval( "$teiEditor:doc-collection/id('"||$docId ||"')" || "//tei:msFrag[" || $indexFragment || "]//tei:msIdentifier/tei:settlement")
                else util:eval( "$teiEditor:doc-collection/id('"||$docId ||"')" || "//tei:msFrag[" || $indexFragment || "]//tei:altIdentifier[@type='former-repository'][" || $indexRepo || "]/tei:settlement")

        let $updateSettlement :=
            if(exists($originalNodeSettlement))
                then ( 
                    update value $originalNodeSettlement/@ref with 
                      $townUri 
                     ,
                    update value $originalNodeSettlement/tei:placeName with  
                        $townLabel
                    )
                else ()

        let $originalNodeRepository:=
            if(lower-case($repositoryType) ="main") then
                util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" || "//tei:msFrag[" || $indexFragment || "]//tei:msIdentifier/tei:repository")
                else 
                util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" || "//tei:msFrag[" || $indexFragment || "]//tei:altIdentifier[@type='former-repository'][" || $indexRepo ||"]/tei:repository")
        
        let $updateRepository:=
            if(exists($originalNodeRepository))
                then ( 
                    update value $originalNodeRepository/@ref with 
                      $repositoryUri 
                     ,
                    update value $originalNodeRepository with  
                        $repositoryLabel
                    )
           else ()
        return ()
        )
        else ()
        )
    let $updatedElement := teiEditor:repositoryManagerOnMsFrag($docId, (util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')")//tei:msFrag))
   return
        <data>
        <updatedElement>{ $updatedElement }</updatedElement>
        </data>
        
};

(:~ Function to generate the HTML widget to add a person to a document.
 : Developped for the Karnak Cachette database. :)
declare function teiEditor:peopleManager($docId as xs:string){
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $people:= $teiDoc//tei:listPerson[@type="peopleInDocument"]
    let $owner := $people//tei:person[@role="owner"] 
    let $otherPeople:= $people//tei:person[@role !="owner"]
    let $temp := $teiEditor:peopleCollection//lawd:Person[@rdf:about= data($people//tei:person[@role="owner"]/@corresp)]
    let $ownerRecord := $teiEditor:peopleCollection//lawd:Person[@rdf:about= data($owner[1]/@corresp)]
    return
<div id="peoplePanel" class="teiElementGroup">
   <div class="TeiElementGroupHeaderBlock"><span class="labelForm">{ teiEditor:label("peopleManager-title", "People Manager") }</span></div>

            <div id="erPanel" class="teiElementGroup">
                    <div class="TeiElementGroupHeaderBlock"><span class="labelForm">{ teiEditor:label("peopleManager-owner", "Owner") }</span></div>
                        {if(count($owner) = 1) then $owner//tei:persName[1]/text()
                            else if(count($owner) > 1)
                                then <ul>
                        {for $owner in $owner
                            return
                            <li>{$owner//tei:persName[1]/text()}</li>}
                        </ul>
                        else 
                        <button onclick="openAddPersonToDocDialog('owner', '')">{ teiEditor:label("peopleManager-addOwner", "Add owner") }</button>
                        }
                        {if(count($owner) >0) then (<div>
                                    {if(count($otherPeople) = 0) then teiEditor:label("peopleManager-noOtherPerson", "There is not any other person linked to this document") else 
                                    <ul>{
                                    for $person in $otherPeople
                                        return
                                        <li>{ $person//tei:persName[1]/text() } [{ data($ownerRecord//snap:hasBond[@rdf:resource = $person/@corresp]/@rdf:type)}]</li>
                                    }
                                    </ul>
                                    }         
                        <h4>{ teiEditor:label("peopleManager-addPerson", "Add a person") }</h4>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'father')">{ teiEditor:label("peopleManager-father", "Father") }</button>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'mother')">{ teiEditor:label("peopleManager-mother", "Mother") }</button>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'spouse')">{ teiEditor:label("peopleManager-spouse", "Spouse") }</button>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('dedicator', '{ $owner/@corresp }', 'child')">{ teiEditor:label("peopleManager-child", "Child") }</button>
                </div>) 
                else ()
                }
            </div>
                

<div id="dialogAddPersonToDocument" title="Add a Person to a Resource" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">{ teiEditor:label("peopleManager-linkaPerson", "Link a Person to Document") }</h4>
                </div>
                <div class="modal-body">
                      <form id="addPersonForm" class="addPersonForm" role="form" data-toggle="validator" novalidate="true">
                      <h4>{ teiEditor:label("peopleManager-linkalreadyInDBPerson", "Link a person already in database") }</h4>
                            <div class="form-group">
                                  <label for="projectPeopleLookup">{ teiEditor:label("search", "Search") }</label>
                                   <input type="text" class="form-control projectPeopleLookup"
                                   id="projectPeopleLookup"
                                   name="projectPeopleLookup"
                                   autocomplete="on"
                                   />
                        </div>
                        <div id="projectPeopleDetailsPreview" class=""/>
                        <input id="currentDocumentUri" type="text" class="hidden"/>
                        <input id="selectedPeopleUri" type="text" class="hidden"/>
                        <button  id="addPersonToDocButton" class="btn btn-primary pull-left" type="submit" onclick="addProjectPersonToDoc()">OK</button>         
                            <hr/>
                            <h4 >{ teiEditor:label("peopleManager-createNewPersonRecord", "Create a new person record") }</h4>
                            <div class="form-group">
                                <label for="citedRange">{ teiEditor:label("peopleManager-standardNameEnglish", "Standardized name (in English)") }</label>
                                <input type="text" class="form-control" id="newPersonStandardizedNameEn" name="newPersonStandardizedNameEn"
                                data-error="Please enter your full name."/>
                            </div>
                            <div class="form-group">
                                <label for="citedRange">{ teiEditor:label("peopleManager-standardNameFrench", "Standardized name (in French)") }</label>
                                <input type="text" class="form-control" id="newPersonStandardizedNameFr" name="newPersonStandardizedNameFr"
                                data-error="Please enter your full name."/>
                            </div>
                            <div class="form-group">
                                <label for="citedRange">{ teiEditor:label("peopleManager-translitName", "Name in translitteration") }</label>
                                <input type="text" class="form-control" id="newPersonTranslitteredName" name="newPersonTranslitteredName"
                                data-error="Please enter your full name."/>
                            </div>
                            
                            {skosThesau:dropDownThesauForElement("sex", "c23490", "en", teiEditor:label("sex", "Sex"), 'row', (), (), "uri")}
                            
                            <input id="person2AddType" class="valueField"/>
                            <input id="person2AddOwner" class="valueField"/>
                            <input id="person2AddBondType" class="valueField"/>
                            
                        <div class="modal-footer">


                        <button  class="btn btn-primary pull-left" type="submit" onclick="createAndAddPersonToDoc('{ $teiEditor:docId }')">OK</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                  </form>
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>

            </div>


    </div>


</div>
};

(:DASHBOARD:)

(:~ Function to generate the HTML dashboard page. :)
declare function teiEditor:dashboard($corpus as xs:string?){
    let $newDocType :=  $teiEditor:appVariables//newDocType/text()
    let $documentCollection := collection("/db/apps/" || $teiEditor:project || "Data/documents")
    let $documentList := doc("/db/apps/" || $teiEditor:project || "Data/lists/list-documents.xml")
    let $docDiff:= (count($documentCollection//tei:TEI) - count($documentList//data))
        return
  <div data-template="templates:surround" data-template-with="templates/page.html" data-template-at="content">
      <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <h2>{ teiEditor:label("dashboard-corpus-title", "Browse collections") }</h2>
                </div>
            </div>
            <div class="row">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#documents" aria-controls="documents" role="tab" data-toggle="tab"><button class="btn btn-xs btn-default" style="border: 0;" onclick="window.open('/admin/corpus/')"><i class="glyphicon glyphicon-home"/></button> Corpus</a></li>
                    <li role="presentation"><a href="#logs" aria-controls="logs" role="tab" data-toggle="tab">Logs</a></li>
                </ul>


                <!-- Tab panes -->
                <div class="tab-content">
                    <style type="text/css">
                        .modal4loading {{
                            display:    none;
                            position:   fixed;
                            z-index:    1000;
                            top:        0;
                            left:       0;
                            height:     100%;
                            width:      100%;
                            background: rgba( 255, 255, 255, .8 ) 
                                        url('http://i.stack.imgur.com/FhHRx.gif') 
                                        50% 50% 
                                        no-repeat;
                        }}


                        body.loading .modal4loading {{
                            overflow: hidden;   
                        }}

                        body.loading .modal4loading {{
                            display: block;
                        }}
                            </style>   

            <div role="tabpanel" class="tab-pane active" id="documents">
<!--                  { teiEditor:newCollectionPanel() } -->
                <div class="row">
                    <div class = "col-xs-3 col-sm-3 col-md-3">
                    { teiEditor:corpusList($corpus) }
                    <div class="modal4loading"><!-- Place at bottom of page --></div>
                    <script type="text/javascript">
                        $body = $(body);
                       
                        $(".loadingLink").click(function() {{
                            $body.addClass("loading");  
                        }})
                        
                    </script>
                                </div>
                                {if ($corpus !="") then
                                <div class = "col-xs-9 col-sm-9 col-md-9">
                                  { switch($newDocType)
                                                                case "simpleTitle" return (teiEditor:newDocumentSimpleTitlePanel($corpus), teiEditor:newDocumentSimpleTitleWithEditionFromExtResourcePanel($corpus))
                                                                case "multi" return teiEditor:newDocumentPanelMultipleChoice($corpus)
                                                                default return teiEditor:newDocumentPanelMultipleChoice($corpus)
                                                         }
                                  {  teiEditor:documentList($corpus) }
 
                                </div>
                                else if($teiEditor:appVariables//dasboardFullList/text() = "yes") then 
                                (
                                <div class= "col-xs-9 col-sm-9 col-md-9">
                                <h4>{ teiEditor:label("dashboard-corpus-allDocs", "All documents") }</h4>
                                {if($docDiff = 0) then "" 
                                else if($docDiff = 1) then "There is 1 document not included in the generated list of documents"
                                else "There are " || $docDiff || " documents not included in the generated list of documents"}
                                {if($docDiff > 0) then
                                    <div class="form-group">
                                            <div class="">
                                                
                                                    <button id="btn-regenerate" class="btn btn-warning" onclick="regenerate()">Re-generate list</button><br/>
                                                    <img id="f-load-indicator" class="hidden" src="/resources/images/ajax-loader.gif"/>
                                                    <div id="messages"></div>
                                            </div>
                                        </div>
                                    else()}
                                <p>{ teiEditor:label("dashboard-corpus-landingAllDocsLanding", "To browse by corpus, click on a corpus name in the left column") }</p>
                                     <table id="fullList" class="table">
                                       <thead>
                                            <tr>
                                                <td class="sortingActive">ID</td>
                                                <td>Document title</td>
                                                <td>Provenance</td>
                                                <td>Dating</td>
                                                <td></td>
                                                <td>Other IDs</td><!--Header for TM -->
                                                <td>Last modified</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {for $doc in $documentCollection//tei:TEI

                                            let $resourceName := if($doc) then util:document-name($doc) else ()
                                          
                                            let $collectionName := if($doc) then  util:collection-name($doc) else ()
                                            let $lastModified := if($doc) then substring-before(string(xmldb:last-modified($collectionName, $resourceName)), "T")
                                                                else "No doc"
                                            let $document:= $documentList//data[id = $doc/@xml:id]
                                            return
                                            <tr>
                                                <td>{ $doc/@xml:id/string() }</td>
                                                <td><span>{ if($document/title/text() ="") then "Cannot retrieve title as doc not in list" else $document/title/text() }
                                                    <a href="/edit-documents/{$doc/@xml:id/string()}" target="_blank" title="Edit document in a new window"><i class="glyphicon glyphicon-edit"/></a>
                                                    <a href="/documents/{$doc/@xml:id/string()}" target="_blank" title="Open public view in a new window"><i class="glyphicon glyphicon-eye-open"/></a>
                                                    </span></td>
                                                <td><a href="{ $document/provenanceUri/text()}" target="_blank">{ $document/provenance/text() }</a></td>
                                                <td>{ $document/datingNotBefore/text() }</td>
                                                <td>{ $document/datingNotAfter/text() }</td>
                                                <td style="overflow-wrap: anywhere; width: 100px">{ $document/otherId/text() }</td>
                                                <td>{ $lastModified }</td>
                                            </tr>
                                            }
                                        </tbody>
                                        </table>
                                </div>
                                )
                                else(teiEditor:label("dashboard-corpus-landing", "Please select a corpus in the left-hand menu to see its documents"))
                                }
                       </div>
                       
                      
                    </div>
                    <div role="tabpanel" class="tab-pane" id="logs">
                    </div>
                </div>
            </div>
        </div>
         <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css"/>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/plug-ins/1.10.21/sorting/any-number.js"/>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"/>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"/>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"/>
    <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/teiEditor-dashboard.js"/>
    <link href="$ausohnum-lib/resources/css/skosThesau.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">$(document).ready( function () {{
                      $('#fullList').DataTable({{
                        //scrollY:        "600px",
                        scrollX:        false,
                        scrollCollapse: true,
                        paging: false,
                        dom: <![CDATA[
                            "<'row'<'col-sm-2'l><'col-sm-4'B><'col-sm-6'f>>" +
                                                        
                                                         "<'row'<'col-sm-12'tr>>" +
                                                         "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                            ]]>
                        buttons: [
                                {{extend: 'csv',
                                className: 'exportButton',
                                text: '<i class="glyphicon glyphicon-export"/> csv'
                                }},
                                {{
                                extend: 'excel',
                                className: 'exportButton',
                                text: '<i class="glyphicon glyphicon-export"/> excel'
                                }},
                                {{
                                extend: 'copy',
                                className: 'exportButton',
                                text: '<i class="glyphicon glyphicon-copy"/> copy'
                                }}],
                        columnDefs: [
                                                 
                                                   {{
                                                       "type": "any-number", targets: [1]
                                                       
                                                   }},
                                                   {{
                                                       "type": "any-number", targets: [3]
                                                       
                                                   }},
                                                   {{
                                                       "type": "any-number", targets: [4]
                                                       
                                                   }},
                                                   {{
                                                       "width": "20%",
                                                       "render": function (data, type, full, meta) {{
                                                        return '<div>'
                                                            + data + "</div>";
                                                            }},
                                                            targets: [5]
                                                       
                                                   }},  
                                                ],
                          fixedColumns: false,
                          language: {{
                                            search: "Search (also by TM no., EDCS no.,... and other identifiers):"
                                                }}
                            
                            }});

                            $( '#fullList' ).searchable();
                        }} );
                        
                        function regenerate() {{
                            $("#messages").empty();
                            $("#btn-regenerate").attr("disabled", true);
                            $("#f-load-indicator").removeClass('hidden');
                            $("#messages").text("The list of documents is being regenerated. Page will be reloaded once this process is finished...");
                            $.ajax({{
                                type: "POST",
                                dataType: "json",
                                url: "/documents/update-list/",
                                success: function (data) {{
                                console.log(data);
                                    $("#f-load-indicator").addClass("hidden");
                                    
                                    $("#btn-regenerate").attr("disabled", false);
                                    if (data.status == "failed") {{
                                        $("#messages").text(data.message);
                                    }} else {{
                                        window.location.href = ".";
                                    }}
                                }}
                            }});
                        }}
</script>
    </div>


};

(:~ Function to generate the list of coprus to be displayed in the dashboard. :)
declare function teiEditor:corpusList($corpus as xs:string?){
(:            let $collections := xmldb:get-child-collections($teiEditor:doc-collection):)

            let $corpora := dbutil:scan-collections(
                                xs:anyURI($teiEditor:doc-collection-path),
                        function($collection) { $collection }
                        )
           
           let $noOfCorpus :=  count($corpora)

           let $collections :=
                for $child at $pos  in $corpora
                        let $collectionName := functx:substring-after-last($child, '/')
                        order by $collectionName
                        return
                        if ($pos > 1) then 
                            (if(sm:has-access($child, 'rw-')) then $child else ())
                    else ()


            return
         
         if ($noOfCorpus = 1 ) then 
            (
            
            )
            
            else if ($noOfCorpus > 1 ) then
                (
                <div class = "list-group">
                             
                  
                { for $child at $order in $collections
                        let $collectionName := functx:substring-after-last($child, '/')
                        let $corpusTitle :=
                            doc(xs:anyURI($teiEditor:doc-collection-path ||
                            '/' || $collectionName ||'.xml'))//title[@type="full"]/text()
                        let $numOfDocs := teiEditor:countDocumentsInCollection(string($child))
                        let $collectionPath:= substring-after($child, '/documents/')
                        return
                     <a href="/admin/corpus/{$collectionPath}" class="list-group-item{if($corpus =$collectionName) then ' active' else()} loadingLink">
                      <h4 class="list-group-item-heading">{ functx:capitalize-first($corpusTitle)}</h4>
                      <p class="list-group-item-text">{ $numOfDocs }
                                            document{if($numOfDocs > 1) then "s" else ()}</p>
                    </a>
                                         }
                    <a href="/admin/corpus/trash" class="list-group-item{if($corpus = "trash" ) then ' active' else()} loadingLink">
                      <h4 class="list-group-item-heading">Trash</h4>
                      <p class="list-group-item-text"></p>
                    </a>
                  
                </div>
                
                
                )
                
    else ("no corpus yet")

};

(:~ Function to generate the list of coprus with documents, to be displayed in the dashboard.
 : Not fully tested ; teiEditor:corpusList($corpus as xs:string?) :)
declare function teiEditor:corpusListWithDocuments(){
(:            let $collections := xmldb:get-child-collections($teiEditor:doc-collection):)
            let $newDocType :=  $teiEditor:appVariables//newDocType/text()
            let $corpus := dbutil:scan-collections(
                                xs:anyURI($teiEditor:doc-collection-path),
                        function($collection) { $collection }
                        )
           
           let $noOfCoprus :=  count($corpus)

           let $collections :=
                            for $child at $pos  in $corpus
                                    let $collectionName := functx:substring-after-last($child, '/')
                                    order by $collectionName
                                    return
                                    if ($pos > 1) then $child else ()

            return
            if ($noOfCoprus > 1 ) then
            <div class = "container">
                        <div class="panel-group" id="collectionList">
                            { for $child at $order in $collections
                                    let $collectionName := functx:substring-after-last($child, '/')

                                    return


                                     <div class="panel panel-default">
                                                <div class="panel-heading" id="heading{$order}">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#collectionList" href="#collectionCollapse{$order}">
                                                          Collection <em>{ functx:capitalize-first($collectionName)}</em>
                                                            ({teiEditor:countDocumentsInCollection($collectionName)}
                                                            document{if(teiEditor:countDocumentsInCollection($collectionName) > 1) then "s" else ()})
                                                      </a>
                                                </div>
                                                <div id="collectionCollapse{$order}" class="panel-collapse collapse{if ($order = 1) then "in" else()}" >
                                                      <div class="panel-body">{ switch($newDocType)
                                                                case "simpleTitle" return teiEditor:newDocumentPanel($collectionName)
                                                                case "multi" return teiEditor:newDocumentPanelMultipleChoice($collectionName)
                                                                default return teiEditor:newDocumentPanelMultipleChoice($collectionName)
                                                         }

                                                          { teiEditor:documentList(functx:substring-after-last($child, '/')) }
                                                      </div>
                                                </div>
                                      </div>
                                     }
                                 </div>
                            </div>
    else ("no corpus yet")

};

(:~ Function to generate the list of document of a specific coprus, to be displayed in the dashboard. :)
declare function teiEditor:documentList($collection as xs:string){
let $documents := if($collection = "trash")
                    then collection($teiEditor:data-repository-path || "/trash/documents")//tei:TEI
                    else collection($teiEditor:doc-collection-path || "/" || $collection)//tei:TEI
let $docPrefix := doc($teiEditor:doc-collection-path || "/" || $collection || ".xml")//docPrefix/text()
let $placeToDisplay := $teiEditor:appVariables//dashboardPlaceToDisplay/text()
let $lang := request:get-parameter("lang", ())
(:let $docPrefix := $teiEditor:appVariables//idPrefix[@type='document']/text():)
let $seletedDocDropDown:=
<div class="dropdown">
  <button class="btn btn-xs btn-default dropdown-toggle" type="button" id="selectedDocDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <i class='glyphicon glyphicon-cog'></i>
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="selectedDocDropdown">
    <li><a onclick="selectedDocAction('delete')"><i class='glyphicon glyphicon-trash'></i> delete</a></li>
  </ul>
</div>
return
    <div id="documentListDiv">{ $collection }
   <!-- Toggle columns: <a class="toggle-vis" data-column="1">TM no.</a>
    <a class="toggle-vis" data-column="7">Other identifiers</a>
    -->
  <!--  
     <div class="input-group">
  <span class="input-group-addon" ><i class="glyphicon glyphicon-filter"/></span>
    <input class="search"  id="search" placeholder="Filter document list"></input>
    </div>
    -->
    <table id="documentList" class="table">
     <span class="pull-right">Sorting language: {$lang}</span>
     <thead>
        <tr>
        <td>{ $seletedDocDropDown }</td>
        <td>Document title</td>
        <td class="sortingActive">ID</td>
        <td>Provenance</td>
        <td>Dating</td>
        <td></td>
        <td>TM no.</td><!--Header for TM -->
        <td>Edition</td>
        <td>Other identifiers</td><!--Header for other identifiers-->
        </tr>
        </thead>
        <tbody>
      
      { for $document at $pos in $documents
         let $title := (
            (if(count($document/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[not(@type='corpus')]) >1) then
                $document/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[not(@type='corpus')][@xml:lang=$lang]/text()
                else $document/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[not(@type='corpus')]/text()
            )||
            (if($collection="trash") then " DELETED "
            || substring-before(substring-after(util:document-name($document), data($document/@xml:id)|| "-"), ".xml")
            else'')
         )
        let $provenanceUri := 
                let $splitRef := tokenize(data($document//tei:sourceDesc/tei:msDesc/tei:history/tei:provenance//tei:placeName/@ref), " ")
                                                return 
                                                    for $uri in $splitRef
                                                    return
                                                    if(contains($uri, $teiEditor:baseUri)) then 
                                                    normalize-space($uri[1]) else ()
           let $datingNotBefore :=
                    if($document//tei:origDate/@notBefore-custom)
                           then data($document//tei:origDate/@notBefore-custom)
                    else if($document//tei:origDate/tei:date/@notBefore)
                    
                    then data($document//tei:origDate/tei:date/@notBefore)
                    else ()
         let $datingNotAfter :=
                    if($document//tei:origDate/@notAfter-custom)
                           then data($document//tei:origDate/@notAfter-custom)
                    else if($document//tei:origDate/tei:date/@notAfter)
                    
                    then data($document//tei:origDate/tei:date/@notAfter)
                    else ()
        let $edition :=
                if($document//tei:div[@type="bibliography"][@subtype="edition"]//tei:listBibl//tei:bibl)
                    then teiEditor:displayBibRef($document/@xml:id, $document//tei:div[@type="bibliography"][@subtype="edition"]/tei:listBibl[1]/tei:bibl[1], "info", 1)
                else if($document//tei:div[@type="bibliography"][@subtype="edition"]//tei:bibl)
                    then teiEditor:displayBibRef($document/@xml:id, $document//tei:div[@type="bibliography"][@subtype="edition"]//tei:bibl[1], "info", 1)
                else "No edition"
        let $editUrlPattern := $teiEditor:appVariables//editResourcesPrefix/text()
        let $editLink := if(contains($editUrlPattern, "edit-document") or ($editUrlPattern =""))
                then ("/edit-documents/" || data($document/@xml:id))
                else ("/documents/" || data($document/@xml:id) || "/edit")
       (:order by $document//tei:fileDesc/tei:titleStmt/tei:title[not(@type='corpus')][@xml:lang=$lang]/text():)
       
       
       (: order by xs:int(substring-after(data($document/@xml:id), $docPrefix )) :)
       order by number(functx:substring-after-last-match(data($document/@xml:id), "[a-zA-Z]" ))

               return
           <tr>
           <td>
           <input type="checkbox" id="selectDoc-{data($document/@xml:id)}" name="selectDoc-{data($document/@xml:id)}" value="{data($document/@xml:id)}"/>
           { if (contains($document//tei:div[@type="edition"]/tei:div[@type="textpart"]/tei:ab/text(), "Error")) then ("&#9888;")
           else ()}
           </td>
           <td>{$title}<a href="/documents/{data($document/@xml:id)}" target="_blank">
           <i class="glyphicon glyphicon-eye-open"/></a>
           <a href="{ $editLink }" target="_blank">
           <i class="glyphicon glyphicon-edit"/></a>
           </td>
           <td>
           { data($document/@xml:id)}
           </td>
           {
                switch ($placeToDisplay)
                  case 'provenance' return
                        (
                        if($provenanceUri != "") then
                            (
                            <td>
                                    <a href="{$provenanceUri}" >{
                                    $teiEditor:placeCollection//pleiades:Place[@rdf:about=$provenanceUri[1]]//dcterms:title[@xml:lang='en'][1]/text()
                                    }
                                    {""
    (:                                $document//tei:sourceDesc/tei:msDesc/tei:history/tei:provenance/string():)
                                    }</a>
                                    <a href="{ $provenanceUri }" target="_blank">
                                    <i class="glyphicon glyphicon-new-window"/></a>
                             </td>
                             )
                             else(<td/>)
                        )
                   case 'origin' return
                   let $uri := data($document//tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origPlace/tei:placeName/@ref)
                   return
                        (<td>{ $document//tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origPlace/tei:placeName/text() }
                                <!--<a href="" >
                                { $document//tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origPlace/string() }
                                </a>
                                <a href="{ data($document//tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origPlace/@ref) }" target="_blank">
                                
                                <i class="glyphicon glyphicon-new-window"/></a>
                                -->
                         </td>
                       )
               
                  default return
                  (<td> 
                        {if($document//tei:history/tei:provenance[1]//tei:placeName/text()!= "")
                        then  (
                            let $placeUri :=data($document//tei:history/tei:provenance[1]//tei:placeName/@ref)
                            return <span>
                            <a href="{ $placeUri }" >{$document//tei:history/tei:provenance[1]//tei:placeName/text()}</a>
                         <a href="{ $placeUri }"><i class="glyphicon glyphicon-new-window"/></a>
                         </span>)
                            else ()}
                        {if($document//tei:origPlace/tei:placeName/text()!= "")
                        then (
                            let $placeUri :=data($document//tei:history//origPlace/tei:placeName/@ref)
                            return 
                        <span>
                        <a href="{ $placeUri }" >{$document//tei:origPlace/tei:placeName/text()}</a>
                         <a href="{ $placeUri }" > <i class="glyphicon glyphicon-new-window"/></a>
                         </span>)
                            else ()}
                                
                         </td>
                        )
                       
           }
           <td>{ $datingNotBefore }</td>
           <td>{ $datingNotAfter }</td>
           <td>{ $document//tei:idno[@subtype="tm"]/text()}</td>
           <td>{ $edition }</td>
           <td>{ string-join($document//tei:idno[not(contains(., $teiEditor:project))]/text(), " | ")}</td>
           </tr>
           

    }
    </tbody>
    </table>
   
    <script type="text/javascript">$(document).ready( function () {{
                        $('#documentList').DataTable({{
                        //scrollY:        "600px",
                        scrollX:        false,
                        scrollCollapse: true,
                        paging: false,
                        columnDefs: [
                                                 {{
                                                       width: 400, targets: [1]
                                                       
                                                   }},  
                                                   {{
                                                       "type": "any-number", targets: [2]
                                                       
                                                   }},
                                                   {{
                                                       "type": "any-number", targets: [4]
                                                       
                                                   }},
                                                   {{
                                                       "type": "any-number", targets: [5]
                                                       
                                                   }},
                                                   {{
                                                       targets: [ 6 ],
                                                       visible: false
                                                   }},
                                                   {{
                                                       targets: [7],
                                                       width: 10
                                                   }},
                                                   {{
                                                       targets: [8],
                                                       width: 200
                                                   }},
                                                ],
                          fixedColumns: true,
                          language: {{
                                            search: "Search (also by TM no., EDCS no.,... and other identifiers):"
                                                }}
                            }});
                        }} );</script>
    
    <script type="text/javascript">$( '#documentList' ).searchable();</script>
    </div>
};

declare function teiEditor:countDocumentsInCollection($collection){
 count(collection(xs:anyURI("/" || $collection))//tei:TEI)

};

declare function teiEditor:listDocuments(){

    <table id="documentList"
    class="table table-striped">

    <tr>
        <th/>
        <th>Title</th>
        <th>id</th>
        <th>Provenance</th>
        <th/>
        <th/>
        <th>Places</th>
        <th>People</th>
        <th>Events</th>

    </tr>
    {for $document in $teiEditor:doc-collection//tei:TEI
    order by data($document/@xml:id)
    return
    <tr>
    <td><a href="documents/{data($document/@xml:id)}" >
    <i class="glyphicon glyphicon-eye-open"/></a>
    </td>
    <td>
    {$document//tei:fileDesc/tei:titleStmt/tei:title/text()}
    </td>
    <td>
    {data($document/@xml:id)}
    </td>
    <td>
    {$document//tei:sourceDesc/tei:msDesc/tei:history/tei:provenance/string()}
    
    </td>
    </tr>
    }
    </table>

};



(:CREATE NEW DOCUMENT:)

(: Function to be included in a dashboard :)

(:~ Function to generate the HTML widget to deal with creation of a new coprus (collection). :)
declare function teiEditor:newCollectionPanel(){
    let $templateList :=  collection( $teiEditor:library-path || 'data/teiEditor/docTemplates')

    return
    <div>
    <a onclick="openDialog('dialogNewCollection')" class="newDocButton"><i class="glyphicon glyphicon-plus"/>New collection</a>
    <!--Dialog for new document-->
    <div id="dialogNewCollection" title="Create a new collection" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Create a new Collection of documents</h4>
                </div>
                <div class="modal-body">
                <div>
                      <div class="form-group row">
                                <label for="newCollectionTitleFull" class="col-sm-2 col-form-label">Full Title</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="newCollectionTitleFull" name="newCollectionTitleFull"/>
                                </div>
                      </div>
                       <div class="form-group row">
                                <label for="newCollectionTitleShort" class="col-sm-2 col-form-label">Short Title</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="newCollectionTitleShort" name="newCollectionTitleShort"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="newCollectionPrefix" class="col-sm-2 col-form-label">Prefix for Doc IDs</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="newCollectionPrefix" name="newCollectionPrefix"/>
                                </div>
                            </div>
                </div>
                    <div class="form-group modal-footer">
                        <button id="createCollection" class="pull-left" onclick="createNewCollection()">Create Collection</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->
                </div>
            </div>
       </div><!--End of dialog-->
    </div>
};


(:~ Function to generate the HTML widget to deal with creation of a new document. :)
declare function teiEditor:newDocumentPanel($collection as xs:string){
(:    let $templateList :=  collection( $teiEditor:library-path || '/data/teiEditor/docTemplates'):)
let $templateList :=  collection('/db/apps/' || $teiEditor:project || '/data/teiEditor/docTemplates')
    let $project := request:get-parameter("project", ())
    return
    <div>
    <a onclick="openDialog('dialogNewDocument{$collection}', '{$project}')" class="newDocButton"><i class="glyphicon glyphicon-plus"/>New document</a>
    <!--Dialog for new document-->
    <div id="dialogNewDocument{$collection}" title="Create a new document" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Create a new document in {$collection}</h4>
                </div>
                <div class="modal-body">
                <div>
                <form autocomplete="off" >
                  <div class="form-group row">
                        <label for="newDocTemplate{$collection}" class="col-sm-2 col-form-label">Select a template</label>
                        <div class="col-sm-10">
                        { <select id="newDocTemplate{$collection}">
                              {for $items in $templateList//tei:TEI
                              order by $items[@xml:id = $teiEditor:project], $items/@xml:id

                                    return
                                        if (($items/@xml:id = $teiEditor:project))
                                        then (<option value="{data($items/@xml:id)}" selected='selected'>{data($items/@xml:id)}</option>)
                                        else (
                                        <option value="{data($items/@xml:id)}">{data($items/@xml:id)}</option>
                                        )

                                     }
                        </select>}

                        </div>
                    </div>
                    <div class="form-group row">
                                <label for="newDocTitle{$collection}" class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="newDocTitle{$collection}" name="newDocTitle{$collection}"/>
                                </div>
                            </div>
                     {skosThesau:dropDownThesauXML('c21851', 'en', 'Type', 'row', (), 1, ())}
                     {skosThesau:dropDownThesauXML('c39', 'en', 'Language', 'row', (), 1, 'xml')}
                     {skosThesau:dropDownThesauXML('c66', 'en', 'Script', 'row', (), 1, 'xml')}
                     <!--<div class="form-group">
                                <label for="zoteroLookupCreateNewDoc">Search in <a href="" target="_blank">Zotero</a>
                                </label>
                                <input type="text" class="form-control zoteroLookup" id="zoteroLookupCreateNewDoc" name="zoteroLookupInputModal"/>
                            </div>

                            <div class="form-group">
                                <label for="citedRange">Cited Range
                                </label>
                                <input type="text" class="form-control" id="citedRange" name="citedRange"/>
                            </div>
                            <div id="selectedBiblioAuthor"/>
                            <div id="selectedBiblioDate"/>
                            <div id="selectedBiblioTitle"/>
                            <div id="selectedBiblioId"/>
                            -->
                 </form></div>
               




                    <div class="form-group modal-footer">


                        <button id="createDocument{ $collection }" class="pull-left" onclick="createNewDocument('{ $collection }')">Create document</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->
                </div>
            </div>
       </div><!--End of dialog-->
    </div>
};

(:~ Function to generate the HTML widget to deal with creation of a new document. Metadata is reduced to minium. :)
declare function teiEditor:newDocumentSimpleTitlePanel($collection as xs:string){
    let $templateList :=  collection('/db/apps/' || $teiEditor:project || '/data/teiEditor/docTemplates')
    let $project := request:get-parameter("project", ())
    
    return
    <div>
        <div id="editResourcesPrefix" class="hidden">{ $teiEditor:appVariables//editResourcesPrefix/text() }</div>
    <a onclick="openDialog('dialogNewDocument{$collection}')" class="newDocButton"><i class="glyphicon glyphicon-plus"/>New document</a>
    <!--Dialog for new document-->
    <div id="dialogNewDocument{$collection}" title="Create a new document" class="modal fade" tabindex="-1" style="display: none;">
            
            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Create a new document in {$collection}</h4>
                </div>
                <div class="modal-body">
                <div>
                  <div class="form-group row">
                        <label for="newDocTemplate{$collection}" class="col-sm-2 col-form-label">Template</label>
                        <div class="col-sm-10">
                        { <select id="newDocTemplate{$collection}" name="newDocTemplate{$collection}">
                              {for $items in $templateList//tei:TEI
                                 
                                    return
                                        if (($items/@xml:id = $teiEditor:project))
                                        then (<option value="{data($items/@xml:id)}" selected="selected">{data($items/@xml:id)}</option>)
                                        else (
                                        <option value="{data($items/@xml:id)}">{data($items/@xml:id)}</option>
                                        )
                
                                     }
                        </select>}
                       
                        </div>
                    </div>
                    <div class="form-group row">
                                <label for="newDocTitle{$collection}" class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="newDocTitle{$collection}" name="newDocTitle{$collection}"/>
                                </div>
                            </div>
                            <button id="selectDropDownc19361_1_1" name="selectDropDownc19361_1_1" value="https://ausohnum.huma-num.fr/concept/c21852" class="btn btn-xs btn-default dropdown-toggle elementWithValue hidden" type="button" data-toggle="dropdown">
                       Epigraphic
                                <span class="caret"></span></button>
                             <button id="selectDropDownc39_1_1" name="selectDropDownc39_1_1" value="grc" class="btn btn-xs btn-default dropdown-toggle elementWithValue hidden" type="button" data-toggle="dropdown">
                       Ancient Greek
                                <span class="caret"></span></button>
                            
                            
                     <!--
                     {skosThesau:dropDownThesauXML('c19361', 'en', 'Type', 'row', (), 1, ())}
                     
                     {skosThesau:dropDownThesauXML('c39', 'en', 'Language', 'row', (), 1, ())}
                     -->
                     <!--<div class="form-group">
                                <label for="zoteroLookupCreateNewDoc">Search in <a href="" target="_blank">Zotero</a>
                                </label>
                                <input type="text" class="form-control zoteroLookup" id="zoteroLookupCreateNewDoc" name="zoteroLookupInputModal"/>
                            </div>
                            
                            <div class="form-group">
                                <label for="citedRange">Cited Range
                                </label>
                                <input type="text" class="form-control" id="citedRange" name="citedRange"/>
                            </div>
                            <div id="selectedBiblioAuthor"/>
                            <div id="selectedBiblioDate"/>
                            <div id="selectedBiblioTitle"/>
                            <div id="selectedBiblioId"/>
                            -->
                </div>

                    <div class="form-group">
                                <label for="externalResourceUri{ $collection }" >Value (URI or id)</label>
                                <input type="text" class="form-control" id="externalResourceUri{ $collection }" name="externalResourceUri{ $collection }"/>
                    </div>
                    <div class="form-group modal-footer">


                        <button id="createDocument{$collection}" class="pull-left" onclick="createNewDocument('{ $collection }')">Create document</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->
                </div>
            </div>
       </div><!--End of dialog-->
    </div>
};

(:~ Function to generate the HTML widget to deal with creation of a new document, with option to impport edition from external respource.
 : Not fully tested :)
declare function teiEditor:newDocumentSimpleTitleWithEditionFromExtResourcePanel($collection as xs:string){
    let $templateList :=  collection('/db/apps/' || $teiEditor:project || '/data/teiEditor/docTemplates')
    let $project := request:get-parameter("project", ())
    return
    <div>
    <a onclick="openDialog('dialogNewDocumentWithEditionFromExtResource{$collection}')" class="newDocButton"><i class="glyphicon glyphicon-plus"/>New document with Edition from external resource</a>
    <!--Dialog for new document-->
    <div id="dialogNewDocumentWithEditionFromExtResource{$collection}" title="Create a new document" class="modal fade" tabindex="-1" style="display: none;">
            
            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Create a new document in {$collection}</h4>
                </div>
                <div class="modal-body">
                <div>
                  <div class="form-group row">
                        <label for="newDocTemplateWithEditionFromExtResource{$collection}" class="col-sm-2 col-form-label">Template</label>
                        <div class="col-sm-10">
                        { <select id="newDocTemplateWithEditionFromExtResource{$collection}" name="newDocTemplateWithEditionFromExtResource{$collection}">
                              {for $items in $templateList//tei:TEI
                                 
                                    return
                                        if (($items/@xml:id = $teiEditor:project))
                                        then (<option value="{data($items/@xml:id)}" selected="selected">{data($items/@xml:id)}</option>)
                                        else (
                                        <option value="{data($items/@xml:id)}">{data($items/@xml:id)}</option>
                                        )
                
                                     }
                        </select>}
                   
                        </div>
                    </div>
                    <div class="form-group row">
                                <label for="newDocTitleWithEditionFromExtResource{$collection}" class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="newDocTitleWithEditionFromExtResource{$collection}" name="newDocTitleWithEditionFromExtResource{$collection}"/>
                                </div>
                            </div>
                            
                                <div class="form-group">
                                <label for="externalResourceUriWithEditionFromExtResource{ $collection }">External resource URI (optional)</label>
                                
                                <input type="text" class="form-control externalResourceURI" id="externalResourceUriWithEditionFromExtResource{ $collection }" name="externalResourceUri{ $collection }"/>
                                
                        </div>
                        <button class="btn btn-primary" id="retrieveEditionFromExtRes" onclick="retrieveEditionFromExternalResource('{$collection}')">Check external resource</button>
                        <div id="externalResourceEditionPreview"/>
                        <button id="selectDropDownc19361_1_1" name="selectDropDownc19361_1_1" value="https://ausohnum.huma-num.fr/concept/c21852" class="btn btn-xs btn-default dropdown-toggle elementWithValue hidden" type="button" data-toggle="dropdown">
                       Epigraphic
                                <span class="caret"></span></button>
                             <button id="selectDropDownc39_1_1" name="selectDropDownc39_1_1" value="grc" class="btn btn-xs btn-default dropdown-toggle elementWithValue hidden" type="button" data-toggle="dropdown">
                       Ancient Greek
                                <span class="caret"></span></button>
                            
                            
                     <!--
                     {skosThesau:dropDownThesauXML('c19361', 'en', 'Type', 'row', (), 1, ())}
                     
                     {skosThesau:dropDownThesauXML('c39', 'en', 'Language', 'row', (), 1, ())}
                     -->
                     <!--<div class="form-group">
                                <label for="zoteroLookupCreateNewDoc">Search in <a href="" target="_blank">Zotero</a>
                                </label>
                                <input type="text" class="form-control zoteroLookup" id="zoteroLookupCreateNewDoc" name="zoteroLookupInputModal"/>
                            </div>
                            
                            <div class="form-group">
                                <label for="citedRange">Cited Range
                                </label>
                                <input type="text" class="form-control" id="citedRange" name="citedRange"/>
                            </div>
                            <div id="selectedBiblioAuthor"/>
                            <div id="selectedBiblioDate"/>
                            <div id="selectedBiblioTitle"/>
                            <div id="selectedBiblioId"/>
                            -->
                </div>

                        



                    <div class="form-group modal-footer">


                        <button id="createDocumentWithEditionFromExtResource{$collection}" class="pull-left"
                        onclick="createNewDocumentFromTemplateWithEditionFromExternalResource('{ $collection }')">Create document</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->
                </div>
            </div>
       </div><!--End of dialog-->
    </div>
};

(:~ Function to generate the HTML widget to deal with creation of a new document. :)
declare function teiEditor:newDocumentPanelMultipleChoice($collection as xs:string){
(:    let $templateList :=  collection( $teiEditor:library-path || '/data/teiEditor/docTemplates'):)
let $templateList :=  collection('/db/apps/' || $teiEditor:project || '/data/teiEditor/docTemplates')
    let $project := request:get-parameter("project", ())
    return
    <div>
        <div id="editResourcesPrefix" class="hidden">{ $teiEditor:appVariables//editResourcesPrefix/text() }</div>

    <a onclick="openDialog('dialogNewDocument{$collection}', '{$project }')" class="newDocButton"><i class="glyphicon glyphicon-plus"/>New document</a>
    <!--Dialog for new document-->
    <div id="dialogNewDocument{$collection}" title="Create a new document" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Create a new document in {$collection}</h4>
                </div>
                <div class="modal-body">
                
                <div id="" class="row">
                        
                      <!--  
                        <div class="col-xs-6 col-sm-6 col-md-6">
    
                            <h3>Import from an external resource</h3>
                        
      
                        <div class="form-group">
                            <label for="externalResource{ $collection }">Select an external resource</label>
                        
                                   <select id="externalResource{ $collection }" name="externalResource{ $collection }">
                                       {for $resource in $teiEditor:externalResources//resource
                                       let $name := $resource//name[@type='full']
                                       order by $name
                                       return
                                           <option value="{data($resource/@xml:id)}">
                                           {$name}
                                           </option>
                                        }
                                     </select>
                        
                        </div>
                        <div class="form-group">
                                <label for="docId{ $collection }">Document ID</label>
                                <input type="text" class="form-control" id="docId{ $collection }" name="docId{ $collection }"/>
                                
                        </div>
                        <hr/>
                        <p class="text-center big-text">- OR - </p>
                        <div class="form-group">
                                <label for="docUri{ $collection }">Insert a Document URI (only EDH)</label>
                                
                                <input type="text" class="form-control" id="docUri{ $collection }" name="docUri{ $collection }"/>
                                
                        </div>
                        <br/>
                        <button id="createDocumentFromExternalResource{ $collection }" 
                        
                        onclick="createNewDocumentFromExternalResource('{ $collection }')">Create document</button>
                        <br/>
                 </div>
                 -->
  
     <div class="col-xs-6 col-sm-6 col-md-6">
    
            <h3>Create from a template</h3>
                  <div class="">
                        <label for="newDocTemplate{$collection}" >Select a template</label>
                        
                        { 
                        <select id="newDocTemplate{$collection}" name="newDocTemplate{$collection}" 
                        class="templateSelect">
                        
                              {for $items in $templateList//tei:TEI
(:                              order by $items[@xml:id = $teiEditor:project], $items/@xml:id:)
                                  return
                                  <option value="{data($items/@xml:id)}">{data($items/@xml:id)}</option>
 (:                                       if ((contains(data($items/@xml:id), $teiEditor:project)))
                                        then (
                                        <option value="{data($items/@xml:id)}" selected="selected">{data($items/@xml:id)}</option>)
                                        else (
                                        <option value="{data($items/@xml:id)}">{data($items/@xml:id)}</option>
                                        ):)

                                     }
                        </select>}
                    </div>
                    <br/>
                    <div class="form-group">
                                <label for="newDocTitle{$collection}" >Give a Title</label>
                                
                                <input type="text" class="form-control" id="newDocTitle{$collection}" name="newDocTitle{$collection}"/>
                                
                      </div>
                    
                     {skosThesau:dropDownThesauXML('c21851', 'en', 'Type', 'inline', 1, 1, 'uri')}
                    
                    {skosThesau:dropDownThesauXML('c39', 'en', 'Language', 'inline', 1, 1, 'xml')}
                    
                    {skosThesau:dropDownThesauXML('c109', 'en', 'Script', 'inline', 1, 1, 'xml')}
                    
                    <hr/>
                    <h4>External resource (optional)</h4>
                                {skosThesau:dropDownThesauXML('c23504', 'en', 'Type', 'inline', 1, 1, 'xml')}
                                {skosThesau:dropDownThesauXML('c23500', 'en', 'Subtype', 'inline', 1, 1, 'xml')}
                    
                    <div class="form-group">
                                <label for="externalResourceUri{ $collection }" >Value (URI or id)</label>
                                <input type="text" class="form-control" id="externalResourceUri{ $collection }" name="externalResourceUri{ $collection }"/>
                                <button class="btn btn-primary" id="retrieveEditionFromExtRes" onclick="retrieveEditionFromExternalResource('{$collection}')">Check external resource</button>
                        <div id="externalResourceEditionPreview"/>
                      </div>
                     <!--<div class="form-group">
                                <label for="zoteroLookupCreateNewDoc">Search in <a href="" target="_blank">Zotero</a>
                                </label>
                                <input type="text" class="form-control zoteroLookup" id="zoteroLookupCreateNewDoc" name="zoteroLookupInputModal"/>
                            </div>

                            <div class="form-group">
                                <label for="citedRange">Cited Range
                                </label>
                                <input type="text" class="form-control" id="citedRange" name="citedRange"/>
                            </div>
                            <div id="selectedBiblioAuthor"/>
                            <div id="selectedBiblioDate"/>
                            <div id="selectedBiblioTitle"/>
                            <div id="selectedBiblioId"/>
                            -->
                    <br/>                 
                    <button id="createDocument{ $collection }" onclick="createNewDocument('{ $collection }')">Create document</button>
                 </div>

            
            </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>    
                
                
                
                
                
                

                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->
                </div>
            </div>
       </div><!--End of dialog-->
    </div>
};

(:~ Function to create a collection. $data is received from HTTP request sent by user through the new collection form. :)
declare function teiEditor:newCollection($data, $project) {
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)

    let $doc-collection := collection($teiEditor:data-repository-path || "/documents")
    let $doc-collection-path := $teiEditor:data-repository-path || "/documents"
    let $collectionPrefix := $data//collectionPrefix/text()
    let $collectionTitleFull := $data//titleFull/text()
    let $collectionTitleShort := $data//titleShort/text()


   let $filename := $collectionTitleShort || '.xml'
   let $metadataFile :=<corpus>
   <title type="full">{ $collectionTitleFull }</title>
   <title type="short">{ $collectionTitleShort }</title>
   <editor ref="{ $collectionTitleFull }">{ $collectionTitleFull }</editor>
   <docPrefix>{ $collectionPrefix }</docPrefix>
</corpus>

    let $collectionList := dbutil:scan-collections(xs:anyURI($doc-collection-path),
                                            function($collection) { $collection })
let $testIfExists :=  for $child at $pos  in $collectionList
                                    let $collectionName := functx:substring-after-last($child, '/')
                                    return
                                    if (matches($collectionName, "^" || $collectionTitleShort || "$")) then "collectionExists" else ()

return
           if ($testIfExists) then <result>erreur {$testIfExists}</result>
            else
            (
                    let $storeMetadataFile := xmldb:store($doc-collection-path, $filename, $metadataFile)
                    let $createCollection := xmldb:create-collection($doc-collection-path, $collectionTitleShort)
                    let $logEvent := teiEditor:logEvent("collection-new" , $collectionTitleShort, (),
                        "New collection " || $collectionTitleShort || " created in repository " || "by " || $currentUser)

                        return
                            <result><newCollection>Collection "{$collectionTitleShort }" created in { $doc-collection-path }</newCollection>
                            <newList>{ teiEditor:corpusList(()) }</newList>
                            </result>
                      )

};

(:~ Function to create a collection. $data is received from HTTP request sent by user through the new document form. :)
declare function teiEditor:newDocument($data, $project) {
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)
    let $template := collection( $teiEditor:library-path || '/data/teiEditor/docTemplates')//.[@xml:id=$data//template/text()]
    let $doc-collection := collection($teiEditor:data-repository-path || "/documents/" || $data//collection/text())
    let $doc-collection-path := $teiEditor:data-repository-path || "/documents/" || $data//collection/text()
    let $collectionPrefix := doc($teiEditor:data-repository-path || "/documents/" || $data//collection/text() || ".xml")//docPrefix/text()
(:    let $docIdPrefix := $teiEditor:appVariables//idPrefix[@type='document']/text():)
    let $docIdList := for $id in $doc-collection//tei:TEI[contains(./@xml:id, $collectionPrefix)]
        return
        <item>
        {substring-after($id/@xml:id, $collectionPrefix)}
        </item>

let $logEventTEST := teiEditor:logEvent("document-new-TEST" , 'test-new-doc', (),
                        "Test:" || $teiEditor:library-path || " created in Collection " || $data//collection/text() || " by " || $currentUser)


let $last-id:= fn:max($docIdList)
let $newDocId := $collectionPrefix || fn:sum(($last-id, 1))
let $newDocUri := $teiEditor:baseUri || "/" || "documents" ||"/" || $newDocId

let $filename := $newDocId || ".xml"


   let $storeNewFile := xmldb:store($doc-collection-path, $filename, $template)

   let $changeMod := sm:chmod(xs:anyURI(concat($doc-collection-path, "/", $filename)), "rw-rw-r--")
   let $changeGroup := sm:chgrp(xs:anyURI(concat($doc-collection-path, "/", $filename)), $data//collection/text())

   let $updateId := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/@xml:id
                            with $newDocId
  let $updateTitle := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/text()
                            with $data//title/text()
  let $updateTypeAtt := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:profileDesc/tei:textClass/tei:catRef/@ref
                            with $data//typeAttributeValue/text()
let $updateTypeText := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:profileDesc/tei:textClass/tei:catRef/text()
                            with $data//scriptTextValue/text()

let $updateMainLang := if (string-length($data//langAttributeValue/text()) > 0) then
                                update replace  util:eval( "doc('" || $doc-collection-path || "/" || $filename
                                ||"')")/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msContents/tei:msItem/tei:textLang/@mainLang
                                with $data//langAttributeValue/text()
                            else ()
(:Update IDs and references to ID with ID of new doc:)

let $updateSurfaceID := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:sourceDoc/tei:surface/@xml:id
                            with $newDocId || "-surface1"

let $updateLayoutID := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:layoutDesc/tei:layout/@xml:id
                            with $newDocId || "-layout1"
let $updateLayoutCorresp := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:layoutDesc/tei:layout/@corresp
                            with "#" || $newDocId || "-surface1"
let $updateMsItemID := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msContents/tei:msItem/@xml:id
                            with $newDocId || "-msItem1"
let $updateDivPartCorresp := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:text/tei:body/tei:div/tei:div[@type="textpart"]/@corresp
                            with "#" || $newDocId || "-surface1"






let $creationNode :=
    <change who="{$currentUser}" when="{$now}">Creation of this file</change>

let $updateCreationChange := update replace util:eval( "doc('" || $doc-collection-path || "/" || $filename
                                ||"')")/tei:TEI/tei:teiHeader/tei:revisionDesc/tei:listChange/tei:change
                                with functx:change-element-ns-deep($creationNode, "http://www.tei-c.org/ns/1.0", "")




let $logEvent := teiEditor:logEvent("document-new" , $newDocId, (),
                        "New document " || $newDocId || " created in Collection " || $data//collection/text() || " by " || $currentUser)
    return
    <result><newDocId>{ $newDocId }</newDocId>
    <sentData>{ $data }</sentData>
    <newList>{ teiEditor:listDocuments() }</newList>
    </result>
};

(:~ Function to create a collection. $data is received from HTTP request sent by user through the new document with external resource form. :)
declare function teiEditor:newDocumentFromExternalResource($data, $project) {
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)
    let $template := collection( $teiEditor:library-path || '/data/teiEditor/docTemplates')//.[@xml:id=$data//template/text()]
    let $doc-collection := collection($teiEditor:data-repository-path || "/documents/" || $data//collection/text())
    let $doc-collection-path := $teiEditor:data-repository-path || "/documents/" || $data//collection/text()
    let $collectionPrefix := doc($teiEditor:data-repository-path || "/documents/" || $data//collection/text() || ".xml")//docPrefix/text()
(:    let $docIdPrefix := $teiEditor:appVariables//idPrefix[@type='document']/text():)
    let $docIdList := for $id in $doc-collection//tei:TEI[contains(./@xml:id, $collectionPrefix)]
        return
        <item>
        {substring-after($id/@xml:id, $collectionPrefix)}
        </item>



(:Get document from external resource:)
let $sourceUri := $data//externalResource/text()
let $externalResourceDetails := $teiEditor:externalResources//.[@xml:id=$sourceUri]
let $UriPrefix := $externalResourceDetails/url[@type='teiDocPrefix']/text()
let $UriSuffix := $externalResourceDetails/url[@type='teiDocSuffix']/text()
let $externalDocId := $data//docId/text()
let $externalDocUri := $data//docUri/text()

let $logEventTEST := teiEditor:logEvent("document-new-TEST" , 'test-new-doc', (),
                        "$UriPrefix || $externalDocId || $UriSuffix:" || $UriPrefix || $externalDocId || $UriSuffix || " $externalDocId" || $externalDocId || " by " || $currentUser)


let $url4httpRequest := 
                    if($externalDocUri !='' ) then 
                            $externalDocUri || ".xml"
                            else if ($externalDocId != "") then
                            $UriPrefix || $externalDocId || $UriSuffix
                            else ($UriPrefix || $externalDocId || $UriSuffix)
                            
                    
let $http-request-data := <request xmlns="http://expath.org/ns/http-client"
    method="GET" href="{$url4httpRequest}"/>




    let $responses :=
    http:send-request($http-request-data)

let $logEventTEST := teiEditor:logEvent("document-new-from-External-resource" , 'test-new-docdddd', <data>{ $responses }</data>,
                        "$responses:" || "ee" || " created in Collection " || $data//collection/text() || " by " || $currentUser )


let $response :=
    <results>
      {if ($responses[1]/@status ne '200')
         then
             <failure>{$responses[1]}</failure>
         else
           <success>
             {$responses[2]}
             {'' (: todo - use string to JSON serializer lib here :) }
           </success>
      }
    </results>

        
(:let $newDocFromExternalResource := $response//*[local-name()="success"]:)

let $newDocFromExternalResource := $response//*[local-name()='TEI']


let $last-id:= fn:max($docIdList)
let $newDocId := $collectionPrefix || fn:sum(($last-id, 1))
let $newDocUri := $teiEditor:baseUri || "/" || $collectionPrefix ||"/" || $newDocId

let $filename := $newDocId || ".xml"


   let $storeNewFile := 
   xmldb:store($doc-collection-path, $filename, $newDocFromExternalResource)

   let $changeMod := sm:chmod(xs:anyURI(concat($doc-collection-path, "/", $filename)), "rw-rw-r--")
   let $changeGroup := sm:chgrp(xs:anyURI(concat($doc-collection-path, "/", $filename)), $data//collection/text())

   let $updateId := if(util:eval( "doc('" || $doc-collection-path ||"/" || $filename||"')")/tei:TEI/@xml:id) then
                            (update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/@xml:id
                            with $newDocId)
                            else 
                            (update insert attribute xml:id {$newDocId} into util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI)
  
(:let $updateTypeAtt := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:profileDesc/tei:textClass/tei:catRef/@ref
                            with $data//typeAttributeValue/text()
:)
(:let $updateTypeText := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:profileDesc/tei:textClass/tei:catRef/text()
                            with $data//scriptTextValue/text()
:)
(:let $updateMainLang := if (string-length($data//langAttributeValue/text()) > 0) then
                                update replace  util:eval( "doc('" || $doc-collection-path || "/" || $filename
                                ||"')")/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msContents/tei:msItem/tei:textLang/@mainLang
                                with $data//langAttributeValue/text()
                            else ():)
(:Update IDs and references to ID with ID of new doc:)

let $updateSurfaceID := 
                                if(util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:sourceDoc/tei:surface/@xml:id) then
                                update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:sourceDoc/tei:surface/@xml:id
                            with $newDocId || "-surface1"
                            else
                            if (util:eval( "doc('" || $doc-collection-path ||"/" || $filename ||"')")/tei:TEI/tei:sourceDoc/tei:surface)
                            then (
                            update insert attribute xml:id {$newDocId || "-surface1"} into util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:sourceDoc/tei:surface
                            )(:INSERT @xml:id:)
                            else if (util:eval( "doc('" || $doc-collection-path ||"/" || $filename ||"')")/tei:TEI/tei:sourceDoc)
                            then (
                            update insert 
                            functx:change-element-ns-deep(
<node>
                            <tei:surface xml:id="{$newDocId || '-surface1'}"></tei:surface>
</node>
, "http://www.tei-c.org/ns/1.0", "")/node()
into
                            util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:sourceDoc
                            )(:Insert /surface:)
                            else (
                            update insert 
                            functx:change-element-ns-deep(
<node>
    <tei:sourceDoc>
        <tei:surface xml:id="{$newDocId || '-surface1'}" ana="front-face">
            <desc>Main written surface of the monument</desc>
       </tei:surface>
   </tei:sourceDoc>
</node>
                            , "http://www.tei-c.org/ns/1.0", "")/node()
                            following
                            util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader
                            )(:insert sourceDoc/surface/@xml:id='':)

let $updateLayoutID := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:layoutDesc/tei:layout/@xml:id
                            with $newDocId || "-layout1"
                            
                            
let $updateLayoutCorresp := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:physDesc/tei:objectDesc/tei:layoutDesc/tei:layout/@corresp
                            with "#" || $newDocId || "-surface1"
let $updateMsItemID := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msContents/tei:msItem/@xml:id
                            with $newDocId || "-msItem1"
let $updateDivPartCorresp := update replace  util:eval( "doc('" || $doc-collection-path ||"/" || $filename
                            ||"')")/tei:TEI/tei:text/tei:body/tei:div/tei:div[@type="textpart"]/@corresp
                            with "#" || $newDocId || "-surface1"






let $creationNode :=
    <change who="{$currentUser}" when="{$now}">Creation of this file</change>

let $updateCreationChange := update replace util:eval( "doc('" || $doc-collection-path || "/" || $filename
                                ||"')")/tei:TEI/tei:teiHeader/tei:revisionDesc/tei:listChange/tei:change
                                with functx:change-element-ns-deep($creationNode, "http://www.tei-c.org/ns/1.0", "")




let $logEvent := teiEditor:logEvent("document-new" , $newDocId, (),
                        "New document " || $newDocId || " created in Collection " || $data//collection/text() || " by " || $currentUser)
    return
    <result><newDocId>{ $newDocId }</newDocId>
    <sentData>{ $data }</sentData>
    <newList>{ teiEditor:listDocuments() }</newList>
    </result>
};

(:~ Function to perform things on selected documents in dashboard.
 : Covered action: move to trash :)
declare function teiEditor:selectedDocsAction($data, $project as xs:string){
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)
    
    let $docIds := tokenize($data//selectedDocs, ", ") 
    let $action := $data//action/text()

    let $processSelectedDocs:=<data>{
        for $docId in $docIds
            let $doc:= collection("/db/apps/" || $project || "Data/documents")/id($docId)
            return
                switch ($action)
                case "delete"
                return
                (
            
                let $changeNode := <node>
            <change when="{ $now }" who="{ $currentUser }">Document moved to trash by { $currentUser }</change></node>
                let $insertRevisionChange := if($doc//tei:revisionDesc/tei:listChange/tei:change) then
                                update insert
                                ('&#xD;&#xa;',
                                functx:change-element-ns-deep($changeNode, "http://www.tei-c.org/ns/1.0", "")/node())
                                following $doc//tei:revisionDesc/tei:listChange/tei:change[last()]
                                else update insert
                                    functx:change-element-ns-deep($changeNode, "http://www.tei-c.org/ns/1.0", "")/node()
                                    into $doc//tei:revisionDesc/tei:listChange

                let $collectionName:= util:collection-name($doc)
                let $filename := $docId || "-" || replace(substring($now, 1, 16), ":", "") || ".xml"
                let $storeInTrash:= xmldb:store("/db/apps/" || $project || "Data/trash/documents", $filename, $doc)
                let $changeMod := sm:chmod(xs:anyURI(concat("/db/apps/" || $project || "Data/trash/documents", "/", $filename)), "rw-rw-r--")
                let $changeGroup := sm:chgrp(xs:anyURI(concat("/db/apps/" || $project || "Data/trash/documents", "/", $filename)), $project)

                let $removeDiscardedResource := xmldb:remove($collectionName||"/", $docId || ".xml")
                    return
                    <data>Resource { $docId } moved to trash</data>
                )
        default return ""
    }</data>
    return $processSelectedDocs

};


(:~ Function to update an element with a reference to a thesaurus concept, including the broader terms of th econcept.
 : Not fully tested. :)
declare function teiEditor:saveDataWithConceptHierarchy($data, $project ){


let $now := fn:current-dateTime()
let $currentUser := data(sm:id()//sm:username)

(:let $doc-collection := collection($config:data-root || "/" || $spatiumStructor:project || "/documents"):)

let $contentType := $data//contentType/text()
let $docId := $data//docId/text()

let $elementNickname := $data//elementNickname/text()
let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]))) 
    then $teiEditor:teiElements//teiElement[nm=$elementNickname] 
                        else $teiEditor:teiElementsCustom//teiElement[nm=$elementNickname]

let $conceptUris := tokenize($data//valueConceptHierarchy/text(), " ")

let $index := $data//index/text()

let $xpath := $data//xpath/text()

let $targetNode :=if(contains($xpath, "/@"))
                then  functx:substring-after-last(substring-before($xpath, "/@"), '/')
                else functx:substring-after-last($xpath, '/')
    
    
let $xpath2Node := if(contains($xpath, "[@"))
        then substring-before($xpath, '[@') else $xpath

let $xpathEnd := if(matches(functx:substring-after-last($xpath, '/'), "/@"))
            then(
                functx:substring-after-last(functx:substring-before-last($xpath, '/'), '/')
                )
            else
            (functx:substring-after-last($xpath, '/')
            )
            



let $xpathEndNoSelector :=if(contains($xpathEnd, "[@"))
        then substring-before($xpathEnd, '[@') else $xpathEnd

let $xpathEndSelector := if(contains($xpathEnd, "[@")) then
                    substring-before(substring-after($xpathEnd, '[@'), ']') else ""
let $xpathEndSelector :=
                    substring-before($xpathEndSelector, '=')
let $xpathEndSelectorValue :=
                    substring-before(substring-after($xpathEndSelector, '="'), '"')


let $endingSelector := if(matches(functx:substring-after-last($xpath, '/'), "@"))
            then(
                functx:substring-after-last($xpath, '/@')
                )
            else
            (
            )

let $updatedData := if($data//value/text())
                then $data//value
                    else " "

let $qnameForNewElement := functx:substring-before-if-contains($targetNode, '[@')
let $specificAttributeName := functx:substring-after-if-contains(substring-before($targetNode, '='), "[@")
let $specificAttributeValue := if($specificAttributeName != "") then substring-before(substring-after($targetNode, '="'), '"]')
                                                else ""

let $attributeName := substring-after($xpathEnd, "@")
let $newElements := <newElement>{
        for $concept in $conceptUris
        
            let $label := if($contentType = "text") then skosThesau:getLabel($concept, "en") else ""
                return
                element {string($qnameForNewElement)}
                {
        
                if(($specificAttributeName != "") and ($specificAttributeName !=$attributeName)) then (
                attribute {$specificAttributeName} {$specificAttributeValue}
                ) else(),


                attribute {$attributeName} { $concept },
                
                $label
              }
              }</newElement>
 

let $updatedDataTextValue := $data//valueTxt/text()



let $xpathWithPrefix := if(contains($data//xpath/text(), "/@"))
                                          then
                                          substring-before($data//xpath/text(), '/@') || '[' || $index || ']/' || functx:substring-after-last($data//xpath/text(), '/')
                                          else
                                          $data//xpath/text() (:|| '[' || $index || ']':)


let $quote := "&amp;quote;"

let $originalXMLNode := util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')/" || $xpath2Node)


let $oldValueTxt := data($originalXMLNode)

let $originalXMLNodeWithoutAttribute :=
            if(contains($xpathWithPrefix, '/@'))
                then 
                util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
                 ||$docId ||"')/"  || functx:substring-before-last($xpathWithPrefix, '/') )
             else (util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')/"  || $xpathWithPrefix ))




let $log := console:log($originalXMLNodeWithoutAttribute)

let $upateData :=
        switch ($contentType)

         case "textNodeAndAttribute" return
                (
(:                Chech if node exists in resource:)
                    (if( exists($originalXMLNode)) then
                            update value $originalXMLNode with $updatedData (:Update attribute:)
        (:                    update value $originalXMLNode with $updatedData:)


                            else
                                (update insert attribute ref { functx:trim($data//value/node()) } into $originalXMLNodeWithoutAttribute,
                                update value $originalXMLNode with $updatedData)
                    ),
       
                update value $originalXMLNodeWithoutAttribute with $updatedDataTextValue (:Updating text node:)
                )
         case "attribute" return
                if(count($conceptUris) >= count($originalXMLNode)) then
                    (
                        for $element at $pos in $conceptUris
                            return 
                                if(exists($originalXMLNode[position() = $pos]))
                                    then update replace $originalXMLNode[position() = $pos] with $newElements/node()[position() = $pos]
                                    else update insert $newElements/node()[position() = $pos] following $originalXMLNode[position() = last()]
                    )
                    else(
                        for $element at $pos in $originalXMLNode
                            return
                            (
                            if(exists($element[position() = $pos]))
                                then update replace $originalXMLNode[position() = $pos] with $newElements/node()[position() = $pos]
                                else update delete $originalXMLNode[position() = $pos]
                            )
                    )
         case "text"  return
                    (
                    (:spatiumStructor:logEvent("logs-debug", "test", $resourceURI, (), "avant: $originalXMLNodeWithoutAttribute/text(): " || serialize($originalXMLNodeWithoutAttribute || " data($updatedData/text()" || data($updatedData/text()), ())
                ),:)
                
                  if ($updatedData = " ") then update value $originalXMLNodeWithoutAttribute/text() with data($updatedData)
                
                else update value $originalXMLNodeWithoutAttribute with data($updatedData/text())
                
                
                
(:                update value $originalXMLNodeWithoutAttribute/text() with data($updatedData):)
                )
(:                        update replace $originalXMLNode with functx:change-element-ns-deep($newElement, "http://www.tei-c.org/ns/1.0", "")/node():)
         case "nodes" return
                update value $originalXMLNode with $updatedData/node()
         default return

                update replace $originalXMLNode with functx:change-element-ns-deep($newElements, "http://www.tei-c.org/ns/1.0", "")
(:            update replace $originalXMLNode/node() with $updatedData/node():)


let $newContent := util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')" )

let $elementNickname4update := if($elementNode/ancestor::teiElement[1]/fieldType/text() = "group")
    then $elementNode/ancestor::teiElement[1]/nm/text()
    else $elementNickname

let $index4Update:= if($elementNode/ancestor::teiElement[1]/fieldType/text() = "group")
    then ()
    else 
    (
    if( $index="") then "1" else $index)
let $xpath4Update:= if($elementNode/ancestor::teiElement[1]/fieldType/text() = "group")
                                                    then $elementNode/ancestor::teiElement[1]/xpath/text()
                                                    else 
                                                    (
                                                    )

let $updatedElement := 
        if($elementNode/ancestor::teiElement[1]/fieldType/text() = "group")

            then teiEditor:displayGroup($elementNickname4update, $docId, $index4Update, (), $xpath4Update)
          
            else teiEditor:displayElement($elementNickname, $docId, (), ())


(:let $log := spatiumStructor:logEvent("all-logs", "place-update" ||$index, $resourceURI,
    (), "Change in " || $resourceURI ||
    "$elementNickname" || $elementNickname ||
    "$originalXMLNode: " || serialize($originalXMLNode, ()) ||
    "$updatedData: " || serialize($updatedData, ())
    || "div id: " || $elementNickname || "_group_" || $index
    )
:)


    return
    (response:set-header("Connection", "Close"),
    <data>{$data}
    
    <conceptUris>{$conceptUris}</conceptUris>
    <newElements>{ $newElements }</newElements>
    <originalNode>{ ""(: $originalXMLNode :) }</originalNode>
    <newElement1>{ $newElements/node()[position() = 1] }</newElement1>
    <specificAttributeName>{ $specificAttributeName }</specificAttributeName>
    <specificAttributeValue>{ $specificAttributeValue }</specificAttributeValue>
    <originalNodePath></originalNodePath>
             <targetnode>{ $targetNode }</targetnode>
             <xpathEnd>{ $xpathEnd }</xpathEnd>
             <xpathEndNoSelector>{$xpathEndNoSelector}</xpathEndNoSelector>
             <xpathEndSelector>{$xpathEndSelector}</xpathEndSelector>
    <oldContent>{ $oldValueTxt }</oldContent>
    <newContent>{ $newContent }</newContent>
    <updatedElement>{ $updatedElement }</updatedElement>
    <newValue2Display>{ $updatedDataTextValue }</newValue2Display>
    <elementIdToReplace>{$elementNickname}_group_1</elementIdToReplace>

</data>
)

};

declare function teiEditor:msFragManager($docId as xs:string){
(:~
: This function manages the msFrag elements of a document
: @author Vincent Razanajao
:)
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $textLocations := $teiDoc//tei:sourceDoc
    let $msFrags := $teiDoc//tei:msFrag
    let $msFragNumber := count($msFrags)
     return
    <div class="teiElementGroup" id="msFragManager">
        <div class="TeiElementGroupHeaderBlock">
            <span class="labelForm">{ teiEditor:label("msFragManager-title", "Fragment manager") }
                <span class="teiInfo">
                    <a title="TEI element: /tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msFrag"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                </span>
            </span>
        </div>


    <button class="btn btn-sm btn-primary pull-right" onclick="addmsFragToDoc('{ $docId }')">{ teiEditor:label("msFragManager-addFragButton", "Add fragment") }</button>
    <div style="padding: 10px 0 10px 5px;">
        {   if($msFragNumber = 0)
               then teiEditor:label("msFragManager-noMsFrag", "This TEI document has no msFrag Element")
            else if($msFragNumber = 1)
                then teiEditor:label("msFragManager-oneMsFrag", "This document is made of one piece")
            else teiEditor:label("msFragManager-severalMsFrag", "Number of fragments:") || " " || data($msFragNumber)
        }</div>
   

    
    {if($msFragNumber > 1)
        then 
            <ul>
                {for $msFrag at $index in $msFrags
                       let $museumTown := $msFrag//tei:settlement[not(ancestor-or-self::tei:altIdentifier)]/tei:placeName/text()
                       let $museumName := $msFrag//tei:repository[not(ancestor-or-self::tei:altIdentifier)]/text()
                       let $invNo := $msFrag//tei:msIdentifier/tei:idno[@type="invNo"]/text()
                            
                        return
                            <li class="sectionPanel">
                               <span class="labelForm" style="">Fragment { $index }</span>
                               
                               <span class="value">{
                               
                                if($museumTown !="") then
                               
                                    $museumTown || ", " || $museumName || ", " || $invNo
                                else teiEditor:label("msFragManager-noInfo", "There is currently no information about repository") }</span>     
                                    
                                    
                                   <button class="removeItem btn btn-xs btn-warning pull-right" style="margin: 0 1em 0 1em"
                                          onclick="removeItemFromList('{$docId}', 'msFrag', 'xmlNode', {$index}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button>
                               </li>
                 }        
                </ul>
            else()
        }
 </div>
};

declare function teiEditor:msItemManager($docId as xs:string){
(:~
: This function manages the msItem elements of a document
: @author Vincent Razanajao
:)
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $msItems := $teiDoc//tei:msItem
    let $msItemTypes := functx:substring-after-last($teiEditor:appVariables//msItemTypesThesau/text(), "/")
     return
    <div class="sectionPanel" id="msItemManager">
        <div class="TeiElementGroupHeaderBlock">
            <span class="labelForm">{ teiEditor:label("msItemManager-title", "Text units") }
                <span class="teiInfo">
                    <a title="TEI element: /tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msContents/tei:msItem"><i class="glyphicon glyphicon glyphicon-info-sign"></i></a>
                </span>
            </span>
        </div>
    
    {if(count($msItems) = 0)
        then <p style="padding: 10px 0 10px 5px;">{ teiEditor:label("msItemManager-noMsItem", "This TEI document has currently no text item") }</p> 
        
        else(
            <div style="padding: 10px 0 10px 5px;">
                <p>{ teiEditor:label("msItemManager-noMsItemNoLabelStart", "This document has ") }<strong>{ count($msItems) }</strong>{ if (count($msItems) < 2) then teiEditor:label("msItemManager-noMsItemNoLabelSing", " msItem Element")
                                            else teiEditor:label("msItemManager-noMsItemNoLabelPlural", " msItem Elements")}</p>
                <ul>
                {for $msItem at $index in $teiDoc//tei:msItem
                       let $textTypes := 
                            for $item at $itemPos in tokenize($msItem/@class, " ")
                                return 
                                   skosThesau:getLabel($item, $teiEditor:lang) || (
                                   if($itemPos < count(tokenize($msItem/@class, " ")) ) then ", " else ()
                                   )
                            order by $textTypes
                        return
                            <li class="sectionPanel">
                               <span class="">{$textTypes}</span>
                               { teiEditor:displayElement("msItemNote", $docId, $index, "")}
                                  <button class="removeItem btn btn-xs btn-warning pull-right" style="margin: 0 1em 0 1em"
                                          onclick="removeItemFromList('{$docId}', 'msItem', 'xmlNode', {$index}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button>
                               </li>
                 }        
                </ul>
              </div>
            )}
        <button class="btn btn-sm btn-primary pull-right" onclick="openDialog('dialogAddmsItem')">{ teiEditor:label("msItemManager-addMsItemButton", "Add a text") }</button>

                
  <div id="dialogAddmsItem" title="Add a msItem to a Document" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content " style="widht: 100%;">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">{ teiEditor:label("textUnits-addMsItemToDoc", "Add a msItem to Document") }</h4>
                </div>
                
                    <div class="modal-body" style="height: 70%!important">
                        <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                        {skosThesau:dropDownThesauXMLMultiple($msItemTypes, 5, 'en', 'Type', 'inline', 1, 1, ())}
                        </div>
                        </div>
                    </div>
                     <div class="modal-footer">
                          <button  id="addmsItemToDocButton" class="btn btn-primary pull-left" type="submit"
                                      onclick="addmsItemToDoc('{$msItemTypes}')">OK</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                     </div>
                
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

               </div>
                
      </div>


 </div>



  
};
declare function teiEditor:msItemManagerWithLocation($docId as xs:string){
(:~
: This function manages the msItem elements of a document, linking one msItem to a type and to a location on the document,
:i.e. a sourceDoc/surface
: @author Vincent Razanajao
:)
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $textLocations := $teiDoc//tei:sourceDoc
    let $msItems := $teiDoc//tei:msItem
    let $msItemTypes := functx:substring-after-last($teiEditor:appVariables//msItemTypesThesau/text(), "/")
    let $textLocations := functx:substring-after-last($teiEditor:appVariables//textLocationsThesau/text(), "/")
           
     return
    <div class="sectionPanel" id="msItemManager">
    <h4 class="subSectionTitle">{ teiEditor:label("textUnits-title", "Text units") }<button class="btn btn-sm btn-primary pull-right" onclick="openDialog('dialogAddmsItem')">{ teiEditor:label("textUnits-addTextButton", "Add a text") }</button></h4>
    
    {if(count($msItems) = 0)
        then <p>{ teiEditor:label("textUnits-noTextUnit", "This TEI document has currently no text item") }</p> 
        
        else(
            <div>
                <p><strong>{ count($msItems) } msItem </strong> Element{ if (count($msItems) > 1) then "s" else ""}</p>
                <ul>
                {for $msItem at $index in $teiDoc//tei:msItem
                       let $textTypes := 
                                for $item at $itemPos in tokenize($msItem/@class, " ")
                                
                                return 
                                   skosThesau:getLabel($item, $teiEditor:lang) || (
                                   if($itemPos < count(tokenize($msItem/@class, " ")) ) then ", " else ()
                                   )
                        let $targetLocationTypes :=
                                for $item at $itemPos in tokenize($msItem/tei:locus/@target, " ")
                                return
                                (if( $item != "") then 
                                skosThesau:getLabelFromXmlValue(data($textLocations//tei:surface[@xml:id = substring-after($item, '#')]/@ana), "fr")
                                 || (
                                   if($itemPos < count(tokenize($msItem/tei:locus/@target, " ")) ) then ", " else ()
                                   )
                                else ()
                                )
                        return
                            <li class="sectionPanel">
                               <span class="">{$textTypes}</span>
                                    <span class=""> [{ $targetLocationTypes }]
                                    
                                    {""(:teiEditor:displayTeiElement('msItemModernTitle', $index, 'input', ()):)}
                                    {""(:teiEditor:displayElement('textMainLang', (), 1, ()):)}
                                    {""(:teiEditor:displayElement('textOtherLangs', (), $index, ()):)}
                                   {""(:teiEditor:displayTeiElement('msItemRefToCanonical', $index, 'input', ()):)}
                                   </span>
                                   <button class="removeItem btn btn-xs btn-warning pull-right" style="margin: 0 1em 0 1em"
                                          onclick="removeItemFromList('{$docId}', 'msItem', 'xmlNode', {$index}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button>
                               </li>
                 }        
                </ul>
              </div>
            )}
                
  <div id="dialogAddmsItem" title="Add a msItem to a Document" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content ">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">{ teiEditor:label("textUnits-addmsItemToDoc", "Add a msItem to Document") }</h4>
                </div>
                
                    <div class="modal-body" style="height: 70%!important">
                        <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                        {skosThesau:dropDownThesauXMLMultiple($msItemTypes, 5, 'en', 'Type', 'inline', 1, 1, ())}
                        
                        <br/>
                        {skosThesau:dropDownThesauXMLMultiple($textLocations, 5, 'en', teiEditor:label("textUnits-location", "Location"), 'inline', 1, 1, ())}
                        </div>
                        </div>
                    </div>
                     <div class="modal-footer">
                          <button  id="addmsItemToDocButton" class="btn btn-primary pull-left" type="submit"
                                      onclick="addmsItemWithLocationToDoc()">OK</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                     </div>
                
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

               </div>
                
      </div>


 </div>



  
};

declare function teiEditor:peopleManagerEgypt($docId as xs:string){
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $people:= $teiDoc//tei:listPerson[@type="peopleInDocument"]
    let $owner := $people//tei:person[@role="owner"] 
    let $royalPersonsTopConceptId := $teiEditor:appVariables//royalPersonsTopConceptId/text()
            => functx:substring-after-last("/")
   let $script4Datatable:= ""
    
    return
        
    <div id="peopleManager" class="teiElementGroup sectionPanel" style="border: solid 1px;" type="peopleManagerEgypt">
            <div class="TeiElementGroupHeaderBlock">
                <span class="labelForm">{ teiEditor:label("peopleManager-title", "People Manager") }</span>
            </div>
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div id="peoplePanel" class="teiElementGroup">
                        <div class="TeiElementGroupHeaderBlock"><span class="labelForm">{ teiEditor:label("peopleManager-owner", "Owner") }</span>
                        </div>
                        { teiEditor:listPeopleManager($docId) }
                        <div>
                
                            <h4>{ teiEditor:label("peopleManager-addaPerson", "Add a person") }</h4>
                                <button class="btn btn-primary" style="margin: 1px;" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'FatherOf')">{ teiEditor:label("peopleManager-father", "Father") }</button>
                                <button class="btn btn-primary" style="margin: 1px;" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'MotherOf')">{ teiEditor:label("peopleManager-mother", "Mother") }</button>
                                <button class="btn btn-primary" style="margin: 1px;" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'SeriousIntimateRelationShipWith')">{ teiEditor:label("peopleManager-spouse", "Spouse") }</button>
                                <button class="btn btn-primary" style="margin: 1px;" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'ChildOf')">{ teiEditor:label("peopleManager-child", "Child") }</button>
                                <button class="btn btn-primary" style="margin: 1px;" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'AncestorOf')">{ teiEditor:label("peopleManager-ancestor", "Ancestor") }</button>
                                <button class="btn btn-primary" style="margin: 1px;" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'DescendentOf')">{ teiEditor:label("peopleManager-descendent", "Descendent") }</button>
                                <button class="btn btn-primary" style="margin: 1px;" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'Bond')">{ teiEditor:label("peopleManager-bond", "Bond") }</button>                
                                <button class="btn btn-primary" style="margin: 1px;" onclick="openDialog('dialogAddRoyalPersonToDocument')">{ teiEditor:label("peopleManager-royalPerson", "Royal person") }</button>                
                        </div>
                    </div>
                </div>
               
                <div class="col-xs-7 col-sm-7 col-md-7">
                    <div id="personRecord" />
                <div>
            </div>
        </div>


</div>
    <script>
          { $script4Datatable }

    </script>
<div id="dialogAddPersonToDocument" title="Add a Person to a Resource" class="modal fade" tabindex="-1" style="display: none;">
            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">{ teiEditor:label("peopleManager-addaPerson", "Link a Person to Document") }</h4>
                </div>
                <div class="modal-body">
                      <!--<form id="addPersonForm" class="addPersonForm" role="form" data-toggle="validator" novalidate="true">
                      -->
                      <div id="actionGroup">
                      <h3><button id="selectPersonButton" onclick="collapseSelectPerson()" class="btn btn-primary" type="button" >{ teiEditor:label("selectButton", "Select") }</button>
                      { teiEditor:label("or", "or") } <button id="createPersonButton" onclick="collapseCreatePerson()" class="btn dropdown btn-primary" type="button">{ teiEditor:label("createButton", "Create") }</button> { teiEditor:label("peopleManager-aRelatedPerson", "a related person") }</h3>
                      <div class="accordion-group">
                            <div class="collapse accordion" data-parent="#accordion-group" id="selectPersonPanel">
                                <div class="card card-body">
                                    <h4>{ teiEditor:label("peopleManager-selectAlreadyExisting", "Select a person already in database") }</h4>
                                          
                                      { teiEditor:listPeopleAsTable() }
                                     
                                      <div id="projectPeopleDetailsPreview" class=""/>
                                      <input id="currentDocumentUri" type="text" class="hidden"/>
                                      <input id="selectedPeopleUri" type="text" class="hidden valueField"/>
                                      <!--<input id="selectedPersonDedicator" type="checkbox" class="valueField" value="dedicator">Dedicator</input>-->
                                      <br/>
                               </div>
                               </div>
                      <div class="collapse accordion" data-parent="#accordion-group" id="createNewPersonPanel">
                            <div class="card card-body">
                      
                                 <h4 >{ teiEditor:label("peopleManager-createNewRecord", "Create a new person record") }</h4>
                                 <div class="form-group">
                                      <label for="newPersonStandardizedNameFr">{ teiEditor:label("peopleManager-standardNameFrench", "Standardized name (in French)") }</label>
                                      <input type="text" class="form-control valueField" id="newPersonStandardizedNameFr" name="newPersonStandardizedNameFr"
                                      data-error=""/>
                                 </div>
                                 <div class="form-group">
                                    <label for="newPersonStandardizedNameEn">{ teiEditor:label("peopleManager-standardNameEnglish", "Standardized name (in English)") }</label>
                                       <input type="text" class="form-control valueField" id="newPersonStandardizedNameEn" name="newPersonStandardizedNameEn"
                                    data-error=""/>
                                 </div>
                                 
                                <div class="form-group">
                                    <label for="newPersonTranslitteredName">{ teiEditor:label("peopleManager-translitName", "Name in translitteration") }</label>
                                    <input type="text" class="form-control valueField" id="newPersonTranslitteredName" name="newPersonTranslitteredName"
                                    data-error=""/>
                                </div>
                                {   let $sex := if($teiEditor:appVariables//sexConceptUri/text() != "") then
                                        functx:substring-after-last($teiEditor:appVariables//sexConceptUri/text(), "/")
                                        else "c23490"
                                    return
                                    skosThesau:dropDownThesauForElement("sex", $sex, $teiEditor:lang, teiEditor:label("sex", "Sex"), 'row', (), (), "uri")}
                                <br/>        
                                
                            </div>
                        </div>
                        </div>
                            </div>
                            <div id="person2AddDedicatorContainer" class="hidden">
                                <input id="person2AddDedicator" type="checkbox" class="valueField" value="dedicator">{ teiEditor:label("peopleManager-dedicator", "Dedicator") }</input>
                            </div>
                        
                            <input id="person2AddType" class="valueField hidden"/>
                            <input id="person2AddOwner" class="valueField hidden"/>
                            <input id="person2AddBondType" class="valueField hidden"/>
                            
                        <div class="modal-footer">
                        
                        <button  id="addExistingPersonToDocButton" class="btn btn-primary pull-left  hidden" type="submit" onclick="addProjectPersonToDocWithRole()">{ teiEditor:label("peopleManager-selectAndAddToDoc", "Select and add to document") }</button>

                        <button  id="createAndAddPersonToDocButton" class="btn btn-primary pull-left  hidden" type="submit" onclick="createAndAddPersonToDoc('{ $teiEditor:docId }')">{ teiEditor:label("peopleManager-createAndAddToDoc", "Create and add to document") }</button>
                        
                        <button type="button" class="btn btn-default" data-dismiss="modal">{ teiEditor:label("cancelButton", "Cancel") }</button>
                    </div>
                  <!--</form>-->
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>

            </div>


    </div>

<div id="dialogAddRoyalPersonToDocument" title="Add a Roayal Person to a Resource" class="modal fade" tabindex="-1" style="display: none;">
  <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">{ teiEditor:label("peopleManager-addaRoyalPerson", "Link a Royal Person to Document") }</h4>
                </div>
                <div class="modal-body">
                    
                    {skosThesau:dropDownThesauForElement("royalPerson", $royalPersonsTopConceptId, $teiEditor:lang, teiEditor:label("peopleManager-royalPerson", "Royal person"), 'inline', 
                                                            (), 9999, "uri")}
                                                    
                    <br/>        
                                
                        
                    <input id="person2AddOwner" class="valueField hidden"/>
                    <input id="person2AddBondType" class="valueField hidden"/>
                            
                    <button  id="addRoyalPersonToDocButton" class="btn btn-primary pull-left" type="submit" onclick="addRoyalPersonToDoc()">OK</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{ teiEditor:label("cancelButton", "Cancel") }</button>
                    </div>
                </div>


    </div>
</div>
};

declare function teiEditor:listPeopleManager($docId as xs:string){
    let $peopleCollection := collection($teiEditor:data-repository-path || "/people/")
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $people:= $teiDoc//tei:listPerson[@type="peopleInDocument"]
    let $owner := $people//tei:person[@role="owner"] 
    let $otherPeople:= $people//tei:person[not(@role="owner")][contains(./@corresp, "people")]
    let $royalPeople:= $people//tei:person[not(@role="owner")][contains(./@corresp, "concept")]

(:    let $temp := $teiEditor:peopleCollection//lawd:person[@rdf:about= data($people//tei:person[@role="owner"]/@corresp)]:)
    let $ownerRecord := if($owner[1]/@corresp != "") then $peopleCollection//lawd:person[@rdf:about= data($owner[1]/@corresp) || "#this"] else()
    (: let $ownerRecord := if($owner[1]/@corresp != "") then $teiEditor:peopleCollection//lawd:person[@rdf:about= data($owner[1]/@corresp) || "#this"] else() :)
    let $relationshipsLabels :=
                <labels>
                        <owner>
                        <label xml:lang="en">Owner</label>
                        <label xml:lang="fr">Propriétaire</label>
                        </owner>
                        <bond type="FatherOf" reverse="SonOf"  sex="male">
                            <label xml:lang="en">Father</label>
                            <label xml:lang="fr">Père</label>
                            </bond>
                         <bond type="MotherOf" reverse="ChildOf" sex="female">
                            <label xml:lang="en">Mother</label>
                            <label xml:lang="fr">Mère</label>
                            </bond>
                        <bond type="ChildOf" sex="male" reverse="FatherOf">
                            <label xml:lang="en">Son</label>
                            <label xml:lang="fr">Fils</label></bond>
                        <bond type="ChildOf" sex="female" reverse="FatherOf"><label xml:lang="en">Daughter</label>
                            <label xml:lang="fr">Fille</label></bond>
                            
                        <bond type="ChildOf" sex="male" reverse="MotherOf">
                            <label xml:lang="en">Child</label>
                            <label xml:lang="fr">Fils</label></bond>
                        <bond type="ChildOf" sex="female" reverse="MotherOf">
                            <label xml:lang="en">Daughter</label>
                            <label xml:lang="fr">Fille</label></bond>
                            
                            
                            
                        <bond type="SonOf" sex="male" reverse="FatherOf" >
                            <label xml:lang="en">Son</label>
                            <label xml:lang="fr">Fils</label></bond>
                        
                        <bond type="DaughterOf" sex="female" reverse="FatherOf">
                            <label xml:lang="en">Daughter</label>
                            <label xml:lang="fr">Fille</label></bond>

                        <bond type="AncestorOf" reverse="DescendentOf">
                            <label xml:lang="en">Ancestor</label>
                            <label xml:lang="fr">Ancêtre</label></bond>
                            
                        <bond type="SeriousIntimateRelationShipWith" sex="male" reverse="SeriousIntimateRelationShipWith">
                            <label xml:lang="en">Spouse</label>
                            <label xml:lang="fr">Époux</label></bond>
                        <bond type="SeriousIntimateRelationShipWith" sex="female" reverse="SeriousIntimateRelationShipWith"><label xml:lang="en">Spouse</label>
                            <label xml:lang="fr">Épouse</label></bond>
                        <bond type="Bond" reverse="Bond">
                            <label xml:lang="en">Bond</label>
                            <label xml:lang="fr">Relation</label></bond>                            
                </labels>
                return
<div id="listPeopleManager">
{if(count($owner) = 1) then
    <span style="margin: 1em 0 0.5em 0;"><strong>{ $owner//tei:persName[1]/text() }</strong>
        <a href="{ substring-after(data($owner/@corresp), $teiEditor:baseUri) }/edit" data-personuri="{ data($owner/@corresp) }" data-id="{ substring-after($owner/@corresp, 'people/') }" title="{ data($owner/@corresp) }" target="personRecord" class="personLinkTEMP"><i class="glyphicon glyphicon-new-window"/></a><br/> [<a href="{ substring-after(data($owner/@corresp), $teiEditor:baseUri) }" data-personuri="{ data($owner/@corresp) }" data-id="{ substring-after($owner/@corresp, 'people/') }" title="{ data($owner/@corresp) }" target="personRecord" class="personLinkTEMP">{ data($owner/@corresp) }</a>]
<button class="removeItem btn btn-xs btn-warning" style="margin: 0 1em 0 2em"
    onclick="removeItemFromList('{$docId}', 'peopleManagerEgypt', '{ data($owner/@corresp) }', 1, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button>
                                                </span>
                    else if(count($owner) > 1)
                        then <ul>
                                {for $owner at $index in $owner
                                return
                                <li style="margin: 1em 0 0.5em 0;">
                                    <span>
                                        <strong>{ $owner//tei:persName[1]/text() }</strong>
                                        <a href="{ substring-after(data($owner//apc:people/@rdf:about), $teiEditor:baseUri) }/edit" title="{ data($owner//apc:people/@rdf:about) }" target="personRecord" class="personLinkTEMP"><i class="glyphicon glyphicon-new-window"/></a><br/>[<a href="{ substring-after(data($owner//apc:people/@rdf:about), $teiEditor:baseUri)  }/edit" title="{ data($owner//apc:people/@rdf:about) }" target="personRecord" class="personLinkTEMP">{data($owner/@corresp)}</a>]</span>
                                <button class="removeItem btn btn-xs btn-warning" style="margin: 0 1em 0 2em"
                                                onclick="removeItemFromList('{$docId}', 'peopleManagerEgypt', '{ data($owner//apc:people/@rdf:about) }', {$index}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button>
                                            </li>}
                            </ul>
                    else 
                        <button onclick="openAddPersonToDocDialog('owner', '')">{ teiEditor:label("peopleManager-addOwner", "Add owner") }</button>
                }
    <br/>
                {if(count($owner) >0) 
                    then (<div>
                        {
                        if(count($otherPeople) = 0) then teiEditor:label("peopleManager-noOtherPerson", "There is not any other person linked to this document")
                        
                        else 
                            <div style="margin: 1em 0 0.5em 0;">
                                <div class="TeiElementGroupHeaderBlock" ><span class="labelForm">{ teiEditor:label("peopleManager-otherPeople", "Other people") }</span>
                                </div>
                                <ul>
                                    { for $person at $index in $otherPeople
                                        let $relatedPersonUri := $person/@corresp
                                        
                                        let $relatedPersonDetails := if(exists($teiEditor:peopleCollection//lawd:person[@rdf:about= $relatedPersonUri || "#this"]))
                                                                        then $teiEditor:peopleCollection//lawd:person[@rdf:about= $relatedPersonUri || "#this"]
                                                                        else util:eval( "collection('" || $teiEditor:data-repository-path || "/people/')//lawd:person[@rdf:about='" || $relatedPersonUri || "#this']")
                                    

                                        let $personSex := switch(lower-case($relatedPersonDetails//apc:sex/text()))
                                                case "homme" case "male" return "male"
                                                case "femme" case "female" return "female"
                                                default return ""
                                        let $bondType := data($relatedPersonDetails//snap:hasBond[@rdf:resource = $owner/@corresp]/@rdf:type)
                                        let $bond2Display := if(exists($relationshipsLabels//bond[@type = $bondType][@sex = $personSex][1]))
                                                        then $relationshipsLabels//bond[@type = $bondType][@sex = $personSex][1]//label[@xml:lang=$teiEditor:lang]/text()
                                                        else $relationshipsLabels//bond[@type = $bondType][1]//label[@xml:lang=$teiEditor:lang]/text()
                                        let $dedicator := if($person/@role ="dedicator") then " [" || teiEditor:label("prosopo-dedicator", "dedicator") || "]" else ""
                                            return
                                            <li style="margin: 0 0 0.5em 0;">
                                            { if($bond2Display!="") then "" || $bond2Display || " : " else () }
                                            { $relatedPersonDetails//lawd:personalName[1]/text() } <a href="{ substring-after(data($relatedPersonDetails//apc:people/@rdf:about), $teiEditor:baseUri) }/edit" title="{ data($relatedPersonDetails//apc:people/@rdf:about) }" target="_about"><i class="glyphicon glyphicon-new-window"/></a>
                                            [<a href="{ substring-after(data($relatedPersonDetails//apc:people/@rdf:about), $teiEditor:baseUri) }/edit" title="{ data($relatedPersonDetails//apc:people/@rdf:about) }" target="_about">{ data($relatedPersonUri) }</a>]
                                                { $dedicator }
                                                <button class="removeItem btn btn-xs btn-warning" style="margin: 0 1em 0 2em"
                                                onclick="removeItemFromList('{$docId}', 'peopleManagerEgypt', '{ $relatedPersonUri }', {$index}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button> 
                                            </li>
                                    }
                                </ul>   
                            </div>
                        }
                        {
                            if(count($royalPeople) > 0)
                            then
                            <div style="margin: 1em 0 0.5em 0;">
                                <div class="TeiElementGroupHeaderBlock" ><span class="labelForm">{ teiEditor:label("peopleManager-royalPeople", "Royal person(s)") }</span>
                                </div>
                                <ul>
                                    { for $person at $index in $royalPeople
                                        let $royalPersonUri := $person/@corresp
                                        let $name := skosThesau:getLabel($royalPersonUri, $teiEditor:lang)
                                        return
                                            <li style="margin: 0 0 0.5em 0;">
                                        
                                            { $name } <a href="{ $royalPersonUri }" target="_about"><i class="glyphicon glyphicon-new-window"/></a>
                                                <button class="removeItem btn btn-xs btn-warning" style="margin: 0 1em 0 2em"
                                                onclick="removeItemFromList('{$docId}', 'peopleManagerEgypt', '{ $royalPersonUri }', {$index}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button> 
                                            </li>
                                    }
                                </ul>   
                            </div>
                            else ()
                        }
                              
                    </div>)
                    else()}
                    
                    <script>
                    $(".personLink").on("click", function(){{
                        
                        $("#personRecord").load("/people/get-record-edit/" + $(this).data("id"))
                    }})
                    </script>
    </div>
};
declare function teiEditor:listPeopleAsTable(){
(:    let $people := $prosopoManager:peopleCollection//lawd:person:)
    
    
    let $complete := concat ('
        $( function() {
                      $.widget( "custom.catcomplete", $.ui.autocomplete, {
                      //$("#search").autocomplete({
                      
                    _create: function() {
                      this._super();
                      
                      
                      this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
                    },
                    _renderMenu: function( ul, items ) {
                      var that = this,
                        currentCategory = "";
                      $.each( items, function( index, item ) {
                        var li;
                        if ( item.category != currentCategory ) {
                          ul.append( "<li class=\"ui-autocomplete-category\">" + item.category + "</li>" );
                          currentCategory = item.category;
                        }
                        li = that._renderItemData( ul, item );
                        if ( item.category ) {
                          li.attr( "aria-label", item.category + " : " + item.label );
                        }
                      });
                    }
                  });
                  
                  var data = [', '
                    
                    
                  ];
               
                  $( "#search" ).catcomplete({
                    delay: 0,
                    source: data
                  });
     } );')
  let $datatableDom := '"dom": "f"'
return
    <div id="peopleListDiv">
        <style>
            .dataTables_wrapper .dataTables_filter {{
                float:left!important;
            }}
            .dataTables_wrapper .mylength .dataTables_length {{
                float:right
            }}
            table.dataTable tbody tr.selected {{
            color: white !important;
            background-color: #7d1d20 !important;
                }}
        </style>
    <table id="peopleList4Selection"  class="display compact" style="width: 100%">
     <thead>
        <tr>
            <td>ID</td>
            <td class="sortingActive">{teiEditor:label("name", "Name")}</td>
            <td class="hidden">TM</td>
            <td>{teiEditor:label("peopleManager-father", "Father")}</td>
            <td>{teiEditor:label("peopleManager-mother", "Mother")}</td>
            <td>URI</td>
        </tr>
        </thead>
        <tbody>
                 {
  for $person in $teiEditor:peopleCollection//lawd:person
                let $name := $person//lawd:personalName[1]/text() 
                        || (if($person//lawd:personalName[@type="nickname"]) 
                            then " (" || $person//lawd:personalName[@type="nickname"][1]/text() || ")" else ())
                let $uri := data($person/@rdf:about)
                let $uriShort :=substring-before($uri, "#")
                let $id := substring-before(functx:substring-after-last($uri, "/"), "#")
                let $exactMacthes := string-join($person//skos:exactMatch/@rdf:resource, " ")
                let $parents := <parents>{
                    for $parent in $person//snap:hasBond[@rdf:type = "ChildOf"]
                      let $parentUri:=data($parent/@rdf:resource)
                      let $parentRecord := $teiEditor:peopleCollection//lawd:person[@rdf:about=$parentUri || "#this"]
                      return 
                      <parent sex="{ $parentRecord//apc:sex/text() }">{ $parentRecord//lawd:personalName[1]/text()}</parent>
                    }</parents>


                order by $name 
            return
                (<tr>
                    <td>{ $id }</td>
                    <td>{$name}</td>
                    <td>{$exactMacthes}</td>
                    <td>{ $parents//parent[contains(./@sex, ("Male", "Homme"))]/text()}</td>
                    <td>{ $parents//parent[contains(./@sex, ("Female", "Femme"))]/text()}</td>
                    <td>{ $uri }</td>
                </tr>)
    }
  
         </tbody>
      </table>
      
      
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.5.0/css/select.dataTables.min.css"/>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"/>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap.min.js"/>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/select/1.5.0/js/dataTables.select.min.js"/>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedcolumns/4.2.1/js/dataTables.fixedColumns.min.js"/>
    
    <script type="text/javascript">$(document).ready( function () {{
            var table = $('#peopleList4Selection').DataTable({{
            <![CDATA["dom": '<f"top">']]>,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": false,
            "select": true,
            "scrollY": '200px',
            "fixedColumns": true,
            "scrollCollapse": true,
                        //"ajax": '/people/json',
                        
                                "columnDefs": [
                                                   {{ "targets": [0],
                                                   "width": "5%"
                                                   }},             
                                                   {{ "targets": [1]
                                                   }},
                                                   {{
                                                       "targets": [ 2 ],
                                                       "visible": false
                                                   }},
                                                   {{
                                                       "targets": [ 3 ],
                                                       "visible": true
                                                   }},
                                                   {{
                                                       "targets": [ 4 ],
                                                       "visible": true
                                                   }},
                                                    {{
                                                       "targets": [ 5 ],
                                                       "visible": false
                                                   }}
                                                ],
                                                "language": {{
                                            "search": "Filter names:"
                                                }}
                            }});
                        table
                                .on( 'select', function ( e, dt, type, indexes ) {{
                                    var rowData = table.rows( indexes ).data().toArray()[0];
                                    console.log(rowData);
                                    console.log(rowData[5]);
                                    $("#selectedPeopleUri").val(rowData[5]);
                                    $("#projectPeopleDetailsPreview").html("<strong>"+ rowData[1] + "</strong> <em>" + rowData[0] +"</em>");
                                     if ($('#addProjectPeopleButtonDocPlaces').hasClass('hidden') === true) {{
                                        $("#addProjectPeopleButtonDocPlaces").toggleClass("hidden");
                                     }}
                                     else{{}};
                                    
                                }} )
                                .on( 'deselect', function ( e, dt, type, indexes ) {{
                                    var rowData = table.rows( indexes ).data().toArray();
                                    $("#selectedPeopleUri").val("");
                                    $("#projectPeopleDetailsPreview").html("<strong>"+ "" + "</strong> <em>" + "" +"</em>");
                                     if ($('#addProjectPeopleButtonDocPlaces').hasClass('hidden') === true) {{
                                        $("#addProjectPeopleButtonDocPlaces").toggleClass("hidden");
                                     }}
                                     else{{}};
                                }} );


                        }} );</script>

                    <script type="text/javascript">
                        function selectPerson(id, name){{

                      
                            $("#selectedPeopleUri").val(id);
                            $("#projectPeopleDetailsPreview").html("<strong>"+ name + "</strong> <em>" +id +"</em>");
                              if ($('#addProjectPeopleButtonDocPlaces').hasClass('hidden') === true) {{
                                  $("#addProjectPeopleButtonDocPlaces").toggleClass("hidden");
                            }} else {{}};
                     }};</script>
  
    
   </div>
};

declare function teiEditor:IIIFViewver($imageUrl as xs:string){
   <div>
    <div id="openseadragon1" style="width: 800px; height: 600px;"/>
    <script src="/openseadragon/openseadragon.min.js"></script>
    <script type="text/javascript">
        var _viewer = OpenSeadragon({{
            element: document.getElementById("contentDiv"),
            loadTilesWithAjax: true,
            tileSources: "https://api.nakala.fr/iiif/10.34847/nkl.3022g59v.v1/bd3c6352dc6d81fce7c5b05b8d49f02e2638f5d5/info.json?bearer=59ec7f4f6fd3b590edc8c27847260102614c42192481e00469fa0878ee2a2b06",
            //homeFillsViewer: true,
            //showZoomControl: false,
            showRotationControl: true,
            gestureSettingsTouch: {{
              pinchRotate: true
            }},
            showHomeControl: false,
            showFullPageControl: true,
            constrainDuringPan: true,
            visibilityRatio: 1,
            prefixUrl: "https://api.nakala.fr/viewers/osd/images/",
            minZoomImageRatio: 1,
            ajaxHeaders: {{
                "Authorization": "Bearer 59ec7f4f6fd3b590edc8c27847260102614c42192481e00469fa0878ee2a2b06"
            }}
        }});
</script>
</div>
};


declare function teiEditor:labelForXMLElement($elementNode as node()){
                     
       ( if($elementNode//formLabel[@xml:lang=$teiEditor:lang][1]/text()!= "") 
            then $elementNode//formLabel[@xml:lang=$teiEditor:lang][1]/text()
        else if($elementNode//formLabel[@xml:lang="en"][1]/text()!= "") 
            then $elementNode//formLabel[@xml:lang="en"][1]/text()
        else if($elementNode//formLabel[1]/text() != "")
            then  $elementNode//formLabel[1]/text()
        else "No label found in element definition")

};

declare function teiEditor:label($key as xs:string, $defaultLabel as xs:string){
    if($teiEditor:customLabels//label[@key=$key]//msg[@xml:lang=$teiEditor:lang])
        then $teiEditor:customLabels//label[@key=$key]//msg[@xml:lang=$teiEditor:lang]
        else if($teiEditor:labels//label[@key=$key]//msg[@xml:lang=$teiEditor:lang])
            then $teiEditor:labels//label[@key=$key]//msg[@xml:lang=$teiEditor:lang]
        else $defaultLabel
};

declare function teiEditor:addMissingParagraphTags($tags as node()){
    let $xsl := doc("/db/apps/ausohnum-library/xslt/addMissingParagraphTags.xsl")
    let $cleanTags := transform:transform($tags, $xsl, ())
    return $cleanTags
};

declare function teiEditor:encodeHtmlToTeiFormat($htmlTags as node()){
    let $xsl := doc("/db/apps/ausohnum-library/xslt/htmlToTeiTags.xsl")
    let $teiTags := transform:transform($htmlTags, $xsl, ())
    return $teiTags
};

(: Cleaner alternative than functx:change-element-ns-deep($element, "", "") to remove empty namespaces :)
declare function teiEditor:cleanEmptyNodesAndNamespaces($tags as node()){
    let $xsl := doc("/db/apps/ausohnum-library/xslt/cleanEmptyNodesAndNamespaces.xsl")
    let $cleanTags := transform:transform($tags, $xsl, ())
    return $cleanTags
};

declare function teiEditor:getCleanedTEIFile($docId as xs:string, $project as xs:string){
   let $now := fn:current-dateTime()
   let $xsltCleanNamespace:= doc("/db/apps/ausohnum-library/xslt/removeExtraNamespace.xsl")
   let $xslt:= doc("/db/apps/ausohnum-library/xslt/cleanTeiFile.xsl")

      
   let $teiFile := collection("/db/apps/" || $project || "Data/documents")/id($docId) 
   let $params := 
   <parameters>
      <param name="output.omit-xml-declaration" value="no"/>
      <param name="output.indent" value="yes"/>
      <param name="output.media-type" value="application/xml"/>
      <param name="output.method" value="xml"/>
   </parameters>
   let $paramMap := 
         map {"method": "xml", "indent": false(), "item-separator": "", "version": "1.0"}
   let $serialization-options := 'method=xml media-type=text/xml omit-xml-declaration=no indent=yes'
   let $removeExtraNamespace:=    transform:transform($teiFile, $xsltCleanNamespace, (), (), $serialization-options)
   let $fileContent := transform:transform($removeExtraNamespace, $xslt, (), (), $serialization-options)
   let $filename := $project || "-doc-" || $docId || "-"|| substring($now, 1, 10) || ".xml"
      
      
return
   response:stream-binary(util:string-to-binary(serialize($fileContent)), "application/zip", $filename)

};

declare function teiEditor:datingManager($docId as xs:string){
    let $origDates:= $teiEditor:teiDoc//tei:origDate

    return



<div class="sectionPanel" id="origDateManager">
    <h4 class="subSectionTitle">{ teiEditor:label("datingManager-title", "Datings") }<button class="btn btn-sm btn-primary pull-right" onclick="openDialog('dialogAddmsItem')">{ teiEditor:label("textUnits-addTextButton", "Add a text") }</button></h4>
    
    {if(count($origDates) = 0)
        then <p>{ teiEditor:label("datingManager-title-noDate", "This TEI document has currently no origDate item") }</p> 
        
        else(
            <div>
                <p><strong>{ count($origDates) } origDate </strong> Element{ if (count($origDates) > 1) then "s" else ""}</p>
                <ul>
                {for $origDate at $index in $origDates
                return 
                <li>
                 {teiEditor:displayElement("origDatePeriod", $docId, $index, "")}
                 
                </li>}
                </ul>
            </div>)
    }
        </div>





      

};
declare function teiEditor:datingFromThesaurus($docId as xs:string, $origDateElementNickname as xs:string){
    let $docId := if($docId !="") then $docId else $teiEditor:docId
    let $teiDoc := if($docId !="") then 
        let $project := request:get-parameter('project', ())
        return 
        collection('/db/apps/' || $project ||'Data/documents')/id($docId)
        else $teiEditor:teiDoc 
    return
        <div id="absDatingPanel" class="panel panel-default">
        <div class="panel-title">
            { teiEditor:label("absDatingPanel-title", "Absolute dates checking")}
            <button class="btn btn-primary" title="Reload panel" onclick="absDatingReloadPanel('{ $docId }', '{ $origDateElementNickname }')">{ teiEditor:label("datingManager-retrievePanel", "Reload") }<i class="glyphicon glyphicon-refresh"/></button>

        </div>
            <div class="panel-body">
            {
            for $origDate at $index in $teiDoc//tei:origDate
                let $notBefore:= $origDate/@notBefore-custom
                let $notAfter:= $origDate/@notAfter-custom
                let $datingConcepts:= 
                    for $concept in tokenize($origDate/@period, " ")
                        return skosThesau:getSkosConcept($concept)
                let $startDateFromConcepts:= min((for $date in $datingConcepts//periodo:earliestYear return $date))
                let $endDateFromConcepts:= max((for $date in $datingConcepts//periodo:latestYear return $date))

            return
                <div class="panel panel-default">
                    <div class="panel-title">
                    { teiEditor:label("absDatingPanel-indDatingTitle", "Absolute dates for dating group no. ")}{ $index }
                    </div>
                    <div class="panel-body">

                    {if(($notBefore = "") and ($notAfter="") or (not($notBefore) and not($notAfter)))
                        then <span>
                            { teiEditor:label("absDatingPanel-noDating", "There is no absolute values in Document")}
                                <button class="btn btn-primary" title="Update values" onclick="absDatingUpdate('{ $teiEditor:docId }', '{ $index }', '{ $origDateElementNickname }')">{ teiEditor:label("absDatingPanel-retrieveFromThs", "Retrieve dates from thesaurus concept") }<i class="glyphicon glyphicon-refresh"/></button>
                            </span>
                    else if(($notBefore != $startDateFromConcepts ) or ($notAfter != $endDateFromConcepts))
                        then <span>{ teiEditor:label("datingManager-discrepency", "Discrepency between dates in doc and dates retrieved from Thesaurus")}
                        <button class="btn btn-primary" title="Update values" onclick="absDatingUpdate('{ $teiEditor:docId }', '{ $index }', '{ $origDateElementNickname }')">{ teiEditor:label("absDatingPanel-update", "Update ") }<i class="glyphicon glyphicon-refresh"/></button>
                        </span>
                        else teiEditor:label("absDatingPanel-datesMatch","Dates in document match dates retrieved from thesaurus")
                    }
                    </div>
                </div>
            }</div>
        </div>
};

declare function teiEditor:absDatingReloadPanel($data as node(), $project as xs:string)
as node(){
<response>
    <absDating>
        { teiEditor:datingFromThesaurus(data($data//docId), $data//origDateElementNickname)}
    </absDating>
</response>
};

declare function teiEditor:absDatingUpdate($data as node(), $project as xs:string)
as node(){
let $docId := data($data//docId)
let $index := data($data//datingIndex)
let $origDate := $teiEditor:doc-collection/id($data//docId)//tei:origDate[xs:int($index)]
let $datingConcepts:= 
        for $concept in tokenize($origDate/@period, " ")
            return skosThesau:getSkosConcept($concept)
       
let $startDateFromConcepts:= min((for $date in $datingConcepts//periodo:earliestYear return $date))
let $endDateFromConcepts:= max((for $date in $datingConcepts//periodo:latestYear return $date))
let $updateDates:= if((string($startDateFromConcepts) != "") and (string($endDateFromConcepts) != "")) then
    
    (update value $origDate/@notBefore-custom with string($startDateFromConcepts),
    update value $origDate/@notAfter-custom with string($endDateFromConcepts)
    )
    
    else()
let $newContent := $teiEditor:doc-collection/id($docId)             
return
<response>
    <updatedElement>
        { teiEditor:displayElement($data//origDateElementNickname, $docId, $index,())}
    </updatedElement>
    <absDating>
        { teiEditor:datingFromThesaurus($docId, $data//origDateElementNickname)}
    </absDating>
    <newContent>{ $newContent }</newContent>
</response>
};

declare function teiEditor:fragmentDimensions($docId as xs:string?, $msFragManager as node()){
    let $frags := if($docId = "") then $teiEditor:teiDoc//tei:msFrag
        else util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')")//tei:msFrag
             
      return       
     <div>{
        for $frag at $pos in $frags//tei:dimensions[@type="statue"]
        let $fragLabel := $frag/ancestor::tei:TEI//tei:titleStmt/tei:title/text()
                          return 
                          <div class="panel panel-default">
                            <h3>Dimensions fragment <em>{ $msFragManager//li[$pos]/span[@class="value"] }</em></h3>
                            { teiEditor:displayElement('fragmentDimensions', $docId, $pos, "//tei:msFrag")}
                          </div>
                      }</div>
};

declare function teiEditor:widgets($data, $project as xs:string?){
    
    switch($data//widgettype)
    case "dropdownEdit" case "dropdownAdd" return
        skosThesau:dropDownThesauForXMLElement($data//teielementnickname/text(),
                        $data//topconceptid/text(), $data//lang/text(), $data//label/text(), $data//format/text(), $data//index/text(), $data//subindex/text(), $data//attributevaluetype/text())

        case "dropdownwithhierarchyEdit" case "dropdownwithhierarchyAdd"  return
            skosThesau:dropDownThesauForElementWithConceptHierarchy($data//teielementnickname/text(),
                        $data//topconceptid/text(), $data//lang/text(), $data//label/text(), $data//format/text(), $data//index/text(), $data//subindex/text(), $data//attributevaluetype/text())
        default return "Error"
        
};

declare function teiEditor:moveItem($data as node(), $project as xs:string){
    
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)
    let $docId := $data//docId/text()
    let $xpath := $data//xpath/text()
    let $teiElementNickname := $data//teiElementNickname/text()
    let $elementNode := if (not(exists($teiEditor:teiElementsCustom//teiElement[./nm eq $teiElementNickname]))) 
                                        then $teiEditor:teiElements//teiElement[./nm eq $teiElementNickname] 
                                        else $teiEditor:teiElementsCustom//teiElement[./nm eq $teiElementNickname]
    let $contentType := $elementNode/contentType/text()
    let $attributeValueType := $elementNode/attributeValueType/text()
    let $pos := xs:int($data//itemPosition/text())
    let $direction := $data//direction/text()

    let $targetElement :=
            switch($contentType)
                case "textNodeAndAttribute"
                case "text" return 
                    util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")/' || $xpath || "[" || $pos || "]" )
                case "attribute" return
                    util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")/' || $xpath )
            default return 
                util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")/' || $xpath || "[" || $pos || "]" )
    let $moveItem :=
            switch($contentType)
                case "textNodeAndAttribute"
                case "text"
                    return 
                    (switch($direction)
                    case "up" return
                        (update insert $targetElement preceding 
                            util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")/' || $xpath || "[" || ($pos -1) || "]" ),
                        update replace 
                             util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")/' || $xpath || "[" || ($pos+1) || "]" )
                        with text { "" }
                        )
                    case "down" return
                        (update insert $targetElement following 
                        util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")/' || $xpath || "[" || ($pos+1) || "]" ),
                        update replace 
                             util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")/' || $xpath || "[" || ($pos) || "]" )
                        with text { "" }
                        )
                    default return ()
                    )
                case "attribute"
                     return
                       (
                        let $values := tokenize($targetElement)
                        let $newValue := switch($direction)
                                case "up" return
                                            ((string-join(($values[position() < ($pos -1)]), " ") || " " ||
                                            $values[$pos] || " " ||  
                                            $values[position() = ($pos -1)] || " " ||
                                            string-join(($values[position()>=($pos +1)]), " "))
                                            => functx:trim())
                                case "down" return 
                                    ((string-join(($values[position() < ($pos)]), " ") || " " ||
                                        $values[$pos+1] || " " ||  
                                        $values[$pos] || " " ||  
                                        string-join(($values[position()>($pos+1)]), " "))
                                        =>functx:trim())
                                default return ()
                        return update value $targetElement with $newValue
                       )
                default return ()
    
        let $newContent := $teiEditor:doc-collection/id($docId)             
    
    return
       <data>
        <updatedElement>
        { teiEditor:displayElement($teiElementNickname, $docId, (), ())}
        </updatedElement>
        <newContent>{ $newContent }</newContent>
    </data>
};

declare function teiEditor:moveBiblioItem($data as node(), $project as xs:string){
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)
    let $docId := $data//docId/text()
    let $biblioType := $data//biblioType/text()
    let $pos := $data//itemPosition/text() =>xs:int()
    let $xpath := '/tei:div[@type="bibliography"][@subtype="' || $biblioType || '"]'
    let $bibRef :=  util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")//tei:div[@type="bibliography"][@subtype="' || $biblioType || '"]//tei:bibl[' || $pos || "]" )

    let $moveItem :=
        (switch($data//direction/text())
            case "up" return
                (update insert $bibRef preceding 
                    util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")//tei:div[@type="bibliography"][@subtype="' || $biblioType || '"]//tei:bibl[' || ($pos -1) || "]" ),
                update replace 
                        util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")//tei:div[@type="bibliography"][@subtype="' || $biblioType || '"]//tei:bibl[' ||  ($pos+1) || "]" )
                with text { "" }
                )
            case "down" return
                (update insert $bibRef following 
                util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")//tei:div[@type="bibliography"][@subtype="' || $biblioType || '"]//tei:bibl[' || ($pos+1) || "]" ),
                update replace 
                        util:eval('collection("' || $teiEditor:doc-collection-path || '")/id("' || $docId || '")//tei:div[@type="bibliography"][@subtype="' || $biblioType || '"]//tei:bibl[' || ($pos) || "]" )
                with text { "" }
                )
            default return ()
        )
    
        let $newContent := $teiEditor:doc-collection/id($docId)             
    
    return
       <xml>
        <updatedElement>
        { teiEditor:bibliographyPanelRefList($newContent, $docId, $biblioType, $xpath, $data//zoteroTag/text(), $data//orderingType/text())}
        </updatedElement>
        <newContent>{ $newContent }</newContent>
    </xml>
};

declare function teiEditor:peopleManagerDeities($docId as xs:string){
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $people:= $teiDoc//tei:listPerson[@type="deitiesInDocument"]
   
    return
        
    <div id="deitiesInDocManager" class="teiElementGroup sectionPanel" style="border: solid 1px;" type="deitiesInDocManager">
            <div class="TeiElementGroupHeaderBlock">
                <span class="labelForm">{ teiEditor:label("deitiesManager-title", "Deities represented or mentioned in the document") }</span>
            </div>
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div id="peoplePanel" class="teiElementGroup">
                        <div class="TeiElementGroupHeaderBlock"><span class="labelForm">{ teiEditor:label("peopleManager-owner", "Owner") }</span>
                        </div>
                        { teiEditor:listPeopleManager($docId) }
                        <div>
                
                            <h4>{ teiEditor:label("peopleManager-addaPerson", "Add a person") }</h4>
                                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'FatherOf')">{ teiEditor:label("peopleManager-father", "Father") }</button>
                                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'MotherOf')">{ teiEditor:label("peopleManager-mother", "Mother") }</button>
                                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'SeriousIntimateRelationShipWith')">{ teiEditor:label("peopleManager-spouse", "Spouse") }</button>
                                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('dedicator', '{ $owner/@corresp }', 'ChildOf')">{ teiEditor:label("peopleManager-child", "Child") }</button>
                                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('dedicator', '{ $owner/@corresp }', 'AncestorOf')">{ teiEditor:label("peopleManager-ancestor", "Ancestor") }</button>
                                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('dedicator', '{ $owner/@corresp }', 'DescendentOf')">{ teiEditor:label("peopleManager-descendent", "Descendent") }</button>
                                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('dedicator', '{ $owner/@corresp }', 'Bond')">{ teiEditor:label("peopleManager-bond", "Bond") }</button>                
                        </div>
                    </div>
                </div>
               
                <div class="col-xs-7 col-sm-7 col-md-7">
                    <div id="personRecord" />
                <div>
            </div>
        </div>


</div>
    <script>
          { $script4Datatable }

    </script>
<div id="dialogAddPersonToDocument" title="Add a Person to a Resource" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">{ teiEditor:label("peopleManager-addaPerson", "Link a Person to Document") }</h4>
                </div>
                <div class="modal-body">
                      <!--<form id="addPersonForm" class="addPersonForm" role="form" data-toggle="validator" novalidate="true">
                      -->
                      <div id="actionGroup">
                      <h3><button id="selectPersonButton" onclick="collapseSelectPerson()" class="btn btn-primary" type="button" >{ teiEditor:label("selectButton", "Select") }</button>
                      { teiEditor:label("or", "or") } <button id="createPersonButton" onclick="collapseCreatePerson()" class="btn dropdown btn-primary" type="button">{ teiEditor:label("createButton", "Create") }</button> { teiEditor:label("peopleManager-aRelatedPerson", "a related person") }</h3>
                      <div class="accordion-group">
                            <div class="collapse accordion" data-parent="#accordion-group" id="selectPersonPanel">
                                <div class="card card-body">
                                    <h4>{ teiEditor:label("peopleManager-selectAlreadyExisting", "Select a person already in database") }</h4>
                                          
                                      { teiEditor:listPeopleAsTable() }
                                     
                                      <div id="projectPeopleDetailsPreview" class=""/>
                                      <input id="currentDocumentUri" type="text" class="hidden"/>
                                      <input id="selectedPeopleUri" type="text" class="hidden"/>
                                      <!--<input id="selectedPersonDedicator" type="checkbox" class="valueField" value="dedicator">Dedicator</input>-->
                                      <br/>
                               </div>
                               </div>
                      <div class="collapse accordion" data-parent="#accordion-group" id="createNewPersonPanel">
                            <div class="card card-body">
                      
                                 <h4 >{ teiEditor:label("peopleManager-createNewRecord", "Create a new person record") }</h4>
                                 <div class="form-group">
                                      <label for="newPersonStandardizedNameFr">{ teiEditor:label("peopleManager-standardNameFrench", "Standardized name (in French)") }</label>
                                      <input type="text" class="form-control" id="newPersonStandardizedNameFr" name="newPersonStandardizedNameFr"
                                      data-error=""/>
                                 </div>
                                 <div class="form-group">
                                    <label for="newPersonStandardizedNameEn">{ teiEditor:label("peopleManager-standardNameEnglish", "Standardized name (in English)") }</label>
                                       <input type="text" class="form-control" id="newPersonStandardizedNameEn" name="newPersonStandardizedNameEn"
                                    data-error=""/>
                                 </div>
                                 
                                <div class="form-group">
                                    <label for="newPersonTranslitteredName">{ teiEditor:label("peopleManager-translitName", "Name in translitteration") }</label>
                                    <input type="text" class="form-control" id="newPersonTranslitteredName" name="newPersonTranslitteredName"
                                    data-error=""/>
                                </div>
                                {skosThesau:dropDownThesauForElement("sex", "c23490", "en", teiEditor:label("sex", "Sex"), 'row', (), (), "uri")}
                                <br/>        
                                
                            </div>
                        </div>
                        </div>
                            </div>
                            <div id="person2AddDedicatorContainer" class="hidden">
                                <input id="person2AddDedicator" type="checkbox" class="valueField" value="dedicator">{ teiEditor:label("peopleManager-dedicator", "Dedicator") }</input>
                            </div>
                        
                            <input id="person2AddType" class="valueField hidden"/>
                            <input id="person2AddOwner" class="valueField hidden"/>
                            <input id="person2AddBondType" class="valueField hidden"/>
                            
                        <div class="modal-footer">
                        
                        <button  id="addExistingPersonToDocButton" class="btn btn-primary pull-left  hidden" type="submit" onclick="addProjectPersonToDocWithRole()">{ teiEditor:label("peopleManager-selectAndAddToDoc", "Select and add to document") }</button>

                        <button  id="createAndAddPersonToDocButton" class="btn btn-primary pull-left  hidden" type="submit" onclick="createAndAddPersonToDoc('{ $teiEditor:docId }')">{ teiEditor:label("peopleManager-createAndAddToDoc", "Create and add to document") }</button>
                        
                        <button type="button" class="btn btn-default" data-dismiss="modal">{ teiEditor:label("cancelButton", "Cancel") }</button>
                    </div>
                  <!--</form>-->
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>

            </div>


    </div>
</div>
};

declare function teiEditor:getLabelFromXmlValue($xmlValue as xs:string, $lang as xs:string){
 skosThesau:getLabelFromXmlValue($xmlValue, $lang)   
};