xquery version "3.1";

import module namespace config="http://exist-db.org/xquery/apps/config"  at "../config.xqm";
import module namespace teiEditor="http://ausonius.huma-num.fr/teiEditor"
      at "xmldb:exist:///db/apps/ausohnum-library-dev/modules/teiEditor/teiEditorApp.xql";
import module namespace functx="http://www.functx.com";

declare namespace apc = "http://patrimonium.huma-num.fr/onto#";
declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dct = "http://purl.org/dc/terms/";
declare namespace ev = "http://www.w3.org/2001/xml-events";
declare namespace local = "local";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace tei = "http://www.tei-c.org/ns/1.0";

declare option exist:serialize "method=xhtml media-type=text/html
omit-xml-declaration=yes indent=yes";

declare function local:saveData($input as xs:string, $data as xs:string?) as xs:string{
  <a>{$data}</a>
};

declare variable $teiEditor:project :=request:get-parameter('project', ());

let $now := fn:current-dateTime()
let $currentUser := sm:id()

let $docId := request:get-parameter('docid', '')
let $doc-collection := collection("/db/apps/" || $teiEditor:project || "Data/documents")
let $teiDoc := $doc-collection/id($docId)

(: let $teiElements := doc('/db/apps/ausohnum-library/data/templates/teiElements.xml') :)
(: let $logs := collection("/db/apps/patrimonium/data/logs") :)

(: let $savePanel :=
<div class="sectionPanel">
<button id="saveDocument" class="btn btn-primary editbutton" onclick="javascript:saveData(titleStmt)"
appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-floppy-save"></i></button>
  </div> :)

(: let $title :=
 <div class="">
 <div id="docTitle" class="">{$teiDoc//tei:fileDesc/tei:titleStmt/tei:title/text()}</div>
 <button id="editTitle" class="btn btn-xs btn-primary editbutton" onclick=""
  appearance="minimal" type="button"><i class="glyphicon glyphicon-edit
                              editConceptIcon"></i></button>
 </div> :)

 (: let $titlePanel :=
    <div>
    <input id="titleStmt" class="fullWidth" name="titleStmt" value="{$teiDoc//tei:fileDesc/tei:titleStmt/tei:title/text()}"></input>
    <button id="saveTitleStmt" class="btn btn-primary" onclick="javascript:saveData('{$docId}', titleStmt, 'tei:fileDesc/tei:titleStmt/tei:title')"
            appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-floppy-save"></i></button>
    </div> :)

(:************************:)
(:*     MAIN RETURN      *:)
(:************************:)

return
<div data-template="templates:surround" data-template-with="templates/page.html" data-template-at="content">
        <script src="$ausohnum-lib/resources/scripts/jquery/jquery.validate.min.js"></script>
        <div id="editorApp"
            class="container-fluid">

            <div class="container-fluid">
            <h2 id="docMainTitle">{$teiDoc//tei:titleStmt/tei:title/string()}</h2><span class="pull-right h3">{$docId}</span>
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item active">
                  <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#nav-metadata" role="tab" aria-controls="pills-home" aria-selected="false">Document Metadata</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-textbearer-tab" data-toggle="pill" href="#nav-textbearer" role="tab" aria-controls="pills-textbearer" aria-selected="false">Support</a>
                </li>
                <!--
                <li class="nav-item">
                  <a class="nav-link" id="pills-fragments-tab" data-toggle="pill" href="#nav-fragments" role="tab" aria-controls="pills-fragments" aria-selected="false">Object &amp; Fragments</a>
                </li>
                -->
                <li class="nav-item">
                  <a class="nav-link" id="pills-text-metadata-tab" data-toggle="pill" href="#nav-text-metadata" role="tab" aria-controls="pills-profile" aria-selected="true">Text Metadata</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-text-tab" data-toggle="pill" href="#nav-text" role="tab" aria-controls="pills-profile" aria-selected="true">Text Units</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#nav-xmlfile" role="tab" aria-controls="pills-profile" aria-selected="false">XML file</a>
                </li>
            </ul>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade in active" id="nav-metadata" role="tabpanel" aria-labelledby="nav-metadata-tab">
                        {teiEditor:variables($docId, $teiEditor:project)}
                        {teiEditor:displayElement('docTitle', (), (), ())}
                        {teiEditor:displayElement('editionDesc', (), (), ())}
                        {teiEditor:displayElement('docType', (), (), ())}
                        {teiEditor:principalBibliography($docId)}
                        {teiEditor:displayElement("tmNumber", (), (), ())}
                        {teiEditor:displayElement("placeOfOrigin", (), (), ())}
                        { teiEditor:displayElement('docKeywords', (), (), ()) }
        
    </div><!-- End of tab -->     
<div class="tab-pane fade in" id="nav-textbearer" role="tabpanel" aria-labelledby="nav-textbearer-tab">


                    { teiEditor:displayElement('docSupportType', (), (), ()) }
                    { teiEditor:displayElement('docSupportMaterial', (), (), ()) }
                    
                    <!--{teiEditor:displayElement('docSupportComment', (), ())}-->
                    { teiEditor:displayElement('docSupportConditionDescription', (), (), ()) }
                    { teiEditor:displayElement('docSupportConditionKeyword', (), (), ()) }
                    
                    { teiEditor:displayFragment($docId) }
        

   </div>    <!--End of tab-->
            <!--
            <div class="tab-pane fade in" id="nav-fragments" role="tabpanel" aria-labelledby="nav-fragments-tab">
            </div>
            -->
    <div class="tab-pane fade" id="nav-text-metadata" role="tabpanel" aria-labelledby="nav-text-metadata-tab">
                {teiEditor:msItemManager()}

                {teiEditor:surfaceManager()}
                {teiEditor:layoutManager()}
                


    </div>

    <div class="tab-pane fade in" id="nav-text" role="tabpanel" aria-labelledby="nav-text-tab">
        <div class="row">
        <!--
        <input type="button" value="click to toggle fullscreen" onclick="toggleFullScreen('nav-text')"><span class="glyphicon glyphicon-resize-full"/></input>
        -->
        <div class="sideToolPane col-sm-3 col-md-3 col-lg-3">
                { teiEditor:manualLemmatizer() }
                { teiEditor:semanticAnnotation("Subject Indexing", "subject", "rs", "c19365") }
                {teiEditor:annotationPlacePeopleTime()}
                
             </div>
                            <!-- col-sm-8 col-md-8 col-lg-8  -->
             <div id="editorPanel" class="ui-widget-content">

                {teiEditor:textEditor($docId, ())}
             </div>
             
        </div>
    </div>
    <div class="tab-pane fade in" id="nav-xmlfile" role="tabpanel" aria-labelledby="nav-text-tab">
    { teiEditor:xmlFileEditor() }
    <!--<div id="xml-editor-file" class="">
           {serialize($teiDoc, ())}

           </div>
           -->
    </div>
    <div id="messageZone"/>
    <div id="currentEditorIndexVariable"/>

    </div>
    </div>
    </div>
          <link rel="stylesheet" href="$ausohnum-lib/resources/css/teiEditor.css"/>
        <link href="$ausohnum-lib/resources/css/skosThesau.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="/resources/css/teiEditor.css"/>
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/ancientTextImportRules.js"/>
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/tei2Html4Preview.js"/>
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/tei2leiden.js"/>
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/teiEditorConfig.js"/>
        <script type="text/javascript" src="$ausohnum-lib/resources/scripts/teiEditor/teiEditorEvents.js"/>
        <script type="text/javascript" >
                    window.onbeforeunload = function() {{
                            return "Are you sure you want to navigate away?";}};
            </script>
  </div>
