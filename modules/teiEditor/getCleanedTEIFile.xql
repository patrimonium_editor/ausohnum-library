xquery version "3.1";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization"; 
declare namespace prod = "http://datypic.com/prod"; 
declare option output:method "xml"; 
declare option output:media-type "application/xml";
declare option output:indent "no"; 
declare option output:omit-xml-declaration "no";
declare option output:encoding "UTF-8";


declare variable $docId := request:get-parameter('docId', ());
declare variable $project := request:get-parameter('project', ());


   let $now := fn:current-dateTime()
   let $xsltCleanNamespace:= doc("/db/apps/ausohnum-library-dev/xslt/removeExtraNamespace.xsl")
   let $xslt:= doc("/db/apps/ausohnum-library-dev/xslt/cleanTeiFile.xsl")

      
   let $teiFile := collection("/db/apps/" || $project || "Data/documents")/id($docId) 
   let $params := 
   <parameters>
      <param name="output.omit-xml-declaration" value="no"/>
      <param name="output.indent" value="yes"/>
      <param name="output.media-type" value="application/xml"/>
      <param name="output.method" value="xml"/>
   </parameters>
   let $paramMap := 
         map {"method": "xml", "indent": false(), "item-separator": "", "version": "1.0"}
   let $serialization-options := 'method=xml media-type=text/xml omit-xml-declaration=no indent=yes'
   let $removeExtraNamespace:=    transform:transform($teiFile, $xsltCleanNamespace, (), (), $serialization-options)
   let $fileContent := transform:transform($removeExtraNamespace, $xslt, (), (), $serialization-options)
   let $filename := "project-" || $project || "-document-" || $docId || "-"|| substring($now, 1, 10) || ".xml"
      
      
return
   response:stream-binary(util:string-to-binary(serialize($fileContent)), "application/zip", $filename)
