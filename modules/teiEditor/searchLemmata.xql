(:~
: AusoHNum Library - prosopographical module
: This function queries the people repository and searches for types of relationships between persons 
: @author Vincent Razanajao
: @param name of the project
: @param string to be searched
: @return JSON list of pesonal relationship types matching the searched string
:)

xquery version "3.1";

import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/" at "../skosThesau/skosThesauApp.xql";

declare namespace apc="http://patrimonium.huma-num.fr/onto#";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace json="http://www.json.org";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";


(: Switch to JSON serialization :)
declare option output:method "json";
declare option output:media-type "application/json";
declare option output:indent "yes";

declare variable $project :=request:get-parameter('project', ());
declare variable $query :=request:get-parameter('query', ());
(:declare variable $parentConceptUri :=request:get-parameter('parentConceptUri', 'https://ausohnum.huma-num.fr/concept/c22265');:)


(:let $functions-collection := collection("/db/apps/" || $project || "Data/functions/" ):)
let $lemmata := collection("/db/apps/" || $project || "Data/documents")

return

    <data>
    <when>{fn:current-dateTime()}</when>
    <list>
    <items>
       <title>Search "{$query}" returns { count(distinct-values(($lemmata//tei:w[ft:query(., $query[1])], $lemmata//tei:w[ft:query(./@lemma, $query[1])]))) }</title>
      </items>
    <items>
        <lemma>{$query}</lemma>
    </items>
    {
  for $lemma at $pos in distinct-values(($lemmata//tei:w[ft:query(., $query[1])], $lemmata//tei:w[ft:query(./@lemma, $query[1])]))
  return 
    <items>
        <lemma>{$lemma}</lemma>
    </items>
  }
  </list>
 </data>
