xquery version "3.1";
(:~
: AusoHNum Library - teiEditor for Egypt module
: This module contains the main functions of the teiEditor module.
: @author Vincent Razanajao
:)

module namespace teiEditorEgypt="http://ausonius.huma-num.fr/teiEditorEgypt";
import module namespace console="http://exist-db.org/xquery/console";

import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/" at "../skosThesau/skosThesauApp.xql";

import module namespace teiEditor="http://ausonius.huma-num.fr/teiEditor" at "./teiEditorApp.xql";
import module namespace config="http://ausonius.huma-num.fr/ausohnum-library/config" at "../config.xqm";

import module namespace functx="http://www.functx.com";

declare namespace apc="http://patrimonium.huma-num.fr/onto#";
declare namespace dc = "http://purl.org/dc/elements/1.1/";
declare namespace dct = "http://purl.org/dc/terms/";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace err = "http://www.w3.org/2005/xqt-errors";
declare namespace ev = "http://www.w3.org/2001/xml-events";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace lawd="http://lawd.info/ontology/";
declare namespace owl="http://www.w3.org/2002/07/owl#";
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace rdfs = "http://www.w3.org/2000/01/rdf-schema#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace snap="http://onto.snapdrgn.net/snap#";
declare namespace spatial="http://geovocab.org/spatial#";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace thot = "http://thot.philo.ulg.ac.be/";
declare namespace xf = "http://www.w3.org/2002/xforms";
declare namespace local = "local";

declare boundary-space preserve;

declare variable $teiEditorEgypt:labels :=
                <labels>
                        <owner>
                        <label xml:lang="en">Owner</label>
                        <label xml:lang="fr">Propriétaire</label>
                        </owner>
                        <bond type="FatherOf" reverse="SonOf">
                            <label xml:lang="en">Father</label>
                            <label xml:lang="fr">Père</label>
                            </bond>
                         <bond type="MotherOf" reverse="ChildOf">
                            <label xml:lang="en">Mother</label>
                            <label xml:lang="fr">Mère</label>
                            </bond>
                        <bond type="ChildOf" sex="male" reverse="FatherOf">
                            <label xml:lang="en">Son</label>
                            <label xml:lang="fr">Fils</label></bond>
                        <bond type="ChildOf" sex="female" reverse="FatherOf"><label xml:lang="en">Daughter</label>
                            <label xml:lang="fr">Fille</label></bond>
                            
                        <bond type="ChildOf" sex="male" reverse="MotherOf">
                            <label xml:lang="en">Child</label>
                            <label xml:lang="fr">Fils</label></bond>
                        <bond type="ChildOf" sex="female" reverse="MotherOf"><label xml:lang="en">Daughter</label>
                            <label xml:lang="fr">Fille</label></bond>
                            
                            
                            
                        <bond type="SonOf" reverse="FatherOf">
                            <label xml:lang="en">Son</label>
                            <label xml:lang="fr">Fils</label></bond>
                        
                        <bond type="DaughterOf" reverse="FatherOf">
                            <label xml:lang="en">Daughter</label>
                            <label xml:lang="fr">Fille</label></bond>

                        <bond type="AncestorOf" reverse="DescendentOf">
                            <label xml:lang="en">Ancestor</label>
                            <label xml:lang="fr">Ancêtre</label></bond>
                            
                        <bond type="SeriousIntimateRelationShipWith" sex="male" reverse="SeriousIntimateRelationShipWith">
                            <label xml:lang="en">Spouse</label>
                            <label xml:lang="fr">Époux</label></bond>
                        <bond type="SeriousIntimateRelationShipWith" sex="female" reverse="SeriousIntimateRelationShipWith"><label xml:lang="en">Spouse</label>
                            <label xml:lang="fr">Épouse</label></bond>
                        <bond type="Bond" reverse="Bond">
                            <label xml:lang="en">Bond</label>
                            <label xml:lang="fr">Relation</label>
                        </bond>
                </labels>;
                
declare function teiEditorEgypt:peopleManager($docId as xs:string){
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $people:= $teiDoc//tei:listPerson[@type="peopleInDocument"]
    let $owner := $people//tei:person[@role="owner"] 
    let $otherPeople:= $people//tei:person[not(@role="owner")]
(:    let $temp := $teiEditor:peopleCollection//lawd:person[@rdf:about= data($people//tei:person[@role="owner"]/@corresp)]:)
    let $ownerRecord := if($owner[1]/@corresp != "") then $teiEditor:peopleCollection//lawd:person[@rdf:about= data($owner[1]/@corresp) || "#this"] else()
    return
<div id="peoplePanel" class="teiElementGroup sectionPanel">
   <div class="TeiElementGroupHeaderBlock"><span class="labelForm">People Manager</span></div>

    <div id="erPanel" class="teiElementGroup">
            <div class="TeiElementGroupHeaderBlock"><span class="labelForm">Owner</span></div>
                {if(count($owner) = 1) then <span><strong>{ $owner//tei:persName[1]/text() }</strong> [{data($owner/@corresp)}]</span>
                    else if(count($owner) > 1)
                        then <ul>
                {for $owner in $owner
                    return
                    <li><span><strong>{ $owner//tei:persName[1]/text() }</strong> [{data($owner/@corresp)}]</span></li>}
                </ul>
                else 
                <button onclick="openAddPersonToDocDialog('owner', '')">Add Owner</button>
                }
                {if(count($owner) >0) then (<div>
                            {if(count($otherPeople) = 0) then "There is not any other person linked to this document" else 
                            <div>
                                        <div class="TeiElementGroupHeaderBlock"><span class="labelForm">Other people</span></div><ul>{
                            for $person at $index in $otherPeople
                                let $relatedPersonUri := $person/@corresp
                                
                                let $relatedPersonDetails := $teiEditor:peopleCollection//lawd:person[@rdf:about= $relatedPersonUri || "#this"]
                                
                                let $bondType := data($relatedPersonDetails//snap:hasBond[@rdf:resource = $owner/@corresp]/@rdf:type)
                                let $bond2Display := $teiEditorEgypt:labels//bond[@reverse = $bondType][1]//label[@xml:lang=$teiEditor:lang]/text()
                                return
                                <li>{ $relatedPersonDetails//lawd:personalName[1]/text() } [{ $bond2Display }]
                                     <button class="removeItem btn btn-xs btn-warning pull-right" style="margin: 0 1em 0 1em"
                                          onclick="removeItemFromList('{$docId}', 'peopleManagerEgypt', 'xmlNode', {$index}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button> 
                                </li>
                            }
                            </ul>
                            </div>}         
                <h4>Add a person</h4>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'FatherOf')">Father</button>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'MotherOf')">Mother</button>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'SeriousIntimateRelationShipWith')">Spouse</button>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'ChildOf')">Child</button>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'AncestorOf')">Ancestor</button>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'DescendentOf')">Descendent</button>
                <button class="btn btn-primary" onclick="openAddPersonToDocDialog('', '{ $owner/@corresp }', 'Bond')">Bond</button>                
                
                </div>) 
                else ()
                }
            </div>
                

<div id="dialogAddPersonToDocument" title="Add a Person to a Resource" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Link a Person to Document</h4>
                </div>
                <div class="modal-body">
                      <form id="addPersonForm" class="addPersonForm" role="form" data-toggle="validator" novalidate="true">
                      <div id="actionGroup">
                      <h3><button id="selectPersonButton" class="btn btn-primary" type="button" >Select</button>
                      or <button id="createPersonButton" class="btn dropdown btn-primary" type="button">Create</button> a related person</h3>
                      <div class="accordion-group">
                            <div class="collapse accordion" data-parent="#accordion-group" id="selectPersonPanel">
                                <div class="card card-body">
                                    <h4>Select a person already in database</h4>
                                          <div class="form-group">
                                                <label for="projectPeopleLookup">Search</label>
                                                 <input type="text" class="form-control projectPeopleLookup"
                                                 id="projectPeopleLookup"
                                                 name="projectPeopleLookup"
                                                 autocomplete="on"
                                                 />

                                      </div>
                                      <div id="projectPeopleDetailsPreview" class=""/>
                                      <input id="currentDocumentUri" type="text" class="hidden"/>
                                      <input id="selectedPeopleUri" type="text" class="hidden"/>
                                      <!--<input id="selectedPersonDedicator" type="checkbox" class="valueField" value="dedicator">Dedicator</input>-->
                                      <br/>
                                      <button  id="addExistingPersonToDocButton" class="btn btn-primary pull-left collapse" type="submit" onclick="addProjectPersonToDocWithRole()">Select and add to document</button>
                               </div>
                               </div>
                      <div class="collapse accordion" data-parent="#accordion-group" id="createNewPersonPanel">
                          <div class="card card-body">
                      
                            <h4 >Create a new person record</h4>
                            <div class="form-group">
                                <label for="newPersonStandardizedNameEn">Standardized name (in English)</label>
                                <input type="text" class="form-control" id="newPersonStandardizedNameEn" name="newPersonStandardizedNameEn"
                                data-error="Please enter your full name."/>
                            </div>
                            <div class="form-group">
                                <label for="newPersonStandardizedNameFr">Standardized name (in French)</label>
                                <input type="text" class="form-control" id="newPersonStandardizedNameFr" name="newPersonStandardizedNameFr"
                                data-error="Please enter your full name."/>
                            </div>
                            <div class="form-group">
                                <label for="newPersonTranslitteredName">Name in translitteration</label>
                                <input type="text" class="form-control" id="newPersonTranslitteredName" name="newPersonTranslitteredName"
                                data-error="Please enter your full name."/>
                            </div>
                            
                            {skosThesau:dropDownThesauForElement("sex", "c23490", "en", 'Sex', 'row', (), (), "uri")}
                            <br/>        
                            <button  id="createAndAddPersonToDocButton" class="btn btn-primary pull-left collapse" type="submit" onclick="createAndAddPersonToDoc('{ $teiEditor:docId }')">Create and add to document</button>
                                </div>
                              </div>
                            </div>
                            </div>
                            <div id="person2AddDedicatorContainer">
                            <input id="person2AddDedicator" type="checkbox" class="valueField collapse" value="dedicator">Dedicator</input>
                            </div>
                            <input id="person2AddType" class="valueField hidden"/>
                            <input id="person2AddOwner" class="valueField hidden"/>
                            <input id="person2AddBondType" class="valueField hidden"/>
                            
                        <div class="modal-footer">

                        
                        
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                  </form>
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>

            </div>


    </div>


</div>
};

declare function teiEditorEgypt:decoDesc($docId as xs:string){
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $decoDesc := $teiDoc//tei:physDesc/tei:decoDesc


    return
<div id="objectDescPanel" class="">
 <div class="TeiElementGroupHeaderBlock">
            <span class="labelForm">{ teiEditor:label("iconographicDesc", "Iconographic description") }</span>
        </div>

       <div id="decoNoteHeadressPanel" class="sectionPanel col-sm-5 col-md-5 col-lg-5">
            <span class="labelForm">{ teiEditor:label("headdress", "Headdress") }</span>
                {teiEditor:displayElement('decoNoteHeaddressKeywords', $docId, (), ())}    
                {teiEditor:displayElement('decoNoteHeaddressSummary', $docId, (), ())}
        </div>        
        <div id="decoDescObjectInHandBodyPanel" class="sectionPanel col-sm-5 col-md-5 col-lg-5">            
            <span class="labelForm">{ teiEditor:label("clothes", "Clothes") }</span>
                {teiEditor:displayElement('decoNoteClothesKeywords', (), (), ())}   
                {teiEditor:displayElement('decoNoteClothesSummary', (), (), ())} 
        </div>
        <div id="decoNoteJewelsBodyPanel" class="sectionPanel col-sm-5 col-md-5 col-lg-5">            
            <span class="labelForm">{ teiEditor:label("objectInRightHand", "Object in right hand") }</span>
                {teiEditor:displayElement('decoNoteObjectInRightHandKeywords', (), (), ())}    
                {teiEditor:displayElement('decoNoteObjectInRightHandSummary', (), (), ())}
        </div>
        <div id="decoDescObjectInHandBodyPanel" class="sectionPanel col-sm-5 col-md-5 col-lg-5">            
            <span class="labelForm">{ teiEditor:label("objectInLeftHand", "Object in left hand") }</span>
                {teiEditor:displayElement('decoNoteObjectInLeftHandKeywords', (), (), ())}    
                {teiEditor:displayElement('decoNoteObjectInLeftHandSummary', (), (), ())}
        </div>
        <div id="decoNoteAccessoriesBodyPanel" class="sectionPanel col-sm-5 col-md-5 col-lg-5">            
            <span class="labelForm">{ teiEditor:label("accessories", "Accessories") }</span>
                {teiEditor:displayElement('decoNoteAccessoriesKeywords', (), (), ())}    
                {teiEditor:displayElement('decoNoteAccessoriesSummary', (), (), ())}
        </div>
        <div id="decoNoteFrontispieceDeities" class="sectionPanel col-sm-5 col-md-5 col-lg-5">            
            <span class="labelForm">{ teiEditor:label("frontispieceFigures", "Figures featured on frontispiece") }</span>
                {teiEditor:displayElement('decoNoteFrontispieceFiguresKeywords', (), (), ())}    
                {teiEditor:displayElement('decoNoteFrontispieceFiguresSummary', (), (), ())}
        </div>
</div>
    
};

declare function teiEditorEgypt:msItemManager($docId as xs:string){
(:~
: This function manages the msItem elements of a document, linking one msItem to a type and to a locatoin on the document,
:i.e. a sourceDoc/surface
: @author Vincent Razanajao
:)
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $textLocations := $teiDoc//tei:sourceDoc
    let $msItems := $teiDoc//tei:msItem
           
     return
    <div class="sectionPanel" id="msItemManager">
    <h4 class="subSectionTitle">Text Units<button class="btn btn-sm btn-primary pull-right" onclick="openDialog('dialogAddmsItem')">Add a Text</button></h4>
    
    {if(count($msItems) = 0)
        then <p>This TEI document has currently no text item</p> 
        
        else(
            <div>
                <p><strong>{ count($msItems) } msItem </strong> Element{ if (count($msItems) > 1) then "s" else ""}</p>
                <ul>
                {for $msItem at $index in $teiDoc//tei:msItem
                       let $textTypes := 
                                for $item at $itemPos in tokenize($msItem/@class, " ")
                                
                                return 
                                   skosThesau:getLabel($item, $teiEditor:lang) || (
                                   if($itemPos < count(tokenize($msItem/@class, " ")) ) then ", " else ()
                                   )
                        let $targetLocationTypes :=
                                for $item at $itemPos in tokenize($msItem/tei:locus/@target, " ")
                                return
                                (if( $item != "") then 
                                skosThesau:getLabelFromXmlValue(data($textLocations//tei:surface[@xml:id = substring-after($item, '#')]/@ana), "fr")
                                 || (
                                   if($itemPos < count(tokenize($msItem/tei:locus/@target, " ")) ) then ", " else ()
                                   )
                                else ()
                                )
                        return
                            <li class="sectionPanel">
                               <span class="">{$textTypes}</span>
                                    <span class=""> [{ $targetLocationTypes }]
                                    
                                    {""(:teiEditor:displayTeiElement('msItemModernTitle', $index, 'input', ()):)}
                                    {""(:teiEditor:displayElement('textMainLang', (), 1, ()):)}
                                    {""(:teiEditor:displayElement('textOtherLangs', (), $index, ()):)}
                                   {""(:teiEditor:displayTeiElement('msItemRefToCanonical', $index, 'input', ()):)}
                                   </span>
                                   <button class="removeItem btn btn-xs btn-warning pull-right" style="margin: 0 1em 0 1em"
                                          onclick="removeItemFromList('{$docId}', 'msItem', 'xmlNode', {$index}, '')"><i class="glyphicon glyphicon-trash" title="Remove reference from list"/></button>
                               </li>
                 }        
                </ul>
              </div>
            )}
                
  <div id="dialogAddmsItem" title="Add a msItem to a Document" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Add a msItem to Document</h4>
                </div>
                
                    <div class="modal-body">
                        <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                        {skosThesau:dropDownThesauXMLMultiple('c20257', 5, 'en', 'Type', 'inline', 1, 1, ())}
                        
                        <br/>
                        {skosThesau:dropDownThesauXMLMultiple('c20266', 5, 'en', 'Location', 'inline', 1, 1, ())}
                        </div>
                        </div>
                    </div>
                     <div class="modal-footer">
                          <button  id="addmsItemToDocButton" class="btn btn-primary pull-left" type="submit"
                                      onclick="addmsItemToDoc()">Validate</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                     </div>
                
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

               </div>
                
      </div>


 </div>



  
};

(:
********************************
*     Updatating Data       *
********************************
:)

declare function teiEditorEgypt:addmsItemToDoc($data as node(), $project as xs:string){
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)
    let $docId := $data//docId/text()
    let $teiDoc := $teiEditor:doc-collection/id($docId)
    let $msItemList:= $teiDoc//tei:msItem
    let $msItemNo := sum((count($msItemList), 1))
    
    let $locationTypeUri :=  $data//locationTypeUri[./text() != ""]
    let $textTypeUri := $data//textTypeUri[./text() != ""]
    
    let $surfaceList := $teiDoc//tei:sourceDoc
    let $surfaceNo := sum((count($surfaceList)), (1))
(:    let $surfaceTypeAttribute := skosThesau:getLabel($locationTypeUri, "xml"):)
    let $surfaceToInsert :=
    <node>
                {
                    for $surface in $locationTypeUri
                    let $surfaceTypeAttribute := skosThesau:getLabel(data($surface), "xml")
                    return
                        if($surfaceList//tei:surface[@ana = $surfaceTypeAttribute]) then ()
                            else 
                  <surface xml:id="{ $docId }-{ $surfaceTypeAttribute }" ana="{ $surfaceTypeAttribute }"/>
              
              }</node>
                
    
    let $surfaceId := string-join((for $surface in $locationTypeUri
                        let $surfaceTypeAttribute := skosThesau:getLabel(data($surface), "xml")
                        return
                        if($surfaceList//tei:surface[@ana = $surfaceTypeAttribute])
                                    then '#' || data($surfaceList//tei:surface[@ana = $surfaceTypeAttribute]/@xml:id)
                                    else '#' || $docId || "-" || $surfaceTypeAttribute), " ")
                                    
    let $msItemNode :=
<node>

                  <msItem xml:id="{ $docId }-msItem-{ $msItemNo }" class="{ $textTypeUri }">
                      <locus target="{ $surfaceId }"/>
                  </msItem>
</node>
    
    let $updateSurface :=
            for $surface in $locationTypeUri
                    let $surfaceTypeAttribute := skosThesau:getLabel(data($surface), "xml")
            return
            if(exists($surfaceList//tei:surface[@ana = $surfaceTypeAttribute]))
            then ()
            else (update insert functx:change-element-ns-deep($surfaceToInsert, "http://www.tei-c.org/ns/1.0", "")/node()
                    into $teiDoc//tei:sourceDoc)
    
    let $updatemsItem :=
            update insert functx:change-element-ns-deep($msItemNode, "http://www.tei-c.org/ns/1.0", "")/node()
                    into $teiDoc//tei:msDesc/tei:msContents
    
    return
    <data>
    <updatedElement>{ teiEditorEgypt:msItemManager($docId)}</updatedElement>
    {$surfaceList}
    {$surfaceToInsert}
    ID : {$surfaceId }
    { $msItemNode }
    </data>

};

declare function teiEditorEgypt:getDocumentOwner( $docId ){
        let $teiDoc := $teiEditor:doc-collection/id($docId)
        let $people:= $teiDoc//tei:listPerson[@type="peopleInDocument"]
        let $owner := $people//tei:person[@role="owner"] 
        let $otherPeople:= $people//tei:person[not(@role="owner")]
    
    (:let $xpath2peopleInDocument :='/tei:listPerson[@type="peopleInDocument"]'
    let $peopleInDocument := util:eval( "$teiEditor:doc-collection/id('" ||$teiEditor:docId ||"')" || $xpath2peopleInDocument)
    let $xpath2owner :=if($teiEditor:teiElementsCustom//teiElement[nm[./text() = "statueOwner"]]) then
                        $teiEditor:teiElementsCustom//teiElement[nm[./text() = "statueOwner"]]//xpath/text()
                        else '//tei:listPerson[@type="peopleInDocument"]/tei:person[@ana="owner"]/@corresp':)
    let $ownerUri := data($owner/@corresp)
    
    let $ownerRecord := $teiEditor:peopleCollection//lawd:person[@rdf:about = $ownerUri || "#this"]
    let $persNameEn := $ownerRecord//lawd:personalName[@xml:lang="en"]
    let $persNameEgy := $ownerRecord//lawd:personalName[contains(./@xml:lang, "egy")]
    
    let $representedRelatives :=
            <div>{
            for $person in $otherPeople
                let $relativeRecord := $teiEditor:peopleCollection//lawd:person[@rdf:about = $person/@corresp || "#this"]
                let $personBondType := data($relativeRecord//snap:hasBond[@rdf:resource= $ownerUri]/@rdf:type)
                let $label := $teiEditorEgypt:labels//bond[@type = $personBondType][1]//label[@xml:lang=$teiEditor:lang]/text()
                let $personPersNameEn := $relativeRecord//lawd:personalName[@xml:lang="en"]
                let $personPersNameEgy := $relativeRecord//lawd:personalName[contains(./@xml:lang, "egy")]
                let $personRole := data($person/@role)
                return
                <div>
                <strong>{ $label } :</strong>
                    { $personPersNameEgy } { if($personPersNameEgy ="") then $personPersNameEn else() }
                    { if($personRole ="dedicator") then "*" else () } <a href="{ $person/@corresp }/edit" target="about"><i class="glyphicon glyphicon-new-window"/></a></div>
            }</div>
    
    (:let $relatives := <div>{
        for $relative in $ownerRecord//snap:hasBond
            let $relativeRecord := $teiEditor:peopleCollection//lawd:person[@rdf:about = $relative/@rdf:resource || "#this"]
            let $label := $teiEditorEgypt:labels//bond[@type = data($relative/@rdf:type)][1]//label[@xml:lang=$teiEditor:lang]/text()
                    
            let $relativePersNameEn := $relativeRecord//lawd:personalName[@xml:lang="en"]
            let $relativePersNameEgy := $relativeRecord//lawd:personalName[contains(./@xml:lang, "egy")]
            let $relativeRole := data($peopleInDocument//tei:person[@corresp = $relative/@rdf:resource ]/@role)
            return 
            
            <div><strong>{ $label } :</strong> { $relativePersNameEgy } { if($relativePersNameEgy ="") then $relativePersNameEn else() } { if($relativeRole ="dedicator") then "*" else () } <a href="{ $relative/@rdf:resource }" target="about"><i class="glyphicon glyphicon-new-window"/></a></div>
            }</div>:)
    return <div><strong>{
                    $teiEditorEgypt:labels//owner/label[@xml:lang=$teiEditor:lang]/text()} :</strong> { $persNameEn} <a href="{ $ownerUri }" target="about"><i class="glyphicon glyphicon-new-window"/></a>
                    { $representedRelatives }
                    </div>
};

declare function teiEditorEgypt:addProjectPersonToDocWithRole( $data as node(), $project as xs:string){

let $now := fn:current-dateTime()
let $currentUser := data(sm:id()//sm:real//sm:username)

(:let $data := request:get-data():)
(:let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)

let $docId := $data//docId/text()
let $personUri := if(contains($data//peopleUri/text(), "#this")) then $data//peopleUri/text() else ($data//peopleUri/text() || "#this")
let $personUriShort := if(contains($personUri, "#this")) then substring-before($personUri, "#this") else $personUri

(:let $docId := request:get-parameter('docid', ()):)
let $teiDoc := $teiEditor:doc-collection/id($docId)
let $personDetails := $teiEditor:peopleCollection//lawd:person[@rdf:about=$personUri][1]
let $dedicator := $data//dedicator/text()
let $ownerUriShort := $data//owner/text()
let $ownerUriLong := $ownerUriShort || "#this"
let $ownerDetails := $teiEditor:peopleCollection//lawd:person[@rdf:about=$ownerUriLong][1]
let $bondType := $data//bondType/text()
let $role := $data//role/text()
let $personRefinDoc :=
            if(not($teiDoc//tei:profileDesc//tei:listPerson[@type="peopleInDocument"]
            )) then(
                <nodeToInsert>
                {element {"listPerson"}
                {attribute {"type"} { "peopleInDocument"},
                
                element {"person"}
                {
                attribute {"corresp"} { $personUriShort },
                 if($dedicator ="true") then (attribute role { "dedicator" }) else()
                ,
                if($personDetails//lawd:personalName[@xml:lang=$teiEditor:lang]/text() != "")
                    then <persName xml:lang="{ $teiEditor:lang }">{ $personDetails//lawd:personalName[@xml:lang=$teiEditor:lang]/text() }</persName>
                    else <persName xml:lang="{ data($personDetails//lawd:personalName[1]/@xml:lang) }">{ $personDetails//lawd:personalName[1]/text() }</persName>
                }
            }}</nodeToInsert>


)
            else
<nodeToInsert>
                    {element {"person"}
                {
                attribute {"corresp"} { $personUriShort },
                    if($dedicator ="true") then (attribute role { "dedicator" }) 
                    else()
                ,
                if($personDetails//lawd:personalName[@xml:lang=$teiEditor:lang]/text() != "")
                    then <persName xml:lang="{ $teiEditor:lang }">{ $personDetails//lawd:personalName[@xml:lang=$teiEditor:lang]/text() }</persName>
                    else <persName xml:lang="{ data($personDetails//lawd:personalName[1]/@xml:lang) }">{ $personDetails//lawd:personalName[1]/text() }</persName>
                }
            }</nodeToInsert>




let $insertRefToPresonInTeiDocument :=

    if($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person[@corresp=$personUriShort]) then()
    else(




         if($teiDoc//tei:listPerson[@type="peopleInDocument"]//tei:person) then(
            update insert ('&#xD;&#xa;',
                functx:change-element-ns-deep($personRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node())
                    following
                 $teiDoc//tei:listPerson[@type="peopleInDocument"]/tei:person[last()]
            )

        else(
                  if($teiDoc//tei:listPerson[@type="peopleInDocument"]) then(
                  update insert functx:change-element-ns-deep($personRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:listPerson[@type="peopleInDocument"]
                 )
                  
                  else


             update insert functx:change-element-ns-deep($personRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:profileDesc
                 )
                 )
let $bondTypeReverseCode := data($teiEditorEgypt:labels//bond[@reverse = $bondType][1]/@type)

let $relationInDocNode :=
                        <node><relation name="{ $bondTypeReverseCode }" active="{ $ownerUriShort }" passive="{ $personUriShort }"/>
</node>
let $insertRelationInDoc :=
            update insert 
            functx:change-element-ns-deep($relationInDocNode, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:listRelation
(:    TODO: update current and Bond People:)
(:        let $updateBond := update:)
        
let $bondNodeForInsertedPerson := <data>
    <snap:hasBond rdf:type="{ $bondType }" rdf:resource="{ $ownerUriShort }"/>
    </data>


let $bondReverseNodeForOwner := <data>
    <snap:hasBond rdf:type="{ $bondTypeReverseCode}" rdf:resource="{ $personUriShort }"/>
    </data>
let $updateInsertedPersonBonds :=
        if($personDetails//snap:hasBond[@rdf:resource= $ownerUriShort][@rdf:type=$bondType])
        then ()
        else
            if($personDetails//snap:hasBond)
                then update insert $bondNodeForInsertedPerson/node() following $personDetails//snap:hasBond[last()]
                else update insert $bondNodeForInsertedPerson/node() into $personDetails//foaf:primaryTopicOf/apc:people
let $updateOwnerBonds :=
        if($ownerDetails//snap:hasBond[@rdf:resource= $personUriShort][@rdf:type=$bondTypeReverseCode])
        then ()
        else
            if($ownerDetails//snap:hasBond)
                then update insert $bondReverseNodeForOwner/node() following $ownerDetails//snap:hasBond[last()]
                else update insert $bondReverseNodeForOwner/node() into $ownerDetails//foaf:primaryTopicOf/apc:people

 let $newContent := util:eval( "collection('" || $teiEditor:doc-collection-path ||"')/id('"
             ||$docId ||"')" )
return
(:    teiEditor:logEvent("document-insert-place" , $docId, $newPlaceRecord, ()),:)
    <result>
        <newContent>{ $newContent }</newContent>
        <newList>{teiEditor:listPeopleManager($docId)}</newList>
        <newListForAnnotation>
            <div id="listOfPeople">{ teiEditor:listPeople($docId, 1) }</div>
        </newListForAnnotation>
        <newListForMentionedNames>
            <div id="newListOfMentionedNames">{ teiEditor:peopleMentionsInDoc($docId) }</div>
        </newListForMentionedNames>
      
{
    if($teiDoc//tei:profileDesc//tei:listPerson//tei:person[contains(./@corresp, $personUri)])
    then 
    (<result>{$personUri} This person is already listed</result>)
    else(     )
}</result>

};

declare function teiEditorEgypt:placesManager($docId){
let $teiDoc := util:eval( "$teiEditor:doc-collection/id('"
             ||$docId ||"')" )
        let $placeOfOrigin := $teiDoc//tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:origin/tei:origPlace
      
      return
        <div class="row">
             
                 <div class="sideToolPane col-sm-3 col-md-3 col-lg-3">
                     <div class="panel panel-default">
                          <div class="panel-body">
                          <h4>List of places linked to this document</h4>
                          {teiEditor:placesList($docId, $teiDoc)}
                          
                      <div id="add-place" class="btn-group xmlToolBar"
                            role="group" aria-label="..." >
                        <div class="panel panel-default">
                           <div class="panel-heading collapsed"  data-toggle="collapse"  href="#add-places-panel">Link a place to the document</div>
                           <div id="add-places-panel" class="panel-collapse">
                                <div class="panel-body">
                                <input type="text" class="form-control hidden"
                                     id="newPlaceUri" 
                                     name="newPlaceUri"
                                     />
                                <span class="subSectionTitle">Search project's places</span>
                                <input type="text" class="form-control projectPlacesLookUp" id="projectPlacesLookUp" name="projectPlacesLookUp" autocomplete="on"
                                placeholder="Start to enter a place name or a place ID"/>
                                <span id="projectPlaceDetailsPreview"/>
                               <span id="newProjectPlaceTypeContainer" class="hidden">
                               {skosThesau:dropDownThesauXML('c22114', 'en', 'Type', 'inline', 1, 1, 'uri')}
                               </span>
                               <span id="newProjectPlaceCertaintyContainer" class="hidden">
                               <input id="newProjectPlaceCertainty" type="checkbox" >Uncertain</input>
                               </span>
                               <button id="addProjectPlaceButtonDocPlaces" class="btn btn-success hidden" onclick="addProjectPlaceToDocument('{$teiEditor:docId}')" appearance="minimal" type="button">Add place to document<i class="glyphicon glyphicon glyphicon-saved"></i></button>
                               <!-- 
                                <div id="placeLookUpPanel" class="sectionInPanel"><span class="subSectionTitle">Add from Pelagios datasets</span>
                             <div class="form-group">
                             
                                    <label for="placesLookupInputDocPlaces">Search in <a href="http://pelagios.org/peripleo/map" target="_blank">Pelagios Peripleo</a></label>
                                     <input type="text" class="form-control"
                                     id="placesLookupInputDocPlaces" 
                                     name="placesLookupInputDocPlaces"
                                     />
                                     
                              </div>
                       <div class="">
                             <iframe id="placesLookupInputDocPlaces_peripleoWidget" allowfullscreen="true" height="380" src="" style="display:none;"> </iframe>
                                     <div id="previewMapDocPlaces" class="hidden"/>
                                     <div id="placePreviewPanelDocPlaces" class="hidden"/>
                                     <span id="newPlaceTypeContainer" class="hidden">
                                     <h4>Add place to document</h4>
                               {skosThesau:dropDownThesauXML('c22114', 'en', 'Relation', 'row', (), 1, ())}
                               </span>
                                     <button id="addNewPlaceButtonDocPlaces" class="btn btn-success hidden" onclick="addPlaceToDoc('{$teiEditor:docId}')" appearance="minimal" type="button">Add <i class="glyphicon glyphicon glyphicon-saved"></i></button>
                         </div>
                     </div>-->
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </div>
         </div>
                    <div class="col-sm-8 col-md-8 col-lg-8">                                      
                   
                   <div id="editorMap"></div>
                               <div id="positionInfo"/>
            <div id="savedPositionInfo">Click to store current position: </div>
                   <!--
                   <div id="mapid"></div>
                   -->
                   </div>

 

   
               </div>
};




declare function teiEditorEgypt:addProjectPlaceToDocument( $data as node(), $project as xs:string){

let $now := fn:current-dateTime()
let $currentUser := data(sm:id()//sm:real//sm:username)

(:let $data := request:get-data():)
(:let $doc-collection := collection($config:data-root || $teiEditor:project || "/documents"):)

let $docId := $data//docId/text()
let $placeUri := $data//placeUri/text()
let $placeUriShort := substring-before($placeUri, "#this")
let $placeType := skosThesau:getLabel($data//placeType/text(), 'xml')
let $placeCertainty := $data//placeCertainty/text()


(:let $docId := request:get-parameter('docid', ()):)
let $teiDoc := $teiEditor:doc-collection/id($docId)
let $placeCollectionProject := collection($teiEditor:data-repository-path|| "/places/" || $project)
let $placeDetails := $placeCollectionProject//spatial:Feature[@rdf:about=$placeUri][1]

let $placeRefinDoc :=
            if(not($teiDoc//tei:fileDesc/tei:sourceDesc//tei:listPlace)) then(
            <nodeToInsert>
        <listPlace>
    <place>
            <placeName ref="{$placeUriShort}{
                if($placeDetails//skos:exactMatch/@rdf:resource!= "") then ( for $exactMatch in functx:distinct-nodes($placeDetails//skos:exactMatch)
                return 
                ' ' || $exactMatch/@rdf:resource 
                ) else()}" ana="{ $placeType }">{$placeDetails//dcterms:title/text()}</placeName>
        </place>
        </listPlace>

    </nodeToInsert>
)
            else
<nodeToInsert>
    <place>
        <placeName ref="{$placeUriShort}{
                if($placeDetails//skos:exactMatch/@rdf:resource != "") then
                ( for $exactMatch in functx:distinct-nodes($placeDetails//skos:exactMatch)
                return 
                ' ' || $exactMatch/@rdf:resource 
                )
                else()}" ana="{ $placeType }">{$placeDetails//dcterms:title/text()}</placeName>
        </place>

    </nodeToInsert>





let $insertRefToPlaceInTeiDocument :=

    if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:listPlace//tei:place[@corresp=$placeUriShort]) then()
    else(



         if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:listPlace//tei:place) then(
            update insert ('&#xD;&#xa;',
                functx:change-element-ns-deep($placeRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node())
                    following
                 $teiDoc//tei:sourceDesc/tei:listPlace//tei:place[last()]
            )
            else(
                  if($teiDoc//tei:fileDesc/tei:sourceDesc//tei:listPlace) then(
                  update insert functx:change-element-ns-deep($placeRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:sourceDesc/tei:listPlace
                 )
                  else

             update insert functx:change-element-ns-deep($placeRefinDoc, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:sourceDesc
                 )
                 )

let $updateProvenance := 
        if(lower-case($placeType) = "provenance") then
            update value $teiDoc//tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:history/tei:provenance/tei:location/tei:placeName/@ref with $placeUriShort
        else ()
let $updateProvenanceFragment := 
        if(lower-case($placeType) = "provenance") then
            update value $teiDoc//tei:msFrag/tei:history//tei:provenance[@type="found"]/tei:placeName/@ref with $placeUriShort
        else ()
let $updateProvenanceFragmentCertainty := 
        if($placeCertainty = "true") then
            update value $teiDoc//tei:msFrag/tei:history//tei:provenance[@type="found"]/@cert with "low"
        else ()



return
(:    teiEditor:logEvent("document-insert-place" , $docId, $newPlaceRecord, ()),:)
    <result>
      <newList>{
        
       teiEditor:placesListNoHeader($docId)
       
      }
      </newList>
        <newListForAnnotation><div id="listOfPlaces">{ teiEditor:placeListForAnnotation($docId, 1)}</div>
      </newListForAnnotation>

    {
    if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:listPlace//tei:place/tei:idno[contains(., $placeUri)])
    then
    (<result>{$placeUri} Place already listed</result>)
    else(
            if($teiDoc//tei:fileDesc/tei:sourceDesc/tei:listPlace//tei:placeName) then(
            <div id="listOfPlacesOverview">{ teiEditor:placesList($docId, ())}</div>
            )
            else(
                  if($teiDoc//tei:fileDesc/tei:sourceDesc//tei:listPlace) then(
              <div id="listOfPlacesOverview">{ teiEditor:placesList($docId, ())}</div>

                 )
                  else

             <div id="listOfPlaces">{ teiEditor:placesList($docId, ())}</div>

                 )
                 )
}</result>

};

declare function teiEditorEgypt:mediaManager($docId as xs:string?){
    let $surfaces := if($docId ="" ) then $teiEditor:teiDoc/tei:facsimile//tei:surface
                                else $teiEditor:doc-collection/id($docId)/tei:facsimile/child::tei:surface
    
    return
    <div id="mediaManager">
    {
    for $surface at $index in $surfaces
        let $surfaceLabel := $surface/tei:desc/text()
            (: functx:capitalize-first(skosThesau:getLabelFromXmlValue($surface/@ana, "fr")) :)

        let $addButton := <button id="graphicAddSurface{ $index }" class="btn btn-primary addItem pull-right"
                        onclick="openAddGraphicToSurfaceDialog('{ data($surface/@xml:id) }', '{ $index }')"
                                appearance="minimal" type="button"><i class="glyphicon glyphicon glyphicon-plus"> image</i></button>

        return
     <div class="sectionPanel col-xs-12">
            <h3>{ $surfaceLabel }{ ""
            (: $addButton  :)
            }</h3>
            <div class="imageGallery">
                {if(count($surface//tei:graphic) = 0) then <strong>Aucune image</strong>
                else 
                for $graphic in $surface//tei:graphic
                    let $imageLegend := $graphic/tei:desc/text()
                    let $imageUrl := $graphic/@url
                    let $imageId := substring-after($imageUrl, "/images/")
                        return
                        <div class="col-lg-3 col-md-4 col-xs-6 thumb panel panel-default"
                            style="background-color: whitesmoke; padding: 0 3px 1em 3px;">
                            <a class="thumbnail" href="" data-image-id="{ $imageId }" data-toggle="modal" data-title="{ $imageLegend }"
                            data-image="/images/thumbnails/{ $imageId }.jpg"
                            data-target="#image-gallery">
                            <input id="radio{ $docId}-{ $imageId}" name="input_thumb_{ $docId }" type="radio" value="{ $imageId}" style="display:inline-block;">
                                <label>vignette principale</label>
                            </input>
                            <img class="img-thumbnail"
                                src="/images/thumbnails/{ $imageId }.jpg"
                                alt="{ $imageId }"
                                style="min-height: 100px; max-height: 300px;"/>
                            </a>
                            
                            <div style="font-size: smaller">{ $imageLegend }</div>
                        </div>
                        }
            </div>
            </div>
            }         
            
            
            <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img id="image-gallery-image" class="img-responsive col-md-12" src=""/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="glyphicon glyphicon-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="glyphicon glyphicon-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
            <!--End of Modal for Gallery -->
            
            <div id="dialogAddGraphicToSurface" title="Add an image" class="modal fade" tabindex="-1" style="display: none;">

            <!-- Modal content-->
            <div class="modal-content modal4editor">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"/>
                    <h4 class="modal-title">Add an image</h4>
                </div>
                <div class="modal-body">
                         <div class="form-group">
                                <label for="addGraphicToSurfaceURL">URL</label>
                                <input type="text" class="form-control" id="addGraphicToSurfaceURL" name="addGraphicToSurfaceURL"
                                data-error="Please enter URL"/>
                            </div>
                            <div class="form-group">
                                <label for="addGraphicToSurfaceCollection">Fonds d'archive</label>
                                <input type="text" class="form-control" id="addGraphicToSurfaceCollection" name="addGraphicToSurfaceCollection"
                                data-error="Please enter a collection."/>
                            </div>
                            <div class="form-group">
                                <label for="addGraphicToSurfaceAuthor">Author</label>
                                <input type="text" class="form-control" id="addGraphicToSurfaceAuthor" name="addGraphicToSurfaceAuthor"
                                data-error="Please enter an author name"/>
                            </div>
                            <div class="form-group">
                                <label for="addGraphicToSurfaceCopyright">Copyright</label>
                                <input type="text" class="form-control" id="addGraphicToSurfaceCopyright" name="addGraphicToSurfaceCopyright"
                                data-error="Please enter a copyright."/>
                            </div>
                            
                
                            <input id="addGraphicSurfaceId" class="valueField hidden"/>
                            <input id="addGraphicSurfaceIndex" class="valueField hidden"/>
                        <div class="modal-footer">


                        <button  class="btn btn-primary pull-left" type="submit" onclick="addGraphicToSurface()">Add</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                  
                    <!--                <a href="javascript: void(0);" onclick="tests()">Test</a>        -->

                </div>
                </div>
            </div><!--End of Modal for adding graphic -->
            
            </div>
};

declare function teiEditorEgypt:addGraphicToSurface($data as node(), $project as xs:string){
    let $now := fn:current-dateTime()
    let $currentUser := data(sm:id()//sm:username)
    let $docId := $data//docId/text()
    
    let $graphicToInsert :=
    <node><graphic url="{ $data//graphicUrl/text() }">
               <desc type="collection">{ $data//collection/text() }</desc>
               <desc type="copyright">{ $data//graphicCopyright/text() }</desc>
               <desc type="author">{ $data//graphicAuthor/text() }</desc>
               <desc type="licence">This work is licensed under a <ref target="https://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</ref></desc>
            </graphic>
                </node>
    let $surfaceNode := util:eval( "$teiEditor:doc-collection/id('" ||$docId ||"')//tei:surface[@xml:id='" || $data//graphicSurfaceId/text() || "']")            
    
    
    let $insertImage :=
            update insert functx:change-element-ns-deep($graphicToInsert, "http://www.tei-c.org/ns/1.0", "")/node()
                    into $surfaceNode
    
    return
    <data>
    <updatedElement>{ teiEditorEgypt:mediaManager($docId)}</updatedElement>
    
    </data>

};
