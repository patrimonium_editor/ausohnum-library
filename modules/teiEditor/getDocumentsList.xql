xquery version "3.1";
import module namespace functx="http://www.functx.com";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:indent "yes";
declare option output:method "json";
declare option output:media-type "text/javascript";
declare option output:json-ignore-whitespace-text-nodes "yes";
declare variable $project := request:get-parameter("project", ());
declare variable $filteringPlaces := request:get-parameter("filteringPlaces", ());
declare variable $list:= doc("/db/apps/" || $project || "Data/lists/list-documents.xml");

if(not($filteringPlaces) )
    then $list/documentsList/root

    else 
<root>{ ($filteringPlaces)}{ $project }
{ $list//data[functx:contains-any-of(regionUri, $filteringPlaces)]}
</root>
