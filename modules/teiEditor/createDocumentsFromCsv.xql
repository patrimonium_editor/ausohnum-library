(:~
: AusoHNum Library - script to create a collection of documents from a csv file
: @author Vincent Razanajao
: Names of columns in the csv file can be (order and presence/absence don't matter):
:           inv_num, inv_num_other, title,
:           links, phi_no, phi_url, ig_num, ig_url
:           provenance_uri, provenance_name, provenance_cert
:           origin_uri, origin_name, origin_cert
:           material, frag_number
:           private_commentary
: Data related to Repositories is not covered.
:)

xquery version "3.1";

declare boundary-space preserve;

import module namespace functx="http://www.functx.com";

(:import module namespace kwic="http://exist-db.org/xquery/kwic";:)
import module namespace xmldb="http://exist-db.org/xquery/xmldb";
import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/" at "/db/apps/ausohnum-library/modules/skosThesau/skosThesauApp.xql";
import module namespace http="http://expath.org/ns/http-client" at "java:org.expath.exist.HttpClientModule";
declare namespace err = "http://www.w3.org/2005/xqt-errors";
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace rdfs="http://www.w3.org/2000/01/rdf-schema#";
declare namespace geo="http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace lawdi="http://lawd.info/ontology/";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace spatial="http://geovocab.org/spatial#";
declare namespace foaf="http://xmlns.com/foaf/0.1/";

declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos="http://www.w3.org/2004/02/skos/core#";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace local="local";

declare variable $project :=request:get-parameter('project', "gymnasia");
declare variable $appVariables := doc("/db/apps/" || $project || "/data/app-general-parameters.xml");
declare variable $importFilename := request:get-parameter("importFilename", "Samos-import.tsv");
declare variable $separator := request:get-parameter("separator", "tab");
declare variable $fragmentHandling := request:get-parameter("fragmentHandling", "tab");
declare variable $documentPrefix := $appVariables//idPrefix[@type="document"]/text();
declare variable $doc-collection-path := "/db/apps/" || $project || "Data/documents/";
declare variable $newDoc-collection-pathTarget := "/db/apps/" || $project || "Data/documents/documents-vrazanajao/";


declare variable $path2Data := "/db/apps/" || $project || "Data";

declare variable $objectRepositoriesUri := $appVariables//objectRepositoriesUri/text();
declare variable $thesaurus := collection("/db/apps/" || $project || "Data/concepts");
declare variable $thesaurusMaterial := skosThesau:getChildren("https://lead.ifao.egnet.net/concept/c22227", "lead");
declare variable $tab := '&#9;'; (: tab :)
declare variable $nl := "&#10;";
declare variable $statueTypes :=doc($path2Data || "/imports/statueTypes.xml");
declare variable $places :=collection($path2Data || "/places/"|| $project);
declare variable $placesInDocs := doc($path2Data || "/imports/louvrePlaces.xml");
declare variable $lang := "fr";
declare variable $now := fn:current-dateTime();

declare option output:omit-xml-declaration "no";

declare function local:importFromExternalResource($externalDocUri as xs:string){
    try{
    let $url4httpRequest := 
                    if($externalDocUri !='' ) then 
                               (if(contains($externalDocUri, 'http://telota.bbaw.de/ig/digitale-edition')) then
                                   "http://telota.bbaw.de/ig/api/xml/" ||
                                            substring-after($externalDocUri, "digitale-edition/inschrift/")
                                else if(contains($externalDocUri, 'edh-www.adw.uni-heidelberg.de/edh/inschrift/')) then
                                            $externalDocUri || ".xml"
                               else if(contains($externalDocUri, 'http://papyri.info/ddbdp')) then
                                            $externalDocUri || "/source"
                               else if(contains($externalDocUri, 'http://mama.csad.ox.ac.uk/monuments/MAMA-XI')) then
                                            (
                                            let $externalDocId := substring-before(substring-after($externalDocUri, "http://mama.csad.ox.ac.uk/monuments/"), ".html")
                                            return 
                                            "http://mama.csad.ox.ac.uk/xml/" || $externalDocId || ".xml"
                                            )
                                            else $externalDocUri)
                            else ()
let $http-request-data := <request xmlns="http://expath.org/ns/http-client"
    method="get" href="{$url4httpRequest}">
        <header name="Content-Type" value="text/xml; charset=utf-8/"/>
    </request>

let $responses :=
    http:send-request($http-request-data)

let $response :=
    <results>
      {if ($responses[1]/@status ne '200')
         then
             <failurea>{$responses[1]}</failurea>
         else
           <success>
             {$responses[2]}
             {'' (: todo - use string to JSON serializer lib here :) }
           </success>
      }
    </results>
let $params :=
        <output:serialization-parameters
        xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
            <output:omit-xml-declaration value="yes"/>
            <output:method value="xml"/>
            <output:indent value="no"/>
            <output:item-separator value=""/>
            <output:undeclare-prefixes value="NO"/>
        </output:serialization-parameters>
let $params :=
        <output:serialization-parameters
        xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
            <output:omit-xml-declaration value="yes"/>
            <output:method value="xml"/>
            <output:indent value="no"/>
            <output:item-separator value="\n"/>
            <output:undeclare-prefixes value="no"/>
        </output:serialization-parameters>
let $paramMap :=
        map {
            "method": "xml",
            "indent": true(),
            "item-separator": ""
}
        
let $textDiv := $response//tei:div[@type='edition']
let $apparatusDiv := $response//tei:div[@type='apparatus']
let $noOfTextpart := count($textDiv//tei:div[@type="textpart"])
let $checkIfAbIsChildOfTextPart := exists($textDiv//tei:div[@type="textpart"]/tei:ab)
let $reconstructedDivEdition :=
        if($checkIfAbIsChildOfTextPart =true()) then $textDiv
            else
                <div type="edition">
              {for $textPart in $textDiv/self::tei:div/tei:ab
                    return 
                        element {"tei:div"}
                        {
                        attribute {"type"} {"textpart"},
                        if(exists($textPart/@subtype)) then 
                            attribute {"subtype"} { $textPart/@subtype}
                            else()
                        ,
                        if(exists($textPart/@n)) then 
                            attribute {"n"} { $textPart/@n}
                            else(),
                        if(exists($textPart/tei:div)) then
                                local:convert2Epidoc(serialize(($textPart/tei:div/node()), $params))
                                else local:convert2Epidoc(serialize(($textPart/node()), $params))
                        
(:                         local:convert2Epidoc($textPart):)
                }}{ $nl}
                </div>
    return ($reconstructedDivEdition)
    } catch*{<error>{ $err:code || '&#10;' ||
   $err:description || '&#10;' ||
   $err:value || '&#10;' ||
   $err:module || '&#10;' ||
   $err:line-number || '&#10;' ||
   $err:additional}</error>}
   };

declare function local:convert2Epidoc($text as xs:string){
  let $text := functx:trim($text)
 (:let $text := replace($text, "\n", "NEWLINE"):)
   
(:End of line in lacuna without ending bracket at the end – – – – :)
    => replace("(\w)(\s–){1,40}\]\\n", '$1<gap reason="lost" extent="unknown" unit="character"/>')   
(:End of line in lacuna without ending bracket at the end – – – – :)
    => replace("(\w)(\s–){1,40}\\n", '$1<gap reason="lost" extent="unknown" unit="character"/>')
(:Start of line in lacuna without ending bracket at the end – – – – :)
    => replace("\\n(–((\s–){1,40})([^\]]))", '<gap reason="lost" extent="unknown" unit="character"/>$1')
 
(:Line in lacuna – – – – at end of text:)
    => replace("<lb/>(\s?–\s–){1,}</ab>", '<gap reason="lost" extent="unknown" unit="line"/></ab>')

(:Line in lacuna – – – – at beginning of text:)
    =>replace("<lb/>(–\s–\s?){1,}", '<gap reason="lost" extent="unknown" unit="line"/>')

(: Vacat. <hi rend="smallit"> vac.</hi>:)
    => replace('<hi rend="smallit"> vac.</hi>',
     '<space extent="unknown" unit="character"/>')

(:cleaning full line in lacuna:)
    => replace("(/>.?<gap)", "/><gap")
(:cleaning full line in lacuna at end of text:)
    => replace("/>–</ab>", "/></ab>")
    
(:Correcting linebreak, ending with character:)
    => replace('([\w])–<lb\sn="([0-9]*)"\s?/>', '$1<lb n="$2" break="no"/>')
  
(:Correcting linebreak, ending with ]:)
    => replace('(\])–<lb\sn="([0-9]*)"\s?/>', '$1<lb n="$2" break="no"/>')
(:Correcting linebreak, in word:)
    => replace('–\\[n]<lb (xmlns="http://www.tei-c.org/ns/1.0" )?n="(\d*)"/>', '<lb n="$2" break="no"/>')
(:Start of line in lacuna no closing square bracket and no reconstructed text:)
(:
 – – – – – – – – – – – – – – – – – – – – – – – – –ν·
 : ]:)

    => replace("(>(–\s–\s?){1,}\s–)", '><gap reason="lost" extent="unknown" unit="character"/>')



(:End of line in lacuna without ending bracket at the end – – – – and with weird character at start:)
    => replace("󰁰(\s–){1,}", '<gap reason="lost" extent="unknown" unit="character"/>')


(:End of line in lacuna with some reconstructions:)
(:σιλ̣[εῖ – – – –]:)
    => replace("(\w*)\[(\w*)(\s–){1,20}\]", '$1<supplied reason="lost">$2</supplied><gap reason="lost" extent="unknown" unit="character" />')

        (:End of line in lacuna with some reconstructions, and no closing square bracket:)
        (:σιλ̣[εῖ – – –]:)

        (:let $text :=:)
        (:    replace($text, "(\w*)\[(\w*)(\s–){1,20}", '$1<supplied reason="lost">$2</supplied><gap reason="lost" extent="unknown" unit="characterXXX" />'):)
        
(: [.]:)
    => replace("\[\.\]", '<gap reason="lost" quantity="1" unit="character"/>')


(: [ . οἱ παῖ]δ̣ες:)
=> replace("\[\s\.\s(.*)\]", '<gap reason="lost" quantity="1" unit="character"/><supplied reason="lost">$1</supplied>')

(:[ .. ] βουλή:)
=> replace("\[\s\.\.\s\]", '<gap reason="lost" extent="unknown" unit="character"/>')

(: [ .. ἐπ]ὶ Δάλιον καὶ:)
=> replace("\[\s\.\.\s(\w*)\]", '<gap reason="lost" extent="unknown" unit="character"/><supplied reason="lost">$1</supplied>')

(:[οις – – – – – – – – – – – – – – – – – – – βασιλέως] Ἀττάλου οὐκ ἐλάσσω τα̣:)
=> replace("\[([α-ωΑ-Ω\s][^\s]*)((\s?–\s){1,20})([α-ωΑ-Ω\s][^\s]*)\]",
           '<supplied reason="lost">$1</supplied><gap reason="lost" extent="unknown" unit="character"/><supplied reason="lost">$4</supplied>') 

(:Simple [τοῖς π]αισὶ:)
=> replace("\[((\w*[^\]])*)\]", '<supplied reason="lost">$1</supplied>')

(:Patch to correct start of line in lacuna with opening square bracket and reconstructed text:)
(:
 [– – – – – – – – – – – – – – – – – – – – – εὔνους ὑ]πάρχων
 : ]:)
 

    => replace('(<supplied reason="lost">)(–\s?–\s?){1,}', '<gap reason="lost" extent="unknown" unit="character"/>$1')

(:Correcting hyphen after opening supplied when preceded by a gap:)
    =>
    replace('(<supplied reason="lost">)–\s?', '$1')


let $text :=
    replace(replace($text, "\[", '<supplied reason="lost">'), "\]", "</supplied>")



(:Correcting end of lacuna:)
    => replace('<gap reason="lost" extent="unknown" unit="character"/> (–\s–\s?){1,}–</supplied>', '</supplied><gap reason="lost" extent="unknown" unit="character"/>')
(:Correcting gap inside supplied:)
    => replace('<gap reason="lost" extent="unknown" unit="character"/></supplied>', '</supplied><gap reason="lost" extent="unknown" unit="character"/>')
(:Correcting lacuna in supplied:)
    => replace('(–\s–\s?){1,}–</supplied>', '</supplied><gap reason="lost" extent="unknown" unit="character"/>')
    
(:Correcting lacuna in supplied:)
    => replace('(</supplied>(\w*))\s?</supplied><gap reason="lost" extent="unknown" unit="character"/>', '$1<gap reason="lost" extent="unknown" unit="character"/>')

    => replace("〚<gap", "<gap")
(: Coorection unclosed supplied before gap   :)
    =>replace('(<supplied reason="lost">(([\w\s]*)))–?<gap ', '$1</supplied><gap ')
   => replace('<gap reason="lost" extent="unknown" unit="character"/> –〛', '<gap reason="lost" extent="unknown" unit="character"/>')
   
(:Correcting <supplied reason="lost">– – – – – παρα</supplied>:)
    => replace('<supplied reason="lost">(–(\s–){1,})(\s)(.*)</supplied>', '<gap reason="lost" extent="unknown" unit="character"/><supplied reason="lost">$4</supplied>')
(:Correcting <supplied reason="lost">– – – – – παρα OTHER WORDS</supplied>:)
    => replace('<supplied reason="lost">(–(\s–){1,})(\s)(\w)', '<gap reason="lost" extent="unknown" unit="character"/><supplied reason="lost">$4') 
(:     〚 – – – – – – – – – – – – – –〛.   :)
    => replace('〚 ((–\s)*)–〛', '<gap reason="lost" extent="unknown" unit="character"/>')

(:Correcdting double gap:)
    =>replace('<gap reason="lost" extent="unknown" unit="character"/><gap reason="lost" extent="unknown" unit="character"/>',
    '<gap reason="lost" extent="unknown" unit="character"/>')

(:Unclear characters with dot:)
=> replace("(\w?)̣",
     '<unclear>$1</unclear>')

(:supralinear lines:)
=> replace("(\w?)̅",
     '<hi rend="supraline">$1</hi>')

(:lb preceded by new lines:)
    => replace("<lb",
     $nl ||'<lb')
 (:Omitted letters]:)
    =>replace('<(\w{1,2})>', '<supplied reason="omitted">$1</supplied>')    
   =>replace('"/>\\n', '"/>')
   let $text:=  '<ab>' || $text || $nl || '</ab>'
            
return 
    parse-xml($text)
(:        "&lt;ab&gt;" :)
            (:        || $newLine :)
         
            (:        || $newLine :)
(:        || "&lt;/ab&gt;":)
         
(:return $text:)
    
};




let $csvFile := util:binary-to-string(util:binary-doc("/db/apps/" || $project || "Data/imports/" || $importFilename))
let $csvFileLines := tokenize($csvFile, "\n")
let $labels := tokenize($csvFileLines[1], $tab)
let $data4Import := (
        <results>{
            for $line in $csvFileLines[position() >1]
                let $fields := tokenize($line, $tab)
                return <document>{
                    for $field at $pos in $fields
                    return
                      element {lower-case(normalize-space($labels[$pos]))} {normalize-space($field)}
                      }</document>
            }</results>)
let $teiTemplate:= (doc("/db/apps/" || $project ||"/data/teiEditor/docTemplates/teiTemplate" || functx:capitalize-first($project) || ".xml")/tei:TEI)
let $teiTemplateUri := xs:anyURI("https://zenodo.org/record/4385455/files/ISic1080.xml?download=1")
let $contents := functx:change-element-ns-deep( $teiTemplate , "http://www.tei-c.org/ns/1.0", "")

let $docIdList := for $id in collection($doc-collection-path)//tei:TEI[contains(./@xml:id, $documentPrefix)]
        return
        <item>
        {substring-after($id/@xml:id, $documentPrefix)}
        </item>
let $last-id:= fn:max($docIdList)

let $createDoc :=
for $document at $pos in $data4Import//document[position() <10]
    
    let $docId :=  $documentPrefix || fn:sum(($last-id, $pos))
    let $invNo := $document/inv_num/text()
    let $otherNo := $document/inv_num_others/text()
    let $title := $document/title/text()
    
    let $filename := $docId 
    let $storeNewFile := xmldb:store($newDoc-collection-pathTarget, ($filename || ".xml"),  $contents)
         
            

    let $changeMod := sm:chmod(xs:anyURI(concat($newDoc-collection-pathTarget, $filename || ".xml")), "rw-rw-r--")
(:    let $changeGroup := sm:chgrp(xs:anyURI(concat($doc-collection-path, "/", $filename)), "documents-ybroux"):)
    let $teiDoc := doc($newDoc-collection-pathTarget || $filename || ".xml")
    let $updateId := update replace  $teiDoc/*:TEI/@xml:id
                            with $docId
    let $updateTitle := if($title != "") then update value $teiDoc//*:teiHeader/*:fileDesc/*:titleStmt/*:title/text() with $title 
        else()
    let $updateSurfaceId :=
        for $surface in $teiDoc//tei:surface
            return 
            update value $surface/@xml:id with replace($surface/@xml:id, "docid", $docId)
                   
    (:let $updateIdnoFileName := update value $teiDoc//tei:fileDesc/tei:publicationStmt/tei:idno[@type="filename"] with $filename
    let $updateIdnoURI := update value $teiDoc//tei:fileDesc/tei:publicationStmt/tei:idno[@type="uri"] with $appVariables//uriBase[@type="app"]/text()|| "/documents/"|| $docId
    let $updateMuseumTownUri := if($townUri !="") then (update value $teiDoc//tei:msDesc/tei:msFrag/tei:msIdentifier/tei:settlement/@ref with $townUri) else ()
    let $updateMuseumTowName := if($townName !="") then (update value $teiDoc//tei:msDesc/tei:msFrag/tei:msIdentifier/tei:settlement/tei:placeName with $townName)
    let $updateMuseumRepoName := if($museumName !="") then (update value $teiDoc//tei:msDesc/tei:msFrag/tei:msIdentifier/tei:repository with $museumName)
    let $updateMuseumRepoUri := if($museumUri !="") then (update value $teiDoc//tei:msDesc/tei:msFrag/tei:msIdentifier/tei:repository/@ref with $museumUri)
    let $updateinvNo := if($invNo !="") then (update value $teiDoc//tei:msDesc/tei:msFrag//tei:msIdentifier/tei:idno[@type="invNo"] with $invNo):)
    
    
    (:let $updateAltInvNo :=
                if($otherNo != "") then
                for $invNo at $pos in tokenize($otherNo, "\|")
                    return 
                        if($pos < 2 ) then
                            (update value $teiDoc//tei:msFrag[1]/tei:msIdentifier/tei:altIdentifier/tei:idno[@type="invNo"]
                             with $invNo)
                        else
                            let $node:= <node>
                     <altIdentifier>
                        <idno type="invNo">{ $invNo }</idno>
                     </altIdentifier></node>
                            return
                            update insert functx:change-element-ns-deep($node, "http://www.tei-c.org/ns/1.0", "")/node()
                            following $teiDoc//tei:sourceDesc/tei:msDesc/tei:msFrag[1]/tei:msIdentifier/tei:altIdentifier
                else():)
    let $updateAltIdForOnlineCatalogue :=
                if($document//links != "") then
                for $link at $pos in tokenize($document/link, "\|")
                    return 
                        let $node:= <node>
                     <altIdentifier>
                        <idno type="uri" subtype="online-museum-collection">{ $link }</idno>
                     </altIdentifier></node>
                        return
                        update insert functx:change-element-ns-deep($node, "http://www.tei-c.org/ns/1.0", "")/node()
                        following $teiDoc//tei:sourceDesc/tei:msDesc/tei:msFrag[1]/tei:msIdentifier/tei:altIdentifier[last()]
                else()
    let $dateRaw := $document/date/text()
    let $datePrecision := $document/date_precision
    let $dateNote:= (if($dateRaw != "") then 
        for $seq at $pos in tokenize($dateRaw, "\|")
                        let $precision := tokenize($datePrecision, "\|")[$pos]
                    return $seq || ( if($precision != "") then " (" || $precision || ")"
                                    else "")
                    else())
                    =>string-join(", ")
                    
    let $dateUri:= 
        if(contains($dateRaw, "|")) then
            ($thesaurus//skos:Concept[contains(.//skos:prefLabel, substring-before($dateRaw, "|"))]/@rdf:about,
            $thesaurus//skos:Concept[contains(.//skos:prefLabel, substring-after($dateRaw, "|"))]/@rdf:about)
        else $thesaurus//skos:Concept[contains(.//skos:prefLabel, $dateRaw)]/@rdf:about
    let $dateLog := $invNo || $tab || $dateRaw || $tab || string-join($dateUri, ", ")
    let $updateOrigDate := 
                if($dateUri != "") then
                    (update value $teiDoc//tei:sourceDesc/tei:msDesc/tei:history//tei:origDate/@period
                        with string-join($dateUri, " "),
                    update value $teiDoc//tei:sourceDesc/tei:msDesc/tei:history//tei:origDate
                        with ($dateNote, " "),
(:                        (if($document/data_precision != "") then:)
(:                        update value $teiDoc//tei:sourceDesc/tei:msDesc/tei:history//tei:origDate/@precision:)
(:                        with "low":)
(:                        else()),:)
                    (if($document/data_cert != "") then
                        update value $teiDoc//tei:sourceDesc/tei:msDesc/tei:history//tei:origDate/@cert
                        with "low"
                        else())
                        )
                    else ()
    let $notBefore := $document//date_not_before
    let $notAfter := $document//date_not_after
    let $updateNotBefore := 
                if($notBefore != "") then
                    (update value $teiDoc//tei:sourceDesc/tei:msDesc/tei:history//tei:origDate/@notBefore-custom
                        with $notBefore)
                else()
    let $updateNotAfter := 
                if($notAfter != "") then
                    (update value $teiDoc//tei:sourceDesc/tei:msDesc/tei:history//tei:origDate/@notAfter-custom
                        with $notAfter)
                else()
(:PROVENANCE:)
     let $provenanceUri := $document//provenance_uri/text()
     
     let $provenanceCert := $document//provenance_cert/text()
     let $provenancePlace := if($provenanceUri != "") then ($places//spatial:Feature[@rdf:about = $provenanceUri|| "#this" ]
(:    ,:)
(:                        $places//spatial:Feature[matches(.//skos:altLabel, $placeReg, "i")]:)
                        )
                        else()
    
    let $provenanceUris := string-join((
        substring-before($provenancePlace/@rdf:about, "#this"),
        $provenancePlace//skos:exactMatch/@rdf:resource), " ")
    let $provenancePlaceName:= if($provenancePlace//skos:altLabel[@xml:lang=$lang]/text() != "") then $provenancePlace//skos:altLabel[@xml:lang=$lang]/text()
        else $provenancePlace//dcterms:title/text()
    let $updateProvenance:= 
        if($provenanceUri !="") then
           (
                if(contains(lower-case($provenanceUri), "unknown")) then 
                     (update value $teiDoc//tei:provenance[@type="found"] with "Exact provenance unknown",
                     update delete $teiDoc//tei:provenance[@type="found"]/@subtype)
                 else 
                 (
                 update value $teiDoc//tei:provenance[@type="found"]/tei:placeName/@ref with $provenanceUris,
                         update value $teiDoc//tei:provenance[@type="found"]/tei:placeName with $provenancePlaceName,
                         (if($provenanceCert ="probablement") then
                             update value $teiDoc//tei:provenance[@type="found"]/@cert with "low" else ()
                          )
                  )
             )
         else()     
               
(:ORIGIN:)
     let $originUri := $document//origin_uri/text()
     
     let $originCert := $document//origin_cert/text()
     let $originPlace := if($originUri != "") then ($places//spatial:Feature[@rdf:about = $originUri|| "#this" ]
(:    ,:)
(:                        $places//spatial:Feature[matches(.//skos:altLabel, $placeReg, "i")]:)
                        )
    else()
    
    let $originUris := string-join((
        substring-before($originPlace/@rdf:about, "#this"),
        $originPlace//skos:exactMatch/@rdf:resource), " ")
    let $originPlaceName:= if($originPlace//skos:altLabel[@xml:lang=$lang]/text() != "") then $originPlace//skos:altLabel[@xml:lang=$lang]/text()
        else $originPlace//dcterms:title/text()
    let $updateOrigin:= if($originUri !="")              
                    then
                    (   update value $teiDoc//tei:origPlace/tei:placeName/@ref with $originUris,
                        (update value $teiDoc//tei:origPlace/tei:placeName with $originPlaceName
                            ),
                        (if($originCert ="low") then
                            update value $teiDoc//tei:origPlace/@cert with "low" else ())
                    )
                    else()
    let $provenanceListNode := <nodeToInsert>
    <place>
        <placeName ref="{$provenanceUris}" ana="provenance">{$provenancePlaceName}</placeName>
    </place>
    
    </nodeToInsert>
    let $originListNode := <nodeToInsert>
    <place>
        <placeName ref="{$originUris}" ana="origin">{$originPlaceName}</placeName>
    </place>

    </nodeToInsert>
    let $updatePlaceList :=
        if($provenanceUri != "") then (update insert functx:change-element-ns-deep($provenanceListNode, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:sourceDesc/tei:listPlace
                 )
        else if($originUri != "") then (update insert functx:change-element-ns-deep($originListNode, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:sourceDesc/tei:listPlace
                 )
       else ()
     
     
     
     let $materialRaw:= $document//material/text()
     let $material := if($materialRaw != "") then 
                $thesaurusMaterial//skos:Concept[.//skos:prefLabel[functx:contains-case-insensitive(., $materialRaw)] | .//skos:altLabel[functx:contains-case-insensitive(., $materialRaw)]]
                else()
            let $updateMaterial := 
                if($material != "") then
                    (update insert attribute ref {$material[1]/@rdf:about}
                    into $teiDoc//tei:fileDesc/tei:sourceDesc//tei:physDesc//tei:material
                 ) else ()
    
     
                    
let $addExtramsFrag := if(xs:int($document/fragment_number) >1) then
                            for $index in 1 to (xs:int($document/nombre-fragment)-1)
                                let $msFrag:=<node>
               { $teiTemplate//tei:msFrag }</node>
                                return update insert functx:change-element-ns-deep($msFrag , "http://www.tei-c.org/ns/1.0", "")/node() following $teiDoc//tei:msFrag[last()]
                        else()
let $updatePrivateCommentary := if($document//private_commentary != "") then
            update value $teiDoc//tei:div[@type="privateCommentary"] with $document//private_commentary/text() else()
let $updateLayoutID := update replace  $teiDoc//tei:objectDesc/tei:layoutDesc/tei:layout/@xml:id
                            with $docId || "-layout1"
                            
                            
let $updateLayoutCorresp := update replace $teiDoc//tei:physDesc/tei:objectDesc/tei:layoutDesc/tei:layout/@corresp
                            with "#" || $docId || "-surface1"

let $updateMsItemID := update replace  $teiDoc//tei:msContents/tei:msItem/@xml:id
                            with $docId || "-msItem1"
let $updateDivPartCorresp := update replace $teiDoc//tei:div[@type="textpart"]/@corresp
                            with "#" || $docId || "-surface1"


let $creationNode := <node><change who="#vrazanajao" when="{$now}">Creation of this file from data provided as spreadsheet by Matthias Pichler.</change>
    </node>

let $updateCreationChange := update insert functx:change-element-ns-deep($creationNode/node(), "http://www.tei-c.org/ns/1.0", "") into $teiDoc//tei:revisionDesc/tei:listChange
                                
    let $updatePHIURI :=
        if($document//phi_url != "") then 
            let $node2Insert := <node>
                <idno type="uri" subtype="phi">{ normalize-space($document//phi_url)}</idno></node>
                return
                    update insert functx:change-element-ns-deep($node2Insert, "http://www.tei-c.org/ns/1.0", "")/node() following
                 $teiDoc//tei:publicationStmt//tei:idno[last()]
        else()
    let $updateIGURL :=
        if($document//ig_url != "") then 
            let $node2Insert := <node>
                <idno type="uri" subtype="ig">{ normalize-space($document//ig_url)}</idno>
                    </node>
                return
                    update insert functx:change-element-ns-deep($node2Insert, "http://www.tei-c.org/ns/1.0", "")/node() following
                 $teiDoc//tei:publicationStmt//tei:idno[last()]
        else()
    let $updateIGno :=
        if($document//ig_no != "") then 
            let $node2Insert := <node>
                <idno type="id" subtype="ig">{ normalize-space($document//ig_no/text())}</idno>
                    </node>
                return
                    update insert functx:change-element-ns-deep($node2Insert, "http://www.tei-c.org/ns/1.0", "")/node() following
                 $teiDoc//tei:publicationStmt//tei:idno[last()]
        else()
let $editionZoteroUri:= normalize-space($document//edition_zotero_uri)
let $updateEdition:= if($editionZoteroUri != "") then
                    (let $editionNode := <node>
                    <bibl>
                         <ptr target="{ $editionZoteroUri }"/>,
                         <citedRange>{ normalize-space($document//edition_range)}</citedRange>
                    </bibl>
                    </node>
                        return
                    update insert functx:change-element-ns-deep($editionNode, "http://www.tei-c.org/ns/1.0", "")/node() into
                 $teiDoc//tei:div[@type="bibliography"][@subtype="edition"]/tei:listBibl
                 ) else ()
   
   
    let $uriForTextImport :=
        if($document//ig_url != "")
            then $document//ig_url/text()
            else if($document//phi_url != "")
            then $document//phi_url/text()
            else ""
            
    let $updateTextEdition:= if($uriForTextImport !="") then
            let $text:= local:importFromExternalResource($uriForTextImport)
            return
                if($text!="") then 
                    update replace $teiDoc//tei:div[@type="edition"] with functx:change-element-ns-deep( $text , "http://www.tei-c.org/ns/1.0", "")
                else()
        else()
    
    
    return 
    $uriForTextImport ||
        $invNo || $tab 
(:         if($provenanceUri = "") then $placeRaw else ():)
(:        "import: " ||$placeRaw || $tab || $place[1]//dcterms:title || $tab || $placeUris || $tab || $placeCert:)
 || $materialRaw || $tab || $material[1]/@rdf:about || $tab 
         || $nl
return 
    <results>{ $createDoc }</results>