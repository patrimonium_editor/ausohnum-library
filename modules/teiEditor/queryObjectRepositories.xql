(:~
: AusoHNum Library - prosopographical module
: This function queries the repositories in thesaurus
: @author Vincent Razanajao
: @param name of the project
: @param string to be searched
:)

xquery version "3.1";

import module namespace functx="http://www.functx.com";

import module namespace kwic="http://exist-db.org/xquery/kwic";
import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/" at "../skosThesau/skosThesauApp.xql";

declare namespace apc="http://patrimonium.huma-num.fr/onto#";
declare namespace dcterms="http://purl.org/dc/terms/";

declare namespace foaf="http://xmlns.com/foaf/0.1/";
declare namespace geo = "http://www.w3.org/2003/01/geo/wgs84_pos#";

declare namespace json="http://www.json.org";
declare namespace lawd="http://lawd.info/ontology/";
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";

declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

declare namespace skos = "http://www.w3.org/2004/02/skos/core#";
declare namespace snap="http://onto.snapdrgn.net/snap#";
declare namespace spatial="http://geovocab.org/spatial#";


declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

(: Switch to JSON serialization :)
declare option output:method "json";
(:declare option output:media-type "application/xhtml";:)
declare option output:indent "yes";

declare variable $project :=request:get-parameter('project', ());
declare variable $query :=request:get-parameter('query', ());
declare variable $lang :=request:get-parameter('lang', ());
declare variable $appVariables := doc("/db/apps/" || $project || "/data/app-general-parameters.xml");
declare variable $objectRepositoriesUri := $appVariables//objectRepositoriesUri/text();

let $repository-collection := skosThesau:getChildren($objectRepositoriesUri, $project)

let $matchingQuery :=
            for $match in (
                    $repository-collection//skos:Concept[not(ancestor-or-self::skos:exactMatch)]
                                    [skos:prefLabel[matches(lower-case(.), lower-case($query))]][not(child::skos:rarrower)]
                   |$repository-collection//skos:Concept[not(ancestor-or-self::skos:exactMatch)][not(child::skos:narrower)]
                   [skos:altLabel[matches(lower-case(.), lower-case($query))]][not(child::skos:rarrower)]
                   )
                   return
                   if($match//skos:narrower) then()
                    else $match/@rdf:about
let $townMatching := 
                for $town in $repository-collection//skos:Concept[not(ancestor-or-self::skos:exactMatch)][child::skos:narrower]
                                    [skos:prefLabel[matches(lower-case(.), lower-case($query))]]                  
                            return
                                for $nt in $town//skos:narrower
                                    return $nt/@rdf:resource 
                                    
let $matchedConcepturis := distinct-values(($matchingQuery, $townMatching))
let $matchedConcepts := for $concept in $matchedConcepturis
            return $repository-collection//skos:Concept[@rdf:about = $concept]
          
                                        
return
 <data>
    <when>{fn:current-dateTime()}</when>
    <list>
    <items>
        <identifier></identifier>
        <searchResult>Search "{$query}" returns {count($matchedConcepturis)} result{if(count($matchedConcepturis) >1) then "s" else ()}</searchResult>
       <title></title>
        <object_type></object_type>
        <dataset_path>
            <title>Patrimonium Functions</title>
            <id>Patrimonium Functions</id>
        </dataset_path>
        <exactMatch></exactMatch>
      </items>

{

for $hit in $matchedConcepts
(:for $hit in ($matchingQuery, $townMatching):)
(:order by ft:score($hit) descending:)
    let $title := if($hit/skos:prefLabel[@xml:lang = $lang]) then $hit/skos:prefLabel[@xml:lang = $lang]/text()
                else $hit/skos:prefLabel[matches(lower-case(.), lower-case($query))][1]/text()
    let $identifier := data($hit/@rdf:about)
    let $parentLabel := $repository-collection//skos:Concept[@rdf:about = data($hit/skos:broader/@rdf:resource)][1]//skos:prefLabel[@xml:lang='en']/text()
return
 <items>
        <identifier>{ $identifier }</identifier>
        <searchResult>{ 
                string-join($title, " ") || "      [" || $identifier || "]   -  " || $parentLabel }</searchResult>
        <title>{ $title }</title>
        <parentLabel>{ $parentLabel  }</parentLabel>
        <parentUri>{ data($hit/skos:broader/@rdf:resource) }</parentUri>
        <dataset_path>
        </dataset_path>
        
        
      </items>
    }
    </list>
 </data>