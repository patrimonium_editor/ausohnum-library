xquery version "3.1";

import module namespace teiEditor="http://ausonius.huma-num.fr/teiEditor"
      at "xmldb:exist:///db/apps/ausohnum-library-dev/modules/teiEditor/teiEditorApp.xql";

import module namespace functx="http://www.functx.com";

import module namespace http="http://expath.org/ns/http-client" at "java:org.expath.exist.HttpClientModule";
declare namespace err = "http://www.w3.org/2005/xqt-errors";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare variable $type := request:get-parameter('type', ());
declare variable $project:= request:get-parameter('project', ());
declare variable $data := request:get-data();

let $text := '
<lb n="46"/>
[Ἀγριανίου]
<lb n="47"/>
δ̅ [πομπὴ βα]–
<lb n="48"/>
σιλ̣[εῖ – – – –]
<lb n="49"/>
ε̅ – – – – – –
<lb n="50"/>
<hi rend="smallit">vac.</hi>
– – – – –
<lb n="51"/>
ζ̅ – – – – – – –'


 
(:Correcting linebreak, ending with character:)
let $text :=
    replace($text, '([\w])–\n<lb\sn="([0-9]*)"\s?/>', '$1<lb n="$2" break="no"/>')
  
(:Correcting linebreak, ending with ]:)
let $text :=
    replace($text, '(\])–\n<lb\sn="([0-9]*)"\s?/>', '$1<lb n="$2" break="no"/>')

(:End of line in lacuna with some reconstructions:)
(:σιλ̣[εῖ – – – –]:)

let $text :=
    replace($text, "(\w*)\[(\w*)(\s–){1,10}\]", '$1<supplied reason="lost">$2</supplied><gap reason="lost" quantity="unknown" unit="character" />')

(:End of line in lacuna with some reconstructions, and no closing square bracket:)
(:σιλ̣[εῖ – – – ]:)

let $text :=
    replace($text, "(\w*)\[(\w*)(\s–){1,10}", '$1<supplied reason="lost">$2</supplied><gap reason="lost" quantity="unknown" unit="character" />')

(:[ .. ] βουλή:)
let $text :=
    replace($text, "\[\s\.\.\s\]", '<gap reason="lost" quantity="unknown" unit="character"/>')

(: [ .. ἐπ]ὶ Δάλιον καὶ:)
let $text :=
    replace($text, "\[\s\.\.\s(\w*)\]", '<gap reason="lost" quantity="unknown" unit="character" precision="low"/><supplied reason="lost">$1</supplied>')

(: [ . οἱ παῖ]δ̣ες:)
let $text :=
    replace($text, "\[\s\.\s(\w*)\]", '<gap reason="lost" quantity="1" unit="character"/><supplied reason="lost">$1</supplied>')

(:Simple [τοῖς π]αισὶ:)
let $text :=
    replace(replace($text, "\[", '<supplied reason="lost">'), "\]", "</supplied>")

(:End of line in lacuna without ending bracket at the end – – – – :)
let $text :=
    replace($text, "(\w)(\s–){1,10}", '$1<gap reason="lost" quantity="unknown" unit="character"/>')


(:Line in lacuna – – – – :)
let $text :=
    replace($text, "–\s–\s–\s–", '<gap reason="lost" quantity="1" unit="line"/>')

(:Unclear characters with dot:)
let $text :=
    replace($text, "(\w?)̣",
     '<unclear>$1</unclear>')

(:supralinear lines:)
let $text :=
    replace($text, "(\w?)̅",
     '<hi rend="supraline">$1</hi>')

return
    <ab>{ parse-xml-fragment($text) }</ab>
