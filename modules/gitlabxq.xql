xquery version "3.1";

module namespace gitlabxq="http://ausonius.huma-num.fr/gitlabxq";
(:~ import module namespace githubxq="http://exist-db.org/lib/githubxq" at "/db/apps/ausohnum-library/modules/githubxq.xql"; ~:)

import module namespace crypto="http://expath.org/ns/crypto";
import module namespace http="http://expath.org/ns/http-client";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json = "http://www.json.org";
(:~ declare variable $tokenName := "patrimoniumAll"; ~:)
(:~ declare variable $authorization-token := "tU18febmTtAFR9eSzyTp"; ~:)
(:declare variable $repo := "https://" || $name || ":" || $authorization-token || "@gitlab.huma-num.fr/vrazanajao/patrimoniumdata";:)
declare variable $gitlabxq:repo := "https://gitlab.huma-num.fr/api/v4/";
(:declare variable $repo := "https://oauth2:" || $authorization-token ||  "gitlab.huma-num.fr/vrazanajao/patrimonium-data";:)
declare variable $gitlabxq:base := "master";
declare variable $gitlabxq:gitlabProjectId := "823";
declare variable $gitlabxq:urlBase := "https://gitlab.huma-num.fr/api/v4/projects/" || $gitlabxq:gitlabProjectId;
declare variable $gitlabxq:repositoryBaseBranch := "test1";
declare variable $gitlabxq:newBranchName := "tezszzt";
declare variable $gitlabxq:url4Branch := "/repository/branches";
declare variable $gitlabxq:url4newBranch := "?branch=" || $gitlabxq:newBranchName || "&amp;ref="|| $gitlabxq:repositoryBaseBranch;
declare variable $gitlabxq:url := $gitlabxq:urlBase ||$gitlabxq:url4Branch;

declare function gitlabxq:newBranch($repo as xs:string,
    $accessToken as xs:string,
    $ref as xs:string,
    $newBranch as xs:string){
    
    let $ref := if($ref!="") then $ref else "main"
    let $url := 
$repo || "/repository/branches?branch=" || $newBranch || "&amp;ref=" || $ref 
    let $httpRequest := 
        http:send-request(<http:request http-version="1.1" href="{xs:anyURI($url)}" method="post">
                        <http:header name="PRIVATE-TOKEN" value="{ $accessToken }"/>
                        <http:header name="Connection" value="close"/>
                        </http:request>)
    return $httpRequest
};


