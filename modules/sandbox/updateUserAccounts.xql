xquery version "3.1";

let $username:= "mpichler"
let $fullname := "Matthias Pichler"
let $email := "Matthias.Pichler@dainst.de"
let $set:= sm:set-account-metadata($username, xs:anyURI("http://axschema.org/namePerson"), $fullname)
let $set:= sm:set-account-metadata($username, xs:anyURI("http://axschema.org/contact/email"), $email)
let $get:= sm:get-account-metadata("vrazanajao", xs:anyURI("http://axschema.org/contact/email"))
return
   $get