xquery version "3.1";

import module namespace http="http://expath.org/ns/http-client" at "java:org.expath.exist.HttpClientModule";
import module namespace teiEditor="http://ausonius.huma-num.fr/teiEditor"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/teiEditor/teiEditorApp.xql";
import module namespace prosopoManager="http://ausonius.huma-num.fr/prosopoManager"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/prosopoManager/prosopoManager.xql";
import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/skosThesau/skosThesauApp.xql";

import module namespace functx="http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare namespace lawd="http://lawd.info/ontology/";
declare namespace apc="http://patrimonium.huma-num.fr/onto#";
declare namespace foaf="http://xmlns.com/foaf/0.1/"; 
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace owl="http://www.w3.org/2002/07/owl#";
declare namespace skos="http://www.w3.org/2004/02/skos/core#"; 
declare namespace geo="http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace ausohnum="http://ausonius.huma-num.fr/onto";
declare namespace spatial="http://geovocab.org/spatial#";
declare boundary-space preserve;

declare variable $tab := '&#9;';
declare variable $nl := "&#10;"; (:New Line:)

let $keywordsList := doc('xmldb:exist:///db/apps/patrimoniumData/egyptianMaterial/keywords/keywordsList.xml')

let $text2keywordsRaw := util:binary-to-string(util:binary-doc('xmldb:exist:///db/apps/patrimoniumData/egyptianMaterial/keywords/TexToKeywords-tabs.txt'))
let $text2Keywords := tokenize($text2keywordsRaw, "\n")


let $docs := collection('xmldb:exist:///db/apps/patrimoniumData/documents/documents-ybroux')
let $places := collection('xmldb:exist:///db/apps/patrimoniumData/places/patrimonium/imports')
let $processKeywords :=
    for $keyword at $pos in $text2Keywords
(:        where $pos < 33:)
        
        let $keywordSplit := tokenize($keyword, "\t")
        let $keywordId := $keywordSplit[2] 
        let $keywordDetails :=$keywordsList//keyword[@id = $keywordId]
        let $keywordUri := data($keywordDetails/@concept_uri) 
        let $keywordTarget := data($keywordDetails/@target)
        let $keywordLabel := data($keywordDetails/@label)
        let $cert := data($keywordDetails/@cert)
        let $textId := $keywordSplit[1]
        let $doc := $docs//tei:TEI[.//tei:idno[@type='tm']/text()= $textId]
       
        let $node2Insert :=if($keywordTarget = "place") then <data xmlns:pleiades="https://pleiades.stoa.org/places/vocab#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <pleiades:hasFeatureType rdf:resource="{ $keywordUri }" type="productionType"/></data>
                    else 
                        switch($cert)
                        case "low" return <data><term ref="{ $keywordUri }" cert="low">{ $keywordLabel }</term>
                        </data>
                            default return <data><term ref="{ $keywordUri }">{ $keywordLabel }</term>
                            </data>
                    
    
        let $whereToInsert :=if($keywordTarget = "place") then (
(:            Place:)
            
            )
            else(
(:                document:)
            $doc//tei:teiHeader/tei:profileDesc/tei:textClass/tei:keywords          
                )
        
(:        let $insertNode :=:)

        return
            if($keywordTarget = "place") then () else
                (
                     
(:                update insert functx:change-element-ns-deep($node2Insert/node(), "http://www.tei-c.org/ns/1.0", "") into $whereToInsert,:)
                        (
                            (if(data($doc/@xml:id) != "") then ()
                            else
(:                            $pos || ") " :)
(:                            ||:)
                            (if(data($doc/@xml:id) != "") then () else "!!!NO APC!!!") ||" TM (text) " ||
                            $textId 
(:                            || " = " || (if(data($doc/@xml:id) != "") then data($doc/@xml:id) else "!!!NO APC!!!"):)
(:                            || " Keyword " || $keywordLabel || " (" || $keywordId || "); URI = " || $keywordUri || "; target=" || $keywordTarget || "  ":)
(:                            ||serialize($node2Insert/node()):)
                            || $nl
                            )
                        )
                )
        

return
    
    <result xmlns:pleiades="https://pleiades.stoa.org/places/vocab#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
    {$processKeywords}
    </result>
    
    
    