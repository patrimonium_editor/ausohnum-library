
	xquery version "3.1";
import module namespace githubxq="http://exist-db.org/lib/githubxq" at "/db/apps/ausohnum-library/modules/githubxq.xql";

import module namespace crypto="http://expath.org/ns/crypto";
import module namespace http="http://expath.org/ns/http-client";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json = "http://www.json.org";
declare variable $name := "patrimoniumAll";
declare variable $authorization-token := "tU18febmTtAFR9eSzyTp";
(:declare variable $repo := "https://" || $name || ":" || $authorization-token || "@gitlab.huma-num.fr/vrazanajao/patrimoniumdata";:)
declare variable $repo := "https://gitlab.huma-num.fr/api/v4/";
(:declare variable $repo := "https://oauth2:" || $authorization-token ||  "gitlab.huma-num.fr/vrazanajao/patrimonium-data";:)
declare variable $base := "master";
declare variable $gitlabProjectId := "823";
declare variable $urlBase := "https://gitlab.huma-num.fr/api/v4/projects/" || $gitlabProjectId;
declare variable $repositoryBaseBranch := "test1";
declare variable $newBranchName := "teszzt";
declare variable $url4Branch := "/repository/branches";
declare variable $url4newBranch := "?branch=" || $newBranchName || "&amp;ref="|| $repositoryBaseBranch;
declare variable $url := $urlBase ||$url4Branch;
 
            http:send-request(<http:request http-version="1.1" href="{xs:anyURI($url)}" method="get">
                                <http:header name="PRIVATE-TOKEN" value="{$authorization-token}"/>
                                <http:header name="Connection" value="close"/>
                                
                              </http:request>)

(:  xs:anyURI(encode-for-uri($urlBase ||$url4newBranch )):)
