xquery version "3.1";
import module namespace functx="http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare boundary-space preserve;

let $project := "lead"

let $docs := collection("/db/apps/" || $project || "Data/documents")


for $doc at $pos in $docs//tei:TEI[not(.//tei:decoNote[@type="frontispieceFigures"])]
where $pos <2

let $newElement := <node>
                     <decoNote type="frontispieceFigures">
                         <desc/>
                         <list/>
                     </decoNote></node>
let $insertNewElement :=
    update insert 
        functx:change-element-ns-deep($newElement/node(),
                                    "http://www.tei-c.org/ns/1.0", "")
            following $doc//tei:decoNote[last()]

return 
    
    <result doc="{$doc/@xml:id}">
    {$doc//tei:body}
    </result>