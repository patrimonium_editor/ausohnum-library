xquery version "3.1";

declare namespace apc="http://patrimonium.huma-num.fr/onto#";
declare namespace lawd="http://lawd.info/ontology/";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace snap="http://onto.snapdrgn.net/snap#";

declare variable $nl := "&#10;"; (:New Line:)

let $people := collection("/db/apps/patrimoniumData/people")

return 
    <results>{
    for $person at $pos in $people//apc:people[exists(.//snap:hasBond)]
        where $pos < 100
    let $personUri := data($person/@rdf:about)
    let $name := $person//lawd:personalName/text()
    let $bondNumber := count($person//snap:hasBond)
    let $bondLabel := if ($bondNumber>1) then "bonds" else "bond"
    let $bonds := string-join(for $bond at $pos in $person//snap:hasBond
            let $bondUri := data($bond/@rdf:resource)
            let $bondDetails := $people//apc:people[@rdf:about = $bondUri]
            let $bondName := $bondDetails//lawd:personalName/text()
            let $bondType := $bond/@rdf:type
            let $bondTypeReverse := data($bondDetails//snap:hasBond[@rdf:resource = $personUri]/@rdf:type)
            let $initialTab := if($pos > 1) then "            " else ""
                return
                    $initialTab ||$bondType || ": " || $bondName || " [" || $bondUri || "] [reverse type: " || $bondTypeReverse || "]", $nl)
    return 
        $name || "    " ||
        $personUri || "    " || $nl ||
        "    " || $bondNumber || " " || $bondLabel || ": " || $nl ||
        "            " ||
        $bonds || $nl ||
        "---------------------------------------" || $nl
        
        
        
    }</results>