xquery version "3.1";
import module namespace functx="http://www.functx.com";


let $projects := ("patrimonium", "lead", "gymnasia")
let $deleteOld := scheduler:delete-scheduled-job("buildAndUpdatePlacesGazetteerGymnasia")
let $createJobs :=  for $project in $projects
    return
        scheduler:schedule-xquery-cron-job(
            "/db/apps/" || $project || "/modules/spatiumStructor/buildGazetteer.xql",
            "0 0/2 * * * ?",
            "buildAndUpdatePlacesGazetteer" || functx:capitalize-first($project),
        <parameters>
            <param name="project" value="{ $project }"/>
        </parameters>
)
return
    scheduler:get-scheduled-jobs()
