xquery version "3.1";

import module namespace http="http://expath.org/ns/http-client" at "java:org.expath.exist.HttpClientModule";
import module namespace teiEditor="http://ausonius.huma-num.fr/teiEditor"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/teiEditor/teiEditorApp.xql";
import module namespace prosopoManager="http://ausonius.huma-num.fr/prosopoManager"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/prosopoManager/prosopoManager.xql";
import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/"
      at "xmldb:exist:///db/apps/ausohnum-library/modules/skosThesau/skosThesauApp.xql";

import module namespace functx="http://www.functx.com";


declare namespace lawd="http://lawd.info/ontology/";
declare namespace apc="http://patrimonium.huma-num.fr/onto#";
declare namespace foaf="http://xmlns.com/foaf/0.1/"; 
declare namespace pleiades="https://pleiades.stoa.org/places/vocab#";
declare namespace owl="http://www.w3.org/2002/07/owl#";
declare namespace skos="http://www.w3.org/2004/02/skos/core#"; 
declare namespace geo="http://www.w3.org/2003/01/geo/wgs84_pos#";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace ausohnum="http://ausonius.huma-num.fr/onto";
declare namespace spatial="http://geovocab.org/spatial#";
declare boundary-space preserve;

declare variable $tab := '&#9;';


let $keywordsList :=  util:binary-to-string(util:binary-doc('xmldb:exist:///db/apps/patrimoniumData/egyptianMaterial/keywordsList-tabs.txt'))

return
    <keywords>
        {
for $keyword at $pos in tokenize($keywordsList, "\r")
        where $pos > 1
        let $keywordSplit := tokenize($keyword, "\t")
        let $labelFromThesau := skosThesau:getLabel("https://ausohnum.huma-num.fr/concept/" ||$keywordSplit[4], "en")
      return
          <keyword id="{replace($keywordSplit[1], '&#xA;', '')}" thesau_id="https://ausohnum.huma-num.fr/concept/{ $keywordSplit[4] }" label="{ $labelFromThesau }" cert="{$keywordSplit[5]}" />
        }
        </keywords>
        
    