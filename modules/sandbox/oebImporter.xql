xquery version "3.1";
declare namespace local="local";
import module namespace hc="http://expath.org/ns/http-client";
declare variable $local:nl := "&#10;";

declare function local:parseOebRISRecord($oebRecord as xs:string){
    let $oebRecord:= tokenize($oebRecord, $local:nl)
    let $recordElements:=<recordElements>
            <recordElement label="author" code="AU  -"/>
            <recordElement label="title" code="TI  -"/>
            <recordElement label="journal" code="JO  -"/>
            <recordElement label="vol" code="VL  -"/>
            <recordElement label="pages" code="SP  -"/>
            <recordElement label="year" code="PY  -"/>
            </recordElements>

    let $title := for $line in $oebRecord
                return
                    if (contains($line, "TI  -")) then $line else ()

    return
        <record>{
            for $element in $recordElements//recordElement
            let $value := for $line in $oebRecord
                return
                    if (contains($line, string($element/@code))) then normalize-space(substring-after($line, $element/@code || " "))  else ()
                return
            element {string($element/@label)} {$value}    
        
        }</record>
};

let $oebNumber := "158017"
let $url4httpRequest := xs:anyURI("http://oeb.griffith.ox.ac.uk/ReferenceExport.aspx?id=" || $oebNumber)

 let $http-request-data := <hc:request xmlns="http://expath.org/ns/http-client"
    method="get" href="{$url4httpRequest}" >
        <hc:header name="Content-Type" value="text/plain; charset=utf-8/"/>
        <!--
        <http:body media-type="application/rdf+xml"
        method="xml" encoding="utf-8"
         omit-xml-declaration="no">a</http:body>
         -->
    </hc:request>

let $request-headers :=<hc:header name="Content-Type" value="text/plain; charset=utf-8/"/>
let $responses :=
    hc:send-request($http-request-data)
let $response :=
    <results>
      {if ($responses[1]/@status ne '200')
         then
             <failure>{$responses[1]}</failure>
         else
           <success>
             {util:binary-to-string($responses[2])}
             {'' (: todo - use string to JSON serializer lib here :) }
           </success>
      }
    </results>

return 
    <data>
        <risFile>{ $response }</risFile>
        {local:parseOebRISRecord($response)}
        </data>