xquery version "3.1";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization"; 
declare namespace prod = "http://datypic.com/prod"; 
declare option output:method "xml"; 
declare option output:media-type "application/xhtml+xml";
declare option output:indent "no"; 
declare option output:omit-xml-declaration "no";

let $xsltCleanNamespace:= doc("/db/apps/ausohnum-library/xslt/removeExtraNamespace.xsl")
let $xslt:= doc("/db/apps/ausohnum-library/xslt/cleanTeiFile.xsl")

(: let $teiFile := doc("/db/apps/leadData/documents/stp-v2/lead-ps43.xml") :)
let $teiFile := doc("/db/apps/patrimoniumData/documents/documents-alberto/apcd1.xml")
let $params := 
<parameters>
   <param name="output.omit-xml-declaration" value="no"/>
   <param name="output.indent" value="yes"/>
   <param name="output.media-type" value="text/xml"/>
   <param name="output.method" value="xml"/>
</parameters>
let $paramMap := 
        map {"method": "xml", "indent": false(), "item-separator": "", "version": "1.0"}
let $serialization-options := 'method=xml media-type=text/xml omit-xml-declaration=no indent=yes'
let $removeExtraNamespace:=    transform:transform($teiFile, $xsltCleanNamespace, (), (), $serialization-options)

return 
    
    (: transform:transform($teiFile, $xslt, (), (), $serialization-options) :)
    
    transform:transform($removeExtraNamespace, $xslt, (), (), $serialization-options)
    (: $teiFile :)
