xquery version "3.1";

import module namespace functx="http://www.functx.com";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace lawd="http://lawd.info/ontology/";
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";

let $now := fn:current-dateTime()
let $currentUser := data(sm:id()//sm:username)
    
let $logs := doc('xmldb:exist:///db/apps/patrimoniumData/logs/logs-import-egyptianMaterial.xml')
let $teiTemplate := doc("xmldb:exist:///db/apps/patrimonium/data/teiEditor/docTemplates/teiTemplatePatrimoniumEgypt.xml")
let $collectionPrefix := "apcd"
let $doc-collection-path := "xmldb:exist:///db/apps/patrimoniumData/documents/documents-ybroux"
let $project-people-collection := collection("xmldb:exist:///db/apps/patrimoniumData/people")
let $lemmatizedCorpus := collection("xmldb:exist:///db/apps/patrimoniumData/egyptianMaterial")

let $HGV_metadata := collection("xmldb:exist:///db/apps/papyriInfo/data/HGV_meta_EpiDoc")
let $HGVNo := "22838"


let $peopleInLemmatizedFile := 
    <peopleInLemmatizedFile>
        {
        for $file in $lemmatizedCorpus//file
            return
                <file apcd="{ $doc-collection-path//tei:TEI[tei:idno[@subtype='tm']/text() = $file/@text]/@xml:id }" tm="{ $file/@text }" HVG="{ $file/@HGV }" xml:lang="{ $file/@lang }">
                {
                for $people in $file//word[@per]
                    let $tmPersNo := $people/@per
        
                return 
                    <mention corresp="{ $people/@per }" apc="{ substring-before($project-people-collection//lawd:person[skos:exactMatch[@rdf:resource=$people/@per]]/@rdf:about, "#this")}"
                    row="{ $people/@row }" token="{ $people/@wordNum }">
                    <persName type="regularized">{ data($people/@regularized) }</persName>
                    <persName type="original">{ data($people/@original) }</persName>
                    </mention>
                }
                </file>
        }
    </peopleInLemmatizedFile>
    
return
    $peopleInLemmatizedFile