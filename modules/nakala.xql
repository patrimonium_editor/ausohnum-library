(:~
: AusoHNum Library - Nakala module
: This module contains the main functions to make use of Naala API (https://api.nakala.fr/doc).
: @author Vincent Razanajao
:)
xquery version "3.1";
module namespace nakalaPlugin="http://ausonius.huma-num.fr/nakalaPlugin";

import module namespace functx="http://www.functx.com";
import module namespace http="http://expath.org/ns/http-client" at "java:org.expath.exist.HttpClientModule";
(: import module namespace skosThesau="https://ausohnum.huma-num.fr/skosThesau/" at "../skosThesau/skosThesauApp.xql"; :)
(: import module namespace templates="http://exist-db.org/xquery/templates" ; :)
import module namespace xmldb="http://exist-db.org/xquery/xmldb";

declare variable $nakalaPlugin:project :=request:get-parameter('project', "lead");
declare variable $nakalaPlugin:mode :=request:get-parameter('mode', ());
declare variable $nakalaPlugin:appVariables := doc("/db/apps/" || $nakalaPlugin:project || "/data/app-general-parameters.xml");
declare variable $nakalaPlugin:apiKey := $nakalaPlugin:appVariables//nakalaAPIKey/text();
declare variable $nakalaPlugin:nakalaBaseUrl := "https://api.nakala.fr";

declare function nakalaPlugin:executeHttpRequest($APIUrl as xs:string){

    let $url4httpRequest := xs:anyURI($nakalaPlugin:nakalaBaseUrl || $APIUrl)
    let $http-request-data := <http:request xmlns="http://expath.org/ns/http-client" method="get" href="{$url4httpRequest}" >
            <http:header name="Content-Type" value="text/plain; charset=utf-8/"/>
            <http:header name="X-API-KEY" value="{ $nakalaPlugin:apiKey }"/>
            <!--
            <http:body media-type="application/rdf+xml"
            method="xml" encoding="utf-8"
            omit-xml-declaration="no">a</http:body>
            -->
        </http:request>
    let $responses := http:send-request($http-request-data)
    
    let $response :=
    <results>
      {if ($responses[1]/@status ne '200')
         then
             <failure>{$responses[1]}</failure>
         else
           <success>
             {util:binary-to-string($responses[2])}
             {'' (: todo - use string to JSON serializer lib here :) }
           </success>
      }
      </results>
    return $response      

};

declare function nakalaPlugin:collectionPaginatedList($collectionId as xs:string){
    let $url := "/collections/" || $collectionId || "/datas"
    return nakalaPlugin:executeHttpRequest($url)
};


declare function nakalaPlugin:IIIFViewer($identifier as xs:string, $fileIdentifier as xs:string){
   let $url := "/embed/" || $identifier || "/" || $fileIdentifier 
    return nakalaPlugin:executeHttpRequest($url)
};


