(:~
: AusoHNum Library - Zotero module
: This module contains the main functions of the Zotero module.
: @author Vincent Razanajao
:)
xquery version "3.1";
module namespace zoteroPlugin="http://ausonius.huma-num.fr/zoteroPlugin";

import module namespace hc="http://expath.org/ns/http-client" at "java:org.expath.exist.HttpClientModule";
import module namespace console="http://exist-db.org/xquery/console";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare variable $zoteroPlugin:project :=request:get-parameter('project', ());

declare variable $zoteroPlugin:appVariables := doc("/db/apps/" || $zoteroPlugin:project || "/data/app-general-parameters.xml");
declare variable $zoteroPlugin:zoteroGroup := $zoteroPlugin:appVariables//zoteroGroup/text();

declare function zoteroPlugin:get-bibItem($zoteroGroup as xs:string, $zoteroItemId as xs:string, $format as xs:string){

    let $headerTeiBibRef := <headers><header name="content" value="{$format}"/>
                                            </headers>
    let $urlTeiBibRef :=
                if(starts-with($zoteroItemId, "http") ) then $zoteroItemId
                else "https://api.zotero.org/groups/" || $zoteroGroup ||"/items/" || $zoteroItemId 
                    || "?format=" || $format
    
    let $url4httpRequest := "https://api.zotero.org/groups/" || $zoteroGroup ||"/items/" || $zoteroItemId 
                    || "?format=" || $format

let $http-request-data := <request xmlns="http://expath.org/ns/http-client"
method="GET" href="{$url4httpRequest}"/>

let $responses := 
hc:send-request($http-request-data)
let $response :=
<results>
  {if ($responses[1]/@status ne '200')
     then
         <failure>{$responses[1]}</failure>
     else
       <success>
         {$responses[2]}
         {'' (: todo - use string to JSON serializer lib here :) }
       </success>
  }
</results>
return $response
    
};

declare function zoteroPlugin:get-zoteroItem($zoteroGroup as xs:string, $zoteroItemId as xs:string, $format as xs:string){

   let $headerTeiBibRef := <headers><header name="content" value="{$format}"/></headers>
    let $urlTeiBibRef := "https://api.zotero.org/groups/" || $zoteroGroup ||"/items/" || $zoteroItemId 
                    || "?format=" || $format
    
    let $url4httpRequest := "https://api.zotero.org/groups/" || $zoteroGroup ||"/items/" || $zoteroItemId 
                    || "?format=" || $format 

let $http-request-data := <request xmlns="http://expath.org/ns/http-client"
method="GET" href="{$url4httpRequest}"/>



                let $responses :=hc:send-request($http-request-data)
                let $response :=
                    <results>
                      {if ($responses[1]/@status ne '200')
                         then
                             <failure>{$responses[1]}</failure>
                         else
                           <success>
                             {$responses[2]}
                             {'' (: todo - use string to JSON serializer lib here :) }
                           </success>
                      }
                    </results>
          
          
(:let $response := hc:get(xs:anyURI($url4httpRequest), true(), (), ())          :)
          
          return 
          if($format = "json")
          then util:binary-to-string($response)
         else  $response
    
    
    
    
    
    
    
    
    
    (:let $teiBibRef := httpclient:get(xs:anyURI($urlTeiBibRef), true(), $headerTeiBibRef)
        return
           $teiBibRef/httpclient:body:)
};

declare function zoteroPlugin:updateRecordWithZoteroTag($zoteroGroup as xs:string, $zoteroItem as xs:string, $tagName as xs:string){
 let $zoteroRecord := zoteroPlugin:get-zoteroItem($zoteroGroup, $zoteroItem, "")
 return $zotero
};

declare function zoteroPlugin:getModifiedItemsSince($project as xs:string, $version){
  let $appVariables := doc("/db/apps/" || $project || "/data/app-general-parameters.xml")
  let $zoteroGroup := $appVariables//zoteroGroup/text()
  let $url4httpRequest := "https://api.zotero.org/groups/" || $zoteroGroup ||"/items/" 
                    || "?since=" || normalize-space($version)
  let $http-request-data := <request xmlns="http://expath.org/ns/http-client" method="GET" href="{ $url4httpRequest }"/>
  let $responses := hc:send-request($http-request-data)
  let $response :=(
              <results>
                {if ($responses[1]/@status ne '200')
                  then
                      <failure><url>{ $url4httpRequest }</url>{$responses[1]}</failure>
                  else
                    <success>
                      {$responses[2]}
                      {'' (: todo - use string to JSON serializer lib here :) }
                    </success>
                }
              </results>
      )
  let $items :=if($response//success) then 
      parse-json( util:binary-to-string($response))
      else ()
  return
    (: if(count($items?*) > 0) then  :)
      <data>
        <zoteroMetadata lastVersion="">{ $responses[1]//hc:header }</zoteroMetadata>
        <items>{
         for $item in $items?*
          let $key := $item?key
          let $bibRef := zoteroPlugin:get-bibItem($zoteroGroup, $key, "tei")//*[local-name()="biblStruct"]
          return $bibRef
        }</items>
        </data>
    (: else $response :)
  };

declare function zoteroPlugin:syncProjectMainBiblioWithGroupLibrary($project as xs:string){
    let $mainBiblio := doc("/db/apps/" || $project || "Data/biblio/biblio.xml")/id("mainBiblio")
    let $currentVersion := data($mainBiblio/@n)
    let $newItems := zoteroPlugin:getModifiedItemsSince($project, $currentVersion)
    let $lastModifiedVersion := data($newItems//hc:header[@name="last-modified-version"]/@value)
    let $log := console:log("currentVersion : " || $currentVersion)
    let $log := console:log("lastModifiedVersion : " || $lastModifiedVersion)
    let $log := console:log(serialize($newItems))
    let $updateNewItems :=
        if($newItems//tei:biblStruct) then
          (for $item in $newItems//tei:biblStruct
            let $newItemUri := $item/@corresp
            return
              if(exists($mainBiblio//tei:biblStruct[@corresp = $newItemUri]))
                then (
                  update replace $mainBiblio//tei:biblStruct[@corresp = $newItemUri] with $item,
                  "Item " || $newItemUri || " already exists in mainiblio and has been updated.")
                else (
                  update insert $item into $mainBiblio,
                  "Item " || $newItemUri || " doesn't exist n mainBiblio and has been added."))
          else ("Project library up-to-date")
      let $updateMainBiblioVersion :=
        if($newItems//tei:biblStruct) then
          update value $mainBiblio/@n with $lastModifiedVersion else ()
     
    return $updateNewItems
    
};